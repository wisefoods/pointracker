﻿namespace AttendanceTracker
{
    partial class TakeActionTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TakeActionTemplate));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dePointLetterGivenOn = new DevExpress.XtraEditors.DateEdit();
            this.labelPointLetterGivenOn = new System.Windows.Forms.Label();
            this.labelActionLabel = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkActionTaken = new DevExpress.XtraEditors.CheckEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSave = new DevExpress.XtraEditors.ButtonEdit();
            this.labelComments = new System.Windows.Forms.Label();
            this.tbComments = new System.Windows.Forms.TextBox();
            this.labelAction = new System.Windows.Forms.Label();
            this.labelPoints = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.checkPrintDoubleSided = new System.Windows.Forms.CheckBox();
            this.checkExportAllRecords = new System.Windows.Forms.CheckBox();
            this.btnGeneratePointLetter = new DevExpress.XtraEditors.ButtonEdit();
            this.labelLastUpdate = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.cbEmployeeList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.buttonEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dePointLetterGivenOn.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dePointLetterGivenOn.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkActionTaken.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnGeneratePointLetter.Properties)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbEmployeeList.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.dePointLetterGivenOn);
            this.panel6.Controls.Add(this.labelPointLetterGivenOn);
            this.panel6.Controls.Add(this.labelActionLabel);
            this.panel6.Controls.Add(this.labelDate);
            this.panel6.Controls.Add(this.panel3);
            this.panel6.Controls.Add(this.panel2);
            this.panel6.Controls.Add(this.labelComments);
            this.panel6.Controls.Add(this.tbComments);
            this.panel6.Controls.Add(this.labelAction);
            this.panel6.Controls.Add(this.labelPoints);
            this.panel6.Controls.Add(this.panel1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(970, 509);
            this.panel6.TabIndex = 1;
            // 
            // dePointLetterGivenOn
            // 
            this.dePointLetterGivenOn.EditValue = null;
            this.dePointLetterGivenOn.Location = new System.Drawing.Point(446, 131);
            this.dePointLetterGivenOn.Name = "dePointLetterGivenOn";
            this.dePointLetterGivenOn.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dePointLetterGivenOn.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dePointLetterGivenOn.Properties.LookAndFeel.SkinName = "Sharp";
            this.dePointLetterGivenOn.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dePointLetterGivenOn.Size = new System.Drawing.Size(109, 20);
            this.dePointLetterGivenOn.TabIndex = 20;
            // 
            // labelPointLetterGivenOn
            // 
            this.labelPointLetterGivenOn.AutoSize = true;
            this.labelPointLetterGivenOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPointLetterGivenOn.Location = new System.Drawing.Point(269, 132);
            this.labelPointLetterGivenOn.Name = "labelPointLetterGivenOn";
            this.labelPointLetterGivenOn.Size = new System.Drawing.Size(160, 17);
            this.labelPointLetterGivenOn.TabIndex = 19;
            this.labelPointLetterGivenOn.Text = "Point letter given on:";
            // 
            // labelActionLabel
            // 
            this.labelActionLabel.AutoSize = true;
            this.labelActionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActionLabel.Location = new System.Drawing.Point(269, 69);
            this.labelActionLabel.Name = "labelActionLabel";
            this.labelActionLabel.Size = new System.Drawing.Size(94, 17);
            this.labelActionLabel.TabIndex = 18;
            this.labelActionLabel.Text = "Information:";
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.Location = new System.Drawing.Point(269, 35);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(175, 17);
            this.labelDate.TabIndex = 17;
            this.labelDate.Text = "DATE OF OCCURENCE";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.checkActionTaken);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(263, 425);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.panel3.Size = new System.Drawing.Size(707, 44);
            this.panel3.TabIndex = 15;
            // 
            // checkActionTaken
            // 
            this.checkActionTaken.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkActionTaken.Location = new System.Drawing.Point(6, 0);
            this.checkActionTaken.Name = "checkActionTaken";
            this.checkActionTaken.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.checkActionTaken.Properties.Appearance.Options.UseFont = true;
            this.checkActionTaken.Properties.Caption = "Check to confirm that the information above is correct";
            this.checkActionTaken.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.SvgCheckBox1;
            this.checkActionTaken.Properties.CheckBoxOptions.SvgColorChecked = System.Drawing.Color.White;
            this.checkActionTaken.Properties.CheckBoxOptions.SvgColorGrayed = System.Drawing.Color.DimGray;
            this.checkActionTaken.Properties.CheckBoxOptions.SvgColorUnchecked = System.Drawing.Color.Black;
            this.checkActionTaken.Size = new System.Drawing.Size(701, 24);
            this.checkActionTaken.TabIndex = 11;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(263, 469);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(707, 40);
            this.panel2.TabIndex = 14;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSave.EditValue = "Submit";
            this.btnSave.Location = new System.Drawing.Point(0, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Properties.Appearance.BackColor = System.Drawing.Color.Green;
            this.btnSave.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold);
            this.btnSave.Properties.Appearance.Options.UseBackColor = true;
            this.btnSave.Properties.Appearance.Options.UseFont = true;
            this.btnSave.Properties.Appearance.Options.UseImage = true;
            this.btnSave.Properties.AutoHeight = false;
            this.btnSave.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            editorButtonImageOptions1.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            editorButtonImageOptions1.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            serializableAppearanceObject1.Font = new System.Drawing.Font("Tahoma", 16F);
            serializableAppearanceObject1.Options.UseFont = true;
            this.btnSave.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, " Submit", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnSave.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnSave.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.btnSave.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSave.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnSave.Size = new System.Drawing.Size(707, 40);
            this.btnSave.TabIndex = 13;
            this.btnSave.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnSave_ButtonClick);
            // 
            // labelComments
            // 
            this.labelComments.AutoSize = true;
            this.labelComments.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelComments.Location = new System.Drawing.Point(269, 158);
            this.labelComments.Name = "labelComments";
            this.labelComments.Size = new System.Drawing.Size(162, 17);
            this.labelComments.TabIndex = 12;
            this.labelComments.Text = "Comments (optional):";
            // 
            // tbComments
            // 
            this.tbComments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbComments.Location = new System.Drawing.Point(269, 178);
            this.tbComments.Multiline = true;
            this.tbComments.Name = "tbComments";
            this.tbComments.Size = new System.Drawing.Size(664, 224);
            this.tbComments.TabIndex = 10;
            // 
            // labelAction
            // 
            this.labelAction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAction.Location = new System.Drawing.Point(362, 69);
            this.labelAction.Name = "labelAction";
            this.labelAction.Size = new System.Drawing.Size(512, 47);
            this.labelAction.TabIndex = 8;
            this.labelAction.Text = "ACTION THAT MUST BE TAKEN";
            // 
            // labelPoints
            // 
            this.labelPoints.AutoSize = true;
            this.labelPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPoints.Location = new System.Drawing.Point(268, 13);
            this.labelPoints.Name = "labelPoints";
            this.labelPoints.Size = new System.Drawing.Size(222, 20);
            this.labelPoints.TabIndex = 7;
            this.labelPoints.Text = "EMPLOYEE POINT TOTAL";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 12, 0, 0);
            this.panel1.Size = new System.Drawing.Size(263, 509);
            this.panel1.TabIndex = 6;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.checkPrintDoubleSided);
            this.panel8.Controls.Add(this.checkExportAllRecords);
            this.panel8.Controls.Add(this.btnGeneratePointLetter);
            this.panel8.Controls.Add(this.labelLastUpdate);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 45);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(243, 464);
            this.panel8.TabIndex = 13;
            // 
            // checkPrintDoubleSided
            // 
            this.checkPrintDoubleSided.Checked = true;
            this.checkPrintDoubleSided.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkPrintDoubleSided.Font = new System.Drawing.Font("Tahoma", 11F);
            this.checkPrintDoubleSided.Location = new System.Drawing.Point(3, 104);
            this.checkPrintDoubleSided.Name = "checkPrintDoubleSided";
            this.checkPrintDoubleSided.Size = new System.Drawing.Size(235, 52);
            this.checkPrintDoubleSided.TabIndex = 17;
            this.checkPrintDoubleSided.Text = "Print on both sides of the page?";
            this.checkPrintDoubleSided.UseVisualStyleBackColor = true;
            // 
            // checkExportAllRecords
            // 
            this.checkExportAllRecords.Font = new System.Drawing.Font("Tahoma", 11F);
            this.checkExportAllRecords.Location = new System.Drawing.Point(3, 54);
            this.checkExportAllRecords.Name = "checkExportAllRecords";
            this.checkExportAllRecords.Size = new System.Drawing.Size(235, 59);
            this.checkExportAllRecords.TabIndex = 16;
            this.checkExportAllRecords.Text = "Display ALL current points on this point letter?";
            this.checkExportAllRecords.UseVisualStyleBackColor = true;
            // 
            // btnGeneratePointLetter
            // 
            this.btnGeneratePointLetter.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGeneratePointLetter.EditValue = "Generate Point Letter";
            this.btnGeneratePointLetter.Location = new System.Drawing.Point(0, 10);
            this.btnGeneratePointLetter.Name = "btnGeneratePointLetter";
            this.btnGeneratePointLetter.Properties.Appearance.BackColor = System.Drawing.Color.Green;
            this.btnGeneratePointLetter.Properties.Appearance.BackColor2 = System.Drawing.Color.Orange;
            this.btnGeneratePointLetter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnGeneratePointLetter.Properties.Appearance.Options.UseBackColor = true;
            this.btnGeneratePointLetter.Properties.Appearance.Options.UseFont = true;
            this.btnGeneratePointLetter.Properties.Appearance.Options.UseImage = true;
            this.btnGeneratePointLetter.Properties.AutoHeight = false;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            editorButtonImageOptions2.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            editorButtonImageOptions2.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            serializableAppearanceObject5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            serializableAppearanceObject5.Options.UseFont = true;
            serializableAppearanceObject5.Options.UseTextOptions = true;
            serializableAppearanceObject5.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnGeneratePointLetter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, " Generate Point Letter", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnGeneratePointLetter.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnGeneratePointLetter.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.btnGeneratePointLetter.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnGeneratePointLetter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnGeneratePointLetter.Size = new System.Drawing.Size(243, 40);
            this.btnGeneratePointLetter.TabIndex = 14;
            this.btnGeneratePointLetter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnGeneratePointLetter_ButtonClick);
            // 
            // labelLastUpdate
            // 
            this.labelLastUpdate.AutoSize = true;
            this.labelLastUpdate.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelLastUpdate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelLastUpdate.Location = new System.Drawing.Point(0, 448);
            this.labelLastUpdate.Name = "labelLastUpdate";
            this.labelLastUpdate.Padding = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.labelLastUpdate.Size = new System.Drawing.Size(128, 16);
            this.labelLastUpdate.TabIndex = 11;
            this.labelLastUpdate.Text = "Data from: UNKNOWN";
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(243, 10);
            this.panel9.TabIndex = 15;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.cbEmployeeList);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.panel4);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 12);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(243, 33);
            this.panel7.TabIndex = 12;
            // 
            // cbEmployeeList
            // 
            this.cbEmployeeList.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbEmployeeList.EditValue = "";
            this.cbEmployeeList.Location = new System.Drawing.Point(26, 13);
            this.cbEmployeeList.Name = "cbEmployeeList";
            this.cbEmployeeList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEmployeeList.Properties.DropDownRows = 15;
            this.cbEmployeeList.Properties.LookAndFeel.SkinName = "Sharp";
            this.cbEmployeeList.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbEmployeeList.Properties.Sorted = true;
            this.cbEmployeeList.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEmployeeList.Size = new System.Drawing.Size(217, 20);
            this.cbEmployeeList.TabIndex = 0;
            this.cbEmployeeList.SelectedIndexChanged += new System.EventHandler(this.CbEmployeeList_SelectedIndexChanged);
            this.cbEmployeeList.BeforePopup += new System.EventHandler(this.cbEmployeeList_BeforePopup);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Location = new System.Drawing.Point(26, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Select An Employee";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.buttonEdit1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(26, 33);
            this.panel4.TabIndex = 4;
            // 
            // buttonEdit1
            // 
            this.buttonEdit1.Location = new System.Drawing.Point(1, 13);
            this.buttonEdit1.Name = "buttonEdit1";
            this.buttonEdit1.Properties.Appearance.Options.UseImage = true;
            this.buttonEdit1.Properties.AutoHeight = false;
            editorButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions3.Image")));
            this.buttonEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEdit1.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.buttonEdit1.Properties.LookAndFeel.SkinName = "Sharp";
            this.buttonEdit1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.buttonEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.buttonEdit1.Size = new System.Drawing.Size(25, 20);
            this.buttonEdit1.TabIndex = 10;
            this.buttonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ButtonEdit1_ButtonClick);
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(243, 12);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(20, 497);
            this.panel5.TabIndex = 9;
            // 
            // TakeActionTemplate
            // 
            this.ClientSize = new System.Drawing.Size(970, 509);
            this.Controls.Add(this.panel6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TakeActionTemplate";
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dePointLetterGivenOn.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dePointLetterGivenOn.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkActionTaken.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnSave.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnGeneratePointLetter.Properties)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbEmployeeList.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraGrid.GridControl gcEmployeeEvents;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.ComboBoxEdit cbEmployeeList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label labelComments;
        private DevExpress.XtraEditors.CheckEdit checkActionTaken;
        private System.Windows.Forms.TextBox tbComments;
        private System.Windows.Forms.Label labelAction;
        private System.Windows.Forms.Label labelPoints;
        private DevExpress.XtraEditors.ButtonEdit btnSave;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelLastUpdate;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label labelActionLabel;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private DevExpress.XtraEditors.ButtonEdit btnGeneratePointLetter;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label labelPointLetterGivenOn;
        private DevExpress.XtraEditors.DateEdit dePointLetterGivenOn;
        private System.Windows.Forms.CheckBox checkExportAllRecords;
        private System.Windows.Forms.CheckBox checkPrintDoubleSided;
    }
}