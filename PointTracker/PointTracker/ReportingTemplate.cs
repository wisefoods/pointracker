﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using System.IO;
using System.Net.Mail;
using DevExpress.DashboardCommon;
using DevExpress.DataAccess.EntityFramework;
using System.Xml.Linq;
using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.Sql;
using DevExpress.Pdf;

namespace AttendanceTracker
{
    public partial class ReportingTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;
        DashboardEFDataSource efDataSource;
        DashboardSqlDataSource sqlDataSource;
        DirectoryInfo dir = null;

        public ReportingTemplate(Form1 pForm)
        {
            try
            {
                InitializeComponent();

                atrack = pForm;

                /// IMPORTANT
                /// disable the WaitForm for the ReportViewer control if this instance is being run automatically
                /// this process is called by the automated task to send reporting emails
                /// however, this type of wait form requires that the application instance is run
                /// in an interactive mode, and not with the /s flag as will be done by the task scheduler entry
                /// get around that problem by disabling it, otherwise it will throw a 0xE0434352 error
                if (atrack.RUNNINGAUTOMATED)
                {
                    dashboardViewer1.EnableWaitForm = false;
                }

                /// set dashboard datasource(s)
                /// entity
                efDataSource = new DashboardEFDataSource();
                efDataSource.ConnectionParameters = new EFConnectionParameters(typeof(ATrackEntitiesConn), "ATrackEntitiesConn");
                efDataSource.Name = "Attendance Tracker Data - EF";
                efDataSource.Fill();
                /// sql
                DataConnectionParametersBase sqlConnParameters = new MsSqlConnectionParameters()
                {
                    ServerName = "USWSEPRSQ4SQL",
                    DatabaseName = "AttendanceTracker",
                    UserName = "App_PointTracker",
                    Password = "WisePointTrackerOwl!!@",
                    AuthorizationType = MsSqlAuthorizationType.SqlServer
                };
                sqlDataSource = new DashboardSqlDataSource("Attendance Tracker Data - SQL", sqlConnParameters);
                sqlDataSource.DataProcessingMode = DataProcessingMode.Server;
                dashboardViewer1.LoadInactiveTabs = true;
                DashboardSqlDataSource.LockUIOnDataLoading = true;
                DashboardSqlDataSource.AllowCustomSqlQueries = true;
                DashboardSqlDataSource.DisableCustomQueryValidation = true;
                sqlDataSource.Fill();

                if (atrack.CheckIfAdmin() && !atrack.ADMINOPTION_OVERRIDEREPORTINGTAB)
                {
                    panelDashboardViewer.Visible = false;
                    dashboardDesigner1.Dashboard.DataSources.Clear();
                    dashboardDesigner1.Dashboard.DataSources.Add(efDataSource);
                    dashboardDesigner1.Dashboard.DataSources.Add(sqlDataSource);
                }
                else
                {
                    dashboardDesigner1.Visible =
                    ribbonControl1.Visible =
                    dashboardBackstageViewControl1.Visible = false;
                    panelDashboardViewer.Dock = DockStyle.Fill;                    
                }

                /// cbReportList is populated no matter the case (above)
                /// because it is required for the "Send Report Emails" admin task
                /// set once with relative path
                /// be sure the "START IN" path is set on the destination server!!
                dir = new DirectoryInfo("../../../Reports/");
                ///// set once from network path
                ///// this will ensure all reports are loaded from the same location
                ///// vvv removed because this app is run as a service account in some locations
                ///// and that service account cannot access this network share
                ///// service account is more valuable here than a single report repo
                //dir = new DirectoryInfo(@"\\uswsefsnas01\MIS\AppRepo\AttendancePointTracker\Reports\");
                /// check for files
                if (dir.GetFiles("*.xml").OrderBy(z => z.Name).Count() <= 0)
                {
                    /// if none then error and return
                    if (atrack.RUNNINGAUTOMATED)
                    {
                        atrack.LogAndDisplayError("Could not locate the Reports folder! Current path is: " + dir.ToString(), true);
                        return;
                    }
                    else
                    {
                        MessageBox.Show("No reports were found! If you believe this was an error please restart the application and try again.",
                                        "No Reports",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    // add each file from the directory to the list
                    foreach (FileInfo file in dir.GetFiles("*.xml").OrderBy(z => z.Name))
                    {
                        cbReportList.Properties.Items.Add(file.Name.Substring(0, file.Name.LastIndexOf(".xml")));
                    }
                }
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        private void btnClearReport_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            cbReportList.SelectedIndex = -1;
            dashboardViewer1.Dashboard.Items.Clear();
        }

        private void cbReportList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbReportList.Text == null || cbReportList.Text == string.Empty)
                {
                    cbReportList.SelectedIndex = -1;
                    dashboardViewer1.Dashboard.Items.Clear();
                    dashboardViewer1.Dashboard.DataSources.Clear();
                }
                else
                {
                    dashboardViewer1.LoadDashboard(dir.ToString() + cbReportList.Text + ".xml");
                }
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }            
        }

        private void dashboardViewer1_ConfigureDataConnection(object sender, DashboardConfigureDataConnectionEventArgs e)
        {
            /// do nothing
            /// report datasources and queries will be automatically loaded by the .xml selected by the user
        }

        public List<Attachment> ReturnAllReportsAsListOfAttachments(string pType)
        {
            try
            {
                List<Attachment> lAttachments = new List<Attachment>();
                int month = DateTime.Today.Month;
                int day = DateTime.Today.Day;
                int year = DateTime.Today.Year;

                cbReportList.SelectedIndex = -1;
                for (int i = 0; i < cbReportList.Properties.Items.Count; i++)
                {
                    cbReportList.SelectedIndex = i;
                    lAttachments.Add(ReturnSingleReportAsAttachment(pType));
                }
                cbReportList.SelectedIndex = -1;

                if (lAttachments.Count <= 0) return null;
                else return lAttachments;
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
            return null;
        }

        public Attachment ReturnSingleReportAsAttachment(string pType)
        {
            try
            {
                int month = DateTime.Today.Month;
                int day = DateTime.Today.Day;
                int year = DateTime.Today.Year;

                if (pType == "Excel")
                {
                    DashboardExcelExportOptions xOpts = new DashboardExcelExportOptions()
                    {
                        DashboardStatePosition = DashboardStateExcelExportPosition.Below,
                        ExportParameters = false,
                        ExportFilters = false,
                        Format = ExcelFormat.Xlsx,
                        IncludeHiddenParameters = false
                    };
                    MemoryStream stream = new MemoryStream();
                    dashboardViewer1.ExportToExcel(stream, xOpts);
                    stream.Seek(0, SeekOrigin.Begin);
                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    attachment.ContentDisposition.FileName = month.ToString() +
                                                             "-" +
                                                             day.ToString() +
                                                             "-" +
                                                             year.ToString() +
                                                             " - " +
                                                             cbReportList.SelectedItem.ToString() +
                                                             ".xlsx";
                    return attachment;
                }
                else if (pType == "PDF")
                {
                    DashboardPdfExportOptions xOpts = new DashboardPdfExportOptions()
                    {
                        ChartSizeMode = ChartExportSizeMode.Stretch,
                        ChartAutomaticPageLayout = true,
                        DashboardAutomaticPageLayout = true,
                        DashboardStatePosition = DashboardStateExportPosition.Below,
                        DocumentScaleMode = DashboardExportDocumentScaleMode.UseScaleFactor,
                        ExportParameters = false,
                        ExportFilters = false,
                        IncludeHiddenParameters = false,
                        PivotPrintHeadersOnEveryPage = false,
                        GridPrintHeadersOnEveryPage = false,
                        GridFitToPageWidth = true
                    };
                    using (PdfDocumentProcessor pdfDocumentProcessor = new PdfDocumentProcessor())
                    {
                        pdfDocumentProcessor.CreateEmptyDocument();
                        for (int i = 0; i < dashboardViewer1.Dashboard.Items.Count; i++)
                        {
                            MemoryStream newStream = new MemoryStream();
                            dashboardViewer1.ExportDashboardItemToPdf(dashboardViewer1.Dashboard.Items[i].ComponentName, newStream, xOpts);
                            newStream.Seek(0, SeekOrigin.Begin);                            
                            pdfDocumentProcessor.AppendDocument(newStream);
                        }
                        MemoryStream finalStream = new MemoryStream();
                        pdfDocumentProcessor.SaveDocument(finalStream);
                        finalStream.Seek(0, SeekOrigin.Begin);
                        System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(finalStream, "application/pdf");
                        attachment.ContentDisposition.FileName = month.ToString() +
                                                                 "-" +
                                                                 day.ToString() +
                                                                 "-" +
                                                                 year.ToString() +
                                                                 " - " +
                                                                 cbReportList.SelectedItem.ToString() +
                                                                 ".pdf";
                        return attachment;
                    }
                }
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
            return null;
        }

        private void btnEmailMeThisReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbReportList.SelectedIndex < 0) return;
                else if (MessageBox.Show("Send this Excel report to " + atrack.EMAILADDRESS + "?",
                                         "Email Report",
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question) == DialogResult.No) return;
                    
                List<Attachment> lAttachments = new List<Attachment>();
                lAttachments.Add(ReturnSingleReportAsAttachment("Excel"));
                atrack.SendEmail(new string[] { atrack.EMAILADDRESS },
                                 null,
                                 null,
                                 lAttachments[0].ContentDisposition.FileName,
                                 "Your requested report is attached to this email." + atrack.EMAIL_FOOTER,
                                 lAttachments,
                                 false);

                MessageBox.Show("Report was successfully sent to " + atrack.EMAILADDRESS + "!",
                                "Email Report",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        private void btnEmailMeThisReportAsPDF_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbReportList.SelectedIndex < 0) return;
                else if (MessageBox.Show("Send this PDF report to " + atrack.EMAILADDRESS + "?",
                                         "Email Report",
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question) == DialogResult.No) return;

                List<Attachment> lAttachments = new List<Attachment>();
                lAttachments.Add(ReturnSingleReportAsAttachment("PDF"));
                atrack.SendEmail(new string[] { atrack.EMAILADDRESS },
                                 null,
                                 null,
                                 lAttachments[0].ContentDisposition.FileName,
                                 "Your requested report is attached to this email." + atrack.EMAIL_FOOTER,
                                 lAttachments,
                                 false);

                MessageBox.Show("Report was successfully sent to " + atrack.EMAILADDRESS + "!",
                                "Email Report",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        private void dashboardViewer1_DataLoadingError(object sender, DataLoadingErrorEventArgs e)
        {
            atrack.LogAndDisplayError(e.ToString(), true);
        }
        private void dashboardViewer1_DashboardItemControlCreated(object sender, DevExpress.DashboardWin.DashboardItemControlEventArgs e)
        {
            if (e.PivotGridControl != null) e.PivotGridControl.BestFit();
        }
        private void dashboardViewer1_DashboardItemControlUpdated(object sender, DevExpress.DashboardWin.DashboardItemControlEventArgs e)
        {
            if (e.PivotGridControl != null) e.PivotGridControl.BestFit();
        }
        private void dashboardDesigner1_DashboardItemControlCreated(object sender, DevExpress.DashboardWin.DashboardItemControlEventArgs e)
        {
            if (e.PivotGridControl != null) e.PivotGridControl.BestFit();
        }
        private void dashboardDesigner1_DashboardItemControlUpdated(object sender, DevExpress.DashboardWin.DashboardItemControlEventArgs e)
        {
            if (e.PivotGridControl != null) e.PivotGridControl.BestFit();
        }

        private void dashboardDesigner1_ValidateCustomSqlQuery(object sender, ValidateDashboardCustomSqlQueryEventArgs e)
        {
            e.Valid = true;
        }
        private void dashboardViewer1_ValidateCustomSqlQuery(object sender, ValidateDashboardCustomSqlQueryEventArgs e)
        {
            e.Valid = true;
        }
    }
}