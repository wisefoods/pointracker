﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using DevExpress.XtraGrid.Views.Grid;

namespace AttendanceTracker
{
    public partial class PointSystemTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;

        public PointSystemTemplate(Form1 pForm)
        {
            try
            {
                InitializeComponent();

                atrack = pForm;

                gcDemerits.DataSource = null;
                gcDemerits.DataSource = atrack.dataset.Demerits.Select(z => new { z.Entry, z.Condition, z.Points }).OrderBy(z => z.Entry).ToList();
                (gcDemerits.MainView as GridView).Columns["Points"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                (gcDemerits.MainView as GridView).Columns["Entry"].Visible = false;
                (gcDemerits.MainView as GridView).BestFitColumns();

                gcMerits.DataSource = null;
                gcMerits.DataSource = atrack.dataset.Merits.Select(z => new { z.Entry, z.Condition, z.Points }).OrderBy(z => z.Entry).ToList();                
                (gcMerits.MainView as GridView).Columns["Points"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                (gcMerits.MainView as GridView).Columns["Entry"].Visible = false;
                (gcMerits.MainView as GridView).BestFitColumns();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }
    }
}
