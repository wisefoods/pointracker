﻿namespace AttendanceTracker
{
    partial class PointLetterTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PointLetterTemplate));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.richEditor = new DevExpress.XtraRichEdit.RichEditControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSendViaEmail = new DevExpress.XtraEditors.ButtonEdit();
            this.btnPrintDirectly = new DevExpress.XtraEditors.ButtonEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSendViaEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrintDirectly.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // richEditor
            // 
            this.richEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richEditor.Location = new System.Drawing.Point(0, 0);
            this.richEditor.Name = "richEditor";
            this.richEditor.Options.Annotations.ShowAllAuthors = false;
            this.richEditor.Options.Behavior.ShowPopupMenu = DevExpress.XtraRichEdit.DocumentCapability.Hidden;
            this.richEditor.Options.HorizontalRuler.ShowLeftIndent = false;
            this.richEditor.Options.HorizontalRuler.ShowRightIndent = false;
            this.richEditor.Options.HorizontalRuler.ShowTabs = false;
            this.richEditor.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this.richEditor.Options.Hyperlinks.ShowToolTip = false;
            this.richEditor.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this.richEditor.ShowCaretInReadOnly = false;
            this.richEditor.Size = new System.Drawing.Size(709, 457);
            this.richEditor.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.richEditor);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(218, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(709, 457);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnSendViaEmail);
            this.panel2.Controls.Add(this.btnPrintDirectly);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(218, 457);
            this.panel2.TabIndex = 2;
            // 
            // btnSendViaEmail
            // 
            this.btnSendViaEmail.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSendViaEmail.EditValue = "Generate Point Letter";
            this.btnSendViaEmail.Location = new System.Drawing.Point(0, 40);
            this.btnSendViaEmail.Name = "btnSendViaEmail";
            this.btnSendViaEmail.Properties.Appearance.BackColor = System.Drawing.Color.Green;
            this.btnSendViaEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnSendViaEmail.Properties.Appearance.Options.UseBackColor = true;
            this.btnSendViaEmail.Properties.Appearance.Options.UseFont = true;
            this.btnSendViaEmail.Properties.Appearance.Options.UseImage = true;
            this.btnSendViaEmail.Properties.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            editorButtonImageOptions1.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            editorButtonImageOptions1.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            serializableAppearanceObject1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            serializableAppearanceObject1.Options.UseFont = true;
            serializableAppearanceObject1.Options.UseTextOptions = true;
            serializableAppearanceObject1.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnSendViaEmail.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "   Send to me via email", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnSendViaEmail.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnSendViaEmail.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.btnSendViaEmail.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSendViaEmail.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnSendViaEmail.Size = new System.Drawing.Size(218, 40);
            this.btnSendViaEmail.TabIndex = 15;
            this.btnSendViaEmail.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnSendViaEmail_ButtonClick);
            // 
            // btnPrintDirectly
            // 
            this.btnPrintDirectly.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPrintDirectly.EditValue = "Generate Point Letter";
            this.btnPrintDirectly.Location = new System.Drawing.Point(0, 0);
            this.btnPrintDirectly.Name = "btnPrintDirectly";
            this.btnPrintDirectly.Properties.Appearance.BackColor = System.Drawing.Color.Green;
            this.btnPrintDirectly.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnPrintDirectly.Properties.Appearance.Options.UseBackColor = true;
            this.btnPrintDirectly.Properties.Appearance.Options.UseFont = true;
            this.btnPrintDirectly.Properties.Appearance.Options.UseImage = true;
            this.btnPrintDirectly.Properties.AutoHeight = false;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            editorButtonImageOptions2.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            editorButtonImageOptions2.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            serializableAppearanceObject5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            serializableAppearanceObject5.Options.UseFont = true;
            serializableAppearanceObject5.Options.UseTextOptions = true;
            serializableAppearanceObject5.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnPrintDirectly.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "       Print directly", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnPrintDirectly.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPrintDirectly.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.btnPrintDirectly.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnPrintDirectly.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnPrintDirectly.Size = new System.Drawing.Size(218, 40);
            this.btnPrintDirectly.TabIndex = 16;
            this.btnPrintDirectly.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnPrintDirectly_ButtonClick);
            // 
            // PointLetterTemplate
            // 
            this.ClientSize = new System.Drawing.Size(927, 457);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PointLetterTemplate";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnSendViaEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrintDirectly.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraGrid.GridControl gcEmployeeEvents;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraRichEdit.RichEditControl richEditor;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.ButtonEdit btnSendViaEmail;
        private DevExpress.XtraEditors.ButtonEdit btnPrintDirectly;
    }
}