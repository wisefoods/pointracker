﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace AttendanceTracker
{
    public partial class AdminTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;

        public AdminTemplate(Form1 pForm)
        {
            try
            {
                InitializeComponent();
                atrack = pForm;

                HideAllAdminPanels();
                AllowEditAdminTable(false);

                cbArea.Properties.Items.Add("Actions Taken");
                cbArea.Properties.Items.Add("Change Requests");
                cbArea.Properties.Items.Add("Current Employees");
                cbArea.Properties.Items.Add("Current Users");
                cbArea.Properties.Items.Add("Data Source");
                cbArea.Properties.Items.Add("Data Source (Archive)");                
                cbArea.Properties.Items.Add("Email Log");
                cbArea.Properties.Items.Add("Email Recipients");
                cbArea.Properties.Items.Add("Error Log");
                cbArea.Properties.Items.Add("Feedback Log");
                cbArea.Properties.Items.Add("FMLA Notifications");
                cbArea.Properties.Items.Add("Headcount Report");
                cbArea.Properties.Items.Add("High Points");
                cbArea.Properties.Items.Add("Login Log");
                cbArea.Properties.Items.Add("Manual Adjustments");
                cbArea.Properties.Items.Add("Org Chart Report");
                cbArea.Properties.Items.Add("PAD Notifications");
                cbArea.Properties.Items.Add("Processes Log");
                cbArea.Properties.Items.Add("Rehires");
                cbArea.Properties.Items.Add("Shift Rotation Report");
                cbArea.Properties.Items.Add("Tracked Bonuses");
                cbArea.Properties.Items.Add("Tracked Penalties");
                cbArea.Properties.Items.Add("Update Log");

                cbOverrideAdminForReporting.Checked = atrack.ADMINOPTION_OVERRIDEREPORTINGTAB;

                /// bind admin data
                atrack.BindAutomatedDatasets();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        private void cbArea_SelectedIndexChanged(object sender, EventArgs e) { BindData(); gcAdmin.Select(); }
        private void btnRefreshData_Click(object sender, EventArgs e) { BindData(); }
        private void BindData()
        {
            try
            {
                string splashMsg = "Yeah hold on, I'm comin'";
                atrack.ShowSplash(splashMsg);

                GridView gvAdmin = gcAdmin.MainView as GridView;
                gcAdmin.DataSource = null;
                gvAdmin.Columns.Clear();

                if (cbArea.SelectedItem.ToString() == "Actions Taken")
                {
                    gcAdmin.DataSource = atrack.dataset.ActionsTakens.OrderBy(z => z.EmployeeName).ThenBy(z => z.DateOfOccurrence).ToList();
                    gvAdmin.Columns["DateOfOccurrence"].DisplayFormat.FormatType = FormatType.DateTime;
                    gvAdmin.Columns["DateOfOccurrence"].DisplayFormat.FormatString = "MM/dd/yyyy";
                    gvAdmin.Columns["ActionTakenOn"].DisplayFormat.FormatType = FormatType.DateTime;
                    gvAdmin.Columns["ActionTakenOn"].DisplayFormat.FormatString = "MM/dd/yyyy hh:mm:ss";
                }                
                else if (cbArea.SelectedItem.ToString() == "Change Requests")
                {
                    gcAdmin.DataSource = atrack.dataset.ChangeRequests.OrderByDescending(z => z.SubmittedOn).ToList();
                    gvAdmin.Columns["SubmittedOn"].DisplayFormat.FormatType = FormatType.DateTime;
                    gvAdmin.Columns["SubmittedOn"].DisplayFormat.FormatString = "MM/dd/yyyy";
                }
                else if (cbArea.SelectedItem.ToString() == "Current Employees")
                {
                    gcAdmin.DataSource = atrack.dataset.CurrentEmployees.OrderBy(z => z.EmployeeName).ToList();
                }
                else if (cbArea.SelectedItem.ToString() == "Current Users")
                {
                    gcAdmin.DataSource = atrack.dataset.UserOptions.OrderBy(z => z.FullName).ToList();
                }
                else if (cbArea.SelectedItem.ToString() == "Data Source")
                {
                    atrack.CloseSplash();
                    if (MessageBox.Show("Load this data? It'll take a while...",
                                        "That's a lot of data!",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Hand) == DialogResult.Yes)
                    {
                        atrack.ShowSplash(splashMsg);
                        gcAdmin.DataSource = atrack.dataset.DataSources.OrderBy(z => z.Employee_DisplayName).ThenBy(z => z.PayDate).ToList();
                        gvAdmin.Columns["PayDate"].DisplayFormat.FormatType =
                        gvAdmin.Columns["RemovalDate"].DisplayFormat.FormatType =
                        gvAdmin.Columns["EntryDate"].DisplayFormat.FormatType = FormatType.DateTime;
                        gvAdmin.Columns["PayDate"].DisplayFormat.FormatString =
                        gvAdmin.Columns["RemovalDate"].DisplayFormat.FormatString =
                        gvAdmin.Columns["EntryDate"].DisplayFormat.FormatString = "MM/dd/yyyy";
                    }
                    else return;
                }
                else if (cbArea.SelectedItem.ToString() == "Data Source (Archive)")
                {
                    atrack.CloseSplash();
                    if (MessageBox.Show("Load this data? It'll take a while...",
                                        "That's a lot of data!",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Hand) == DialogResult.Yes)
                    {
                        atrack.ShowSplash(splashMsg);
                        gcAdmin.DataSource = atrack.dataset.DataSource_Archive.OrderBy(z => z.Employee_DisplayName).ThenBy(z => z.PayDate).ToList();
                        gvAdmin.Columns["PayDate"].DisplayFormat.FormatType =
                        gvAdmin.Columns["RemovalDate"].DisplayFormat.FormatType =
                        gvAdmin.Columns["EntryDate"].DisplayFormat.FormatType = FormatType.DateTime;
                        gvAdmin.Columns["PayDate"].DisplayFormat.FormatString =
                        gvAdmin.Columns["RemovalDate"].DisplayFormat.FormatString =
                        gvAdmin.Columns["EntryDate"].DisplayFormat.FormatString = "MM/dd/yyyy";
                    }
                    else return;
                }
                else if (cbArea.SelectedItem.ToString() == "Email Log")
                {
                    atrack.CloseSplash();
                    if (MessageBox.Show("Load this data? It'll take a while...",
                                        "That's a lot of data!",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Hand) == DialogResult.Yes)
                    {
                        gcAdmin.DataSource = atrack.dataset.EmailLogs.OrderByDescending(z => z.Sent).ToList();
                        gvAdmin.Columns["Sent"].DisplayFormat.FormatType = FormatType.DateTime;
                        gvAdmin.Columns["Sent"].DisplayFormat.FormatString = "MM/dd/yyyy hh:mm:ss";
                    }
                    else return;
                }
                else if (cbArea.SelectedItem.ToString() == "Email Recipients")
                {
                    gcAdmin.DataSource = atrack.dataset.EmailRecipients.OrderBy(z => z.Type)
                                                                       .ThenBy(z => z.Department)
                                                                       .ThenBy(z => z.EmailRecipient_Email)
                                                                       .ToList();
                }                
                else if (cbArea.SelectedItem.ToString() == "Error Log")
                {
                    gcAdmin.DataSource = atrack.dataset.ErrorLogs.OrderByDescending(z => z.CreatedOn).ToList();
                    gvAdmin.Columns["CreatedOn"].DisplayFormat.FormatType = FormatType.DateTime;
                    gvAdmin.Columns["CreatedOn"].DisplayFormat.FormatString = "MM/dd/yyyy hh:mm:ss";
                }
                else if (cbArea.SelectedItem.ToString() == "Feedback Log")
                {
                    gcAdmin.DataSource = atrack.dataset.FeedbackLogs.OrderByDescending(z => z.SubmittedOn).ToList();
                    gvAdmin.Columns["SubmittedOn"].DisplayFormat.FormatType = FormatType.DateTime;
                    gvAdmin.Columns["SubmittedOn"].DisplayFormat.FormatString = "MM/dd/yyyy hh:mm:ss";
                }
                else if (cbArea.SelectedItem.ToString() == "FMLA Notifications")
                {
                    gcAdmin.DataSource = atrack.dataset.FMLANotifications.OrderByDescending(z => z.NotifiedOn).ToList();
                    gvAdmin.Columns["NotifiedOn"].DisplayFormat.FormatType = FormatType.DateTime;
                    gvAdmin.Columns["NotifiedOn"].DisplayFormat.FormatString = "MM/dd/yyyy hh:mm:ss";
                }
                else if (cbArea.SelectedItem.ToString() == "Headcount Report")
                {
                    gcAdmin.DataSource = atrack.dataset.FromCeridianAPI_Headcount.OrderBy(z => z.Employee_NameNumber).ToList();
                }
                else if (cbArea.SelectedItem.ToString() == "High Points")
                {
                    gcAdmin.DataSource = atrack.dataset.HighPoints.OrderBy(z => z.Name).ThenBy(z => z.Pay_Date).ToList();
                    gvAdmin.Columns["Pay_Date"].DisplayFormat.FormatType = FormatType.DateTime;
                    gvAdmin.Columns["Pay_Date"].DisplayFormat.FormatString = "MM/dd/yyyy";
                }
                else if (cbArea.SelectedItem.ToString() == "Login Log")
                {
                    gcAdmin.DataSource = atrack.dataset.Logins.OrderByDescending(z => z.Date).ToList();
                    gvAdmin.Columns["Date"].DisplayFormat.FormatType = FormatType.DateTime;
                    gvAdmin.Columns["Date"].DisplayFormat.FormatString = "MM/dd/yyyy hh:mm:ss";
                }
                else if (cbArea.SelectedItem.ToString() == "Manual Adjustments")
                {
                    gcAdmin.DataSource = atrack.dataset.ManualAdjustments.OrderBy(z => z.EmployeeID).ThenBy(z => z.DateToAdjust).ToList();
                    gvAdmin.Columns["DateToAdjust"].DisplayFormat.FormatType = FormatType.DateTime;
                    gvAdmin.Columns["DateToAdjust"].DisplayFormat.FormatString = "MM/dd/yyyy";

                    //RepositoryItemTextEdit edit = new RepositoryItemTextEdit();
                    RepositoryItemMemoEdit edit = new RepositoryItemMemoEdit();
                    gcAdmin.RepositoryItems.Add(edit);
                    gvAdmin.Columns["LongNote"].ColumnEdit = edit;
                }
                else if (cbArea.SelectedItem.ToString() == "Org Chart Report")
                {
                    gcAdmin.DataSource = atrack.dataset.FromCeridianAPI_OrgChart.OrderBy(z => z.Employee_DisplayName).ToList();
                }
                else if (cbArea.SelectedItem.ToString() == "PAD Notifications")
                {
                    gcAdmin.DataSource = atrack.dataset.PADNotifications.OrderByDescending(z => z.NotifiedOn).ToList();
                    gvAdmin.Columns["NotifiedOn"].DisplayFormat.FormatType = FormatType.DateTime;
                    gvAdmin.Columns["NotifiedOn"].DisplayFormat.FormatString = "MM/dd/yyyy hh:mm:ss";
                }
                else if (cbArea.SelectedItem.ToString() == "Processes Log")
                {
                    gcAdmin.DataSource = atrack.dataset.UpdateProcesses.OrderBy(z => z.ProcessName).ToList();
                    gvAdmin.Columns["LastRun"].DisplayFormat.FormatType =
                    gvAdmin.Columns["LastFinished"].DisplayFormat.FormatType = FormatType.DateTime;
                    gvAdmin.Columns["LastRun"].DisplayFormat.FormatString =
                    gvAdmin.Columns["LastFinished"].DisplayFormat.FormatString = "MM/dd/yyyy hh:mm:ss";
                }
                else if (cbArea.SelectedItem.ToString() == "Rehires")
                {
                    gcAdmin.DataSource = atrack.dataset.Rehires.OrderBy(z => z.RehireDate).ToList();
                }
                else if (cbArea.SelectedItem.ToString() == "Shift Rotation Report")
                {
                    gcAdmin.DataSource = atrack.dataset.FromCeridianAPI_ShiftRotations.OrderBy(z => z.Employee_DisplayName).ToList();
                }
                else if (cbArea.SelectedItem.ToString() == "Tracked Bonuses")
                {
                    gcAdmin.DataSource = atrack.dataset.TrackedBonuses.OrderBy(z => z.EmployeeID).ThenBy(z => z.BonusDate).ToList();
                }
                else if (cbArea.SelectedItem.ToString() == "Tracked Penalties")
                {
                    gcAdmin.DataSource = atrack.dataset.TrackedPenalties.OrderBy(z => z.EmployeeID).ThenBy(z => z.PenaltyDate).ToList();
                }
                else if (cbArea.SelectedItem.ToString() == "Update Log")
                {
                    gcAdmin.DataSource = atrack.dataset.UpdateLogs.OrderByDescending(z => z.ServerUpdateDate).ToList();
                    gvAdmin.Columns["ServerUpdateDate"].DisplayFormat.FormatType = FormatType.DateTime;
                    gvAdmin.Columns["ServerUpdateDate"].DisplayFormat.FormatString = "MM/dd/yyyy hh:mm:ss";
                }
                else return;

                gvAdmin.OptionsView.ColumnAutoWidth = true;
                /// width is too narrow to use .BestFitColumns(), would need to use horizontal scrollbar
                //gvAdmin.BestFitColumns();
                SetTableRowCountMsg();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                atrack.CloseSplash();
            }
        }

        private void HideAllAdminPanels()
        {
            panelAddManualAdjustment.Visible = false;
            panelUpdateSingleEmployee.Visible = false;
            panelAddEmailRecipient.Visible = false;
            panelAddNewRehire.Visible = false;
            panelPointLetter.Visible = false;
        }

        private void AllowEditAdminTable(bool? pFlag)
        {
            if ((pFlag != null && pFlag == false) ||
                (pFlag == null && gvAdmin.OptionsBehavior.Editable))
            {
                gvAdmin.OptionsBehavior.Editable = false;
                gvAdmin.OptionsBehavior.AllowAddRows = DefaultBoolean.False;
                gvAdmin.OptionsBehavior.AllowDeleteRows = DefaultBoolean.False;
                //gvAdmin.OptionsBehavior.EditorShowMode = EditorShowMode.Click;
                //gvAdmin.OptionsBehavior.EditingMode = GridEditingMode.EditFormInplace;
                lblAllowEdit.Text = "Admin table is LOCKED! (click to change)";
                lblAllowEdit.ForeColor = Color.Red;
            }
            else
            {
                gvAdmin.OptionsBehavior.Editable = true;
                gvAdmin.OptionsBehavior.AllowAddRows = DefaultBoolean.False;
                gvAdmin.OptionsBehavior.AllowDeleteRows = DefaultBoolean.True;
                gvAdmin.OptionsBehavior.EditorShowMode = EditorShowMode.Click;
                gvAdmin.OptionsBehavior.EditingMode = GridEditingMode.EditFormInplace;
                lblAllowEdit.Text = "Admin table is EDITABLE! (click to change)";
                lblAllowEdit.ForeColor = Color.Lime;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ya sure?",
                                "Refresh All App Data",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                atrack.ShowSplash("Incepting");
                atrack.UpdateTheData_fromBeyond();

                /// finish up
                atrack.CloseSplash();
                MessageBox.Show("All data has been refreshed.",
                                "Yay; refresh data as necessary!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
        }
                    
        private void YellAboutAdminTasksTab() { atrack.CloseSplash(); MessageBox.Show("Ut oh", "Could not locate AdminTasks tab!", MessageBoxButtons.OK, MessageBoxIcon.Error); }

        private void btnUpdateCurrentEmployeesOnly_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ya sure?",
                                "Update Current Employees Only",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                try
                {
                    atrack.ShowSplash("Staring into the void");

                    (atrack.GetTabFromMain("AdminTasks") as PageTemplate).UpdateCurrentEmployeeTable();

                    /// finish up
                    atrack.CloseSplash();
                    MessageBox.Show("All current employees have been updated.",
                                    "Yay; refresh data as necessary!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    YellAboutAdminTasksTab();
                    atrack.LogAndDisplayError(ex.ToString(), true);
                }
                finally
                {
                    atrack.CloseSplash();
                }
            }
        }

        private void btnSendFMLAEmail_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ya sure?",
                                "Send FMLA Emails",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                try
                {
                    atrack.ShowSplash("I think I see something");

                    (atrack.GetTabFromMain("AdminTasks") as PageTemplate).SendFMLANotificationEmails();

                    /// finish up
                    atrack.CloseSplash();
                    MessageBox.Show("FMLA notification emails have been sent.",
                                    "Yay; refresh data as necessary!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    YellAboutAdminTasksTab();
                    atrack.LogAndDisplayError(ex.ToString(), true);
                }
                finally
                {
                    atrack.CloseSplash();
                }
            }
        }

        private void btnSendPADEmail_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ya sure?",
                                "Send PAD Emails",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                try
                {
                    atrack.ShowSplash("It's, it's beautiful");

                    (atrack.GetTabFromMain("AdminTasks") as PageTemplate).SendPADNotificationEmails();

                    /// finish up
                    atrack.CloseSplash();
                    MessageBox.Show("PAD notification emails have been sent.",
                                    "Yay; refresh data as necessary!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    YellAboutAdminTasksTab();
                    atrack.LogAndDisplayError(ex.ToString(), true);
                }
                finally
                {
                    atrack.CloseSplash();
                }
            }
        }

        private void btnSendReportEmail_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ya sure?",
                                "Send Report Emails",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                try
                {
                    atrack.ShowSplash("Just going to take a quick nap");

                    (atrack.GetTabFromMain("AdminTasks") as PageTemplate).SendReportEmails();
                    
                    /// finish up
                    atrack.CloseSplash();
                    MessageBox.Show("Report emails have been sent.",
                                    "Yay; refresh data as necessary!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    YellAboutAdminTasksTab(); 
                    atrack.LogAndDisplayError(ex.ToString(), true);
                }
                finally
                {
                    atrack.CloseSplash();
                }
            }
        }

        private void btnSendPointLetterReminderEmail_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ya sure?",
                                "Send Point Letter Reminder Emails",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                try
                {
                    atrack.ShowSplash("Discovering lost knowledge");

                    (atrack.GetTabFromMain("AdminTasks") as PageTemplate).SendActionsRequiredEmails(false, true);
                    
                    /// finish up
                    atrack.CloseSplash();
                    MessageBox.Show("Point letter reminder emails have been sent.",
                                    "Yay; refresh data as necessary!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    YellAboutAdminTasksTab();
                    atrack.LogAndDisplayError(ex.ToString(), true);
                }
                finally
                {
                    atrack.CloseSplash();
                }
            }
        }

        private void btnSendMissedPointLetterEmail_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ya sure?",
                                "Send Missed Point Letter Emails",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                try
                {
                    atrack.ShowSplash("Learning to love");

                    (atrack.GetTabFromMain("AdminTasks") as PageTemplate).SendActionsRequiredEmails(true, true);
                    
                    /// finish up
                    atrack.CloseSplash();
                    MessageBox.Show("Missed point letter notification emails have been sent.",
                                    "Yay; refresh data as necessary!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    YellAboutAdminTasksTab();
                    atrack.LogAndDisplayError(ex.ToString(), true);
                }
                finally
                {
                    atrack.CloseSplash();
                }
            }
        }       

        private void btnUpdateAllForSingleEmployee_Click(object sender, EventArgs e)
        {
            if (panelUpdateSingleEmployee.Visible) return;

            SINGLE_cbEmployeeList.SelectedIndex = -1;
            SINGLE_cbEmployeeList.Properties.Items.Clear();            
            SINGLE_cbEmployeeList.Properties.Items.AddRange(atrack.MYEMPLOYEES_NICELIST);

            HideAllAdminPanels();
            panelUpdateSingleEmployee.Dock = DockStyle.Fill;
            panelUpdateSingleEmployee.Visible = true;            
        }

        private void btnCancelUpdateAllSingleEmployee_Click(object sender, EventArgs e)
        {
            panelUpdateSingleEmployee.Visible = false;
        }
        private void btnGoUpdateAllSingleEmployee_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ya sure?",
                                "Update All for Single Employee",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                try
                {
                    atrack.ShowSplash("Hypothesizing");

                    if (SINGLE_cbEmployeeList.SelectedIndex < 0) { ValidationFail("You didn't select anyone"); return; }

                    /// re-bind admin data
                    atrack.BindAutomatedDatasets();

                    atrack.RUNNINGADMINTASK = true;
                    (atrack.GetTabFromMain("AdminTasks") as PageTemplate).PopulateHighPointsAndActionsToTakeTables(SINGLE_cbEmployeeList.SelectedIndex);
                    atrack.RUNNINGADMINTASK = false;                    

                    /// hide and clear editor panel
                    panelUpdateSingleEmployee.Visible = false;
                    SINGLE_cbEmployeeList.SelectedIndex = -1;
                    SINGLE_cbEmployeeList.Properties.Items.Clear();

                    /// finish up
                    atrack.CloseSplash();
                    MessageBox.Show("All data options were updated for the selected employee.",
                                    "Yay; refresh data as necessary!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);                    
                }
                catch (Exception ex)
                {
                    YellAboutAdminTasksTab();
                    atrack.LogAndDisplayError(ex.ToString(), true);
                }
                finally
                {
                    atrack.RUNNINGADMINTASK = false;
                    atrack.CloseSplash();
                }
            }
        }

        private void btnAddManualAdjustment_Click(object sender, EventArgs e)
        {
            if (panelAddManualAdjustment.Visible) return;

            ClearAddManualAdjustmentArea();
            PopulateAddManualAdjustmentArea();

            HideAllAdminPanels();
            panelAddManualAdjustment.Dock = DockStyle.Fill;
            panelAddManualAdjustment.Visible = true;
        }
        private void ClearAddManualAdjustmentArea()
        {
            /// do not reset these values so they remain in-between button clicks
            /// there are usually more than 1 adjustment to add per-employee so this will make it a little easier/faster
            //ME_cbEmployeeList.SelectedIndex = -1;
            ME_cbEmployeeList.Properties.Items.Clear();
            //ME_deDateToAdjust.Text = string.Empty;
            ME_cbExistingPayCode.Properties.Items.Clear();
            //ME_numExistingNetHours.Value = 0;
            ME_cbNewPayCode.Properties.Items.Clear();
            //ME_numNewNetHours.Value = 0;
            //ME_tbShortNote.Text = string.Empty;
            //ME_meLongNote.Text = string.Empty;
        }
        private void PopulateAddManualAdjustmentArea()
        {
            ME_cbEmployeeList.Properties.Items.AddRange(atrack.MYEMPLOYEES_NICELIST);             
            ME_cbEmployeeList.Properties.Items.Insert(0, "ALL");

            List<string> lPayCodes = atrack.PAYCODES_LIST.Select(z => z.Code).OrderBy(z => z.ToString()).ToList();
            ME_cbExistingPayCode.Properties.Items.AddRange(lPayCodes);
            ME_cbExistingPayCode.Properties.Items.Insert(0, "ALL");
            ME_cbExistingPayCode.Properties.Items.Insert(0, "NONE");
            //ME_cbExistingPayCode.SelectedIndex = 0;
            ME_cbNewPayCode.Properties.Items.AddRange(lPayCodes);
            ME_cbNewPayCode.Properties.Items.Insert(0, "NONE");
            //ME_cbNewPayCode.SelectedIndex = 0;
        }
        private void btnCancelAddManualAdjustment_Click(object sender, EventArgs e)
        {
            panelAddManualAdjustment.Visible = false;
        }
        private void btnConfirmAddManualAdjustment_Click(object sender, EventArgs e)
        {    
            try
            {
                atrack.ShowSplash("Validating");

                /// validate values
                if (ME_cbEmployeeList.Text == null || ME_cbEmployeeList.Text == string.Empty) { ValidationFail("Select an employee"); return; }
                else if (ME_deDateToAdjust.Text == null || ME_deDateToAdjust.Text == string.Empty) { ValidationFail("Enter a date to adjust"); return; }
                else if (ME_cbExistingPayCode.Text == null || ME_cbExistingPayCode.Text == string.Empty) { ValidationFail("Select a pay code to change"); return; }
                else if (ME_numExistingNetHours.Text == null || ME_numExistingNetHours.Text == string.Empty) { ValidationFail("Enter the net hours to change"); return; }
                else if (ME_cbNewPayCode.Text == null || ME_cbNewPayCode.Text == string.Empty) { ValidationFail("Select a pay code to replace this with"); return; }
                else if (ME_numNewNetHours.Text == null || ME_numNewNetHours.Text == string.Empty) { ValidationFail("Enter the net hours to replace this with"); return; }
                else if (ME_tbShortNote.Text == null || ME_tbShortNote.Text == string.Empty) { ValidationFail("Enter a short note"); return; }
                else if (ME_meLongNote.Text == null || ME_meLongNote.Text == string.Empty) { ValidationFail("Enter a long note"); return; }

                /// write multiline text field as lines to preserve linebreaks
                string lineLongNote = string.Empty;
                /// <br> is unnecessary here and will be worse, actually
                //foreach (string line in ME_meLongNote.Lines) { lineLongNote += line + "<br>"; }
                foreach (string line in ME_meLongNote.Lines) { lineLongNote += line; }

                /// generate new db entry
                string empID = atrack.GetEmployeeIDFromSelection(ME_cbEmployeeList.Text);
                ManualAdjustment newME = new ManualAdjustment();
                newME.ID = Guid.NewGuid();
                newME.EmployeeID = empID;
                newME.DateToAdjust = ME_deDateToAdjust.DateTime.Date;
                newME.ActualEntry_PayCode = ME_cbExistingPayCode.Text == "NONE" ? string.Empty : ME_cbExistingPayCode.Text;
                newME.ActualEntry_NetHours = ME_numExistingNetHours.Value;
                newME.AdjustedEntry_PayCode = ME_cbNewPayCode.Text == "NONE" ? string.Empty : ME_cbNewPayCode.Text;
                newME.AdjustedEntry_NetHours = ME_numNewNetHours.Value;
                newME.ShortNote = ME_tbShortNote.Text;
                newME.LongNote = lineLongNote;
                newME.DateModified = DateTime.Now;
                atrack.dataset.ManualAdjustments.Add(newME);
                atrack.dataset.SaveChanges();

                /// re-cache local values for manual adjustments
                atrack.BindManualAdjustmentsData();

                /// re-cache local values for current employees
                atrack.BindCurrentEmployeesData();

                /// hide and clear editor panel
                panelAddManualAdjustment.Visible = false;
                ClearAddManualAdjustmentArea();

                /// finish up
                atrack.CloseSplash();
                MessageBox.Show("New manual adjustment was successfully added.",
                                "Yay; refresh data as necessary!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                atrack.CloseSplash();                
            }
        }
        private void ValidationFail(string pMsg)
        {
            atrack.CloseSplash();
            MessageBox.Show(pMsg + "!",
                            "Invalid entry",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
        }

        private void lblAllowEdit_Click(object sender, EventArgs e)
        {
            AllowEditAdminTable(null);
        }

        private void gvAdmin_DoubleClick(object sender, EventArgs e)
        {
            if (!gvAdmin.OptionsBehavior.Editable)
            {
                MessageBox.Show("Admin table is currently locked; click the label to unlock it.",
                                "Locked",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void gvAdmin_KeyDown(object sender, KeyEventArgs e)
        {
            if (gvAdmin.OptionsBehavior.Editable &&
                e.Modifiers == Keys.Control && e.KeyCode == Keys.Delete)
            {
                if (MessageBox.Show("Really delete the current row?",
                                    "Delete Row",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question) != DialogResult.Yes) return;
                try
                {
                    if (cbArea.SelectedItem.ToString() == "Actions Taken")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        ActionsTaken theAT = atrack.dataset.ActionsTakens.Where(z => z.ID == id).First();
                        atrack.dataset.ActionsTakens.Remove(theAT);
                        atrack.dataset.SaveChanges();
                    }                    
                    else if (cbArea.SelectedItem.ToString() == "Change Requests")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        ChangeRequest theCR = atrack.dataset.ChangeRequests.Where(z => z.ID == id).First();
                        atrack.dataset.ChangeRequests.Remove(theCR);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Current Employees")
                    {
                        string id = gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "EmployeeID").ToString();
                        CurrentEmployee theCE = atrack.dataset.CurrentEmployees.Where(z => z.EmployeeID == id).First();
                        atrack.dataset.CurrentEmployees.Remove(theCE);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Current Users")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        UserOption theUO = atrack.dataset.UserOptions.Where(z => z.ID == id).First();
                        atrack.dataset.UserOptions.Remove(theUO);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Data Source")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        DataSource theDS = atrack.dataset.DataSources.Where(z => z.ID == id).First();
                        atrack.dataset.DataSources.Remove(theDS);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Data Source (Archive)")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        DataSource_Archive theDS_A = atrack.dataset.DataSource_Archive.Where(z => z.ID == id).First();
                        atrack.dataset.DataSource_Archive.Remove(theDS_A);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Email Log")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        EmailLog theEmail = atrack.dataset.EmailLogs.Where(z => z.ID == id).First();
                        atrack.dataset.EmailLogs.Remove(theEmail);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Email Recipients")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        EmailRecipient theRecipient = atrack.dataset.EmailRecipients.Where(z => z.ID == id).First();
                        atrack.dataset.EmailRecipients.Remove(theRecipient);
                        atrack.dataset.SaveChanges();
                    }                    
                    else if (cbArea.SelectedItem.ToString() == "Error Log")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        ErrorLog theError = atrack.dataset.ErrorLogs.Where(z => z.ID == id).First();
                        atrack.dataset.ErrorLogs.Remove(theError);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Feedback Log")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        FeedbackLog theFeedback = atrack.dataset.FeedbackLogs.Where(z => z.ID == id).First();
                        atrack.dataset.FeedbackLogs.Remove(theFeedback);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "FMLA Notifications")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        FMLANotification theFN = atrack.dataset.FMLANotifications.Where(z => z.ID == id).First();
                        atrack.dataset.FMLANotifications.Remove(theFN);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Headcount Report")
                    {
                        int id = Convert.ToInt32(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        FromCeridianAPI_Headcount theHC = atrack.dataset.FromCeridianAPI_Headcount.Where(z => z.ID == id).First();
                        atrack.dataset.FromCeridianAPI_Headcount.Remove(theHC);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "High Points")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        HighPoint theHP = atrack.dataset.HighPoints.Where(z => z.ID == id).First();
                        atrack.dataset.HighPoints.Remove(theHP);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Login Log")
                    {
                        int id = Convert.ToInt32(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        Login theLogin = atrack.dataset.Logins.Where(z => z.ID == id).First();
                        atrack.dataset.Logins.Remove(theLogin);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Manual Adjustments")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        ManualAdjustment theME = atrack.dataset.ManualAdjustments.Where(z => z.ID == id).First();
                        string empID = theME.EmployeeID;
                        atrack.dataset.ManualAdjustments.Remove(theME);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Org Chart Report")
                    {
                        int id = Convert.ToInt32(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        FromCeridianAPI_OrgChart theOC = atrack.dataset.FromCeridianAPI_OrgChart.Where(z => z.ID == id).First();
                        atrack.dataset.FromCeridianAPI_OrgChart.Remove(theOC);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "PAD Notifications")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        PADNotification thePN = atrack.dataset.PADNotifications.Where(z => z.ID == id).First();
                        atrack.dataset.PADNotifications.Remove(thePN);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Processes Log")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        UpdateProcess theUP = atrack.dataset.UpdateProcesses.Where(z => z.ID == id).First();
                        atrack.dataset.UpdateProcesses.Remove(theUP);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Rehires")
                    {
                        string id = gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "EmployeeID").ToString();
                        Rehire theRH = atrack.dataset.Rehires.Where(z => z.EmployeeID == id).First();
                        atrack.dataset.Rehires.Remove(theRH);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Shift Rotation Report")
                    {
                        int id = Convert.ToInt32(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        FromCeridianAPI_ShiftRotations theSR = atrack.dataset.FromCeridianAPI_ShiftRotations.Where(z => z.ID == id).First();
                        atrack.dataset.FromCeridianAPI_ShiftRotations.Remove(theSR);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Tracked Bonuses")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        TrackedBonus theTB = atrack.dataset.TrackedBonuses.Where(z => z.ID == id).First();
                        atrack.dataset.TrackedBonuses.Remove(theTB);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Tracked Penalties")
                    {
                        Guid id = new Guid(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        TrackedPenalty theTP = atrack.dataset.TrackedPenalties.Where(z => z.ID == id).First();
                        atrack.dataset.TrackedPenalties.Remove(theTP);
                        atrack.dataset.SaveChanges();
                    }
                    else if (cbArea.SelectedItem.ToString() == "Update Log")
                    {
                        int id = Convert.ToInt32(gvAdmin.GetRowCellValue(gvAdmin.FocusedRowHandle, "ID").ToString());
                        UpdateLog theUL = atrack.dataset.UpdateLogs.Where(z => z.ID == id).First();
                        atrack.dataset.UpdateLogs.Remove(theUL);
                        atrack.dataset.SaveChanges();
                    }
                    else { return; }

                    gvAdmin.DeleteRow(gvAdmin.FocusedRowHandle);
                    SetTableRowCountMsg();

                    /// and provide an FYI about refreshing local data
                    MessageBox.Show("Deleted! Be sure to refresh data as necessary (or restart the application).",
                                    "Just a heads up",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
                }
                catch (Exception ex)
                {
                    atrack.LogAndDisplayError(ex.ToString(), true);
                }                               
            }
            else if (!gvAdmin.OptionsBehavior.Editable)
            {
                MessageBox.Show("Admin table is currently locked; click the label to unlock it.",
                                "Locked",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void gvAdmin_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (gvAdmin.OptionsBehavior.Editable)
            {
                try 
                {                    
                    /// save pending changes back to db since all tables are read from a .List()/IEnumerable setup
                    /// and not .Local.BindingList() as EF/DevEx would require here
                    atrack.dataset.SaveChanges();

                    /// special cases
                    if (cbArea.SelectedItem.ToString() == "Manual Adjustments")
                    {
                        /// re-update the modification date
                        Guid id = new Guid(gvAdmin.GetRowCellValue(e.RowHandle, "ID").ToString());
                        ManualAdjustment theME = atrack.dataset.ManualAdjustments.Where(z => z.ID == id).First();
                        theME.DateModified = DateTime.Now;
                        atrack.dataset.SaveChanges();
                    }                    
                    
                    /// and provide an FYI about refreshing local data
                    MessageBox.Show("Saved! Be sure to refresh data as necessary (or restart the application).",
                                    "Just a heads up",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);                    
                }
                catch (Exception ex)
                {
                    atrack.LogAndDisplayError(ex.ToString(), true);
                }
            }
            else
            {
                MessageBox.Show("Admin table is currently locked; click the label to unlock it.",
                                "Locked",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnAddEmailRecipient_Click(object sender, EventArgs e)
        {
            if (panelAddEmailRecipient.Visible) return;

            ClearAddEmailRecipientArea();

            cbAddEmailRecipient_Department.SelectedIndex = -1;
            cbAddEmailRecipient_Department.Properties.Items.Clear();
            cbAddEmailRecipient_Department.Properties.Items.AddRange(atrack.ALLDEPARTMENTS);
            cbAddEmailRecipient_Department.Properties.Items.Insert(0, string.Empty);

            cbAddEmailRecipient_Type.SelectedIndex = -1;
            cbAddEmailRecipient_Type.Properties.Items.Clear();
            List<string> lOptions = atrack.dataset.EmailRecipients.Select(z => z.Type)
                                                                  .Distinct()
                                                                  .OrderBy(z => z.ToString())
                                                                  .ToList();
            cbAddEmailRecipient_Type.Properties.Items.AddRange(lOptions);

            HideAllAdminPanels();
            panelAddEmailRecipient.Dock = DockStyle.Fill;
            panelAddEmailRecipient.Visible = true;
        }
        private void btnCancelAddEmailRecipient_Click(object sender, EventArgs e)
        {
            panelAddEmailRecipient.Visible = false;
        }
        private void btnSaveNewEmailRecipient_Click(object sender, EventArgs e)
        {
            try
            {
                atrack.ShowSplash("Validating");

                /// validate values
                if (tbAddEmailRecipient_Name.Text == null || tbAddEmailRecipient_Name.Text == string.Empty) { ValidationFail("Enter a name"); return; }
                else if (tbAddEmailRecipient_Email.Text == null || tbAddEmailRecipient_Email.Text == string.Empty) { ValidationFail("Enter an email address"); return; }
                else if (cbAddEmailRecipient_Type.Text == null || cbAddEmailRecipient_Type.Text == string.Empty) { ValidationFail("Select an email type"); return; }
                //else if (cbAddEmailRecipient_Department.Text == null || cbAddEmailRecipient_Department.Text == string.Empty) { ValidateEmailRecipientFail("Select a department"); return; }

                /// generate new db entry
                EmailRecipient newER = new EmailRecipient();
                newER.ID = Guid.NewGuid();
                newER.EmailRecipient_Name = tbAddEmailRecipient_Name.Text;
                newER.EmailRecipient_Email = tbAddEmailRecipient_Email.Text;
                newER.Type = cbAddEmailRecipient_Type.Text;
                newER.Department = (cbAddEmailRecipient_Department.Text == null || cbAddEmailRecipient_Department.Text == string.Empty) ? null : cbAddEmailRecipient_Department.Text;
                atrack.dataset.EmailRecipients.Add(newER);
                atrack.dataset.SaveChanges();

                /// data is not cached locally so no need to re-bind anything

                /// hide and clear editor panel
                panelAddEmailRecipient.Visible = false;
                ClearAddEmailRecipientArea();

                /// finish up
                atrack.CloseSplash();
                MessageBox.Show("New email recipient was successfully added.",
                                "Yay; refresh data as necessary!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                atrack.CloseSplash();
            }
        }
        private void ClearAddEmailRecipientArea()
        {
            tbAddEmailRecipient_Email.Text = string.Empty;
            tbAddEmailRecipient_Name.Text = string.Empty;
            cbAddEmailRecipient_Type.Properties.Items.Clear();
            cbAddEmailRecipient_Department.Properties.Items.Clear();
        }

        private void SetTableRowCountMsg()
        {
            lblRowCount.Text = gvAdmin.RowCount.ToString() + " row(s)";
        }

        private void gvAdmin_ColumnFilterChanged(object sender, EventArgs e)
        {
            SetTableRowCountMsg();
        }

        private void btnAddNewRehire_Click(object sender, EventArgs e)
        {
            if (panelAddNewRehire.Visible) return;

            cbRehireEmployeeName.SelectedIndex = -1;
            cbRehireEmployeeName.Properties.Items.Clear();
            cbRehireEmployeeName.Properties.Items.AddRange(atrack.MYEMPLOYEES_NICELIST);            

            deDateOfRehire.Text = string.Empty;

            HideAllAdminPanels();
            panelAddNewRehire.Dock = DockStyle.Fill;
            panelAddNewRehire.Visible = true;
        }
        private void btnAddNewRehireDoIt_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ya sure?",
                                "Add New Re-Hire",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                try
                {
                    atrack.ShowSplash("Preparing the circle");

                    if (cbRehireEmployeeName.SelectedIndex < 0) { ValidationFail("You didn't select anyone"); return; }
                    else if (deDateOfRehire.Text == null || deDateOfRehire.Text == string.Empty) { ValidationFail("Enter a date to adjust"); return; }

                    /// generate new db entry
                    string empID = atrack.GetEmployeeIDFromSelection(cbRehireEmployeeName.Text);
                    Rehire newRH = new Rehire();
                    newRH.EmployeeID = empID;
                    newRH.RehireDate = deDateOfRehire.DateTime.Date;                  
                    atrack.dataset.Rehires.Add(newRH);
                    atrack.dataset.SaveChanges();

                    /// hide and clear editor panel
                    panelAddNewRehire.Visible = false;
                    cbRehireEmployeeName.SelectedIndex = -1;
                    cbRehireEmployeeName.Properties.Items.Clear();

                    deDateOfRehire.Text = string.Empty;

                    /// finish up
                    atrack.CloseSplash();
                    MessageBox.Show("Re-hire has been successfully added.",
                                    "Yay; refresh data as necessary!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);                   
                }
                catch (Exception ex)
                {
                    atrack.LogAndDisplayError(ex.ToString(), true);
                }
                finally
                {
                    atrack.CloseSplash();
                }
            }
        }
        private void btnAddNewRehireCancel_Click(object sender, EventArgs e)
        {
            panelAddNewRehire.Visible = false;
        }

        private void btnPrintPointLetter_Click(object sender, EventArgs e)
        {
            if (panelPointLetter.Visible) return;

            cbPointLetterEmployeeName.SelectedIndex = -1;
            cbPointLetterEmployeeName.Properties.Items.Clear();
            cbPointLetterEmployeeName.Properties.Items.AddRange(atrack.MYEMPLOYEES_NICELIST);

            cbPointLetterPointValue.SelectedIndex = -1;
            cbPointLetterPointValue.Properties.Items.Clear();
            cbPointLetterPointValue.Properties.Items.AddRange(new string[] { "3", "6", "9", "11", "12" });

            HideAllAdminPanels();
            panelPointLetter.Dock = DockStyle.Fill;
            panelPointLetter.Visible = true;
        }

        private void btnPointLetterDoIt_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ya sure?",
                                "Print Point Letter",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                try
                {
                    atrack.ShowSplash("Supercolliding a new point letter");

                    if (cbPointLetterEmployeeName.SelectedIndex < 0) { ValidationFail("You didn't select anyone"); return; }
                    else if (cbPointLetterPointValue.Text == null || cbPointLetterPointValue.Text == string.Empty) { ValidationFail("Enter a point value to print"); return; }

                    /// generate letter
                    string empID = atrack.GetEmployeeIDFromSelection(cbPointLetterEmployeeName.Text);
                    double pointVal = Convert.ToDouble(cbPointLetterPointValue.Text);
                    CurrentEmployee thisEmp = atrack.dataset.CurrentEmployees.Where(z => z.EmployeeID == empID).First();

                    atrack.ShowTakeActionPage();
                    (atrack.GetTabFromMain("Give Point Letter") as TakeActionTemplate).GoGenerateLetter_Click(thisEmp.EmployeeID,
                                                                                                              thisEmp.EmployeeName,
                                                                                                              thisEmp.Department,
                                                                                                              DateTime.Today,
                                                                                                              pointVal,
                                                                                                              true,
                                                                                                              true);
                    
                    /// hide and clear editor panel
                    panelPointLetter.Visible = false;
                    cbPointLetterEmployeeName.SelectedIndex = -1;
                    cbPointLetterEmployeeName.Properties.Items.Clear();
                    cbPointLetterEmployeeName.Properties.Items.AddRange(atrack.MYEMPLOYEES_NICELIST);

                    cbPointLetterPointValue.SelectedIndex = -1;
                    cbPointLetterPointValue.Properties.Items.Clear();
                    cbPointLetterPointValue.Properties.Items.AddRange(new string[] { "3", "6", "9", "11", "12" });

                    /// finish up
                    atrack.CloseSplash();
                    MessageBox.Show("Point letter was successfully generated.",
                                    "Yay; refresh data as necessary!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    atrack.LogAndDisplayError(ex.ToString(), true);
                }
                finally
                {
                    atrack.CloseSplash();
                }
            }
        }

        private void btnPointLetterCancel_Click(object sender, EventArgs e)
        {
            panelPointLetter.Visible = false;
        }

        private void cbOverrideAdminForReporting_CheckedChanged(object sender, EventArgs e)
        {
            atrack.ADMINOPTION_OVERRIDEREPORTINGTAB = cbOverrideAdminForReporting.Checked;
        }
    }
}