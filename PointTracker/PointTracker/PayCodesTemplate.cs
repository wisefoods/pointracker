﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using DevExpress.XtraGrid.Views.Grid;

namespace AttendanceTracker
{
    public partial class PayCodesTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;

        public PayCodesTemplate(Form1 pForm)
        {
            try
            {
                InitializeComponent();

                atrack = pForm;
                
                gcPayCodes.DataSource = null;
                (gcPayCodes.MainView as GridView).Columns.Clear();
                gcPayCodes.DataSource = atrack.dataset.PayCodes.Where(z => z.Considered).Select(z => z.Code).OrderBy(z => z.ToString()).ToList();
                (gcPayCodes.MainView as GridView).BestFitColumns();

                (gcPayCodes.MainView as GridView).Columns["Column"].Caption = "Ceridian Pay Code";
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }
    }
}
