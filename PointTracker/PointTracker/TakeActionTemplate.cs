﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using DevExpress.XtraGrid.Views.Grid;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using OpenXmlPowerTools;
using DevExpress.XtraBars;

namespace AttendanceTracker
{
    public partial class TakeActionTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;
        ActionsTaken SELECTEDACTION = null;

        public TakeActionTemplate(Form1 pForm)
        {
            try
            {
                InitializeComponent();
                atrack = pForm;                
                BindPageData();
                EnableControls(false);

                atrack.BindPointLetterDatasets();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }
        public void BindPageData()
        {
            try
            {
                BindEmployeeList(true);
                if (!atrack.RUNNINGAUTOMATED)
                {
                    DateTime updateDate = atrack.LASTTASKSUPDATE;
                    labelLastUpdate.Text = "Data from: " + updateDate.ToString();                    
                }
            }
            catch (Exception ex)
            {
                if (atrack.UPDATEACTIVE) { MessageBox.Show("Something went wrong; please close this page and try again.", "Data Update Failed", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                else { atrack.LogAndDisplayError(ex.ToString(), true); }
            }
        }

        private void EnableControls(bool pFlag)
        {
            tbComments.Clear();
            /// let these be user set only
            //checkExportAllRecords.Checked = false;
            //checkPrintDoubleSided.Checked = true;

            /// vvv this has been disabled for now because it isn't wanted, but that's stupid...
            checkPrintDoubleSided.Checked =
            checkPrintDoubleSided.Enabled =
            checkPrintDoubleSided.Visible = false;

            tbComments.Enabled = tbComments.Visible =
            checkExportAllRecords.Enabled = checkExportAllRecords.Visible =            
            checkActionTaken.Enabled = checkActionTaken.Visible =
            btnSave.Enabled = btnSave.Visible =
            dePointLetterGivenOn.Enabled = dePointLetterGivenOn.Visible =
            btnGeneratePointLetter.Enabled = btnGeneratePointLetter.Visible =
            labelActionLabel.Visible = labelActionLabel.Visible =
            labelComments.Visible = labelActionLabel.Visible =
            labelPointLetterGivenOn.Visible = labelActionLabel.Visible = pFlag;

            if (!pFlag)
            {
                labelPoints.Text = "Select an employee to get started";
                labelDate.Text = string.Empty;
                labelAction.Text = string.Empty;
                checkActionTaken.CheckState = CheckState.Unchecked;
                dePointLetterGivenOn.Text = string.Empty;
                cbEmployeeList.Text = string.Empty;
            }
        }

        private bool BindEmployeeList(bool pYell)
        {
            try
            {
                object selectedItem = cbEmployeeList.SelectedItem;
                cbEmployeeList.Properties.Items.Clear();

                bool condition;
                if (atrack.MYEMPLOYEES_IDS.Count <= 0) condition = atrack.dataset.ActionsTakens.Any(z => z.ActionTaken == false && z.IsLocked == false);
                else condition = atrack.dataset.ActionsTakens.Where(z => atrack.MYEMPLOYEES_IDS.Contains(z.EmployeeID)).Any(z => z.ActionTaken == false && z.IsLocked == false);
                if (condition == false)
                {
                    atrack.tabFormControl1.Pages.Remove(atrack.tabFormControl1.Pages.Where(z => z.Name == "Give Point Letter").FirstOrDefault());
                    atrack.ReclaimButton("Give Point Letter");
                    atrack.CloseSplash();
                    if (pYell) YoureDone();
                    return true;
                }
                
                List<string> employeelist = atrack.dataset.ActionsTakens.Where(z => z.ActionTaken == false &&
                                                                                    z.IsLocked == false)
                                                                        .Select(z => z.EmployeeName + " (#" + z.EmployeeID + ")")
                                                                        .Distinct()
                                                                        .OrderBy(z => z.ToString())
                                                                        .ToList();
                cbEmployeeList.Properties.Items.AddRange(employeelist);
                if (pYell && 
                    selectedItem != null &&
                    selectedItem.ToString() != string.Empty &&
                    cbEmployeeList.Properties.Items.IndexOf(selectedItem) < 0)
                {
                    MessageBox.Show("This point letter no longer exists. This could mean that someone else has already distributed it or that the data you are looking at is outdated.\n\nPlease close and re-open this page (to obtain the newest data) and try again.", "Data Update Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                return false;
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
                
                return false;
            }
        }

        private void YoureDone() { MessageBox.Show("You have no pending point letters!", "Nothing To Do", MessageBoxButtons.OK, MessageBoxIcon.Information); }

        private void CbEmployeeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string id = atrack.GetEmployeeIDFromSelection(cbEmployeeList.SelectedItem.ToString());

                if (id != null & id != string.Empty &&
                    atrack.dataset.ActionsTakens.Any(z => z.ActionTaken == false &&
                                                          z.IsLocked == false &&
                                                          z.EmployeeID == id) == true)
                {
                    if (atrack.dataset.ActionsTakens.Where(z => z.ActionTaken == false &&
                                                                z.IsLocked == false &&
                                                                z.EmployeeID == id).ToList().Count >= 2)
                    {
                        MessageBox.Show(cbEmployeeList.SelectedItem.ToString() + " has more than one point letter that must be distributed.\n\n" +
                                        "You will see these point letters appear in the same order that they should be handed out.\n\n" + 
                                        "Once you are finished and submit a letter the next one will become available automatically.",
                                        "Multiple Point Letters Found",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                    }
                    SELECTEDACTION = atrack.dataset.ActionsTakens.Where(z => z.ActionTaken == false &&
                                                                             z.IsLocked == false &&
                                                                             z.EmployeeID == id).OrderBy(z => z.PointValue).FirstOrDefault();

                    labelPoints.Text = SELECTEDACTION.EmployeeName + " (#" + SELECTEDACTION.EmployeeID + ") has " + SELECTEDACTION.PointValue + " points";

                    labelDate.Text = "This point level was reached on " + SELECTEDACTION.DateOfOccurrence.ToShortDateString();

                    checkActionTaken.Checked = false;

                    labelAction.Text = SELECTEDACTION.ActionToTake;
                    /// only consider up to a breakpoint
                    if (labelAction.Text.Contains("<br>")) labelAction.Text = labelAction.Text.Substring(0, labelAction.Text.IndexOf("<br>"));

                    dePointLetterGivenOn.Properties.MinValue = SELECTEDACTION.DateOfOccurrence;
                    dePointLetterGivenOn.Properties.MaxValue = DateTime.Today;
                    dePointLetterGivenOn.DateTime = DateTime.Today;

                    EnableControls(true);
                }
                else EnableControls(false);
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }            
        }

        private void ButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            EnableControls(false);
        }

        private void BtnSave_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                if (SELECTEDACTION == null || cbEmployeeList.SelectedItem.ToString() == string.Empty) return;
                //else if (tbComments.Text == string.Empty)
                //{
                //    MessageBox.Show("Please enter some comments, including actions that you took, before saving.", "Enter Some Comments", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    return;
                //}
                else if(dePointLetterGivenOn.Text == null || dePointLetterGivenOn.Text == string.Empty)
                {
                    MessageBox.Show("Please enter the date that you gave out this point letter.", "Enter Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (checkActionTaken.CheckState != CheckState.Checked)
                {
                    MessageBox.Show("Please verify that the information on this page is accurate.", "Verify Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (MessageBox.Show("Submit data?" + 
                                    "\n\n* Note that this will also update the attendance records for this employee, which may take a minute. Please be patient while it processes.", "Confirm Submit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //atrack.ShowSplash("Saving data");
                    atrack.ShowSplash("Please wait while your request is processed");

                    /// check that this object still exists; these records are rebuilt during each update
                    /// so if a user had this page open for long enough this record may be gone
                    if (atrack.dataset.ActionsTakens.Any(z => z.ID == SELECTEDACTION.ID) == false)
                    {
                        try
                        {
                            /// try to find the new entry which matches this selected action
                            SELECTEDACTION = atrack.dataset.ActionsTakens.Where(z => z.ActionTaken == false &&
                                                                                     z.IsLocked == false &&
                                                                                     z.EmployeeID == SELECTEDACTION.EmployeeID &&
                                                                                     z.PointValue == SELECTEDACTION.PointValue &&
                                                                                     z.DateOfOccurrence == SELECTEDACTION.DateOfOccurrence).First();
                        }
                        catch
                        {
                            /// could not find new record; notify user to close and re-open this page
                            MessageBox.Show("This point letter no longer exists. This could mean that someone else has already distributed it or that the data you are looking at is outdated.\n\nPlease close and re-open this page (to obtain the newest data) and try again.", "Data Update Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            BindEmployeeList(false);
                            return;
                        }
                    }

                    /// write multiline text field as lines to preserve linebreaks
                    string lineComment = string.Empty;
                    foreach (string line in tbComments.Lines) { lineComment += line + "<br>"; }

                    SELECTEDACTION.ActionTaken = true;
                    SELECTEDACTION.PointLetterDistributedOn = dePointLetterGivenOn.DateTime;
                    SELECTEDACTION.Comments = lineComment;
                    SELECTEDACTION.ActionTakenBy = atrack.USERNAME;
                    SELECTEDACTION.ActionTakenOn = DateTime.Now;
                    atrack.dataset.SaveChanges();

                    /// .LastPointLevelActedOn
                    /// super very ultra crucial; re-set this employee's lowest-verified point level/value
                    /// to be the point value which is being acted on right now
                    /// this value can be increased here
                    /// this value can be increased or decreased from within PageTemplate
                    CurrentEmployee lastpointlevelactedon = atrack.dataset.CurrentEmployees.Where(z => z.EmployeeID == SELECTEDACTION.EmployeeID).FirstOrDefault();
                    lastpointlevelactedon.LastPointLevelActedOn = SELECTEDACTION.PointValue;
                    atrack.dataset.SaveChanges();

                    string selection = this.cbEmployeeList.Text;
                    EnableControls(false);

                    /// compare apples to apples
                    PageTemplate adminTasks = (PageTemplate)atrack.GetTabFromMain("AdminTasks");
                    int index = adminTasks.cbEmployeeList.Properties.Items.IndexOf(selection);
                    atrack.RUNNINGADMINTASK = true;
                    adminTasks.PopulateHighPointsAndActionsToTakeTables(index);                    

                    bool userisdone = BindEmployeeList(false);
                    SELECTEDACTION = null;
                    cbEmployeeList.Text = string.Empty;

                    /// update app data since a user action was taken
                    /// this probably only has to update the data within ActionsTaken but eh
                    atrack.UpdateAllPagedAppData();
                    atrack.CloseSplash();

                    atrack.RUNNINGADMINTASK = false;

                    MessageBox.Show("That point letter has been removed!", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    /// this is pushed off to now in order for ^^^ that messagebox to appear first
                    if (userisdone) YoureDone();
                }
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                atrack.RUNNINGADMINTASK = false;
            }
        }

        private void btnGeneratePointLetter_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            GoGenerateLetter_Click(SELECTEDACTION.EmployeeID, SELECTEDACTION.EmployeeName, SELECTEDACTION.Department, SELECTEDACTION.DateOfOccurrence, SELECTEDACTION.PointValue, checkExportAllRecords.Checked, false);
        }
        public void GoGenerateLetter_Click(string pEmpID, string pEmpName, string pDept, DateTime pDate, double pPointVal, bool pExportAll, bool pIsAdminCall)
        {
            try
            {
                atrack.ShowSplash("Generating letter");

                byte[] thePointLetter = GeneratePointLetter(pEmpID, pEmpName, pDept, pDate, pPointVal, pExportAll, pIsAdminCall);
                if (thePointLetter != null)
                {
                    string tabname = "Point Letter for " + pEmpName;
                    foreach (TabFormPage page in atrack.tabFormControl1.Pages) { if (page.Name == tabname) { atrack.tabFormControl1.ClosePage(page); break; } }
                    TabFormPage tab = new TabFormPage();
                    tab.Name = tab.Text = tabname;
                    atrack.tabFormControl1.Pages.Add(tab);
                    atrack.tabFormControl1.SelectedPage = tab;

                    PointLetterTemplate template;
                    if (!pIsAdminCall) template = new PointLetterTemplate(atrack, SELECTEDACTION, thePointLetter, checkPrintDoubleSided.Checked);
                    else template = new PointLetterTemplate(atrack, null, thePointLetter, checkPrintDoubleSided.Checked);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                }
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                atrack.CloseSplash();
            }
        }
        public byte[] GeneratePointLetter(string pEmpID, string pEmpName, string pDept, DateTime pDate, double pPointVal, bool pExportAll, bool pIsAdminCall)
        {
            /// obtain the text from the underlying email template in .docx format
            string docText = "";
            try
            {
                /// LIFTED FROM WISEFORMS, THE BEAUTIFUL BEAST THAT IT IS

                /// fill a byte[] with all data from the template
                byte[] byteArray = Properties.Resources.Point_Letter_Template_03_24_2020;
                //byte[] byteArray = Properties.Resources.Point_Letter_Template_07_01_2020;
                ///
                /// this block is very important as it uses OpenXML PowerTools in order to properly convert the contents of the Word document into a string for editing
                /// 
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    /// fill this stream with data from the byte[]
                    memoryStream.Write(byteArray, 0, byteArray.Length);
                    /// open the stream as a .docx file
                    using (WordprocessingDocument doc = WordprocessingDocument.Open(memoryStream, true))
                    {
                        /// convert .docx to html, html to string
                        HtmlConverterSettings hcs = new HtmlConverterSettings();
                        System.Xml.Linq.XElement html = HtmlConverter.ConvertToHtml(doc, hcs);
                        /// from https://msdn.microsoft.com/en-us/library/office/ff628051(v=office.14).aspx regarding .ToStringNewLineOnAttributes()
                        /// This extension method serializes the XML with each attribute on its own line, which makes the XHTML easier to read when an element contains several attributes.
                        docText = html.ToStringNewLineOnAttributes();
                    }
                }

                string shift = "n/a";
                try { shift = atrack.dataset.CurrentEmployees.Where(z => z.EmployeeID == pEmpID).First().Shift.ToString(); }
                catch { }
                string namelong = pEmpName;
                int commaloc = 0;
                commaloc = namelong.LastIndexOf(", ");
                string nameshort = namelong.Substring(commaloc + (commaloc > 0 ? ", ".Length : 0));
                if (nameshort.Substring(nameshort.Length - 2, 1) == " ") nameshort = nameshort.Substring(0, nameshort.Length - 2);
                string points = pPointVal.ToString();

                docText = docText.Replace("{{NameLong}}", namelong);
                docText = docText.Replace("{{Shift}}", shift);
                docText = docText.Replace("{{Department}}", pDept);
                docText = docText.Replace("{{TodayDate}}", DateTime.Today.ToString("MM/dd/yyyy"));
                docText = docText.Replace("{{NameShort}}", nameshort);
                docText = docText.Replace("{{Points}}", points);
                docText = docText.Replace("{{ViolationDate}}", pDate.ToString("MM/dd/yyyy"));
                docText = docText.Replace("{{h3}}",  (points == "3"   ? "</b><font color = 'red' !important;>" : "</font></font></font>"));
                docText = docText.Replace("{{3}}",   (points == "3"   ? "<b>X"                                 : string.Empty));
                docText = docText.Replace("{{h6}}",  (points == "6"   ? "</b><font color = 'red' !important;>" : "</font></font></font>"));
                docText = docText.Replace("{{6}}",   (points == "6"   ? "<b>X"                                 : string.Empty));
                docText = docText.Replace("{{h9}}",  (points == "9"   ? "</b><font color = 'red' !important;>" : "</font></font></font>"));
                docText = docText.Replace("{{9}}",   (points == "9"   ? "<b>X"                                 : string.Empty));
                docText = docText.Replace("{{h11}}", (points == "11"  ? "</b><font color = 'red' !important;>" : "</font></font></font>"));
                docText = docText.Replace("{{11}}",  (points == "11"  ? "<b>X"                                 : string.Empty));
                docText = docText.Replace("{{h12}}", (points == "12"  ? "</b><font color = 'red' !important;>" : "</font></font></font>"));
                docText = docText.Replace("{{12}}",  (points == "12"  ? "<b>X"                                 : string.Empty));
                /// add in one last trio of </font>s in case this is for a 12 point letter
                /// ...this is in the first line of the FMLA/ADA disclaimer thing at the bottom.
                docText = docText.Replace("All Wise Foods, Inc. employees", "</font></font></font>All Wise Foods, Inc. employees");
                
                double pointtotal = 0;
                /// if the underlying template is ever updated then these will have to be updated as well
                /// use Notepad++ to read docText and determine the correct classes, format text with "XML Tools" to be less annoying (and do whatever ctrl + shift + alt + b does)
                string newrow = "<tr>" +
                                "<td class=\"pt-000020\"><p dir=\"ltr\" class=\"pt-Normal-000026\">{{EntryDate}}</p></td>" +
                                "<td class=\"pt-000022\"><p dir=\"ltr\" class=\"pt-Normal-000026\"><span class=\"pt-DefaultParagraphFont-000019\">{{EntryReason}}</span></p></td>" +
                                "<td class=\"pt-000023\"><p dir=\"ltr\" class=\"pt-Normal-000027\"><span class=\"pt-DefaultParagraphFont-000019\">{{EntryPoints}}</span></p></td>" +
                                "<td class=\"pt-000025\"><p dir=\"ltr\" class=\"pt-Normal-000027\"><span class=\"pt-DefaultParagraphFont-000019\">{{EntryCumulativePoints}}</span></p></td>" +
                                "</tr>";

                /// use HighPoints table (from server) to populate point letter
                /// this includes a special rule to also output delivered point letters
                List<HighPoint> lHighPoints = atrack.dataset.HighPoints.Where(z => z.Pay_Date >= atrack.Date_StartOfRollingYear &&
                                                                                   z.EmployeeID == pEmpID &&
                                                                                   !z.Pay_Code.StartsWith("ADDED: ") &&
                                                                                   !z.Pay_Code.StartsWith("ADJUSTED: ") &&
                                                                                   !z.Pay_Code.StartsWith("REMOVED: "))
                                                                       .OrderBy(z => z.index)
                                                                       .ToList();
                /// use AnalyzePoints() method to populate point letter
                //List<HighPoint> lHighPoints = new List<HighPoint>();
                //TabFormPage page = atrack.tabFormControl1.Pages.Where(z => z.Name == "AdminTasks").First();
                //PageTemplate adminTasks = atrack.GetAdminTasksTab();
                //lHighPoints = adminTasks.GetPointEntriesForEmployeeFromAdminTasksPage(cbEmployeeList.SelectedItem.ToString());
                ///// fall back to old method if there is no data or the admin tab wasn't found
                //if ((adminTasks == null || lHighPoints == null) && atrack.dataset.HighPoints.Any(z => z.EmployeeID == pEmpID))
                //{
                //    lHighPoints = atrack.dataset.HighPoints.Where(z => z.EmployeeID == pEmpID).ToList();
                //}
                ///// remove unnecessary entries and order the list
                //lHighPoints = lHighPoints.Where(z => z.Points != 0).OrderBy(z => z.index).ToList();
                /// default to empty list if there were problems
                if (lHighPoints == null) lHighPoints = new List<HighPoint>();

                /// only print out the points leading up to this point letter's total
                if (!pExportAll)
                {
                    DateTime lastPayDate = new DateTime();
                    foreach (HighPoint entry in lHighPoints)
                    {
                        pointtotal += entry.Points;
                        if (pointtotal < 0) pointtotal = 0;

                        docText = docText.Replace("{{EntryDate}}", entry.Pay_Date.ToString("MM/dd/yyyy"));
                        docText = docText.Replace("{{EntryReason}}", entry.Description);
                        docText = docText.Replace("{{EntryPoints}}", entry.Points.ToString());

                        /// determines how much data goes into the point letter
                        if (pointtotal < pPointVal || entry.Pay_Date.Date != pDate.Date)
                        {
                            docText = docText.Replace("{{EntryCumulativePoints}}", pointtotal.ToString() + newrow);
                        }
                        else
                        {
                            docText = docText.Replace("{{EntryCumulativePoints}}", pointtotal.ToString() + "</table>");
                            lastPayDate = entry.Pay_Date;
                            break;
                        }
                    }

                    /// information is not complete!
                    if (pointtotal < pPointVal || lastPayDate.Date != pDate.Date)
                    {
                        atrack.CloseSplash();
                        MessageBox.Show("The information for this employee does not appear to be complete.\n\nThis can happen when Ceridian data has been added but the Attendance Tracker has not yet been updated.\n\nPlease check back and try again in a while, and if this problem persists use the Send Feedback button at the top-right of your screen to notify the administrator.", "Data Is Incomplete", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return null;
                    }
                }
                /// print out all records which exist today
                else
                {
                    foreach (HighPoint entry in lHighPoints)
                    {
                        pointtotal += entry.Points;
                        if (pointtotal < 0) pointtotal = 0;

                        docText = docText.Replace("{{EntryDate}}", entry.Pay_Date.ToString("MM/dd/yyyy"));
                        docText = docText.Replace("{{EntryReason}}", entry.Description);
                        docText = docText.Replace("{{EntryPoints}}", entry.Points.ToString());

                        /// determines how much data goes into the point letter
                        if (lHighPoints.IndexOf(entry) < lHighPoints.Count - 1)
                        {
                            docText = docText.Replace("{{EntryCumulativePoints}}", pointtotal.ToString() + newrow);
                        }
                        else
                        {
                            docText = docText.Replace("{{EntryCumulativePoints}}", pointtotal.ToString() + "</table>");
                            break;
                        }
                    }
                }

                /// after replacing all fields...
                /// proceed to find and replace all instances of newlines with actual carriage returns
                /// this HTML <-> XML conversion makes all double newlines (<br><br>) into a single newline
                /// this is probably because whitespace is not being preserved, even though in the raw XML those properties are, indeed, set
                /// to get around this, the portions of the form which SHOULD be new lines are denoted by:
                /// SOME_CLASS_NAME\"> </span>
                /// the space there is where the newline *should* be
                /// this isn't perfect...but will probably work exactly as it should in 99% of instances
                docText = docText.Replace("\"> </span>", "\"><br /></span>");
                /// any of these will work...
                /// HTML newline: <br />
                /// Word newline: \r\n
                /// XML newline: &#13;&#10;
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }

            return Encoding.ASCII.GetBytes(docText);
        }

        private void cbEmployeeList_BeforePopup(object sender, EventArgs e)
        {
            BindEmployeeList(false);
        }
    }
}