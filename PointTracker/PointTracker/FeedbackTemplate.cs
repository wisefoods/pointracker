﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using System.IO;
using System.Net.Mail;
using DevExpress.XtraBars;

namespace AttendanceTracker
{
    public partial class FeedbackTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;

        public FeedbackTemplate(Form1 pForm)
        {
            try
            {
                InitializeComponent();

                atrack = pForm;
                RefreshTabList();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }
        private void cbTabNameForAttachment_MouseEnter(object sender, EventArgs e)
        {
            RefreshTabList();
        }
        private void RefreshTabList()
        {
            cbTabNameForAttachment.Properties.Items.Clear();
            cbTabNameForAttachment.Properties.Items.AddRange(atrack.PopulateTabNameList());
        }

        private void BtnSubmitFeedback_Click(object sender, EventArgs e)
        {
            try
            {
                if (rtbFeedback.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("Type in some feedback before clicking send.", "No Feedback", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else if (MessageBox.Show("Submit the feedback you've provided?", "Submit Feedback", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;

                atrack.ShowSplash("Sending feedback");

                FeedbackLog feedback = new FeedbackLog();
                feedback.ID = Guid.NewGuid();
                feedback.Feedback = rtbFeedback.Text;
                feedback.Score = (double)ratingByUser.Rating;
                feedback.SubmittedBy = atrack.USERNAME;
                feedback.SubmittedOn = DateTime.Now;
                atrack.dataset.FeedbackLogs.Add(feedback);
                atrack.dataset.SaveChanges();

                using (MemoryStream stream = new MemoryStream())
                {
                    Attachment attachment = atrack.GetContentsOfTabAndAttach(cbTabNameForAttachment.Text, stream, false);
                    atrack.SendEmail(new string[] { Properties.Settings.Default.AdminEmail },
                                     new string[] { atrack.EMAILADDRESS },
                                     null,
                                     "New Feedback Submitted by " + atrack.FULLNAME,
                                     String.Format("<b>Full Name</b>: {0}<br><b>Username</b>: {1}<br><b>Email Address</b>: {2}<br><b>Date</b>: {3}<br><b>Feedback Rating</b>: {4}<br><b>Feedback</b>: {5}", atrack.FULLNAME, atrack.USERNAME, atrack.EMAILADDRESS, feedback.SubmittedOn.ToString(), ratingByUser.Rating.ToString(), rtbFeedback.Text),
                                     attachment != null ? new List<System.Net.Mail.Attachment> { attachment } : null,
                                     true);
                }

                atrack.tabFormControl1.Pages.Remove(atrack.tabFormControl1.SelectedPage);
                atrack.ReclaimButton("Send App Feedback");
                atrack.CloseSplash();
                MessageBox.Show("Feedback has been sent!", "Thanks!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                atrack.CloseSplash();
            }
        }
    }
}
