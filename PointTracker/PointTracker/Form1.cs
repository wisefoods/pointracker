﻿using DevExpress.XtraBars;
using DevExpress.XtraSplashScreen;
using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices.AccountManagement;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Timers;
using System.Windows.Forms;

namespace AttendanceTracker
{
    public partial class Form1 : DevExpress.XtraBars.TabForm
    {
        public ATrackEntitiesConn dataset;
        public IQueryable<DataSource> datasource_datelimited;

        public DateTime Date_EarliestPossible = new DateTime(1, 1, 1);
        public DateTime Date_StartOfDataCollectionInDB;
        public DateTime Date_StartOfRollingYear;
        public DateTime Date_LatestPossible;

        public string USERNAME = string.Empty;
        public string EMAILADDRESS = string.Empty;
        public string FULLNAME = string.Empty;
        public List<string> ALLDEPARTMENTS = new List<string>();
        public List<string> MYEMPLOYEES_DEPARTMENTS = new List<string>();
        public List<int> MYEMPLOYEES_SHIFTS = new List<int>();
        public List<string> MYEMPLOYEES_IDS = new List<string>();
        public List<string> MYEMPLOYEES_NICELIST = new List<string>();
        public DateTime LASTCERIDIANUPDATE;
        public DateTime LASTTASKSUPDATE;
        public string CONSOLEMESSAGE = string.Empty;
        public bool RUNNINGAUTOMATED = false;
        public bool RUNNINGADMINTASK = false;
        public bool UPDATEACTIVE = false;
        private bool TABBEINGADDEDISADMINTASKSTAB;
        private bool ISTHEFIRSTLAUNCH = true;
        /// this is set in the Admin tab and makes the Reporting tab open as a regular, non-admin user
        public bool ADMINOPTION_OVERRIDEREPORTINGTAB = false;
        /// this is not set anywhere, just manually, and will force the tracker to count now-obsolete points on a rolling year schedule
        /// can be used in conjunction with Date_LatestPossible and other Date_ fields
        public bool ADMINOPTION_DONOTIGNOREOBSOLETEPOINTS = false;

        public string EMAIL_FOOTER = "<br><br><i>*** For help with creating email rules <a href='https://support.office.com/en-us/article/manage-email-messages-by-using-rules-c24f5dea-9465-4df4-ad17-a50704d66c59'>click here</a><br>*** Please do not reply to this email as this inbox is not monitored. Email " + Properties.Settings.Default.AdminEmail + " for further assistance.</i><br><br><br>";

        public bool SPLASH = false;

        /// cache data for faster loading
        public List<PayCode> PAYCODES_LIST = new List<PayCode>();
        public List<Merit> MERITS_LIST = new List<Merit>();
        public List<Demerit> DEMERITS_LIST = new List<Demerit>();
        public List<Reprimand> REPRIMANDS_LIST = new List<Reprimand>();
        public List<CurrentEmployee> EMPLOYEES_LIST = new List<CurrentEmployee>();

        /// cache data for faster processing
        public List<UserOption> USEROPTIONS_LIST = new List<UserOption>(); 
        public List<ActionsTaken> ACTIONSTAKEN_LIST = new List<ActionsTaken>();
        public List<FMLANotification> FMLA_LIST = new List<FMLANotification>();
        //public List<PADNotifications> PAD_LIST = new List<PADNotifications>();
        public List<ManualAdjustment> ADJUSTMENTS_LIST = new List<ManualAdjustment>();
        public List<TrackedBonus> BONUSES_LIST = new List<TrackedBonus>();
        public List<TrackedPenalty> PENALTIES_LIST = new List<TrackedPenalty>();
        public List<FromCeridianAPI_Headcount> API_HEADCOUNT_LIST = new List<FromCeridianAPI_Headcount>();
        public List<FromCeridianAPI_ShiftRotations> API_SHIFTROTATIONS_LIST = new List<FromCeridianAPI_ShiftRotations>();
        public List<FromCeridianAPI_OrgChart> API_ORGCHART_LIST = new List<FromCeridianAPI_OrgChart>();

        public Form1(string[] pAutomationMessage)
        {
            try
            {
                /// *** admin/debug tasks 1/2
                if (pAutomationMessage != null && pAutomationMessage.Length > 0)
                //if (true)
                /// *** admin/debug tasks 1/2
                {
                    InitializeComponent();
                    RUNNINGAUTOMATED = true;                                            /// automation login
                    this.SuspendLayout();                                               /// suspend UI updates
                    //this.ResumeLayout();                                              /// resume UI updates

                    SetDateVariables();                                                 /// set different date variables used throughout the app
                    BindRequiredDatasets();                                             /// bind the local datasets first
                    BindAutomatedDatasets();                                            /// bind the local datasets first
                    HeyILoggedIn();                                                     /// log in
                    BindAndUpdateMyEmployees();                                         /// bind and manage list of employees

                    /// obtain and parse command
                    /// *** admin/debug tasks 2/2
                    CONSOLEMESSAGE = pAutomationMessage[0];
                    //CONSOLEMESSAGE = "UpdateData";
                    /// *** admin/debug tasks 2/2
                    if (CONSOLEMESSAGE == "UpdateData") tabFormControl1.AddNewPage();
                    else if (CONSOLEMESSAGE == "SendEmails") tabFormControl1.AddNewPage();
                    else this.Close();
                }
                else
                {
                    /// splash is opened before InitializeComponent()
                    ShowSplash(string.Empty);
                    InitializeComponent();
                    RUNNINGAUTOMATED = false;                                           /// user login

                    SetDateVariables();                                                 /// set different date variables used throughout the app
                    BindRequiredDatasets();                                             /// bind the local datasets first
                    LASTCERIDIANUPDATE = GetLastCeridianUpdate();                       /// record ceridian update times
                    LASTTASKSUPDATE = GetLastTasksUpdate();                             /// record app automation update times
                    StartNewUpdateTimer();                                              /// start data update timer
                    bool isnewuser = HeyILoggedIn();                                    /// log user in (and check/do admin things)
                    BindAndUpdateMyEmployees();                                         /// bind and manage list of employees
                    TABBEINGADDEDISADMINTASKSTAB = true;
                    tabFormControl1.AddNewPage();                                       /// add Admin Tasks page
                    TABBEINGADDEDISADMINTASKSTAB = false;
                    tabFormControl1.AddNewPage();                                       /// add default page

                    /// this is a new user + they opted to view the options screen
                    if (isnewuser && ShowWelcomeScreen()) AddNewOptionsPage();          /// open options screen                  
                }
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
                this.Close();   /// this is a deal breaker
            }
            finally
            {
                CloseSplash();
            }
        }

        private bool ShowWelcomeScreen()
        {
            try
            {
                CloseSplash();
                if (dataset.UserOptions.Where(z => z.Username == USERNAME).FirstOrDefault().Departments == string.Empty)
                {
                    if (MessageBox.Show("Welcome to the Wise Attendance Tracker!\n\n" +
                                        "It is recommended that you take the time to select the departments and shifts that you are responsible for. This will help you focus on only the employee data that you are interested in!\n\n" +
                                        "Would you like to do this now?\n\n(This can be done at any time by clicking the 'Options' button at the top of the screen.)",
                                        "Welcome!",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question) == DialogResult.Yes) return true;
                    else return false;
                }
                else return false;
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            return false;
        }

        private void SetDateVariables()
        {
            RefreshEntity();

            DataSource earliest = dataset.DataSources.OrderBy(z => z.PayDate).First();
            Date_StartOfDataCollectionInDB = earliest.PayDate;
            Date_StartOfRollingYear = DateTime.Today.AddYears(-1).AddDays(1);
            Date_LatestPossible = DateTime.Today.Date;
            //Date_LatestPossible = new DateTime(2020, 12, 31);
        }
        private void BindRequiredDatasets()
        {
            try
            {
                RefreshEntity();

                PAYCODES_LIST = dataset.PayCodes.ToList();
                MERITS_LIST = dataset.Merits.ToList();
                DEMERITS_LIST = dataset.Demerits.ToList();
                REPRIMANDS_LIST = dataset.Reprimands.ToList(); 
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
                this.Close();   /// this is a deal breaker
            }
        }
        public void RefreshEntity()
        {
            if (dataset != null) { dataset.Dispose(); }
            dataset = null;
            dataset = new ATrackEntitiesConn();
            dataset.Database.CommandTimeout = 43200;  /// 12 hours

            datasource_datelimited = null;
            datasource_datelimited = dataset.DataSources.Where(z => z.PayDate >= Date_StartOfDataCollectionInDB);
        }
        public void BindAutomatedDatasets()
        {
            try
            {
                BindCurrentEmployeesData();
                BindUserOptionsData();
                BindActionsTakenData();
                BindFMLAData();
                BindManualAdjustmentsData();
                BindBonusesData();
                BindPenaltiesData();
                //BindHeadcountData();              /// moved to UpdateCurrentEmployeesTable()
                //BindShiftRotationsData();         /// moved to UpdateCurrentEmployeesTable()
                //BindOrgChartData();               /// moved to UpdateCurrentEmployeesTable()
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }            
        }
        public void BindPointLetterDatasets()
        {
            try
            {
                BindCurrentEmployeesData();
                BindUserOptionsData();
                BindFMLAData();
                /// also requires BindActionsTakenData(), BindManualAdjustmentsData(), BindBonusesData(), and BindPenaltiesData()
                /// but these are called as needed, and per employee, within PopulateHighPointsAndActionsToTakeTables()
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
        }
        public void BindActionsTakenData() { ACTIONSTAKEN_LIST.Clear(); ACTIONSTAKEN_LIST = dataset.ActionsTakens.ToList(); }
        public void BindActionsTakenData(string pEmpID) 
        {
            IEnumerable<ActionsTaken> lToRemove = ACTIONSTAKEN_LIST.Where(z => z.EmployeeID == pEmpID).ToList();
            foreach (ActionsTaken act in lToRemove) { ACTIONSTAKEN_LIST.Remove(act); }
            ACTIONSTAKEN_LIST.AddRange(dataset.ActionsTakens.Where(z => z.EmployeeID == pEmpID));
        }
        public void BindFMLAData() { FMLA_LIST.Clear(); FMLA_LIST = dataset.FMLANotifications.ToList(); }
        //public void BindPADData() { PAD_LIST.Clear(); PAD_LIST = dataset.PADNotifications.ToList(); }
        public void BindManualAdjustmentsData() { ADJUSTMENTS_LIST.Clear(); ADJUSTMENTS_LIST = dataset.ManualAdjustments.ToList(); }
        public void BindManualAdjustmentsData(string pEmpID) 
        {
            IEnumerable<ManualAdjustment> lToRemove = ADJUSTMENTS_LIST.Where(z => z.EmployeeID == pEmpID || z.EmployeeID == "ALL").ToList();
            foreach (ManualAdjustment manadj in lToRemove) { ADJUSTMENTS_LIST.Remove(manadj); }
            ADJUSTMENTS_LIST.AddRange(dataset.ManualAdjustments.Where(z => z.EmployeeID == pEmpID || z.EmployeeID == "ALL"));
        }
        public void BindBonusesData() { BONUSES_LIST.Clear(); BONUSES_LIST = dataset.TrackedBonuses.ToList(); }
        public void BindBonusesData(string pEmpID) 
        {
            IEnumerable<TrackedBonus> lToRemove = BONUSES_LIST.Where(z => z.EmployeeID == pEmpID || z.EmployeeID == "ALL").ToList();
            foreach (TrackedBonus bonus in lToRemove) { BONUSES_LIST.Remove(bonus); }
            BONUSES_LIST.AddRange(dataset.TrackedBonuses.Where(z => z.EmployeeID == pEmpID || z.EmployeeID == "ALL"));
        }
        public void BindPenaltiesData() { PENALTIES_LIST.Clear(); PENALTIES_LIST = dataset.TrackedPenalties.ToList(); }
        public void BindPenaltiesData(string pEmpID)
        {
            IEnumerable<TrackedPenalty> lToRemove = PENALTIES_LIST.Where(z => z.EmployeeID == pEmpID).ToList();
            foreach (TrackedPenalty penalty in lToRemove) { PENALTIES_LIST.Remove(penalty); }
            PENALTIES_LIST.AddRange(dataset.TrackedPenalties.Where(z => z.EmployeeID == pEmpID));
        }
        public void BindCurrentEmployeesData() { EMPLOYEES_LIST.Clear(); EMPLOYEES_LIST = dataset.CurrentEmployees.ToList(); }
        public void BindUserOptionsData() { USEROPTIONS_LIST.Clear(); USEROPTIONS_LIST = dataset.UserOptions.ToList(); }
        public void BindHeadcountData() { API_HEADCOUNT_LIST.Clear(); API_HEADCOUNT_LIST = dataset.FromCeridianAPI_Headcount.ToList(); }
        public void BindShiftRotationsData() { API_SHIFTROTATIONS_LIST.Clear(); API_SHIFTROTATIONS_LIST = dataset.FromCeridianAPI_ShiftRotations.ToList(); }
        public void BindOrgChartData() { API_ORGCHART_LIST.Clear(); API_ORGCHART_LIST = dataset.FromCeridianAPI_OrgChart.ToList(); }
        
        public void BindAndUpdateMyEmployees()
        {
            try
            {
                List<string> listOfIDs = dataset.CurrentEmployees.Select(z => z.EmployeeID).Distinct().ToList();
                ALLDEPARTMENTS = dataset.DataSources.Where(z => listOfIDs.Contains(z.EmployeeEmploymentStatus_EmployeeNumber))
                                                                         .Select(z => z.OrgUnit_ShortName)
                                                                         .Distinct()
                                                                         .OrderBy(z => z.ToString())
                                                                         .ToList();
                MYEMPLOYEES_DEPARTMENTS.Clear();
                MYEMPLOYEES_SHIFTS.Clear();
                MYEMPLOYEES_IDS.Clear();

                if (!RUNNINGAUTOMATED)
                {
                    /// only permit departments which appear in the ALLDEPARTMENTS list
                    /// update this user's .Employees option to be accurate
                    UserOption userOptions = dataset.UserOptions.Where(z => z.Username == USERNAME).FirstOrDefault();
                    List<string> tempMYEMPLOYEES_DEPARTMENTS = userOptions.Departments.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    foreach (string dept in tempMYEMPLOYEES_DEPARTMENTS) { if (ALLDEPARTMENTS.Contains(dept)) MYEMPLOYEES_DEPARTMENTS.Add(dept); }
                    string newDeptString = string.Empty;
                    foreach (string newDept in MYEMPLOYEES_DEPARTMENTS) { newDeptString += newDept + "|||"; }
                    userOptions.Departments = newDeptString;
                    dataset.SaveChanges();

                    /// obtain array of shifts
                    List<string> lShifts_temp = userOptions.Shift.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    /// shift 0 indicates that a shift was not found; so add it for everyone
                    lShifts_temp.Add("0");
                    foreach (string shift in lShifts_temp) { MYEMPLOYEES_SHIFTS.Add(Convert.ToInt32(shift)); }

                    /// only active employees, and their respective departments, are considered
                    MYEMPLOYEES_IDS = dataset.CurrentEmployees.Where(z => MYEMPLOYEES_DEPARTMENTS.Contains(z.Department) &&
                                                                          MYEMPLOYEES_SHIFTS.Contains(z.Shift))
                                                              .Select(z => z.EmployeeID)
                                                              .Distinct()
                                                              .OrderBy(z => z.ToString())
                                                              .ToList();
                }
                /// IF RUNNINGAUTOMATED THEN ALL SHIFTS AND EMPLOYEES ARE ADDED; DEPARTMENTS DON'T REALLY MATTER
                else
                {
                    /// shift 0 indicates that a shift was not found; so add it for everyone
                    MYEMPLOYEES_SHIFTS.Add(0);
                    MYEMPLOYEES_SHIFTS.Add(1);
                    MYEMPLOYEES_SHIFTS.Add(2);
                    MYEMPLOYEES_SHIFTS.Add(3);

                    /// only active employees, and their respective departments, are considered
                    MYEMPLOYEES_IDS = dataset.CurrentEmployees.Select(z => z.EmployeeID)
                                                              .Distinct()
                                                              .OrderBy(z => z.ToString())
                                                              .ToList();
                }
                BindMyEmployees_NiceList();
            }
            catch { }
        }
        public void BindMyEmployees_NiceList()
        {
            try
            {
                if (!RUNNINGAUTOMATED)
                {
                    /// limit by list of departments
                    MYEMPLOYEES_NICELIST = dataset.CurrentEmployees
                                                   .Where(z => MYEMPLOYEES_IDS.Contains(z.EmployeeID) &&
                                                               MYEMPLOYEES_SHIFTS.Contains(z.Shift))
                                                   .Select(z => z.EmployeeName + " (#" + z.EmployeeID + ")")
                                                   .Distinct()
                                                   .OrderBy(z => z.ToString())
                                                   .ToList();
                }
                /// IF RUNNINGAUTOMATED THEN ALL SHIFTS AND EMPLOYEES ARE ADDED; DEPARTMENTS DON'T REALLY MATTER
                else
                {
                    /// select all
                    MYEMPLOYEES_NICELIST = dataset.CurrentEmployees
                                                  .Select(z => z.EmployeeName + " (#" + z.EmployeeID + ")")
                                                  .Distinct()
                                                  .OrderBy(z => z.ToString())
                                                  .ToList();
                }
                MYEMPLOYEES_NICELIST.Sort();
            }
            catch { }
        }
        public string GetEmployeeIDFromSelection(string pSelection)
        {
            if (pSelection == null || pSelection == string.Empty) return string.Empty;
            try
            {
                string str = string.Empty;
                str = pSelection.Substring(pSelection.LastIndexOf(" (#") + " (#".Length);
                str = str.Remove(str.Length - 1);
                return str;
            }
            catch { return string.Empty; }
        }

        private DateTime GetLastCeridianUpdate()
        {
            try { return dataset.UpdateLogs.Where(z => z.Type == "CeridianMerge").OrderByDescending(z => z.ServerUpdateDate).FirstOrDefault().ServerUpdateDate; }
            catch { return new DateTime(); }
        }
        private DateTime GetLastTasksUpdate()
        {
            try { return dataset.UpdateLogs.Where(z => z.Type == "Tasks").OrderByDescending(z => z.ServerUpdateDate).FirstOrDefault().ServerUpdateDate; }
            catch { return new DateTime(); }
        }

        private bool HeyILoggedIn()
        {
            bool returnVal = false;

            /// app run normally
            if (!RUNNINGAUTOMATED)
            {
                /// get user info from UserPrincipal
                USERNAME = UserPrincipal.Current.SamAccountName;
                if (USERNAME != null && USERNAME != string.Empty) USERNAME = USERNAME.ToLower();
                else USERNAME = "n/a";
                /// user is admin
                if (CheckIfAdmin())
                {
                    FULLNAME = "Admin";
                    EMAILADDRESS = Properties.Settings.Default.AdminEmail.ToLower();
                    /// show admin menu
                    tabFormControl1.Items["barAdmin"].Visibility = BarItemVisibility.Always;
                    /// show reporting menu
                    tabFormControl1.Items["barReportingMenu"].Visibility = BarItemVisibility.Always;
                }
                /// user is not admin
                else
                {
                    /// get remaining user info from UserPrincipal
                    FULLNAME = UserPrincipal.Current.DisplayName != null ? UserPrincipal.Current.DisplayName : "n/a";
                    EMAILADDRESS = UserPrincipal.Current.EmailAddress != null ? UserPrincipal.Current.EmailAddress : "n/a";
                    if (EMAILADDRESS != null && EMAILADDRESS != string.Empty) EMAILADDRESS = EMAILADDRESS.ToLower();
                    /// hide admin menu
                    tabFormControl1.Items["barAdmin"].Visibility = BarItemVisibility.Never;
                    /// hide reporting menu if user is not a report-email recipient
                    string wordoftheday = "Reporting";
                    if (dataset.EmailRecipients.Any(z => z.Type == wordoftheday && z.EmailRecipient_Email == EMAILADDRESS))
                    {
                        tabFormControl1.Items["barReportingMenu"].Visibility = BarItemVisibility.Always;
                    }
                    else tabFormControl1.Items["barReportingMenu"].Visibility = BarItemVisibility.Never;
                }
            }
            /// app run for automated tasks
            else
            {
                USERNAME = "automation";
                FULLNAME = "Automation";
                EMAILADDRESS = Properties.Settings.Default.AdminEmail.ToLower();
                /// hide admin menu (but why?)
                tabFormControl1.Items["barAdmin"].Visibility = BarItemVisibility.Never;
                /// show reporting menu
                tabFormControl1.Items["barReportingMenu"].Visibility = BarItemVisibility.Always;
            }

            /// create new user if one does not exist
            if (dataset.UserOptions.Any(z => z.Username.ToLower() == USERNAME) == false)
            {
                UserOption option = new UserOption();
                option.ID = Guid.NewGuid();
                option.Username = USERNAME;
                option.EmailAddress = EMAILADDRESS;
                option.FullName = FULLNAME;
                option.Shift = "0";
                option.Departments = string.Empty;
                option.LastLogin = DateTime.Now;
                option.Active = true;
                dataset.UserOptions.Add(option);
                dataset.SaveChanges();

                returnVal = true;
            }
            else returnVal = false;

            /// log a login; exclude admin accesseses
            /// a valid entry in .UserOptions is required before calling LogThisLogin()
            if (FULLNAME != "Admin") LogThisLogin();

            return returnVal;
        }
        private void LogThisLogin()
        {
            try
            {
                DateTime now = DateTime.Now;

                Login login = new Login();
                login.Username = USERNAME;
                login.Date = now;
                dataset.Logins.Add(login);

                UserOption option = dataset.UserOptions.Where(z => z.Username == USERNAME).First();
                option.Active = true;
                option.LastLogin = now;

                dataset.SaveChanges();
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
        }

        public bool CheckIfAdmin()
        {
            try
            {
                int admin = 0;
                try
                {
                    if (dataset.Admins.Any(z => z.Username.ToLower() == USERNAME))
                    {
                        admin = dataset.Admins.Where(z => z.Username.ToLower() == USERNAME).FirstOrDefault().Level;
                    }
                }
                catch { }

                if (admin < 100) return false;
                else return true;
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
                return false;
            }
        }

        public void StartNewUpdateTimer()
        {
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(CheckForUpdates_Timer);
            aTimer.Interval = 1000 * 60 * 30;   /// 30 minutes
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }
        private void CheckForUpdates_Timer(object source, ElapsedEventArgs e)
        {
            try
            {
                UPDATEACTIVE = true;

                /// set these regardless of other conditions
                SetDateVariables();

                using (ATrackEntitiesConn newceridian = new ATrackEntitiesConn())
                {
                    DateTime mostrecent_ceridian = GetLastCeridianUpdate();
                    DateTime mostrecent_tasks = GetLastTasksUpdate();
                    if (mostrecent_ceridian > LASTCERIDIANUPDATE || mostrecent_tasks > LASTTASKSUPDATE)
                    {
                        LASTCERIDIANUPDATE = mostrecent_ceridian;
                        LASTTASKSUPDATE = mostrecent_tasks;
                        UpdateTheData();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Please close the page you are on and try again.", "Data Update Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                UPDATEACTIVE = false;
            }
        }
        public void UpdateTheData()
        {           
            SetDateVariables();
            BindRequiredDatasets();
            BindAndUpdateMyEmployees();
            UpdateAllPagedAppData();
        }
        public void UpdateTheData_fromBeyond()
        {
            LASTCERIDIANUPDATE = GetLastCeridianUpdate();
            LASTTASKSUPDATE = GetLastTasksUpdate();
            UpdateTheData();
        }

        public void UpdateAllPagedAppData()
        {
            foreach (TabFormPage page in tabFormControl1.Pages)
            {
                foreach (Control ctrl in page.ContentContainer.Controls)
                {
                    Invoke((MethodInvoker)delegate
                    {
                        if (ctrl.GetType() == typeof(PageTemplate)) (ctrl as PageTemplate).BindPageData();
                        else if (ctrl.GetType() == typeof(TakeActionTemplate)) (ctrl as TakeActionTemplate).BindPageData();
                        else if (ctrl.GetType() == typeof(LapsedActionsTemplate)) (ctrl as LapsedActionsTemplate).BindPageData();
                        else if (ctrl.GetType() == typeof(BonusAgreementTemplate)) (ctrl as BonusAgreementTemplate).BindPageData();
                        /// vvv this probably doesn't have to be called but eh
                        else if (ctrl.GetType() == typeof(ActionsTakenTemplate)) (ctrl as ActionsTakenTemplate).BindPageData();
                    });
                }
            }
        }

        private void TabFormControl1_PageCreated(object sender, PageCreatedEventArgs e)
        {
            if (!TABBEINGADDEDISADMINTASKSTAB)
            {
                e.Page.Text = "Make a Selection";
                e.Page.Name = "Employee Data";
                ShowSplash("Adding page");
                tabFormControl1.SelectedPage = e.Page;
                AddNewTemplatePage(e.Page);
                ISTHEFIRSTLAUNCH = false;
                CloseSplash();
            }
            else
            {
                AddNewTemplatePage(e.Page);
                e.Page.Text = "AdminTasks";
                e.Page.Name = "AdminTasks";
                e.Page.Visible = false;
            }
        }
        private void AddNewTemplatePage(TabFormPage pTab)
        {
            try
            {
                PageTemplate template = new PageTemplate(this, pTab);
                if (!RUNNINGAUTOMATED)
                {
                    System.Timers.Timer timer30 = new System.Timers.Timer();
                    Get30SecondTimer(ref timer30);
                    while (timer30.Enabled)
                    {
                        template.TopLevel = false;
                        pTab.ContentContainer.Controls.Add(template);
                        template.Dock = DockStyle.Fill;
                        template.Show();
                        timer30.Stop();
                        timer30.Enabled = false;
                    }                    
                }
                else this.Close();
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
        }
        private void btnViewAgreement_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                ShowSplash("Recalling agreement");
                e.Item.Visibility = BarItemVisibility.Never;

                foreach (TabFormPage page in tabFormControl1.Pages)
                {
                    if (page.Name == "View Union Agreements")
                    {
                        tabFormControl1.SelectedPage = page;
                        return;
                    }
                }
                TabFormPage tab = new TabFormPage();
                tab.Name = tab.Text = "View Union Agreements";
                tabFormControl1.Pages.Add(tab);
                tabFormControl1.SelectedPage = tab;

                System.Timers.Timer timer30 = new System.Timers.Timer();
                Get30SecondTimer(ref timer30);
                while (timer30.Enabled)
                {
                    AgreementTemplate template = new AgreementTemplate(this);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                    timer30.Stop();
                    timer30.Enabled = false;
                }                
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                CloseSplash();
            }
        }
        private void BtnShowPointSystem_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowPointSystem(e.Item);
        }
        public void ShowPointSystem(BarItem pBarItem)
        {
            try
            {
                ShowSplash("Displaying point system");
                pBarItem.Visibility = BarItemVisibility.Never;

                foreach (TabFormPage page in tabFormControl1.Pages)
                {
                    if (page.Name == "View Penalties and Bonuses")
                    {
                        tabFormControl1.SelectedPage = page;
                        return;
                    }
                }
                TabFormPage tab = new TabFormPage();
                tab.Name = tab.Text = "View Penalties and Bonuses";
                tabFormControl1.Pages.Add(tab);
                tabFormControl1.SelectedPage = tab;

                System.Timers.Timer timer30 = new System.Timers.Timer();
                Get30SecondTimer(ref timer30);
                while (timer30.Enabled)
                {
                    PointSystemTemplate template = new PointSystemTemplate(this);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                    timer30.Stop();
                    timer30.Enabled = false;
                }                
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                CloseSplash();
            }
        }
        private void barReportingMenu_ItemClick(object sender, ItemClickEventArgs e)
        {
            //e.Item.Visibility = BarItemVisibility.Never;
            AddReportPage();
        }
        public void AddReportPage()
        {
            try
            {
                ShowSplash("Sorting reports");

                tabFormControl1.Items.Where(z => z.Name == "barReportingMenu").First().Visibility = BarItemVisibility.Never;

                foreach (TabFormPage page in tabFormControl1.Pages)
                {
                    if (page.Name == "Reporting")
                    {
                        tabFormControl1.SelectedPage = page;
                        return;
                    }
                }
                TabFormPage tab = new TabFormPage();
                tab.Name = tab.Text = "Reporting";
                tabFormControl1.Pages.Add(tab);
                tabFormControl1.SelectedPage = tab;

                System.Timers.Timer timer30 = new System.Timers.Timer(); 
                Get30SecondTimer(ref timer30);
                while (timer30.Enabled)
                {
                    ReportingTemplate template = new ReportingTemplate(this);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                    timer30.Stop();
                    timer30.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                CloseSplash();
            }
        }
        private void barAdmin_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                ShowSplash("Divining the admin realm");
                e.Item.Visibility = BarItemVisibility.Never;

                foreach (TabFormPage page in tabFormControl1.Pages)
                {
                    if (page.Name == "Admin")
                    {
                        tabFormControl1.SelectedPage = page;
                        return;
                    }
                }
                TabFormPage tab = new TabFormPage();
                tab.Name = tab.Text = "Admin";
                tabFormControl1.Pages.Add(tab);
                tabFormControl1.SelectedPage = tab;

                System.Timers.Timer timer30 = new System.Timers.Timer();
                Get30SecondTimer(ref timer30);
                while (timer30.Enabled)
                {
                    AdminTemplate template = new AdminTemplate(this);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                    timer30.Stop();
                    timer30.Enabled = false;
                }                
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                CloseSplash();
            }
        }
        private void BtnSendFeedback_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                ShowSplash("Preparing feedback form");
                e.Item.Visibility = BarItemVisibility.Never;

                foreach (TabFormPage page in tabFormControl1.Pages)
                {
                    if (page.Name == "Send App Feedback")
                    {
                        tabFormControl1.SelectedPage = page;
                        return;
                    }
                }
                TabFormPage tab = new TabFormPage();
                tab.Name = tab.Text = "Send App Feedback";
                tabFormControl1.Pages.Add(tab);
                tabFormControl1.SelectedPage = tab;

                System.Timers.Timer timer30 = new System.Timers.Timer();
                Get30SecondTimer(ref timer30);
                while (timer30.Enabled)
                {
                    FeedbackTemplate template = new FeedbackTemplate(this);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                    timer30.Stop();
                    timer30.Enabled = false;
                }                
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                CloseSplash();
            }
        }
        private void btnRequestAChange_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                ShowSplash("Preparing change request form");
                e.Item.Visibility = BarItemVisibility.Never;

                foreach (TabFormPage page in tabFormControl1.Pages)
                {
                    if (page.Name == "Request a Data Change")
                    {
                        tabFormControl1.SelectedPage = page;
                        return;
                    }
                }
                TabFormPage tab = new TabFormPage();
                tab.Name = tab.Text = "Request a Data Change";
                tabFormControl1.Pages.Add(tab);
                tabFormControl1.SelectedPage = tab;

                System.Timers.Timer timer30 = new System.Timers.Timer();
                Get30SecondTimer(ref timer30);
                while (timer30.Enabled)
                {
                    RequestChangeTemplate template = new RequestChangeTemplate(this);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                    timer30.Stop();
                    timer30.Enabled = false;
                }                
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                CloseSplash();
            }
        }
        private void BtnShowReprimands_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowReprimands(e.Item);
        }
        public void ShowReprimands(BarItem pBarItem)
        {
            try
            {
                ShowSplash("Finding point levels");
                pBarItem.Visibility = BarItemVisibility.Never;

                foreach (TabFormPage page in tabFormControl1.Pages)
                {
                    if (page.Name == "View Point Levels")
                    {
                        tabFormControl1.SelectedPage = page;
                        return;
                    }
                }
                TabFormPage tab = new TabFormPage();
                tab.Name = tab.Text = "View Point Levels";
                tabFormControl1.Pages.Add(tab);
                tabFormControl1.SelectedPage = tab;

                System.Timers.Timer timer30 = new System.Timers.Timer();
                Get30SecondTimer(ref timer30);
                while (timer30.Enabled)
                {
                    ReprimandsTemplate template = new ReprimandsTemplate(this);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                    timer30.Stop();
                    timer30.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                CloseSplash();
            }
        }
        private void barShow23For1Agreement_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                ShowSplash("Locating agreement");
                e.Item.Visibility = BarItemVisibility.Never;

                foreach (TabFormPage page in tabFormControl1.Pages)
                {
                    if (page.Name == "2 / 3-for-1 Agreement")
                    {
                        tabFormControl1.SelectedPage = page;
                        return;
                    }
                }
                TabFormPage tab = new TabFormPage();
                tab.Name = tab.Text = "2 / 3-for-1 Agreement";
                tabFormControl1.Pages.Add(tab);
                tabFormControl1.SelectedPage = tab;

                System.Timers.Timer timer30 = new System.Timers.Timer();
                Get30SecondTimer(ref timer30);
                while (timer30.Enabled)
                {
                    BonusAgreementTemplate template = new BonusAgreementTemplate(this);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                    timer30.Stop();
                    timer30.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                CloseSplash();
            }
        }
        private void BtnShowPayCodes_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                ShowSplash("Gathering pay codes");
                e.Item.Visibility = BarItemVisibility.Never;

                foreach (TabFormPage page in tabFormControl1.Pages)
                {
                    if (page.Name == "View Pay Codes")
                    {
                        tabFormControl1.SelectedPage = page;
                        return;
                    }
                }
                TabFormPage tab = new TabFormPage();
                tab.Name = tab.Text = "View Pay Codes";
                tabFormControl1.Pages.Add(tab);
                tabFormControl1.SelectedPage = tab;

                System.Timers.Timer timer30 = new System.Timers.Timer();
                Get30SecondTimer(ref timer30);
                while (timer30.Enabled)
                {
                    PayCodesTemplate template = new PayCodesTemplate(this);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                    timer30.Stop();
                    timer30.Enabled = false;
                }                
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                CloseSplash();
            }
        }
        private void BarShowOptions_ItemClick(object sender, ItemClickEventArgs e) { AddNewOptionsPage(); }
        private void AddNewOptionsPage()
        {
            try
            {
                ShowSplash("Calculating options");
                barShowOptions.Visibility = BarItemVisibility.Never;

                foreach (TabFormPage page in tabFormControl1.Pages)
                {
                    if (page.Name == "Options")
                    {
                        tabFormControl1.SelectedPage = page;
                        return;
                    }
                }
                TabFormPage tab = new TabFormPage();
                tab.Name = tab.Text = "Options";
                tabFormControl1.Pages.Add(tab);
                tabFormControl1.SelectedPage = tab;

                System.Timers.Timer timer30 = new System.Timers.Timer();
                Get30SecondTimer(ref timer30);
                while (timer30.Enabled)
                {
                    OptionsTemplate template = new OptionsTemplate(this);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                    timer30.Stop();
                    timer30.Enabled = false;
                }                
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                CloseSplash();
            }
        }
        private void BarTakeAction_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowTakeActionPage();
        }
        public void ShowTakeActionPage()
        {
            try
            {
                ShowSplash("Discovering letters");
                barTakeAction.Visibility = BarItemVisibility.Never;

                foreach (TabFormPage page in tabFormControl1.Pages)
                {
                    if (page.Name == "Give Point Letter")
                    {
                        tabFormControl1.SelectedPage = page;
                        return;
                    }
                }
                TabFormPage tab = new TabFormPage();
                tab.Name = tab.Text = "Give Point Letter";
                tabFormControl1.Pages.Add(tab);
                tabFormControl1.SelectedPage = tab;

                System.Timers.Timer timer30 = new System.Timers.Timer();
                Get30SecondTimer(ref timer30);
                while (timer30.Enabled)
                {
                    TakeActionTemplate template = new TakeActionTemplate(this);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                    timer30.Stop();
                    timer30.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                CloseSplash();
            }
        }
        private void barMyActionsTaken_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                ShowSplash("Retrieving records");
                e.Item.Visibility = BarItemVisibility.Never;

                foreach (TabFormPage page in tabFormControl1.Pages)
                {
                    if (page.Name == "My Sent Point Letters")
                    {
                        tabFormControl1.SelectedPage = page;
                        return;
                    }
                }
                TabFormPage tab = new TabFormPage();
                tab.Name = tab.Text = "My Sent Point Letters";
                tabFormControl1.Pages.Add(tab);
                tabFormControl1.SelectedPage = tab;

                System.Timers.Timer timer30 = new System.Timers.Timer();
                Get30SecondTimer(ref timer30);
                while (timer30.Enabled)
                {
                    ActionsTakenTemplate template = new ActionsTakenTemplate(this);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                    timer30.Stop();
                    timer30.Enabled = false;
                }                
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                CloseSplash();
            }
        }
        private void BarLapsedActions_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                ShowSplash("Analyzing letters");
                e.Item.Visibility = BarItemVisibility.Never;

                foreach (TabFormPage page in tabFormControl1.Pages)
                {
                    if (page.Name == "My Missed Point Letters")
                    {
                        tabFormControl1.SelectedPage = page;
                        return;
                    }
                }
                TabFormPage tab = new TabFormPage();
                tab.Name = tab.Text = "My Missed Point Letters";
                tabFormControl1.Pages.Add(tab);
                tabFormControl1.SelectedPage = tab;

                System.Timers.Timer timer30 = new System.Timers.Timer();
                Get30SecondTimer(ref timer30);
                while (timer30.Enabled)
                {
                    LapsedActionsTemplate template = new LapsedActionsTemplate(this);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                    timer30.Stop();
                    timer30.Enabled = false;
                }                
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                CloseSplash();
            }
        }
        private void barHelp_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                ShowSplash("Getting assistance");
                e.Item.Visibility = BarItemVisibility.Never;

                foreach (TabFormPage page in tabFormControl1.Pages)
                {
                    if (page.Name == "Help")
                    {
                        tabFormControl1.SelectedPage = page;
                        return;
                    }
                }
                TabFormPage tab = new TabFormPage();
                tab.Name = tab.Text = "Help";
                tabFormControl1.Pages.Add(tab);
                tabFormControl1.SelectedPage = tab;

                System.Timers.Timer timer30 = new System.Timers.Timer();
                Get30SecondTimer(ref timer30);
                while (timer30.Enabled)
                {
                    HelpTemplate template = new HelpTemplate(this);
                    template.TopLevel = false;
                    tab.ContentContainer.Controls.Add(template);
                    template.Dock = DockStyle.Fill;
                    template.Show();
                    timer30.Stop();
                    timer30.Enabled = false;
                }                
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                CloseSplash();
            }
        }

        private void TabFormControl1_PageClosed(object sender, PageClosedEventArgs e)
        {
            ReclaimButton(e.Page.Name);
        }
        public void ReclaimButton(string pName)
        {
            try
            {
                BarItem item = tabFormControl1.Items.Where(z => z.Caption == pName).First();
                item.Visibility = BarItemVisibility.Always;
            }
            catch { }
        }

        public void SendEmail(string[] pTo, string[] pCC, string[] pBCC, string pSubject, string pBody, List<Attachment> pAttachments, bool pSaveToLog)
        {
            try
            {
                ShowSplash("Sending email");

                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress("WiseAttendanceTracker@wisesnacks.com", "Wise Attendance Tracker");
                    mail.Subject = pSubject;
                    mail.Body = "<span style=\"font-family:Calibri;font-size: 11pt;\">" + pBody + "\n\n\n</span>";
                    mail.IsBodyHtml = true;
                    foreach (string to in pTo) { mail.To.Add(to); }
                    if (pCC != null) foreach (string cc in pCC) { mail.CC.Add(cc); }
                    /// BCCS (WHICH ARE ONLY TO ADMIN) ARE DISABLED CURRENTLY BECAUSE THEY WORK AND NO NEED TO CLUTTER MY INBOX
                    //if (pBCC != null) foreach (string bcc in pBCC) { mail.Bcc.Add(bcc); }
                    if (pAttachments != null) { foreach (Attachment attachment in pAttachments) { if (attachment != null) mail.Attachments.Add(attachment); } }

                    /// do not use hostname as the Wise DNS is finnicky
                    using (SmtpClient smtp = new SmtpClient("10.67.51.214", 25))
                    {
                        smtp.EnableSsl = false;
                        smtp.UseDefaultCredentials = false;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.Send(mail);
                    }
                }

                if(pSaveToLog)
                {
                    EmailLog log = new EmailLog();
                    log.ID = Guid.NewGuid();
                    log.From = USERNAME;
                    log.To = String.Join(", ", pTo);
                    log.Subject = pSubject;
                    log.Body = pBody;
                    log.Sent = DateTime.Now;
                    dataset.EmailLogs.Add(log);
                    dataset.SaveChanges();
                }                
            }
            catch (Exception ex)
            {
                LogAndDisplayError(ex.ToString(), false);
            }
            finally
            {
                CloseSplash();
            }
        }

        public void LogAndDisplayError(string pError, bool pSendEmailAndDisplayError)
        {
            try
            {
                ErrorLog error = new ErrorLog();
                error.ID = Guid.NewGuid();
                error.Error = pError;
                error.CreatedBy = USERNAME;
                error.CreatedOn = DateTime.Now;
                dataset.ErrorLogs.Add(error);
                dataset.SaveChanges();
            }
            catch { }
            try
            {
                if (pSendEmailAndDisplayError)
                {
                    SendEmail(new string[] { Properties.Settings.Default.AdminEmail },
                              null,
                              null, 
                              "New Error Generated by " + FULLNAME,
                              String.Format("<b>Full Name</b>: {0}<br><b>Username</b>: {1}<br><b>Email Address</b>: {2}<br><b>Error</b>: {3}", FULLNAME, USERNAME, EMAILADDRESS, pError),
                              null,
                              true);

                    if (!RUNNINGAUTOMATED)
                    {
                        MessageBox.Show("Something went wrong!\n\nThe error that occurred is as follows; please forward this to the helpdesk if you require further assistance:\n\n" + pError, "Oh no!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch { }
        }

        public void ShowSplash(string pMsg)
        {
            try
            {
                if (RUNNINGAUTOMATED) return;
                else if (!SPLASH && !ISTHEFIRSTLAUNCH)
                {
                    SPLASH = true;
                    SplashScreenManager.ShowForm(typeof(SplashScreen1));
                }
                else return;

                if (pMsg == null) pMsg = string.Empty;
                SplashScreenManager.Default.SendCommand(SplashScreen1.SplashScreenCommand.ChangeText, pMsg);
            }
            catch (Exception ex)
            {
                //LogAndDisplayError(ex.ToString());
                CloseSplash();
            }
        }
        public void CloseSplash()
        {
            try
            {
                if (RUNNINGAUTOMATED) return;

                SPLASH = false;

                SplashScreenManager.ActivateParentOnSplashFormClosing = true;
                SplashScreenManager.CloseForm(false);
            }
            catch { }
        }

        public List<string> PopulateTabNameList()
        {
            try
            {
                List<string> returnVals = new List<string>();
                foreach (TabFormPage tab in tabFormControl1.Pages) 
                {
                    if (tab.Text != "AdminTasks" &&
                        tab.Text != "Send App Feedback" &&
                        tab.Text != "Request a Data Change") { returnVals.Add(tab.Text); }
                }
                return returnVals;
            }
            catch { return null; }
        }
        public Attachment GetContentsOfTabAndAttach(string pTabName, MemoryStream pStream, bool pIsBeingRerun)
        {
            /// export the contents of a tab to an image
            /// lifted mostly from ShiftReports!

            TabFormPage currentPage = tabFormControl1.SelectedPage;
            bool fail = false;
            try
            {
                if (pTabName == null || pTabName == string.Empty) return null;
                TabFormPage screenshotPage;
                try { screenshotPage = tabFormControl1.Pages.Where(z => z.Text == pTabName).First(); }
                catch { return null; }

                /// select tab to be screenshotted to ensure that its displaycontainer is active and complete
                /// unsure if this actually does anything but it can't hurt
                tabFormControl1.SelectedPage = screenshotPage;
                (screenshotPage.ContentContainer).Invalidate();
                (screenshotPage.ContentContainer).Update();
                (screenshotPage.ContentContainer).Refresh();
                Application.DoEvents();
                /// this will effectively select the tab which the user has decided to screenshot after this operation is complete
                /// is favorable

                /// set properties for the panels so .DrawToBitmap gets ALL controls and not just what is visible at the time (with the scrollbar)
                //tab.ContentContainer.AutoScroll = false;
                screenshotPage.ContentContainer.AutoScrollOffset = new Point(0, 0);
                screenshotPage.ContentContainer.AutoSize = true;
                screenshotPage.ContentContainer.Dock = DockStyle.None;
                /// set bmp image properties according to this panel
                /// it is important to use the .DisplayRectangle version of .Width and .Height to account for any area which has been scrolled
                Bitmap bmp = new Bitmap(screenshotPage.ContentContainer.DisplayRectangle.Width, screenshotPage.ContentContainer.DisplayRectangle.Height);
                /// draw the bitmap from the tab
                screenshotPage.ContentContainer.DrawToBitmap(bmp, new Rectangle(0, 0, screenshotPage.ContentContainer.DisplayRectangle.Width, screenshotPage.ContentContainer.DisplayRectangle.Height));
                /// save the bitmap to the passed stream
                bmp.Save(pStream, System.Drawing.Imaging.ImageFormat.Png);

                /// undo property changes
                screenshotPage.ContentContainer.AutoSize = false;
                screenshotPage.ContentContainer.Dock = DockStyle.Fill;

                /// create a byte array from the stream properties
                byte[] byteArray = new byte[pStream.Length];
                /// seek to beginning
                pStream.Seek(0, System.IO.SeekOrigin.Begin);
                /// read the contents
                pStream.Read(byteArray, 0, byteArray.Length);
                /// seek to beginning (again)
                pStream.Seek(0, System.IO.SeekOrigin.Begin);

                /// setup content type; octet is very robust
                ContentType content = new ContentType(MediaTypeNames.Application.Octet);
                /// create an attachment for emailing
                Attachment attachment = new Attachment(pStream, content);
                /// set the filename
                attachment.ContentDisposition.FileName = pTabName.Trim().Replace(" ", "_") + ".png";
                /// return the attachment
                return attachment;
            }
            catch (Exception ex)
            {
                /// allow one re-run before throwing an error
                if (!pIsBeingRerun) fail = true;
                else LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                /// re-select original page so it gets closed properly
                tabFormControl1.SelectedPage = currentPage;
            }
            
            /// some fuckery to ensure that the .ContentContainer is active and complete
            if (fail) return GetContentsOfTabAndAttach(pTabName, pStream, true);
            else return null;
        }
        
        public object GetTabFromMain(string pTabName)
        {
            TabFormPage page = tabFormControl1.Pages.Where(z => z.Name == pTabName).First();
            object returnThing = null;
            bool found = false;
            foreach (Control ctrl in page.ContentContainer.Controls)
            {
                if (found) break;
                switch (pTabName){
                    case "AdminTasks":
                        if (!found && ctrl.GetType() == typeof(PageTemplate)) { found = true; returnThing = ctrl; }
                        break;
                    case "Reporting":
                        if (!found && ctrl.GetType() == typeof(ReportingTemplate)) { found = true; returnThing = ctrl; }
                        break;
                    case "Give Point Letter":
                        if (!found && ctrl.GetType() == typeof(TakeActionTemplate)) { found = true; returnThing = ctrl; }
                        break;
                    default:
                        break;
                }
            }
            return returnThing;
        }

        public void Get30SecondTimer(ref System.Timers.Timer pTimer)
        {
            pTimer.Interval = 30000; /// 30 seconds
            pTimer.Enabled = true;
            pTimer.Elapsed += new ElapsedEventHandler(Stop30SecondTimer);
            pTimer.Start();
        }
        private void Stop30SecondTimer(object source, ElapsedEventArgs e)
        {
            (source as System.Timers.Timer).Stop();
            (source as System.Timers.Timer).Enabled = false;
        }
    }
}