//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AttendanceTracker
{
    using System;
    using System.Collections.Generic;
    
    public partial class FromCeridianAPI_Headcount
    {
        public int ID { get; set; }
        public string PayClass_ShortName { get; set; }
        public string OrgUnit_ShortName { get; set; }
        public string Employee_NameNumber { get; set; }
        public string PayType_LongName { get; set; }
        public string PayGroup_ShortName { get; set; }
        public string Department_ShortName { get; set; }
        public string EmploymentStatus_LongName { get; set; }
        public string Employee_OriginalHireDate { get; set; }
        public string EmployeeUnion_DFUnionId { get; set; }
        public string DFUnion_LongName { get; set; }
        public string DeptJob_ShortName { get; set; }
        public string UnionNonUnionNonUnionOperations { get; set; }
        public string EmployeeWorkAssignment_OrgUnitId { get; set; }
        public string ARCAREPORTINGLOCATION { get; set; }
        public string Employee_DisplayName { get; set; }
        public string Employee_EmployeeId { get; set; }
    }
}
