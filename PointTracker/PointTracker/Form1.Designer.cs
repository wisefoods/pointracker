﻿namespace AttendanceTracker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, null, true, true);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dockPanel2 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.tabFormControl1 = new DevExpress.XtraBars.TabFormControl();
            this.lblLogo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnShowPointSystem = new DevExpress.XtraBars.BarButtonItem();
            this.btnSendFeedback = new DevExpress.XtraBars.BarButtonItem();
            this.btnShowReprimands = new DevExpress.XtraBars.BarButtonItem();
            this.btnShowPayCodes = new DevExpress.XtraBars.BarButtonItem();
            this.barShowOptions = new DevExpress.XtraBars.BarButtonItem();
            this.barTakeAction = new DevExpress.XtraBars.BarButtonItem();
            this.barInfoMenu = new DevExpress.XtraBars.BarSubItem();
            this.barShow23For1Agreement = new DevExpress.XtraBars.BarButtonItem();
            this.btnViewAgreement = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barMyActionsTaken = new DevExpress.XtraBars.BarButtonItem();
            this.barLapsedActions = new DevExpress.XtraBars.BarButtonItem();
            this.barShowLogins = new DevExpress.XtraBars.BarButtonItem();
            this.barShowUsers = new DevExpress.XtraBars.BarButtonItem();
            this.barAdmin = new DevExpress.XtraBars.BarButtonItem();
            this.barHelp = new DevExpress.XtraBars.BarButtonItem();
            this.btnRequestAChange = new DevExpress.XtraBars.BarButtonItem();
            this.barReportingMenu = new DevExpress.XtraBars.BarButtonItem();
            this.gcForDataExport = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dockPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabFormControl1)).BeginInit();
            this.tabFormControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcForDataExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // splashScreenManager1
            // 
            splashScreenManager1.ClosingDelay = 500;
            // 
            // dockPanel2
            // 
            this.dockPanel2.Controls.Add(this.dockPanel2_Container);
            this.dockPanel2.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this.dockPanel2.DockedAsTabbedDocument = true;
            this.dockPanel2.ID = new System.Guid("b1d29354-6c11-4204-af54-93fa457db55f");
            this.dockPanel2.Location = new System.Drawing.Point(0, 0);
            this.dockPanel2.Name = "dockPanel2";
            this.dockPanel2.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanel2.SavedIndex = 0;
            this.dockPanel2.SavedMdiDocument = true;
            this.dockPanel2.Size = new System.Drawing.Size(200, 200);
            this.dockPanel2.Text = "dockPanel2";
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Location = new System.Drawing.Point(4, 22);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(192, 175);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // tabFormControl1
            // 
            this.tabFormControl1.AllowMoveTabsToOuterForm = false;
            this.tabFormControl1.Controls.Add(this.lblLogo);
            this.tabFormControl1.Controls.Add(this.label1);
            this.tabFormControl1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.tabFormControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnShowPointSystem,
            this.btnSendFeedback,
            this.btnShowReprimands,
            this.btnShowPayCodes,
            this.barShowOptions,
            this.barTakeAction,
            this.barInfoMenu,
            this.barSubItem1,
            this.barLapsedActions,
            this.barMyActionsTaken,
            this.barShowLogins,
            this.barShowUsers,
            this.barAdmin,
            this.btnViewAgreement,
            this.barHelp,
            this.btnRequestAChange,
            this.barReportingMenu,
            this.barShow23For1Agreement});
            this.tabFormControl1.Location = new System.Drawing.Point(0, 0);
            this.tabFormControl1.MaxTabWidth = 400;
            this.tabFormControl1.Name = "tabFormControl1";
            this.tabFormControl1.Size = new System.Drawing.Size(1241, 55);
            this.tabFormControl1.TabForm = this;
            this.tabFormControl1.TabIndex = 1;
            this.tabFormControl1.TabRightItemLinks.Add(this.btnSendFeedback);
            this.tabFormControl1.TabRightItemLinks.Add(this.btnRequestAChange);
            this.tabFormControl1.TabStop = false;
            this.tabFormControl1.TitleItemLinks.Add(this.barSubItem1);
            this.tabFormControl1.TitleItemLinks.Add(this.barInfoMenu);
            this.tabFormControl1.TitleItemLinks.Add(this.barReportingMenu);
            this.tabFormControl1.TitleItemLinks.Add(this.barShowOptions);
            this.tabFormControl1.TitleItemLinks.Add(this.barHelp);
            this.tabFormControl1.TitleItemLinks.Add(this.barAdmin);
            this.tabFormControl1.PageCreated += new DevExpress.XtraBars.PageCreatedEventHandler(this.TabFormControl1_PageCreated);
            this.tabFormControl1.PageClosed += new DevExpress.XtraBars.PageClosedEventHandler(this.TabFormControl1_PageClosed);
            // 
            // lblLogo
            // 
            this.lblLogo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblLogo.AutoSize = true;
            this.lblLogo.BackColor = System.Drawing.Color.Transparent;
            this.lblLogo.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblLogo.Location = new System.Drawing.Point(530, 4);
            this.lblLogo.Name = "lblLogo";
            this.lblLogo.Padding = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.lblLogo.Size = new System.Drawing.Size(214, 25);
            this.lblLogo.TabIndex = 4;
            this.lblLogo.Text = "Wise Attendance Tracker v3.0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Right;
            this.label1.Font = new System.Drawing.Font("Tahoma", 1F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(1140, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 2);
            this.label1.TabIndex = 3;
            this.label1.Text = "                                                                                 " +
    "                  ";
            // 
            // btnShowPointSystem
            // 
            this.btnShowPointSystem.Caption = "View Penalties and Bonuses";
            this.btnShowPointSystem.Id = 0;
            this.btnShowPointSystem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnShowPointSystem.ImageOptions.Image")));
            this.btnShowPointSystem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnShowPointSystem.ImageOptions.LargeImage")));
            this.btnShowPointSystem.Name = "btnShowPointSystem";
            this.btnShowPointSystem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnShowPointSystem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnShowPointSystem_ItemClick);
            // 
            // btnSendFeedback
            // 
            this.btnSendFeedback.Caption = "Send App Feedback";
            this.btnSendFeedback.Id = 1;
            this.btnSendFeedback.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSendFeedback.ImageOptions.Image")));
            this.btnSendFeedback.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnSendFeedback.ImageOptions.LargeImage")));
            this.btnSendFeedback.Name = "btnSendFeedback";
            this.btnSendFeedback.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnSendFeedback.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnSendFeedback_ItemClick);
            // 
            // btnShowReprimands
            // 
            this.btnShowReprimands.Caption = "View Point Levels";
            this.btnShowReprimands.Id = 0;
            this.btnShowReprimands.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnShowReprimands.ImageOptions.Image")));
            this.btnShowReprimands.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnShowReprimands.ImageOptions.LargeImage")));
            this.btnShowReprimands.Name = "btnShowReprimands";
            this.btnShowReprimands.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnShowReprimands.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnShowReprimands_ItemClick);
            // 
            // btnShowPayCodes
            // 
            this.btnShowPayCodes.Caption = "View Pay Codes";
            this.btnShowPayCodes.Id = 1;
            this.btnShowPayCodes.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnShowPayCodes.ImageOptions.Image")));
            this.btnShowPayCodes.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnShowPayCodes.ImageOptions.LargeImage")));
            this.btnShowPayCodes.Name = "btnShowPayCodes";
            this.btnShowPayCodes.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnShowPayCodes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnShowPayCodes_ItemClick);
            // 
            // barShowOptions
            // 
            this.barShowOptions.Caption = "Options";
            this.barShowOptions.Id = 0;
            this.barShowOptions.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barShowOptions.ImageOptions.Image")));
            this.barShowOptions.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barShowOptions.ImageOptions.LargeImage")));
            this.barShowOptions.Name = "barShowOptions";
            this.barShowOptions.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barShowOptions.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarShowOptions_ItemClick);
            // 
            // barTakeAction
            // 
            this.barTakeAction.Caption = "Give Point Letter";
            this.barTakeAction.Id = 1;
            this.barTakeAction.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barTakeAction.ImageOptions.Image")));
            this.barTakeAction.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barTakeAction.ImageOptions.LargeImage")));
            this.barTakeAction.Name = "barTakeAction";
            this.barTakeAction.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barTakeAction.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarTakeAction_ItemClick);
            // 
            // barInfoMenu
            // 
            this.barInfoMenu.Caption = "Info";
            this.barInfoMenu.Id = 0;
            this.barInfoMenu.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barInfoMenu.ImageOptions.Image")));
            this.barInfoMenu.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barInfoMenu.ImageOptions.LargeImage")));
            this.barInfoMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barShow23For1Agreement, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnShowPayCodes),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnShowPointSystem),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnShowReprimands),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnViewAgreement)});
            this.barInfoMenu.Name = "barInfoMenu";
            this.barInfoMenu.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barShow23For1Agreement
            // 
            this.barShow23For1Agreement.Caption = "2 / 3-for-1 Agreement";
            this.barShow23For1Agreement.Id = 0;
            this.barShow23For1Agreement.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barShow23For1Agreement.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.LargeImage")));
            this.barShow23For1Agreement.Name = "barShow23For1Agreement";
            this.barShow23For1Agreement.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barShow23For1Agreement_ItemClick);
            // 
            // btnViewAgreement
            // 
            this.btnViewAgreement.Caption = "View Union Agreements";
            this.btnViewAgreement.Id = 0;
            this.btnViewAgreement.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnViewAgreement.ImageOptions.Image")));
            this.btnViewAgreement.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnViewAgreement.ImageOptions.LargeImage")));
            this.btnViewAgreement.Name = "btnViewAgreement";
            this.btnViewAgreement.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnViewAgreement_ItemClick);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Point Letters";
            this.barSubItem1.Id = 0;
            this.barSubItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem1.ImageOptions.Image")));
            this.barSubItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem1.ImageOptions.LargeImage")));
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barTakeAction),
            new DevExpress.XtraBars.LinkPersistInfo(this.barMyActionsTaken),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLapsedActions)});
            this.barSubItem1.Name = "barSubItem1";
            this.barSubItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barMyActionsTaken
            // 
            this.barMyActionsTaken.Caption = "My Sent Point Letters";
            this.barMyActionsTaken.Id = 0;
            this.barMyActionsTaken.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barMyActionsTaken.ImageOptions.Image")));
            this.barMyActionsTaken.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barMyActionsTaken.ImageOptions.LargeImage")));
            this.barMyActionsTaken.Name = "barMyActionsTaken";
            this.barMyActionsTaken.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barMyActionsTaken.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barMyActionsTaken_ItemClick);
            // 
            // barLapsedActions
            // 
            this.barLapsedActions.Caption = "My Missed Point Letters";
            this.barLapsedActions.Id = 1;
            this.barLapsedActions.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barLapsedActions.ImageOptions.Image")));
            this.barLapsedActions.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barLapsedActions.ImageOptions.LargeImage")));
            this.barLapsedActions.Name = "barLapsedActions";
            this.barLapsedActions.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barLapsedActions.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarLapsedActions_ItemClick);
            // 
            // barShowLogins
            // 
            this.barShowLogins.Caption = "View Logins";
            this.barShowLogins.Id = 0;
            this.barShowLogins.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barShowLogins.ImageOptions.Image")));
            this.barShowLogins.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barShowLogins.ImageOptions.LargeImage")));
            this.barShowLogins.Name = "barShowLogins";
            // 
            // barShowUsers
            // 
            this.barShowUsers.Caption = "View Users";
            this.barShowUsers.Id = 1;
            this.barShowUsers.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barShowUsers.ImageOptions.Image")));
            this.barShowUsers.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barShowUsers.ImageOptions.LargeImage")));
            this.barShowUsers.Name = "barShowUsers";
            // 
            // barAdmin
            // 
            this.barAdmin.Caption = "Admin";
            this.barAdmin.Id = 2;
            this.barAdmin.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barAdmin.ImageOptions.Image")));
            this.barAdmin.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barAdmin.ImageOptions.LargeImage")));
            this.barAdmin.Name = "barAdmin";
            this.barAdmin.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barAdmin.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barAdmin_ItemClick);
            // 
            // barHelp
            // 
            this.barHelp.Caption = "Help";
            this.barHelp.Id = 0;
            this.barHelp.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barHelp.ImageOptions.Image")));
            this.barHelp.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barHelp.ImageOptions.LargeImage")));
            this.barHelp.Name = "barHelp";
            this.barHelp.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barHelp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barHelp_ItemClick);
            // 
            // btnRequestAChange
            // 
            this.btnRequestAChange.Caption = "Request a Data Change";
            this.btnRequestAChange.Id = 1;
            this.btnRequestAChange.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRequestAChange.ImageOptions.Image")));
            this.btnRequestAChange.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnRequestAChange.ImageOptions.LargeImage")));
            this.btnRequestAChange.Name = "btnRequestAChange";
            this.btnRequestAChange.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnRequestAChange.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRequestAChange_ItemClick);
            // 
            // barReportingMenu
            // 
            this.barReportingMenu.Caption = "Reporting";
            this.barReportingMenu.Id = 0;
            this.barReportingMenu.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barReportingMenu.ImageOptions.Image")));
            this.barReportingMenu.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barReportingMenu.ImageOptions.LargeImage")));
            this.barReportingMenu.Name = "barReportingMenu";
            this.barReportingMenu.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barReportingMenu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barReportingMenu_ItemClick);
            // 
            // gcForDataExport
            // 
            this.gcForDataExport.Dock = System.Windows.Forms.DockStyle.Right;
            this.gcForDataExport.Location = new System.Drawing.Point(626, 55);
            this.gcForDataExport.LookAndFeel.SkinName = "Sharp";
            this.gcForDataExport.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gcForDataExport.MainView = this.gridView3;
            this.gcForDataExport.Name = "gcForDataExport";
            this.gcForDataExport.Size = new System.Drawing.Size(615, 540);
            this.gcForDataExport.TabIndex = 8;
            this.gcForDataExport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            this.gcForDataExport.Visible = false;
            // 
            // gridView3
            // 
            this.gridView3.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.gridView3.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView3.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridView3.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView3.GridControl = this.gcForDataExport;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.Editable = false;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gridView3.OptionsView.EnableAppearanceOddRow = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1241, 595);
            this.Controls.Add(this.gcForDataExport);
            this.Controls.Add(this.tabFormControl1);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("Form1.IconOptions.Icon")));
            this.LookAndFeel.SkinName = "Sharp Plus";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "Form1";
            this.TabFormControl = this.tabFormControl1;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.dockPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabFormControl1)).EndInit();
            this.tabFormControl1.ResumeLayout(false);
            this.tabFormControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcForDataExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraBars.Docking.DockPanel dockPanel2;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        public DevExpress.XtraBars.TabFormControl tabFormControl1;
        private DevExpress.XtraBars.BarButtonItem btnShowPointSystem;
        private DevExpress.XtraBars.BarButtonItem btnSendFeedback;
        private DevExpress.XtraBars.BarButtonItem btnShowReprimands;
        private DevExpress.XtraBars.BarButtonItem btnShowPayCodes;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraBars.BarButtonItem barShowOptions;
        private DevExpress.XtraBars.BarButtonItem barTakeAction;
        private DevExpress.XtraBars.BarSubItem barInfoMenu;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem barLapsedActions;
        public DevExpress.XtraGrid.GridControl gcForDataExport;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraBars.BarButtonItem barMyActionsTaken;
        private DevExpress.XtraBars.BarButtonItem barShowLogins;
        private DevExpress.XtraBars.BarButtonItem barShowUsers;
        private DevExpress.XtraBars.BarButtonItem barAdmin;
        private DevExpress.XtraBars.BarButtonItem btnViewAgreement;
        private DevExpress.XtraBars.BarButtonItem barHelp;
        private DevExpress.XtraBars.BarButtonItem btnRequestAChange;
        public System.Windows.Forms.Label lblLogo;
        private DevExpress.XtraBars.BarButtonItem barReportingMenu;
        private DevExpress.XtraBars.BarButtonItem barShow23For1Agreement;
    }
}