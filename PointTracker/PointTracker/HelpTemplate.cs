﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using DevExpress.XtraGrid.Views.Grid;
using System.IO;
using DevExpress.Pdf;

namespace AttendanceTracker
{
    public partial class HelpTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;

        public HelpTemplate(Form1 pForm)
        {
            try
            {
                InitializeComponent();
                atrack = pForm;
                BindPageData();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }
        public void BindPageData()
        {
            try
            {
                /// use word-style web layout
                recHelpDoc.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
                recHelpDoc.LoadDocument(new MemoryStream(Properties.Resources.AttendanceTracker_HelpDoc_05_05_2020));
            }
            catch (Exception ex) { atrack.LogAndDisplayError(ex.ToString(), true); }
        }
    }
}
