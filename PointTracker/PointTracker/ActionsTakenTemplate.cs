﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;

namespace AttendanceTracker
{
    public partial class ActionsTakenTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;

        public ActionsTakenTemplate(Form1 pForm)
        {
            try
            {
                InitializeComponent();
                atrack = pForm;
                BindPageData();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        public void BindPageData()
        {
            try
            {
                gcMyActionsTaken.DataSource = null;
                (gcMyActionsTaken.MainView as GridView).Columns.Clear();
                /// admins view data for all users
                bool isAdmin = atrack.CheckIfAdmin();
                gcMyActionsTaken.DataSource = atrack.dataset.ActionsTakens
                                                            .Where(z => z.ActionTaken == true && z.IsLocked == false && (isAdmin || z.ActionTakenBy == atrack.USERNAME))
                                                            .Select(z => new { z.PointLetterDistributedOn, z.ActionTakenOn, z.EmployeeName, z.EmployeeID, z.Department, z.DateOfOccurrence, z.PointValue, z.ActionToTake, z.ActionTaken, z.Comments })
                                                            .OrderBy(z => z.PointLetterDistributedOn)
                                                            .ToList();
                (gcMyActionsTaken.MainView as GridView).Columns["PointLetterDistributedOn"].DisplayFormat.FormatType = FormatType.DateTime;
                (gcMyActionsTaken.MainView as GridView).Columns["PointLetterDistributedOn"].DisplayFormat.FormatString = "MM/dd/yyyy";
                (gcMyActionsTaken.MainView as GridView).Columns["ActionTakenOn"].DisplayFormat.FormatType = FormatType.DateTime;
                (gcMyActionsTaken.MainView as GridView).Columns["ActionTakenOn"].DisplayFormat.FormatString = "MM/dd/yyyy hh:mm:ss";
                (gcMyActionsTaken.MainView as GridView).BestFitColumns();

                if ((gcMyActionsTaken.MainView as GridView).RowCount <= 0)
                {
                    atrack.tabFormControl1.Pages.Remove(atrack.tabFormControl1.Pages.Where(z => z.Name == "My Sent Point Letters").FirstOrDefault());
                    atrack.ReclaimButton("My Sent Point Letters");
                    atrack.CloseSplash();
                    MessageBox.Show("You have not sent any point letters!", "Nothing To See", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                if (atrack.UPDATEACTIVE) { MessageBox.Show("Something went wrong; please close this page and try again.", "Data Update Failed", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                else { atrack.LogAndDisplayError(ex.ToString(), true); }
            }
        }
    }
}
