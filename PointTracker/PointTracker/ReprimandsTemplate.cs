﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Repository;

namespace AttendanceTracker
{
    public partial class ReprimandsTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;

        public ReprimandsTemplate(Form1 pForm)
        {
            try
            {
                InitializeComponent();

                atrack = pForm;

                gcReprimands.DataSource = null;
                (gcReprimands.MainView as GridView).Columns.Clear();
                gcReprimands.DataSource = atrack.dataset.Reprimands.Select(z => new { z.AtPointValue, z.Action }).OrderBy(z => z.AtPointValue).ToList();

                RepositoryItemMemoEdit memoedit = new RepositoryItemMemoEdit();
                memoedit.WordWrap = true;
                memoedit.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
                (gcReprimands.MainView as GridView).Columns["Action"].ColumnEdit = memoedit;

                (gcReprimands.MainView as GridView).Columns["AtPointValue"].Width =
                (gcReprimands.MainView as GridView).Columns["AtPointValue"].MaxWidth = 100;
                (gcReprimands.MainView as GridView).Columns["AtPointValue"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                (gcReprimands.MainView as GridView).BestFitColumns();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }
    }
}
