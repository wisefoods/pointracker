﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using DevExpress.XtraGrid.Views.Grid;
using System.Diagnostics;
using DevExpress.XtraBars;

namespace AttendanceTracker
{
    public partial class OptionsTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;
        UserOption OPTION;
        bool PAGEISLOADING = false;

        public OptionsTemplate(Form1 pForm)
        {
            try
            {
                InitializeComponent();
                atrack = pForm;
                if (atrack.dataset.UserOptions.Any(z => z.Username == atrack.USERNAME))
                {
                    GetNewOptions();
                }
                else
                {
                    OPTION = null;
                }                
                BindEmployeeLists();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }
        private void GetNewOptions()
        {
            OPTION = atrack.dataset.UserOptions.Where(z => z.Username == atrack.USERNAME).FirstOrDefault();
        }

        private void BindEmployeeLists()
        {
            PAGEISLOADING = true;
            BindYourEmployees();
            BindAvailableEmployees();
            BindSelectedShifts();
            PAGEISLOADING = false;
        }
        private void BindYourEmployees()
        {
            listYourDepartments.Items.Clear();
            string searchYourEmployees = tbSearchYourEmployees.Text;

            /// strictly by USER OPTIONS in DB
            listYourDepartments.Items.AddRange(atrack.MYEMPLOYEES_DEPARTMENTS.Where(z => z.ToString().Contains(searchYourEmployees)).OrderBy(z => z.ToString()).ToArray());
        }
        private void BindAvailableEmployees()
        {
            listAvailableDepartments.Items.Clear();
            string searchAvailableEmployees = tbSearchAvailableEmployees.Text;

            
            /// using both the HR report and the Shift Rotation report
            listAvailableDepartments.Items.AddRange(atrack.ALLDEPARTMENTS.Where(z => z.ToString().Contains(searchAvailableEmployees) &&
                                                                                   !atrack.MYEMPLOYEES_DEPARTMENTS.Contains(z.ToString())).ToArray());
        }

        private void BindSelectedShifts()
        {
            if (atrack.MYEMPLOYEES_SHIFTS.Contains(1)) checkedShifts.SetItemChecked(0, true);
            if (atrack.MYEMPLOYEES_SHIFTS.Contains(2)) checkedShifts.SetItemChecked(1, true);
            if (atrack.MYEMPLOYEES_SHIFTS.Contains(3)) checkedShifts.SetItemChecked(2, true);
        }

        private void BtnAddEmployee_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                atrack.ShowSplash("Adding departments");

                if (listAvailableDepartments.SelectedIndex < 0) return;
                for (int i = 0; i < listAvailableDepartments.SelectedItems.Count; i++)
                {
                    OPTION.Departments += listAvailableDepartments.SelectedItems[i].ToString() + "|||";
                }
                atrack.dataset.SaveChanges();

                atrack.BindAndUpdateMyEmployees();
                GetNewOptions();
                BindEmployeeLists();
                atrack.UpdateAllPagedAppData();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                atrack.CloseSplash();
            }            
        }
        private void BtnRemoveEmployee_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                atrack.ShowSplash("Removing departments");

                if (listYourDepartments.SelectedIndex < 0) return;
                for (int i = 0; i < listYourDepartments.SelectedItems.Count; i++)
                {
                    OPTION.Departments = OPTION.Departments.Replace(listYourDepartments.SelectedItems[i].ToString() + "|||", string.Empty);
                }
                atrack.dataset.SaveChanges();

                atrack.BindAndUpdateMyEmployees();
                GetNewOptions();
                BindEmployeeLists();
                atrack.UpdateAllPagedAppData();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                atrack.CloseSplash();
            }
        }

        private void checkedShifts_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            try
            {
                if (PAGEISLOADING) return;

                /// get current setup
                bool shift1 = false, shift2 = false, shift3 = false;
                shift1 = atrack.MYEMPLOYEES_SHIFTS.Contains(1);
                shift2 = atrack.MYEMPLOYEES_SHIFTS.Contains(2);
                shift3 = atrack.MYEMPLOYEES_SHIFTS.Contains(3);

                /// consider changes
                if (e.Index == 0 && e.NewValue == CheckState.Checked) shift1 = true;
                else if (e.Index == 0 && e.NewValue == CheckState.Unchecked) shift1 = false;
                else if (e.Index == 1 && e.NewValue == CheckState.Checked) shift2 = true;
                else if (e.Index == 1 && e.NewValue == CheckState.Unchecked) shift2 = false;
                else if (e.Index == 2 && e.NewValue == CheckState.Checked) shift3 = true;
                else if (e.Index == 2 && e.NewValue == CheckState.Unchecked) shift3 = false;

                /// nothing is selected...
                if (!shift1 && !shift2 && !shift3)
                {
                    MessageBox.Show("You must have at least one shift selected!",
                                    "Select Shift",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);

                    /// force a selection and skip saving
                    if (e.Index == 0)
                    {
                        e.NewValue = CheckState.Checked;
                        return;
                    }
                    else if (e.Index == 1)
                    {
                        e.NewValue = CheckState.Checked;
                        return;
                    }
                    else if (e.Index == 2)
                    {
                        e.NewValue = CheckState.Checked;
                        return;
                    }
                }

                atrack.ShowSplash("Changing shifts");

                /// build and save entry
                string shiftEntry = string.Empty;
                if (shift1) shiftEntry += "1,";
                if (shift2) shiftEntry += "2,";
                if (shift3) shiftEntry += "3,";
                OPTION.Shift = shiftEntry;
                atrack.dataset.SaveChanges();

                /// rebuild data
                atrack.BindAndUpdateMyEmployees();
                GetNewOptions();
                BindEmployeeLists();
                atrack.UpdateAllPagedAppData();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                atrack.CloseSplash();
            }
        }

        private void TbSearchYourEmployees_KeyUp(object sender, KeyEventArgs e)
        {
            BindYourEmployees();
        }
        private void TbSearchAvailableEmployees_KeyUp(object sender, KeyEventArgs e)
        {
            BindAvailableEmployees();
        }

        private void BtnClearSearchYourEmployees_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            tbSearchYourEmployees.Text = string.Empty;
            BindYourEmployees();
        }
        private void BtnClearSearchAvailableEmployees_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            tbSearchAvailableEmployees.Text = string.Empty;
            BindAvailableEmployees();
        }

        private void BtnClearYourEmployeesSelections_MouseUp(object sender, MouseEventArgs e) { listYourDepartments.UnSelectAll(); }
        private void BtnClearYourEmployeesSelections_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e) { listYourDepartments.UnSelectAll(); }
        private void BtnYourEmployeesSelectAll_MouseUp(object sender, MouseEventArgs e) { listYourDepartments.SelectAll(); }
        private void BtnYourEmployeesSelectAll_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e) { listYourDepartments.SelectAll(); }
        private void BtnClearAvailableEmployeesSelections_MouseUp(object sender, MouseEventArgs e) { listAvailableDepartments.UnSelectAll(); }
        private void BtnClearAvailableEmployeesSelections_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e) { listAvailableDepartments.UnSelectAll(); }
        private void BtnAvailableEmployeesSelectAll_MouseUp(object sender, MouseEventArgs e) { listAvailableDepartments.SelectAll(); }
        private void BtnAvailableEmployeesSelectAll_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e) { listAvailableDepartments.SelectAll(); }

        private void HLinkRuleHelp_Click(object sender, EventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("https://support.office.com/en-us/article/manage-email-messages-by-using-rules-c24f5dea-9465-4df4-ad17-a50704d66c59");
            Process.Start(sInfo);        
        }
    }
}