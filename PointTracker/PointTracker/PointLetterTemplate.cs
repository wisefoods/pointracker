﻿using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.XtraRichEdit.Commands;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;

namespace AttendanceTracker
{
    public partial class PointLetterTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;
        ActionsTaken thisAction;
        bool doDuplexPrinting = false;

        public PointLetterTemplate(Form1 pForm, ActionsTaken pAction, byte[] pContent, bool pDuplexPrinting)
        {
            try
            {
                InitializeComponent();

                if (pContent == null) return;

                atrack = pForm;
                thisAction = pAction;
                BindPageData(pContent);
                doDuplexPrinting = pDuplexPrinting;
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }
        public void BindPageData(byte[] pContent)
        {
            try
            {
                richEditor.ReadOnly = false;

                MemoryStream stream = new MemoryStream(pContent);
                stream.Seek(0, SeekOrigin.Begin);

                richEditor.LoadDocument(stream, DevExpress.XtraRichEdit.DocumentFormat.OpenXml, true);

                Document document = richEditor.Document;
                document.Unit = DevExpress.Office.DocumentUnit.Inch;
                document.Sections[0].Margins.Left =
                document.Sections[0].Margins.Top =
                document.Sections[0].Margins.Right =
                document.Sections[0].Margins.Bottom = 0.5f;

                /// replace {{PAGEBREAK}} with actual pagebreaks
                richEditor.Document.BeginUpdate();

                string pagebreak = "{{PAGEBREAK}}";
                ISearchResult searchResult = richEditor.Document.StartSearch(pagebreak, SearchOptions.CaseSensitive, SearchDirection.Forward, richEditor.Document.Range);

                while (searchResult.FindNext())
                {
                    searchResult.Replace(String.Empty);
                    richEditor.Document.CaretPosition = searchResult.CurrentResult.Start;
                    InsertPageBreakCommand cmd = new InsertPageBreakCommand(richEditor);
                    cmd.Execute();
                }

                //richEditor.Document.CaretPosition = richEditor.Document.Range.Start;
                //richEditor.ScrollToCaret();
                richEditor.VerticalScrollValue = 0;
                richEditor.Document.EndUpdate();
            }
            catch (Exception ex) { atrack.LogAndDisplayError(ex.ToString(), true); }
            finally { richEditor.ReadOnly = true; }
        }

        private void btnPrintDirectly_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            /// force the default setting in the print dialog to have a PORTRAIT ORIENTATION
            richEditor.Document.Sections[0].Page.Landscape = false;
            /// allow user to dictate print settings
            richEditor.ShowPrintDialog();

            ///// dictate print settings
            //PrinterSettings printerSettings = new PrinterSettings();
            ///// Duplex.Vertical means the tops all align so you can flip the page over, side to side
            ///// Duplex.Horizontal means the tops and bottoms all align so you can flip the page over, top to bottom
            ///// vvv this has been disabled for now because it isn't wanted, but that's stupid...
            ////if (doDuplexPrinting && printerSettings.CanDuplex) printerSettings.Duplex = Duplex.Vertical;
            //printerSettings.DefaultPageSettings.Landscape = false;
            //richEditor.Print(printerSettings);
        }
        private void btnSendViaEmail_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                //string emailBody = richEditor.HtmlText;
                string emailBody = "Your requested point letter is attached to this email.<br><br><br><i>*** Please do not reply to this email as this inbox is not monitored. Email " + Properties.Settings.Default.AdminEmail + " for further assistance.</i><br><br><br>";
                MemoryStream stream = new MemoryStream();
                richEditor.ExportToPdf(stream);
                stream.Seek(0, SeekOrigin.Begin);
                string filenameandsubject = thisAction.PointValue.ToString() + " Point Letter for " + thisAction.EmployeeName + " (from " + thisAction.DateOfOccurrence.ToString("MM-dd-yyyy") + ")";
                System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(stream, "System.Net.Mime.MediaTypeNames.Application.Pdf");
                attachment.ContentDisposition.FileName = filenameandsubject + ".pdf";
                List<System.Net.Mail.Attachment> lAttachments = null;
                lAttachments = new List<System.Net.Mail.Attachment>();
                lAttachments.Add(attachment);

                atrack.SendEmail(new string[] { atrack.EMAILADDRESS },
                                 null,
                                 new string[] { Properties.Settings.Default.AdminEmail },
                                 filenameandsubject,
                                 emailBody,
                                 lAttachments,
                                 true);

                MessageBox.Show("This point letter has been emailed to " + atrack.EMAILADDRESS + "!",
                                "Letter Sent",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (Exception ex) { atrack.LogAndDisplayError(ex.ToString(), true); }
        }
    }
}