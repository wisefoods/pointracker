﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using DevExpress.XtraGrid.Views.Grid;
using System.IO;
using DevExpress.Pdf;
using DocumentFormat.OpenXml.Packaging;
using OpenXmlPowerTools;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.XtraRichEdit.Commands;
using System.Drawing.Printing;

namespace AttendanceTracker
{
    public partial class BonusAgreementTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;
        CurrentEmployee selectedEmployee;

        public BonusAgreementTemplate(Form1 pForm)
        {
            try
            {
                InitializeComponent();
                atrack = pForm;
                BindPageData();
                ResetPageControls(true);
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        public void BindPageData()
        {
            BindEmployeeList();
        }
        public void ResetPageControls(bool pFlag)
        {
            btnPrintDirectly.Enabled =
            btnSendViaEmail.Enabled =
            richEditor.Visible = !pFlag;
        }
        private void BindEmployeeList()
        {
            try
            {
                cbEmployeeList.Properties.Items.Clear();
                cbEmployeeList.Properties.Items.AddRange(atrack.MYEMPLOYEES_NICELIST);
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        public byte[] GenerateBonusAgreement(string pEmpID, string pEmpName, string pDept)
        {
            /// obtain the text from the underlying email template in .docx format
            string docText = "";
            try
            {
                /// LIFTED FROM WISEFORMS, THE BEAUTIFUL BEAST THAT IT IS

                /// fill a byte[] with all data from the template
                byte[] byteArray = Properties.Resources._2__or_3_for_1_Agreement_Template;
                ///
                /// this block is very important as it uses OpenXML PowerTools in order to properly convert the contents of the Word document into a string for editing
                /// 
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    /// fill this stream with data from the byte[]
                    memoryStream.Write(byteArray, 0, byteArray.Length);
                    /// open the stream as a .docx file
                    using (WordprocessingDocument doc = WordprocessingDocument.Open(memoryStream, true))
                    {
                        /// convert .docx to html, html to string
                        HtmlConverterSettings hcs = new HtmlConverterSettings();
                        System.Xml.Linq.XElement html = HtmlConverter.ConvertToHtml(doc, hcs);
                        /// from https://msdn.microsoft.com/en-us/library/office/ff628051(v=office.14).aspx regarding .ToStringNewLineOnAttributes()
                        /// This extension method serializes the XML with each attribute on its own line, which makes the XHTML easier to read when an element contains several attributes.
                        docText = html.ToStringNewLineOnAttributes();
                    }
                }

                string shift = "n/a";
                try { shift = atrack.dataset.CurrentEmployees.Where(z => z.EmployeeID == pEmpID).First().Shift.ToString(); }
                catch { }
                string namelong = pEmpName;

                docText = docText.Replace("{{NameLong}}", namelong);
                docText = docText.Replace("{{Shift}}", shift);
                docText = docText.Replace("{{Department}}", pDept);
                docText = docText.Replace("{{TodayDate}}", DateTime.Today.ToString("MM/dd/yyyy"));

                /// after replacing all fields...
                /// proceed to find and replace all instances of newlines with actual carriage returns
                /// this HTML <-> XML conversion makes all double newlines (<br><br>) into a single newline
                /// this is probably because whitespace is not being preserved, even though in the raw XML those properties are, indeed, set
                /// to get around this, the portions of the form which SHOULD be new lines are denoted by:
                /// SOME_CLASS_NAME\"> </span>
                /// the space there is where the newline *should* be
                /// this isn't perfect...but will probably work exactly as it should in 99% of instances
                docText = docText.Replace("\"> </span>", "\"><br /></span>");
                /// any of these will work...
                /// HTML newline: <br />
                /// Word newline: \r\n
                /// XML newline: &#13;&#10;
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }

            return Encoding.ASCII.GetBytes(docText);
        }

        /// Lifted from PointLetterTemplate.cs > BindPageData()
        public void BindBonusAgreement(byte[] pContent)
        {
            try
            {
                richEditor.ReadOnly = false;

                MemoryStream stream = new MemoryStream(pContent);
                stream.Seek(0, SeekOrigin.Begin);

                richEditor.LoadDocument(stream, DevExpress.XtraRichEdit.DocumentFormat.OpenXml, true);

                Document document = richEditor.Document;
                document.Unit = DevExpress.Office.DocumentUnit.Inch;
                document.Sections[0].Margins.Left =
                document.Sections[0].Margins.Top =
                document.Sections[0].Margins.Right =
                document.Sections[0].Margins.Bottom = 0.5f;

                /// replace {{PAGEBREAK}} with actual pagebreaks
                richEditor.Document.BeginUpdate();

                string pagebreak = "{{PAGEBREAK}}";
                ISearchResult searchResult = richEditor.Document.StartSearch(pagebreak, SearchOptions.CaseSensitive, SearchDirection.Forward, richEditor.Document.Range);

                while (searchResult.FindNext())
                {
                    searchResult.Replace(String.Empty);
                    richEditor.Document.CaretPosition = searchResult.CurrentResult.Start;
                    InsertPageBreakCommand cmd = new InsertPageBreakCommand(richEditor);
                    cmd.Execute();
                }

                //richEditor.Document.CaretPosition = richEditor.Document.Range.Start;
                //richEditor.ScrollToCaret();
                richEditor.VerticalScrollValue = 0;
                richEditor.Document.EndUpdate();
            }
            catch (Exception ex) { atrack.LogAndDisplayError(ex.ToString(), true); }
            finally { richEditor.ReadOnly = true; }
        }

        private void cbEmployeeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbEmployeeList.SelectedIndex < 0 || cbEmployeeList.Text == string.Empty) return;
                string id = atrack.GetEmployeeIDFromSelection(cbEmployeeList.SelectedItem.ToString());
                selectedEmployee = atrack.dataset.CurrentEmployees.Where(z => z.EmployeeID == id).First();

                DateTime oneYearAgoToday = DateTime.Today.AddYears(-1);
                if (atrack.dataset.TrackedBonuses.Any(z => z.EmployeeID == id &&
                                                           (z.BonusPayCode == "FORGIVENESS (2-FOR-1)" || z.BonusPayCode == "FORGIVENESS (3-FOR-1)") &&
                                                           z.BonusDate >= oneYearAgoToday))
                {
                    ResetPageControls(true);

                    DateTime dateOfLastBonus = atrack.dataset.TrackedBonuses.Where(z => z.EmployeeID == id &&
                                                                                        (z.BonusPayCode == "FORGIVENESS (2-FOR-1)" || z.BonusPayCode == "FORGIVENESS (3-FOR-1)") &&
                                                                                        z.BonusDate >= oneYearAgoToday)
                                                                            .First()
                                                                            .BonusDate;
                    MessageBox.Show(selectedEmployee.EmployeeName + " has received a 2/3-for-1 bonus within the past year (on " + dateOfLastBonus.ToShortDateString() + ").\n\nOnly one such bonus can be applied per rolling year!",
                                    "Unable To Generate Agreement Letter",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);

                    cbEmployeeList.Text = string.Empty;
                    selectedEmployee = null;
                }
                else
                {
                    atrack.ShowSplash("Please wait while the template is loaded");
                    ResetPageControls(false);

                    BindBonusAgreement(GenerateBonusAgreement(id, selectedEmployee.EmployeeName, selectedEmployee.Department));
                }
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
            finally { atrack.CloseSplash(); }
        }

        /// Taken directly from PointLetterTemplate.cs
        private void btnPrintDirectly_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            /// force the default setting in the print dialog to have a PORTRAIT ORIENTATION
            richEditor.Document.Sections[0].Page.Landscape = false;
            /// allow user to dictate print settings
            richEditor.ShowPrintDialog();
        }
        /// Taken directly from PointLetterTemplate.cs
        private void btnSendViaEmail_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                //string emailBody = richEditor.HtmlText;
                string emailBody = "Your requested 2/3-for-1 bonus agreement is attached to this email.<br><br><br><i>*** Please do not reply to this email as this inbox is not monitored. Email " + Properties.Settings.Default.AdminEmail + " for further assistance.</i><br><br><br>";
                MemoryStream stream = new MemoryStream();
                richEditor.ExportToPdf(stream);
                stream.Seek(0, SeekOrigin.Begin);
                string filenameandsubject = "2 or 3-for-1 Bonus Agreement for " + selectedEmployee.EmployeeName;
                System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(stream, "System.Net.Mime.MediaTypeNames.Application.Pdf");
                attachment.ContentDisposition.FileName = filenameandsubject + ".pdf";
                List<System.Net.Mail.Attachment> lAttachments = null;
                lAttachments = new List<System.Net.Mail.Attachment>();
                lAttachments.Add(attachment);

                atrack.SendEmail(new string[] { atrack.EMAILADDRESS },
                                 null,
                                 new string[] { Properties.Settings.Default.AdminEmail },
                                 filenameandsubject,
                                 emailBody,
                                 lAttachments,
                                 true);

                MessageBox.Show("This bonus agreement has been emailed to " + atrack.EMAILADDRESS + "!",
                                "Agreement Sent",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (Exception ex) { atrack.LogAndDisplayError(ex.ToString(), true); }
        }

        private void buttonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            cbEmployeeList.Text = string.Empty;
            selectedEmployee = null;
            richEditor.Text = string.Empty;

            ResetPageControls(true);
        }
    }
}
