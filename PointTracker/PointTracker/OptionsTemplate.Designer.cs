﻿namespace AttendanceTracker
{
    partial class OptionsTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionsTemplate));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            this.listYourDepartments = new DevExpress.XtraEditors.ListBoxControl();
            this.listAvailableDepartments = new DevExpress.XtraEditors.ListBoxControl();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnRemoveEmployee = new DevExpress.XtraEditors.ButtonEdit();
            this.btnAddEmployee = new DevExpress.XtraEditors.ButtonEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbSearchYourEmployees = new System.Windows.Forms.TextBox();
            this.tbSearchAvailableEmployees = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnClearSearchYourEmployees = new DevExpress.XtraEditors.ButtonEdit();
            this.btnClearSearchAvailableEmployees = new DevExpress.XtraEditors.ButtonEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.btnClearYourEmployeesSelections = new DevExpress.XtraEditors.ButtonEdit();
            this.btnClearAvailableEmployeesSelections = new DevExpress.XtraEditors.ButtonEdit();
            this.btnYourEmployeesSelectAll = new DevExpress.XtraEditors.ButtonEdit();
            this.btnAvailableEmployeesSelectAll = new DevExpress.XtraEditors.ButtonEdit();
            this.hLinkRuleHelp = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.label11 = new System.Windows.Forms.Label();
            this.checkedShifts = new System.Windows.Forms.CheckedListBox();
            this.separatorControl1 = new DevExpress.XtraEditors.SeparatorControl();
            ((System.ComponentModel.ISupportInitialize)(this.listYourDepartments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listAvailableDepartments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemoveEmployee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddEmployee.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClearSearchYourEmployees.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClearSearchAvailableEmployees.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClearYourEmployeesSelections.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClearAvailableEmployeesSelections.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnYourEmployeesSelectAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAvailableEmployeesSelectAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // listYourDepartments
            // 
            this.listYourDepartments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listYourDepartments.Location = new System.Drawing.Point(308, 103);
            this.listYourDepartments.Name = "listYourDepartments";
            this.listYourDepartments.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listYourDepartments.Size = new System.Drawing.Size(260, 312);
            this.listYourDepartments.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listYourDepartments.TabIndex = 0;
            // 
            // listAvailableDepartments
            // 
            this.listAvailableDepartments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listAvailableDepartments.Location = new System.Drawing.Point(688, 103);
            this.listAvailableDepartments.Name = "listAvailableDepartments";
            this.listAvailableDepartments.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listAvailableDepartments.Size = new System.Drawing.Size(260, 312);
            this.listAvailableDepartments.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listAvailableDepartments.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(304, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "Your Departments";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(684, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(196, 19);
            this.label4.TabIndex = 3;
            this.label4.Text = "Available Departments";
            // 
            // btnRemoveEmployee
            // 
            this.btnRemoveEmployee.EditValue = "Submit Feedback";
            this.btnRemoveEmployee.Location = new System.Drawing.Point(584, 273);
            this.btnRemoveEmployee.Name = "btnRemoveEmployee";
            this.btnRemoveEmployee.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold);
            this.btnRemoveEmployee.Properties.Appearance.Options.UseFont = true;
            this.btnRemoveEmployee.Properties.Appearance.Options.UseImage = true;
            this.btnRemoveEmployee.Properties.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            serializableAppearanceObject1.Font = new System.Drawing.Font("Tahoma", 16F);
            serializableAppearanceObject1.Options.UseFont = true;
            this.btnRemoveEmployee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnRemoveEmployee.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.btnRemoveEmployee.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.btnRemoveEmployee.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnRemoveEmployee.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnRemoveEmployee.Size = new System.Drawing.Size(90, 40);
            this.btnRemoveEmployee.TabIndex = 4;
            this.btnRemoveEmployee.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnRemoveEmployee_ButtonClick);
            // 
            // btnAddEmployee
            // 
            this.btnAddEmployee.EditValue = "Submit Feedback";
            this.btnAddEmployee.Location = new System.Drawing.Point(584, 175);
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold);
            this.btnAddEmployee.Properties.Appearance.Options.UseFont = true;
            this.btnAddEmployee.Properties.Appearance.Options.UseImage = true;
            this.btnAddEmployee.Properties.AutoHeight = false;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            serializableAppearanceObject5.Font = new System.Drawing.Font("Tahoma", 16F);
            serializableAppearanceObject5.Options.UseFont = true;
            this.btnAddEmployee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnAddEmployee.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.btnAddEmployee.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.btnAddEmployee.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAddEmployee.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnAddEmployee.Size = new System.Drawing.Size(90, 40);
            this.btnAddEmployee.TabIndex = 5;
            this.btnAddEmployee.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnAddEmployee_ButtonClick);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(582, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 32);
            this.label5.TabIndex = 6;
            this.label5.Text = "Add Selected Department(s)";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(575, 243);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 31);
            this.label6.TabIndex = 7;
            this.label6.Text = "Remove Selected Department(s)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(979, 22);
            this.panel1.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(979, 22);
            this.label7.TabIndex = 1;
            this.label7.Text = "   Shifts and departments that you select here will make up the employees that yo" +
    "u see within this application and any emails that it sends.";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.label8.Location = new System.Drawing.Point(308, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Search";
            // 
            // tbSearchYourEmployees
            // 
            this.tbSearchYourEmployees.Location = new System.Drawing.Point(380, 76);
            this.tbSearchYourEmployees.Name = "tbSearchYourEmployees";
            this.tbSearchYourEmployees.Size = new System.Drawing.Size(188, 21);
            this.tbSearchYourEmployees.TabIndex = 10;
            this.tbSearchYourEmployees.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TbSearchYourEmployees_KeyUp);
            // 
            // tbSearchAvailableEmployees
            // 
            this.tbSearchAvailableEmployees.Location = new System.Drawing.Point(760, 76);
            this.tbSearchAvailableEmployees.Name = "tbSearchAvailableEmployees";
            this.tbSearchAvailableEmployees.Size = new System.Drawing.Size(188, 21);
            this.tbSearchAvailableEmployees.TabIndex = 12;
            this.tbSearchAvailableEmployees.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TbSearchAvailableEmployees_KeyUp);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.label9.Location = new System.Drawing.Point(687, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Search";
            // 
            // btnClearSearchYourEmployees
            // 
            this.btnClearSearchYourEmployees.Location = new System.Drawing.Point(354, 76);
            this.btnClearSearchYourEmployees.Name = "btnClearSearchYourEmployees";
            this.btnClearSearchYourEmployees.Properties.Appearance.Options.UseImage = true;
            this.btnClearSearchYourEmployees.Properties.AutoHeight = false;
            editorButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions3.Image")));
            this.btnClearSearchYourEmployees.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnClearSearchYourEmployees.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnClearSearchYourEmployees.Properties.LookAndFeel.SkinName = "Sharp";
            this.btnClearSearchYourEmployees.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnClearSearchYourEmployees.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnClearSearchYourEmployees.Size = new System.Drawing.Size(25, 21);
            this.btnClearSearchYourEmployees.TabIndex = 13;
            this.btnClearSearchYourEmployees.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnClearSearchYourEmployees_ButtonClick);
            // 
            // btnClearSearchAvailableEmployees
            // 
            this.btnClearSearchAvailableEmployees.Location = new System.Drawing.Point(733, 76);
            this.btnClearSearchAvailableEmployees.Name = "btnClearSearchAvailableEmployees";
            this.btnClearSearchAvailableEmployees.Properties.Appearance.Options.UseImage = true;
            this.btnClearSearchAvailableEmployees.Properties.AutoHeight = false;
            editorButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions4.Image")));
            this.btnClearSearchAvailableEmployees.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnClearSearchAvailableEmployees.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnClearSearchAvailableEmployees.Properties.LookAndFeel.SkinName = "Sharp";
            this.btnClearSearchAvailableEmployees.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnClearSearchAvailableEmployees.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnClearSearchAvailableEmployees.Size = new System.Drawing.Size(23, 21);
            this.btnClearSearchAvailableEmployees.TabIndex = 14;
            this.btnClearSearchAvailableEmployees.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnClearSearchAvailableEmployees_ButtonClick);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label10);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 22);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(979, 22);
            this.panel2.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(979, 22);
            this.label10.TabIndex = 1;
            this.label10.Text = "   Note: If you do not select any shifts or departments here then you will not se" +
    "e any employees nor will you receive any emailed reports!";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnClearYourEmployeesSelections
            // 
            this.btnClearYourEmployeesSelections.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearYourEmployeesSelections.EditValue = "Clear Selections";
            this.btnClearYourEmployeesSelections.Location = new System.Drawing.Point(308, 448);
            this.btnClearYourEmployeesSelections.Name = "btnClearYourEmployeesSelections";
            this.btnClearYourEmployeesSelections.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnClearYourEmployeesSelections.Properties.Appearance.Options.UseFont = true;
            this.btnClearYourEmployeesSelections.Properties.Appearance.Options.UseImage = true;
            this.btnClearYourEmployeesSelections.Properties.Appearance.Options.UseTextOptions = true;
            this.btnClearYourEmployeesSelections.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnClearYourEmployeesSelections.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.btnClearYourEmployeesSelections.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnClearYourEmployeesSelections.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.btnClearYourEmployeesSelections.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnClearYourEmployeesSelections.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.btnClearYourEmployeesSelections.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnClearYourEmployeesSelections.Properties.AutoHeight = false;
            editorButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions5.Image")));
            editorButtonImageOptions5.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnClearYourEmployeesSelections.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnClearYourEmployeesSelections.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnClearYourEmployeesSelections.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.btnClearYourEmployeesSelections.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnClearYourEmployeesSelections.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnClearYourEmployeesSelections.Size = new System.Drawing.Size(260, 21);
            this.btnClearYourEmployeesSelections.TabIndex = 16;
            this.btnClearYourEmployeesSelections.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnClearYourEmployeesSelections_ButtonClick);
            this.btnClearYourEmployeesSelections.MouseUp += new System.Windows.Forms.MouseEventHandler(this.BtnClearYourEmployeesSelections_MouseUp);
            // 
            // btnClearAvailableEmployeesSelections
            // 
            this.btnClearAvailableEmployeesSelections.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearAvailableEmployeesSelections.EditValue = "Clear Selections";
            this.btnClearAvailableEmployeesSelections.Location = new System.Drawing.Point(688, 448);
            this.btnClearAvailableEmployeesSelections.Name = "btnClearAvailableEmployeesSelections";
            this.btnClearAvailableEmployeesSelections.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnClearAvailableEmployeesSelections.Properties.Appearance.Options.UseFont = true;
            this.btnClearAvailableEmployeesSelections.Properties.Appearance.Options.UseImage = true;
            this.btnClearAvailableEmployeesSelections.Properties.Appearance.Options.UseTextOptions = true;
            this.btnClearAvailableEmployeesSelections.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnClearAvailableEmployeesSelections.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.btnClearAvailableEmployeesSelections.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnClearAvailableEmployeesSelections.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.btnClearAvailableEmployeesSelections.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnClearAvailableEmployeesSelections.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.btnClearAvailableEmployeesSelections.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnClearAvailableEmployeesSelections.Properties.AutoHeight = false;
            editorButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions6.Image")));
            editorButtonImageOptions6.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnClearAvailableEmployeesSelections.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnClearAvailableEmployeesSelections.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnClearAvailableEmployeesSelections.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.btnClearAvailableEmployeesSelections.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnClearAvailableEmployeesSelections.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnClearAvailableEmployeesSelections.Size = new System.Drawing.Size(260, 21);
            this.btnClearAvailableEmployeesSelections.TabIndex = 17;
            this.btnClearAvailableEmployeesSelections.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnClearAvailableEmployeesSelections_ButtonClick);
            this.btnClearAvailableEmployeesSelections.MouseUp += new System.Windows.Forms.MouseEventHandler(this.BtnClearAvailableEmployeesSelections_MouseUp);
            // 
            // btnYourEmployeesSelectAll
            // 
            this.btnYourEmployeesSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnYourEmployeesSelectAll.EditValue = "Select All";
            this.btnYourEmployeesSelectAll.Location = new System.Drawing.Point(308, 421);
            this.btnYourEmployeesSelectAll.Name = "btnYourEmployeesSelectAll";
            this.btnYourEmployeesSelectAll.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnYourEmployeesSelectAll.Properties.Appearance.Options.UseFont = true;
            this.btnYourEmployeesSelectAll.Properties.Appearance.Options.UseImage = true;
            this.btnYourEmployeesSelectAll.Properties.Appearance.Options.UseTextOptions = true;
            this.btnYourEmployeesSelectAll.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnYourEmployeesSelectAll.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.btnYourEmployeesSelectAll.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnYourEmployeesSelectAll.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.btnYourEmployeesSelectAll.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnYourEmployeesSelectAll.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.btnYourEmployeesSelectAll.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnYourEmployeesSelectAll.Properties.AutoHeight = false;
            editorButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions7.Image")));
            editorButtonImageOptions7.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnYourEmployeesSelectAll.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnYourEmployeesSelectAll.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnYourEmployeesSelectAll.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.btnYourEmployeesSelectAll.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnYourEmployeesSelectAll.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnYourEmployeesSelectAll.Size = new System.Drawing.Size(260, 21);
            this.btnYourEmployeesSelectAll.TabIndex = 18;
            this.btnYourEmployeesSelectAll.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnYourEmployeesSelectAll_ButtonClick);
            this.btnYourEmployeesSelectAll.MouseUp += new System.Windows.Forms.MouseEventHandler(this.BtnYourEmployeesSelectAll_MouseUp);
            // 
            // btnAvailableEmployeesSelectAll
            // 
            this.btnAvailableEmployeesSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAvailableEmployeesSelectAll.EditValue = "Select All";
            this.btnAvailableEmployeesSelectAll.Location = new System.Drawing.Point(688, 421);
            this.btnAvailableEmployeesSelectAll.Name = "btnAvailableEmployeesSelectAll";
            this.btnAvailableEmployeesSelectAll.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnAvailableEmployeesSelectAll.Properties.Appearance.Options.UseFont = true;
            this.btnAvailableEmployeesSelectAll.Properties.Appearance.Options.UseImage = true;
            this.btnAvailableEmployeesSelectAll.Properties.Appearance.Options.UseTextOptions = true;
            this.btnAvailableEmployeesSelectAll.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnAvailableEmployeesSelectAll.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.btnAvailableEmployeesSelectAll.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnAvailableEmployeesSelectAll.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.btnAvailableEmployeesSelectAll.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnAvailableEmployeesSelectAll.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.btnAvailableEmployeesSelectAll.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnAvailableEmployeesSelectAll.Properties.AutoHeight = false;
            editorButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions8.Image")));
            editorButtonImageOptions8.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnAvailableEmployeesSelectAll.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnAvailableEmployeesSelectAll.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnAvailableEmployeesSelectAll.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.btnAvailableEmployeesSelectAll.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAvailableEmployeesSelectAll.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnAvailableEmployeesSelectAll.Size = new System.Drawing.Size(260, 21);
            this.btnAvailableEmployeesSelectAll.TabIndex = 19;
            this.btnAvailableEmployeesSelectAll.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BtnAvailableEmployeesSelectAll_ButtonClick);
            this.btnAvailableEmployeesSelectAll.MouseUp += new System.Windows.Forms.MouseEventHandler(this.BtnAvailableEmployeesSelectAll_MouseUp);
            // 
            // hLinkRuleHelp
            // 
            this.hLinkRuleHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.hLinkRuleHelp.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.hLinkRuleHelp.Appearance.ForeColor = System.Drawing.Color.Purple;
            this.hLinkRuleHelp.Appearance.Options.UseFont = true;
            this.hLinkRuleHelp.Appearance.Options.UseForeColor = true;
            this.hLinkRuleHelp.Appearance.Options.UseTextOptions = true;
            this.hLinkRuleHelp.AppearanceHovered.ForeColor = System.Drawing.Color.Orange;
            this.hLinkRuleHelp.AppearanceHovered.Options.UseForeColor = true;
            this.hLinkRuleHelp.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.hLinkRuleHelp.Location = new System.Drawing.Point(12, 477);
            this.hLinkRuleHelp.LookAndFeel.SkinName = "Sharp";
            this.hLinkRuleHelp.LookAndFeel.UseDefaultLookAndFeel = false;
            this.hLinkRuleHelp.Name = "hLinkRuleHelp";
            this.hLinkRuleHelp.Size = new System.Drawing.Size(522, 19);
            this.hLinkRuleHelp.TabIndex = 21;
            this.hLinkRuleHelp.Text = "Click here for help with creating new email rules within Outlook";
            this.hLinkRuleHelp.Click += new System.EventHandler(this.HLinkRuleHelp_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(8, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 19);
            this.label11.TabIndex = 22;
            this.label11.Text = "Your Shifts";
            // 
            // checkedShifts
            // 
            this.checkedShifts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.checkedShifts.CheckOnClick = true;
            this.checkedShifts.FormattingEnabled = true;
            this.checkedShifts.Items.AddRange(new object[] {
            "1st",
            "2nd",
            "3rd"});
            this.checkedShifts.Location = new System.Drawing.Point(12, 79);
            this.checkedShifts.Name = "checkedShifts";
            this.checkedShifts.Size = new System.Drawing.Size(260, 388);
            this.checkedShifts.TabIndex = 23;
            this.checkedShifts.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedShifts_ItemCheck);
            // 
            // separatorControl1
            // 
            this.separatorControl1.LineOrientation = System.Windows.Forms.Orientation.Vertical;
            this.separatorControl1.Location = new System.Drawing.Point(278, 53);
            this.separatorControl1.Name = "separatorControl1";
            this.separatorControl1.Size = new System.Drawing.Size(22, 418);
            this.separatorControl1.TabIndex = 24;
            // 
            // OptionsTemplate
            // 
            this.ClientSize = new System.Drawing.Size(979, 504);
            this.Controls.Add(this.btnAddEmployee);
            this.Controls.Add(this.btnRemoveEmployee);
            this.Controls.Add(this.separatorControl1);
            this.Controls.Add(this.checkedShifts);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.hLinkRuleHelp);
            this.Controls.Add(this.btnAvailableEmployeesSelectAll);
            this.Controls.Add(this.btnYourEmployeesSelectAll);
            this.Controls.Add(this.btnClearAvailableEmployeesSelections);
            this.Controls.Add(this.btnClearYourEmployeesSelections);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnClearSearchAvailableEmployees);
            this.Controls.Add(this.btnClearSearchYourEmployees);
            this.Controls.Add(this.tbSearchAvailableEmployees);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbSearchYourEmployees);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.listAvailableDepartments);
            this.Controls.Add(this.listYourDepartments);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "OptionsTemplate";
            ((System.ComponentModel.ISupportInitialize)(this.listYourDepartments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listAvailableDepartments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemoveEmployee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddEmployee.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnClearSearchYourEmployees.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClearSearchAvailableEmployees.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnClearYourEmployeesSelections.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClearAvailableEmployeesSelections.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnYourEmployeesSelectAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAvailableEmployeesSelectAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraGrid.GridControl gcEmployeeEvents;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.ListBoxControl listYourDepartments;
        private DevExpress.XtraEditors.ListBoxControl listAvailableDepartments;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.ButtonEdit btnRemoveEmployee;
        private DevExpress.XtraEditors.ButtonEdit btnAddEmployee;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbSearchYourEmployees;
        private System.Windows.Forms.TextBox tbSearchAvailableEmployees;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.ButtonEdit btnClearSearchYourEmployees;
        private DevExpress.XtraEditors.ButtonEdit btnClearSearchAvailableEmployees;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.ButtonEdit btnClearYourEmployeesSelections;
        private DevExpress.XtraEditors.ButtonEdit btnClearAvailableEmployeesSelections;
        private DevExpress.XtraEditors.ButtonEdit btnYourEmployeesSelectAll;
        private DevExpress.XtraEditors.ButtonEdit btnAvailableEmployeesSelectAll;
        private DevExpress.XtraEditors.HyperlinkLabelControl hLinkRuleHelp;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckedListBox checkedShifts;
        private DevExpress.XtraEditors.SeparatorControl separatorControl1;
    }
}