﻿namespace AttendanceTracker
{
    partial class HelpTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.recHelpDoc = new DevExpress.XtraRichEdit.RichEditControl();
            this.SuspendLayout();
            // 
            // recHelpDoc
            // 
            this.recHelpDoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recHelpDoc.Location = new System.Drawing.Point(0, 0);
            this.recHelpDoc.Name = "recHelpDoc";
            this.recHelpDoc.ReadOnly = true;
            this.recHelpDoc.Size = new System.Drawing.Size(927, 457);
            this.recHelpDoc.TabIndex = 0;
            // 
            // HelpTemplate
            // 
            this.ClientSize = new System.Drawing.Size(927, 457);
            this.Controls.Add(this.recHelpDoc);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "HelpTemplate";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraGrid.GridControl gcEmployeeEvents;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraRichEdit.RichEditControl recHelpDoc;
    }
}