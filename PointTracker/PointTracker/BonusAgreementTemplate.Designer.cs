﻿namespace AttendanceTracker
{
    partial class BonusAgreementTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BonusAgreementTemplate));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            this.cbEmployeeList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnSendViaEmail = new DevExpress.XtraEditors.ButtonEdit();
            this.btnPrintDirectly = new DevExpress.XtraEditors.ButtonEdit();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.buttonEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.richEditor = new DevExpress.XtraRichEdit.RichEditControl();
            ((System.ComponentModel.ISupportInitialize)(this.cbEmployeeList.Properties)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSendViaEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrintDirectly.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbEmployeeList
            // 
            this.cbEmployeeList.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbEmployeeList.EditValue = "";
            this.cbEmployeeList.Location = new System.Drawing.Point(26, 13);
            this.cbEmployeeList.Name = "cbEmployeeList";
            this.cbEmployeeList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEmployeeList.Properties.DropDownRows = 15;
            this.cbEmployeeList.Properties.LookAndFeel.SkinName = "Sharp";
            this.cbEmployeeList.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbEmployeeList.Properties.Sorted = true;
            this.cbEmployeeList.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEmployeeList.Size = new System.Drawing.Size(217, 20);
            this.cbEmployeeList.TabIndex = 0;
            this.cbEmployeeList.SelectedIndexChanged += new System.EventHandler(this.cbEmployeeList_SelectedIndexChanged);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnSendViaEmail);
            this.panel8.Controls.Add(this.btnPrintDirectly);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 45);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(243, 412);
            this.panel8.TabIndex = 13;
            // 
            // btnSendViaEmail
            // 
            this.btnSendViaEmail.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSendViaEmail.EditValue = "Generate Point Letter";
            this.btnSendViaEmail.Location = new System.Drawing.Point(0, 50);
            this.btnSendViaEmail.Name = "btnSendViaEmail";
            this.btnSendViaEmail.Properties.Appearance.BackColor = System.Drawing.Color.Green;
            this.btnSendViaEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnSendViaEmail.Properties.Appearance.Options.UseBackColor = true;
            this.btnSendViaEmail.Properties.Appearance.Options.UseFont = true;
            this.btnSendViaEmail.Properties.Appearance.Options.UseImage = true;
            this.btnSendViaEmail.Properties.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            editorButtonImageOptions1.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            editorButtonImageOptions1.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            serializableAppearanceObject1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            serializableAppearanceObject1.Options.UseFont = true;
            serializableAppearanceObject1.Options.UseTextOptions = true;
            serializableAppearanceObject1.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnSendViaEmail.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "   Send to me via email", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnSendViaEmail.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnSendViaEmail.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.btnSendViaEmail.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSendViaEmail.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnSendViaEmail.Size = new System.Drawing.Size(243, 40);
            this.btnSendViaEmail.TabIndex = 17;
            this.btnSendViaEmail.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnSendViaEmail_ButtonClick);
            // 
            // btnPrintDirectly
            // 
            this.btnPrintDirectly.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPrintDirectly.EditValue = "Generate Point Letter";
            this.btnPrintDirectly.Location = new System.Drawing.Point(0, 10);
            this.btnPrintDirectly.Name = "btnPrintDirectly";
            this.btnPrintDirectly.Properties.Appearance.BackColor = System.Drawing.Color.Green;
            this.btnPrintDirectly.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnPrintDirectly.Properties.Appearance.Options.UseBackColor = true;
            this.btnPrintDirectly.Properties.Appearance.Options.UseFont = true;
            this.btnPrintDirectly.Properties.Appearance.Options.UseImage = true;
            this.btnPrintDirectly.Properties.AutoHeight = false;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            editorButtonImageOptions2.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            editorButtonImageOptions2.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            serializableAppearanceObject5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            serializableAppearanceObject5.Options.UseFont = true;
            serializableAppearanceObject5.Options.UseTextOptions = true;
            serializableAppearanceObject5.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnPrintDirectly.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "       Print directly", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnPrintDirectly.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPrintDirectly.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.btnPrintDirectly.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnPrintDirectly.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnPrintDirectly.Size = new System.Drawing.Size(243, 40);
            this.btnPrintDirectly.TabIndex = 18;
            this.btnPrintDirectly.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnPrintDirectly_ButtonClick);
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(243, 10);
            this.panel9.TabIndex = 15;
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(243, 12);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(20, 445);
            this.panel5.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 12, 0, 0);
            this.panel1.Size = new System.Drawing.Size(263, 457);
            this.panel1.TabIndex = 20;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.cbEmployeeList);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.panel4);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 12);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(243, 33);
            this.panel7.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Location = new System.Drawing.Point(26, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Select An Employee";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.buttonEdit1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(26, 33);
            this.panel4.TabIndex = 4;
            // 
            // buttonEdit1
            // 
            this.buttonEdit1.Location = new System.Drawing.Point(1, 13);
            this.buttonEdit1.Name = "buttonEdit1";
            this.buttonEdit1.Properties.Appearance.Options.UseImage = true;
            this.buttonEdit1.Properties.AutoHeight = false;
            editorButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions3.Image")));
            this.buttonEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEdit1.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.buttonEdit1.Properties.LookAndFeel.SkinName = "Sharp";
            this.buttonEdit1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.buttonEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.buttonEdit1.Size = new System.Drawing.Size(25, 20);
            this.buttonEdit1.TabIndex = 10;
            this.buttonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEdit1_ButtonClick);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.richEditor);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(263, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(664, 457);
            this.panel2.TabIndex = 21;
            // 
            // richEditor
            // 
            this.richEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richEditor.Location = new System.Drawing.Point(0, 0);
            this.richEditor.Name = "richEditor";
            this.richEditor.Options.Annotations.ShowAllAuthors = false;
            this.richEditor.Options.Behavior.ShowPopupMenu = DevExpress.XtraRichEdit.DocumentCapability.Hidden;
            this.richEditor.Options.HorizontalRuler.ShowLeftIndent = false;
            this.richEditor.Options.HorizontalRuler.ShowRightIndent = false;
            this.richEditor.Options.HorizontalRuler.ShowTabs = false;
            this.richEditor.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this.richEditor.Options.Hyperlinks.ShowToolTip = false;
            this.richEditor.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this.richEditor.ShowCaretInReadOnly = false;
            this.richEditor.Size = new System.Drawing.Size(664, 457);
            this.richEditor.TabIndex = 1;
            // 
            // BonusAgreementTemplate
            // 
            this.ClientSize = new System.Drawing.Size(927, 457);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BonusAgreementTemplate";
            ((System.ComponentModel.ISupportInitialize)(this.cbEmployeeList.Properties)).EndInit();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnSendViaEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrintDirectly.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraGrid.GridControl gcEmployeeEvents;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.ComboBoxEdit cbEmployeeList;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraRichEdit.RichEditControl richEditor;
        private DevExpress.XtraEditors.ButtonEdit btnSendViaEmail;
        private DevExpress.XtraEditors.ButtonEdit btnPrintDirectly;
    }
}