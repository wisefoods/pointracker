﻿namespace AttendanceTracker
{
    partial class FeedbackTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FeedbackTemplate));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.rtbFeedback = new System.Windows.Forms.RichTextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.cbTabNameForAttachment = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ratingByUser = new DevExpress.XtraEditors.RatingControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSubmitFeedback = new DevExpress.XtraEditors.ButtonEdit();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbTabNameForAttachment.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ratingByUser.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSubmitFeedback.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel12);
            this.panel1.Controls.Add(this.panel11);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(927, 457);
            this.panel1.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.rtbFeedback);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(20, 59);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(887, 302);
            this.panel6.TabIndex = 6;
            // 
            // rtbFeedback
            // 
            this.rtbFeedback.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbFeedback.Location = new System.Drawing.Point(0, 0);
            this.rtbFeedback.Margin = new System.Windows.Forms.Padding(10);
            this.rtbFeedback.Name = "rtbFeedback";
            this.rtbFeedback.Size = new System.Drawing.Size(887, 300);
            this.rtbFeedback.TabIndex = 0;
            this.rtbFeedback.Text = "";
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 300);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(887, 2);
            this.panel7.TabIndex = 6;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label4);
            this.panel12.Controls.Add(this.cbTabNameForAttachment);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(20, 36);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(887, 23);
            this.panel12.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(673, 23);
            this.label4.TabIndex = 4;
            this.label4.Text = "Attach a screenshot from this tab (optional):";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbTabNameForAttachment
            // 
            this.cbTabNameForAttachment.Dock = System.Windows.Forms.DockStyle.Right;
            this.cbTabNameForAttachment.EditValue = "";
            this.cbTabNameForAttachment.Location = new System.Drawing.Point(673, 0);
            this.cbTabNameForAttachment.Name = "cbTabNameForAttachment";
            this.cbTabNameForAttachment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTabNameForAttachment.Properties.DropDownRows = 15;
            this.cbTabNameForAttachment.Properties.ImmediatePopup = true;
            this.cbTabNameForAttachment.Properties.LookAndFeel.SkinName = "Sharp";
            this.cbTabNameForAttachment.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbTabNameForAttachment.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTabNameForAttachment.Size = new System.Drawing.Size(214, 20);
            this.cbTabNameForAttachment.TabIndex = 3;
            this.cbTabNameForAttachment.MouseEnter += new System.EventHandler(this.cbTabNameForAttachment_MouseEnter);
            // 
            // panel11
            // 
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(907, 36);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(20, 325);
            this.panel11.TabIndex = 5;
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(0, 36);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(20, 325);
            this.panel10.TabIndex = 4;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label5);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 18);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(927, 18);
            this.panel5.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(927, 18);
            this.label5.TabIndex = 1;
            this.label5.Text = "Don\'t forget to click Submit Feedback when you\'re finished!";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(927, 18);
            this.panel4.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(927, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "Enter some feedback, good or bad, below. You can also vote from 1 to 10 using the" +
    " hearts on the bottom.";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ratingByUser);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 361);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(927, 96);
            this.panel3.TabIndex = 2;
            // 
            // ratingByUser
            // 
            this.ratingByUser.Dock = System.Windows.Forms.DockStyle.Top;
            this.ratingByUser.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ratingByUser.Location = new System.Drawing.Point(0, 0);
            this.ratingByUser.Name = "ratingByUser";
            this.ratingByUser.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.ratingByUser.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ratingByUser.Properties.Appearance.Options.UseFont = true;
            this.ratingByUser.Properties.Appearance.Options.UseForeColor = true;
            this.ratingByUser.Properties.Appearance.Options.UseTextOptions = true;
            this.ratingByUser.Properties.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.ratingByUser.Properties.AutoHeight = false;
            this.ratingByUser.Properties.AutoSize = false;
            this.ratingByUser.Properties.ItemCount = 10;
            this.ratingByUser.Properties.LookAndFeel.SkinName = "Valentine";
            this.ratingByUser.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ratingByUser.Properties.ShowText = true;
            this.ratingByUser.Properties.TextLocation = DevExpress.XtraEditors.Repository.RatingTextLocation.Top;
            this.ratingByUser.Properties.TextToRatingIndent = -999;
            this.ratingByUser.Rating = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ratingByUser.Size = new System.Drawing.Size(927, 50);
            this.ratingByUser.TabIndex = 1;
            this.ratingByUser.Text = "Rate this app from 1 to 10";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnSubmitFeedback);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 411);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(927, 46);
            this.panel2.TabIndex = 2;
            // 
            // btnSubmitFeedback
            // 
            this.btnSubmitFeedback.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSubmitFeedback.EditValue = "Submit Feedback";
            this.btnSubmitFeedback.Location = new System.Drawing.Point(0, 0);
            this.btnSubmitFeedback.Name = "btnSubmitFeedback";
            this.btnSubmitFeedback.Properties.Appearance.BackColor = System.Drawing.Color.Green;
            this.btnSubmitFeedback.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold);
            this.btnSubmitFeedback.Properties.Appearance.Options.UseBackColor = true;
            this.btnSubmitFeedback.Properties.Appearance.Options.UseFont = true;
            this.btnSubmitFeedback.Properties.Appearance.Options.UseImage = true;
            this.btnSubmitFeedback.Properties.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            editorButtonImageOptions1.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            editorButtonImageOptions1.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            serializableAppearanceObject1.BackColor = System.Drawing.Color.Green;
            serializableAppearanceObject1.Font = new System.Drawing.Font("Tahoma", 16F);
            serializableAppearanceObject1.ForeColor = System.Drawing.Color.White;
            serializableAppearanceObject1.Options.UseBackColor = true;
            serializableAppearanceObject1.Options.UseFont = true;
            serializableAppearanceObject1.Options.UseForeColor = true;
            this.btnSubmitFeedback.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, " Submit Feedback", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnSubmitFeedback.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.btnSubmitFeedback.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.btnSubmitFeedback.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSubmitFeedback.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnSubmitFeedback.Size = new System.Drawing.Size(927, 46);
            this.btnSubmitFeedback.TabIndex = 0;
            this.btnSubmitFeedback.Click += new System.EventHandler(this.BtnSubmitFeedback_Click);
            // 
            // FeedbackTemplate
            // 
            this.ClientSize = new System.Drawing.Size(927, 457);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FeedbackTemplate";
            this.panel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbTabNameForAttachment.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ratingByUser.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnSubmitFeedback.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraGrid.GridControl gcEmployeeEvents;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.ButtonEdit btnSubmitFeedback;
        private DevExpress.XtraEditors.RatingControl ratingByUser;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RichTextBox rtbFeedback;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.ComboBoxEdit cbTabNameForAttachment;
    }
}