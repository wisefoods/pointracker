﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraSplashScreen;

namespace AttendanceTracker
{
    public partial class SplashScreen1 : SplashScreen
    {
        public SplashScreen1()
        {
            InitializeComponent();
        }

        #region Overrides

        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
            SplashScreenCommand command = (SplashScreenCommand)cmd;
            if (command == SplashScreenCommand.ChangeText)
            {
                string msg = arg.ToString();
                if (msg == null || msg == string.Empty) labelStatus.Text = "Loading content" + "...";
                else labelStatus.Text = arg.ToString() + "...";
            }
        }

        #endregion

        public enum SplashScreenCommand
        {
            ChangeText
        }
    }
}