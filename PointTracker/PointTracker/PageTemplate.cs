﻿using DevExpress.Data;
using DevExpress.DataAccess.EntityFramework;
using DevExpress.XtraBars;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace AttendanceTracker
{
    public partial class PageTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;
        TabFormPage thisPageName;

        bool CERIDIANDATASHOWN = false;

        bool? VIEWACTIVEONLY = true;
        CurrentEmployee SELECTEDEMPLOYEE;
        DateTime? SELECTEDDATE_FROM = null;
        DateTime? SELECTEDDATE_TO = null;
        DateTime EmployeeDayBeforeEarliestWorkDate;

        bool ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS = true;
        DataTable ANALYZEPOINTS_DATATABLE_CACHE = null;

        bool GRIDVIEW1PROPSSET = false;
        bool GRIDVIEW2PROPSSET = false;
        bool VIEWALLDATAPROPSSET = false;

        bool LEGEND_SHOWN = false;
        bool EMPLOYEEINFO_SHOWN = false;
        static string TITLE_ViewEmployeeData_Default = "Make a Selection";
        string TITLE_ViewEmployeeData = TITLE_ViewEmployeeData_Default;
        string TITLE_ViewAllEmployees = "View All Employees";

        /// a list of paycodes which break attendance streaks
        /// see CODES section for a breakdown on how these are determined
        /// write in UPPER CASE (ctrl+shift+u)
        public List<string> ATTENDANCEBREAKINGPAYCODES_LIST = new string[] { "ADA ABSENT",
                                                                             "BRV",
                                                                             "E-OUT",
                                                                             "EXCUSED ABSENCE",
                                                                             "FMLA ABSENT",
                                                                             "FMLA REFUSED OVERTIME",
                                                                             "FMLA VACATION",
                                                                             "L-IN",
                                                                             "NO CALL/NO SHOW",
                                                                             "REFUSED OVERTIME",
                                                                             "SICK",
                                                                             "SICK ABSENT",
                                                                             "SUSPENSION WITHOUT PAY",
                                                                             "UNEXCUSED",
                                                                             "VOLUNTEER REFUSED OT" }.ToList();
        /// a list of paycodes which carry attendance streaks
        /// these are pulled out of thin air
        /// write in UPPER CASE (ctrl+shift+u)
        public List<string> ATTENDANCECARRYINGPAYCODES_LIST = new string[] { "BRK",
                                                                             "MEAL",
                                                                             "SU",
                                                                             "WRK" }.ToList();
        /// special rule; company-wide COVID-19 forgiveness dates
        /// REMOVED absences (the ones listed above) during these dates will still break an attendance streak
        /// per HR        
        DateTime COVID_FORGIVENESS_START = new DateTime(2020, 3, 23);
        DateTime COVID_FORGIVENESS_END = new DateTime(2020, 5, 31);
        List<DateTime> COVID_FORGIVENESS_DAYS = new List<DateTime>();
        string COVID_PLACEHOLDER_TEXT = "Placeholder for COVID-19 forgiveness provided to all employees.";

        /// cache data for faster loading
        IQueryable<DataSource> TABLEENTRIES = null;
        List<DataSource> TABLEENTRIES_LIST = new List<DataSource>();    /// this is the ONLY _LIST that utilizes a deep vs. shallow copy
                                                                        /// everything else is bound to its datasource
        double employeeBonusCounter = 0.00;
        List<TrackedBonus> listConsumedBonusesForEmployee = new List<TrackedBonus>();
        public PageTemplate(Form1 pForm, TabFormPage pPage)
        {
            try
            {
                InitializeComponent();

                /// eh? ehhhhh....
                btnRefresh.Visible = false;
                /// COVID-19 provision, add placeholder text to list of attendance breaking pay codes
                /// use .ToUpper()!
                ATTENDANCEBREAKINGPAYCODES_LIST.Add(COVID_PLACEHOLDER_TEXT.ToUpper());

                atrack = pForm;
                thisPageName = pPage;
                BindPageData();

                /// set splitter and screen properties
                //atrack.AutoScrollMinSize =
                atrack.MinimumSize = new Size(atrack.lblLogo.Location.X + (atrack.lblLogo.Width * 2),
                                              atrack.tabFormControl1.Height + 100 + panelTopSelectors.Height + panelAnalysis.Height + btnShowCeridianData.Height + panelFMLA.Height);
                /// the 100 used ^^^ is an overestimation of the size of the tab of this pane, where the feedback button is
                splitContainer1.SplitterDistance = panelTopSelectors.Location.Y + panelTopSelectors.Height;
                splitContainer1.Panel1.AutoScrollMinSize =
                splitContainer1.Panel2.AutoScrollMinSize = new Size(0, 150);

                /// date properties
                deListByPayDate_From.Properties.MinValue = deListByPayDate_To.Properties.MinValue = atrack.Date_StartOfDataCollectionInDB;
                deListByPayDate_From.Properties.MaxValue = deListByPayDate_To.Properties.MaxValue = DateTime.Today;

                /// hide initial areas
                HideCeridianData();
                ShowPointPanels(false);

                if (atrack.RUNNINGAUTOMATED)
                {
                    if (atrack.CONSOLEMESSAGE == "UpdateData")
                    {
                        /// start Tasks process
                        UpdateProcess taskslogentry = atrack.dataset.UpdateProcesses.Where(z => z.ProcessName == "Tasks").First();
                        taskslogentry.Running = true;
                        taskslogentry.LastRun = DateTime.Now;
                        atrack.dataset.SaveChanges();

                        UpdateCurrentEmployeeTable();                               /// insert any new employees into the CurrentEmployees table so their .LastPointLevelActedOn value can be tracked                        
                        PopulateHighPointsAndActionsToTakeTables(-1);               /// generate HighPoints DB entries + actions data + FMLA data for all employees

                        /// update the update log
                        UpdateLog tasksLog = new UpdateLog();
                        tasksLog.Type = "Tasks";
                        tasksLog.ServerUpdateDate = DateTime.Now;
                        atrack.dataset.UpdateLogs.Add(tasksLog);
                        atrack.dataset.SaveChanges();

                        /// stop Tasks process                        
                        taskslogentry.Running = false;
                        taskslogentry.LastFinished = DateTime.Now;
                        atrack.dataset.SaveChanges();
                    }
                    else if (atrack.CONSOLEMESSAGE == "SendEmails")
                    {
                        /// start Emails process
                        UpdateProcess emailslogentry = atrack.dataset.UpdateProcesses.Where(z => z.ProcessName == "Emails").First();
                        emailslogentry.Running = true;
                        emailslogentry.LastRun = DateTime.Now;
                        atrack.dataset.SaveChanges();

                        SendActionsRequiredEmails(false, true);                     /// send point letter reminder emails
                        SendActionsRequiredEmails(true, true);                      /// send missed point letter notification emails
                        SendFMLANotificationEmails();                               /// send emails for FMLA violations (ensure PopulateHighPointsAndActionsToTakeTables(X, Y, true, #) was run)
                        SendPADNotificationEmails();                                /// send emails for Perfect Attendance Day (PAD) awards
                        SendReportEmails();                                         /// send emails which contains reports

                        /// update the update log
                        UpdateLog emailLog = new UpdateLog();
                        emailLog.Type = "Emails";
                        emailLog.ServerUpdateDate = DateTime.Now;
                        atrack.dataset.UpdateLogs.Add(emailLog);
                        atrack.dataset.SaveChanges();

                        /// stop Emails process
                        emailslogentry.Running = false;
                        emailslogentry.LastFinished = DateTime.Now;
                        atrack.dataset.SaveChanges();
                    }

                    this.Close();
                }
                else
                {
                    /// focus main selector by default
                    cbEmployeeList.Focus();
                }
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }
        private void ShowPointPanels(bool pFlag) { panelBonuses.Visible = panelFMLA.Visible = panelAnalysis.Visible = pFlag; }
        private void LoadEmployeeFMLAStatus()
        {
            if (atrack.FMLA_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID))
            {
                FMLANotification fmla = atrack.FMLA_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID).First();
                lblFMLATotalHours.Text = (fmla.FMLANonVacationHoursTotal + fmla.FMLAVacationHoursTotal).ToString("N2");
                lblFMLANonVacationHours.Text = fmla.FMLANonVacationHoursTotal.ToString("N2");
                lblFMLAVacationHours.Text = fmla.FMLAVacationHoursTotal.ToString("N2");
            }            
        }
        public void BindPageData()
        {
            try
            {
                BindEmployeeList();
                SetupViewAllData();

                if (!atrack.RUNNINGAUTOMATED)
                {
                    /// just show task update times; leave Ceridian out of it
                    //DateTime updateDate = atrack.LASTCERIDIANUPDATE;
                    //labelCeridianUpdateDate.Text = "Data from: " + updateDate.ToString();
                    DateTime lasttasksupdate = atrack.LASTTASKSUPDATE;
                    labelCeridianUpdateDate.Text = "Data from: " + lasttasksupdate.ToString();
                    /// re-select this employee to pickup the latest changes (if any)
                    object selectedItem = cbEmployeeList.SelectedItem;
                    if (selectedItem != null && selectedItem.ToString() != string.Empty)
                    {
                        cbEmployeeList.SelectedIndex = -1;
                        cbEmployeeList.SelectedIndex = cbEmployeeList.Properties.Items.IndexOf(selectedItem);
                    }
                    /// or clear it out
                    /// this can occur when changing selected departments (via Options)
                    else
                    {
                        cbEmployeeList.Text = string.Empty;
                        /// reset page name
                        if (thisPageName.Text != "AdminTasks" && thisPageName.Text != TITLE_ViewAllEmployees) thisPageName.Text = TITLE_ViewEmployeeData = TITLE_ViewEmployeeData_Default;
                        BindEmployeeEntries();
                    }
                }
            }
            catch (Exception ex)
            {
                if (atrack.UPDATEACTIVE) { MessageBox.Show("Please close the page you are on and try again.", "Data Update Failed", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                else { atrack.LogAndDisplayError(ex.ToString(), true); }
            }
        }

        private void SetupViewAllData()
        {
            GridView thisGrid = gcHighPoints.MainView as GridView;
            DateTime lasttasksupdate = atrack.LASTTASKSUPDATE;
            labelLastUpdate.Text = "Data from: " + lasttasksupdate.ToString();

            gcHighPoints.DataSource = null;
            //thisGrid.Columns.Clear();
            if (atrack.ADMINOPTION_DONOTIGNOREOBSOLETEPOINTS)
            {
                gcHighPoints.DataSource = atrack.dataset.HighPoints
                                                        .Where(z => z.Pay_Date >= atrack.Date_StartOfDataCollectionInDB &&
                                                                    atrack.MYEMPLOYEES_IDS.Contains(z.EmployeeID))
                                                        .OrderBy(z => z.Name).ThenBy(z => z.index)
                                                        .ToList();
            }
            else
            {
                gcHighPoints.DataSource = atrack.dataset.HighPoints
                                                        .Where(z => z.Pay_Date >= atrack.Date_StartOfDataCollectionInDB &&
                                                                    z.Pay_Date >= atrack.Date_StartOfRollingYear &&
                                                                    atrack.MYEMPLOYEES_IDS.Contains(z.EmployeeID))
                                                        .OrderBy(z => z.Name).ThenBy(z => z.index)
                                                        .ToList();
            }

            if (!VIEWALLDATAPROPSSET)
            {
                thisGrid.Columns["ID"].Visible = false;
                thisGrid.Columns["index"].Visible = false;
                thisGrid.Columns["Name"].Visible = false;
                thisGrid.Columns["EmployeeID"].Visible = false;

                GridColumn col = new GridColumn();
                col.UnboundType = DevExpress.Data.UnboundColumnType.String;
                col.UnboundExpression = "[Name] + ' (#' + [EmployeeID] + ')'";
                col.Name = col.FieldName = "NameAndID";
                col.Caption = "Employee";
                col.VisibleIndex = 0;
                col.SortOrder = ColumnSortOrder.Ascending;
                thisGrid.Columns.Add(col);

                thisGrid.Columns["Net_Hours"].Caption = "Net Hours";
                thisGrid.Columns["Net_Hours"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                thisGrid.Columns["Net_Hours"].DisplayFormat.FormatString = "n2";

                thisGrid.Columns["Pay_Date"].Caption = "Pay Date";
                thisGrid.Columns["Pay_Code"].Caption = "Pay Code";

                /// this still results in the names being sorted reverse-alphabetically, because of the .Descending sort order
                /// below, but that is the only way I've found to properly order the Points value in descending order as well
                thisGrid.Columns["NameAndID"].GroupIndex = 0;
                GridSummaryItem summaryItem = thisGrid.GroupSummary.Add(SummaryItemType.Sum, "Points", null, "{0} points");
                thisGrid.GroupSummarySortInfo.Add(summaryItem, ColumnSortOrder.Descending, thisGrid.Columns["NameAndID"]);

                //thisGrid.OptionsView.GroupFooterShowMode = GroupFooterShowMode.VisibleAlways;
                //GridGroupSummaryItem summaryItem = new GridGroupSummaryItem();
                //summaryItem.FieldName = "Points";
                //summaryItem.ShowInGroupColumnFooter = thisGrid.Columns["Points"];
                //summaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
                //summaryItem.DisplayFormat = "{0} point(s)";
                //thisGrid.GroupSummary.Add(summaryItem);
                ////GridColumn firstGroupColumn = thisGrid.SortInfo[0].Column;
                ///// vvv this used to use ColumnSortOrder.Descending but it reverses the order of employee names, too, which is odd
                ///// so, just do it by points and then by name + date, in ascending order
                //GroupSummarySortInfo[] groupSummaryToSort = { new GroupSummarySortInfo(summaryItem, thisGrid.Columns["NameAndID"], ColumnSortOrder.Ascending) };
                //thisGrid.GroupSummarySortInfo.ClearAndAddRange(groupSummaryToSort);

                VIEWALLDATAPROPSSET = true;
            }

            thisGrid.OptionsView.BestFitMode = GridBestFitMode.Fast;
            thisGrid.OptionsView.BestFitMaxRowCount = 100;
            thisGrid.BestFitColumns();
            thisGrid.FocusedRowHandle = -1;
        }

        public void PopulateHighPointsAndActionsToTakeTables(int pProcessThisIndex)
        {
            try
            {
                /// point levels
                List<double> pointThresholds = atrack.REPRIMANDS_LIST.Select(z => z.AtPointValue).ToList();
                pointThresholds.Sort();
                /// track when to clear ^^^ that list
                string lastEmployeeIDSelected = string.Empty;
                ///// all high points data, so it can be added and saved all at once
                ///// vvv removed in favor of saving entries as they are created as it is messing up 3rd shift when these updates are processed
                //List<HighPoint> lHighPointsToAdd = new List<HighPoint>();
                /// all fmla data, so it can be added and saved all at once
                List<FMLANotification> lFMLAToAdd = new List<FMLANotification>();
                List<FMLANotification> lFMLAToRemove = new List<FMLANotification>();
                /// date this employee was re-hired (if applicable)
                DateTime rehireDate = atrack.Date_EarliestPossible;
                /// a list of actions which were missed; used to track multiple occurrences on the same day (special case)
                List<ActionsTaken> missedActionsForThisEmployee = new List<ActionsTaken>();
                List<ActionsTaken> missedActionsForThisEmployee_Processed = new List<ActionsTaken>();

                ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS = true;
                int highPointsEntryIndex = 0;
                int i;
                int loopTo;
                if (pProcessThisIndex == -1)
                {
                    highPointsEntryIndex = 0;
                    i = 0;
                    loopTo = cbEmployeeList.Properties.Items.Count;
                }
                else
                {
                    highPointsEntryIndex = atrack.dataset.HighPoints.OrderByDescending(z => z.index).First().index + 1000;
                    i = pProcessThisIndex;
                    loopTo = pProcessThisIndex + 1;
                }
                for (; i < loopTo; i++)
                {
                    /// adjust dataset before selecting employee (for the first time) and beginning process                        
                    string temp_empID_forAdjustingDB = atrack.GetEmployeeIDFromSelection(cbEmployeeList.Properties.Items[i].ToString());
                    if (lastEmployeeIDSelected != temp_empID_forAdjustingDB)
                    {
                        lastEmployeeIDSelected = temp_empID_forAdjustingDB;

                        /// refresh the entity context
                        atrack.RefreshEntity();
                        /// ensure the following cached lists are updated for this specific employee --
                        /// ACTIONSTAKEN_LIST
                        atrack.BindActionsTakenData(temp_empID_forAdjustingDB);
                        /// ADJUSTMENTS_LIST
                        atrack.BindManualAdjustmentsData(temp_empID_forAdjustingDB);
                        /// BONUSES_LIST
                        atrack.BindBonusesData(temp_empID_forAdjustingDB);
                        /// PENALTIES_LIST
                        atrack.BindPenaltiesData(temp_empID_forAdjustingDB);
                        /// clear list of missed actions for this employee
                        missedActionsForThisEmployee.Clear();
                        missedActionsForThisEmployee_Processed.Clear();
                        /// reset employee's bonus counter
                        employeeBonusCounter = 0.00;

                        /// date this employee was re-hired (if applicable)
                        if (atrack.dataset.Rehires.Any(z => z.EmployeeID == temp_empID_forAdjustingDB))
                        {
                            rehireDate = atrack.dataset.Rehires.Where(z => z.EmployeeID == temp_empID_forAdjustingDB).First().RehireDate;
                        }
                        else rehireDate = atrack.Date_EarliestPossible;

                        /// remove old OBSOLETE entries so new OBSOLETE entries can replace them (if there are any)
                        /// "obsolete" entries are those which have been made redundant by a point letter which came before OR after (with some limitations)
                        /// it but was bumped due to some changes, either because of bonuses being given, or adjustments being made, etc.
                        /// these are removed and rebuilt and are therefore somewhat ethereal, but still important
                        if (atrack.ACTIONSTAKEN_LIST.Any(z => z.EmployeeID == temp_empID_forAdjustingDB &&
                                                              z.ActionTaken &&
                                                              z.IsLocked))
                        {
                            IEnumerable<ActionsTaken> listToRemove = atrack.ACTIONSTAKEN_LIST.Where(z => z.EmployeeID == temp_empID_forAdjustingDB &&
                                                                                                         z.ActionTaken &&
                                                                                                         z.IsLocked);
                            atrack.dataset.ActionsTakens.RemoveRange(listToRemove);
                            List<Guid> listIDsToRemove = listToRemove.Select(z => z.ID).ToList();
                            foreach (Guid entry in listIDsToRemove) { atrack.ACTIONSTAKEN_LIST.Remove(atrack.ACTIONSTAKEN_LIST.Where(z => z.ID == entry).First()); }
                        }
                        /// also remove all pending point letters
                        if (atrack.ACTIONSTAKEN_LIST.Any(z => z.EmployeeID == temp_empID_forAdjustingDB &&
                                                              !z.ActionTaken &&
                                                              !z.IsLocked &&
                                                              z.DateOfLapse == null))
                        {
                            IEnumerable<ActionsTaken> listToRemove = atrack.ACTIONSTAKEN_LIST.Where(z => z.EmployeeID == temp_empID_forAdjustingDB &&
                                                                                                         !z.ActionTaken &&
                                                                                                         !z.IsLocked &&
                                                                                                         z.DateOfLapse == null);
                            atrack.dataset.ActionsTakens.RemoveRange(listToRemove);
                            List<Guid> listIDsToRemove = listToRemove.Select(z => z.ID).ToList();
                            foreach (Guid entry in listIDsToRemove) { atrack.ACTIONSTAKEN_LIST.Remove(atrack.ACTIONSTAKEN_LIST.Where(z => z.ID == entry).First()); }
                        }

                        /// save/update employee cache
                        atrack.dataset.SaveChanges();
                         
                        /// High Points cleanups
                        /// remove all current entries for the selected employee
                        atrack.dataset.HighPoints.RemoveRange(atrack.dataset.HighPoints.Where(z => z.EmployeeID == lastEmployeeIDSelected));
                        atrack.dataset.SaveChanges();
                    }

                    /// begin process by selecting next employee
                    cbEmployeeList.SelectedIndex = -1;
                    cbEmployeeList.SelectedIndex = i;
                    ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS = true;

                    /// for the loop(s) ahead
                    Dictionary<double, DateTime> listPointThresholdsReached = new Dictionary<double, DateTime>();

                    /// point accumulation and action creation
                    double currentPointValue = 0.0;
                    for (int k = 0; k < gridView2.RowCount; k++)
                    {
                        double points = 0.00;
                        DateTime paydate = Convert.ToDateTime(gridView2.GetRowCellValue(k, "Pay Date").ToString());
                        if (atrack.ADMINOPTION_DONOTIGNOREOBSOLETEPOINTS || paydate >= atrack.Date_StartOfRollingYear)
                        {
                            points = Convert.ToDouble(gridView2.GetRowCellValue(k, "Points").ToString().Replace(" ", ""));
                        }
                        currentPointValue += points;                        
                        if (currentPointValue < 0) currentPointValue = 0;

                        /// remove any pending reached-thresholds which are greater than the current point value
                        /// only process if the change in points is negative
                        /// this means YOU CAN ONLY HAVE PENDING ACTIONS REMOVED BY GOING DOWN IN POINTS
                        if (points < 0 && pointThresholds.Any(z => z > currentPointValue))
                        {
                            foreach (double threshold in pointThresholds.Where(z => z > currentPointValue))
                            {
                                if (listPointThresholdsReached.ContainsKey(threshold))
                                {
                                    listPointThresholdsReached.Remove(threshold);
                                }
                            }
                        }

                        /// add pending reached-thresholds
                        /// only process if the change in points is positive
                        /// this means that YOU CAN ONLY ACHIEVE A NEW POINT LEVEL BY HAVING POINTS ADDED
                        else if (points > 0 && pointThresholds.Any(z => z <= currentPointValue))
                        {
                            foreach (double threshold in pointThresholds.Where(z => z <= currentPointValue))
                            {
                                /// THIS WILL FAVOR THE FIRST-ADDED OFFENSE FOR REACHING A POINT LEVEL
                                /// rather than replacing that entry with the more-recent occurrence
                                if (!listPointThresholdsReached.ContainsKey(threshold))
                                {
                                    listPointThresholdsReached.Add(threshold, paydate);
                                }
                            }
                        }
                    }

                    /// .LastPointLevelActedOn
                    /// super very ultra crucial; re-set this employee's lowest-verified point level/value
                    /// if this employee's current point total is less than the one in the db
                    /// this value can be increased from with TakeActionTemplate
                    /// this value can be increased (here, but elsewhere) or decreased (here)
                    /// testing lastPointLevelActedOnForThisEmployee for null was added to avoid an error from occurring that has since been resolved
                    /// kept here just in case (though it's not an ideal situation; need to determine how this is hit (or if it even still is?))
                    if (SELECTEDEMPLOYEE != null &&
                        currentPointValue < SELECTEDEMPLOYEE.LastPointLevelActedOn)
                    {
                        SELECTEDEMPLOYEE.LastPointLevelActedOn = currentPointValue;
                        atrack.dataset.SaveChanges();
                        /// vvv unnecessary as .LastPointLevelActedOn is always read from the server
                        //atrack.BindCurrentEmployeesData();
                    }

                    bool stopLoop = false;
                    /// actions to take
                    foreach (var thing in listPointThresholdsReached.OrderBy(z => z.Key))
                    {
                        /// loop vals
                        double pointValue = thing.Key;
                        DateTime dateOfOccurrence = thing.Value;
                        /// determine if this is referencing a point letter which should be locked
                        /// this can occur when historical data has been changed or adjusted
                        DateTime? cutoff = FindXCompleteWorkDaysAFTERDateForThisDataSource(14,
                                                                                           atrack.datasource_datelimited.Where(z => z.EmployeeEmploymentStatus_EmployeeNumber == SELECTEDEMPLOYEE.EmployeeID).ToList(),
                                                                                           dateOfOccurrence,
                                                                                           false);
                        bool actionHasExpired;
                        if (cutoff != null && cutoff.GetValueOrDefault().Date < atrack.Date_LatestPossible) actionHasExpired = true;
                        else actionHasExpired = false;

                        /// new action has to be inserted as one does not exist for this
                        /// employee, point value, and date
                        /// or...multiple point letters were to be given on the same day
                        /// this can occur when a penalty is added to an absence and the point letter is then missed
                        /// that is what spurred this addition, anyway
                        /// this 'freepass' can be utilized once per missed point letter per day
                        bool freepass = false;
                        foreach (ActionsTaken missed in missedActionsForThisEmployee)
                        {
                            /// each missed point letter can be used once for re-entry
                            if (missedActionsForThisEmployee_Processed.Contains(missed)) continue;

                            /// if these values are the same then permit this action to be created regardless of other conditions
                            if (missed.PointValue == pointValue &&
                                missed.DateOfOccurrence == dateOfOccurrence)
                            {
                                freepass = true;
                                missedActionsForThisEmployee_Processed.Add(missed);
                                break;
                            }
                        }
                        /// check against existing point letters
                        if (freepass || 
                            atrack.ACTIONSTAKEN_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                                              z.PointValue == pointValue &&
                                                              z.DateOfOccurrence == dateOfOccurrence) == false)
                        {
                            /// current point value is high enough according to the pending action 
                            if (currentPointValue >= pointValue)
                            {
                                /// add action as "current", set rest of properties in conditions below

                                ///////////////////////////////////////////////////////////////////////////////////////////////////
                                /// special rule by HR where a point letter may only be awarded after an absence following the
                                /// previous point letter's .PointLetterDistributedOn date
                                ///
                                /// determine which point letter to find before this one
                                int pointValToFind = 0;
                                /// if this is the 3/first letter then there is nothing to compare against
                                if (pointValue > 3) 
                                {
                                    /// otherwise it is the point letter which precedes the current one
                                    if (pointValue == 6) pointValToFind = 3;
                                    else if (pointValue == 9) pointValToFind = 6;
                                    else if (pointValue == 11) pointValToFind = 9;
                                    else if (pointValue == 12) pointValToFind = 11;

                                    /// determine when that point letter was actually delivered
                                    try
                                    {
                                        DateTime? prevAction_DeliveredOn = atrack.ACTIONSTAKEN_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                                                                                               z.PointValue == pointValToFind &&
                                                                                                               z.ActionTaken &&
                                                                                                               !z.IsLocked &&
                                                                                                               z.DateOfLapse == null)
                                                                                                   .OrderByDescending(z => z.PointLetterDistributedOn)
                                                                                                   .First()
                                                                                                   .PointLetterDistributedOn;
                                        DateTime dateToTest = Convert.ToDateTime(prevAction_DeliveredOn);

                                        /// lifted from elsewhere to include all pay codes which warrant an infraction
                                        List<string> ALL_entails = atrack.PAYCODES_LIST.Where(z => z.Considered).Select(z => z.Code.Trim().ToUpper()).ToList();
                                        /// if there has not been any absences after the last point letter delivery date
                                        /// then there have not been any absences between then and now, and the loop should stop

                                        if (!TABLEENTRIES_LIST.Any(z => z.PayDate.Date > dateToTest.Date &&
                                                                        ALL_entails.Contains(z.PayAdjCode_ShortName.Trim().ToUpper())))
                                        {
                                            break;
                                        }
                                    }
                                    /// do nothing if no match found for dateToTest
                                    catch { }
                                    /// bump point letter .DateOfOccurrence to be the date of this employee's next absence
                                    /// this works HOWEVER it will currently apply to every next point letter...
                                    /// in order for this to work actually correctly it would need to be done on the 
                                    /// condition that a point letter was given which included enough points to
                                    /// bump this employee to the next point tier
                                    //else
                                    //{
                                    //    /// the DATE OF THIS POINT LETTER BECOMES THE DATE ON WHICH THIS EMPLOYEE MISSED THEIR NEXT DAY OF WORK!!!
                                    //    DateTime nextAbsence = TABLEENTRIES_LIST.Where(z => z.PayDate > dateToTest &&
                                    //                                                        ALL_entails.Contains(z.PayAdjCode_ShortName))
                                    //                                            .OrderBy(z => z.PayDate)
                                    //                                            .First()
                                    //                                            .PayDate;
                                    //    if (atrack.ACTIONSTAKEN_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                    //                                          z.PointValue == pointValue &&
                                    //                                          z.DateOfOccurrence == nextAbsence)) continue;
                                    //    else dateOfOccurrence = nextAbsence;
                                    //}
                                }
                                ///////////////////////////////////////////////////////////////////////////////////////////////////

                                string reprimand_new = atrack.REPRIMANDS_LIST.Where(z => z.AtPointValue == pointValue).FirstOrDefault().Action;
                                ActionsTaken actionToTake_new = new ActionsTaken();
                                /// record this ID so it doesn't get deleted later
                                Guid id = Guid.NewGuid();
                                actionToTake_new.ID = id;
                                actionToTake_new.EmployeeID = SELECTEDEMPLOYEE.EmployeeID;
                                actionToTake_new.EmployeeName = SELECTEDEMPLOYEE.EmployeeName;
                                actionToTake_new.Department = SELECTEDEMPLOYEE.Department;
                                actionToTake_new.DateOfOccurrence = dateOfOccurrence;
                                actionToTake_new.DateOfLapse = null;
                                actionToTake_new.PointValue = pointValue;
                                actionToTake_new.ActionToTake = reprimand_new;
                                actionToTake_new.ActionTaken = false;                   /// FALSE, and
                                actionToTake_new.IsLocked = false;                      /// FALSE for "current"

                                /// action is "obsolete" meaning that it's point value does not exceed .LastPointLevelActedOn
                                /// and that this point letter already came up IN THE PAST and was handled
                                /// OR it came up IN THE FUTURE but the action itself was resolved BEFORE this event's cutoff date (important!)
                                if (pointValue <= SELECTEDEMPLOYEE.LastPointLevelActedOn &&
                                                  atrack.ACTIONSTAKEN_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                                                                    z.PointValue == pointValue &&
                                                                                    (z.DateOfOccurrence < dateOfOccurrence ||
                                                                                    (z.PointLetterDistributedOn != null && cutoff != null && z.PointLetterDistributedOn.GetValueOrDefault().Date <= cutoff.GetValueOrDefault().Date)) &&
                                                                                    z.DateOfOccurrence.Date >= rehireDate.Date &&
                                                                                    z.ActionTaken &&
                                                                                    !z.IsLocked))
                                {
                                    actionToTake_new.ActionTaken = true;                /// TRUE, and
                                    actionToTake_new.IsLocked = true;                   /// TRUE for "obsolete"                                             
                                }
                                /// action is "current" but is also "expired" (or missed)
                                else if (actionHasExpired)
                                {
                                    actionToTake_new.IsLocked = true;                   /// TRUE and SETDATE for "expired"
                                    actionToTake_new.DateOfLapse = Convert.ToDateTime(cutoff).AddDays(1);
                                    if (!freepass) missedActionsForThisEmployee.Add(actionToTake_new);
                                    else missedActionsForThisEmployee_Processed.Add(actionToTake_new);
                                }

                                atrack.dataset.ActionsTakens.Add(actionToTake_new);
                                atrack.dataset.SaveChanges();                               /// actions are saved as they are created
                                //atrack.BindActionsTakenData(SELECTEDEMPLOYEE.EmployeeID); /// re-bind local cache of actions for this employee
                                atrack.ACTIONSTAKEN_LIST.Add(actionToTake_new);             /// ...or, add it to the cache manually                                       

                                                                                            /// this likely does not need to be rerun if a point letter was not obsolete or missed
                                                                                            /// however, AnalyzePoints() *would* need to be called again (once) at the end of this process
                                                                                            /// because it is unlikely that more than 1 or 2 point letters would be active at a time (with a max of 5)
                                                                                            /// it probably isn't worth it to do that
                                ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS = false;            /// re-process this employee
                                if (!ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS) break;        /// and do not continue parsing any more actions or anything else                                           
                            }
                        }
                        /// action exists already (the state of .ActionTaken and .IsLocked don't matter)
                        else
                        {
                            IEnumerable<ActionsTaken> lActions_Pending = atrack.ACTIONSTAKEN_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                                                                                             z.PointValue == pointValue &&
                                                                                                             z.DateOfOccurrence == dateOfOccurrence);
                            foreach (ActionsTaken existingAction in lActions_Pending.OrderBy(z => z.PointValue).ThenBy(z => z.DateOfOccurrence))
                            {
                                /// is expired but is not reflected that way yet
                                /// find and lock/lapse all findings which have not been taken but are also not locked
                                /// (on the condition that this new action has already expired)
                                if (actionHasExpired &&
                                    !existingAction.IsLocked &&
                                    existingAction.DateOfLapse == null &&
                                    !existingAction.ActionTaken)
                                {
                                    DateTime dateOfLapse = Convert.ToDateTime(cutoff).AddDays(1);
                                    /// mark ON THE SERVER as .IsLocked and provide a .DateOfLapse for each action entry
                                    foreach (ActionsTaken onServer in atrack.dataset.ActionsTakens.Where(z => z.ID == existingAction.ID))
                                    {
                                        onServer.IsLocked = true;
                                        onServer.DateOfLapse = dateOfLapse;
                                    }
                                    atrack.dataset.SaveChanges();
                                    /// do the same for local entries
                                    foreach (ActionsTaken local in atrack.ACTIONSTAKEN_LIST.Where(z => z.ID == existingAction.ID))
                                    {
                                        local.IsLocked = true;
                                        local.DateOfLapse = dateOfLapse;
                                    }

                                    /// trigger reloop
                                    ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS = false;
                                }
                                /// has already expired
                                /// do nothing
                                /// (on the condition that this new action has already expired)
                                else if (actionHasExpired &&
                                         existingAction.IsLocked &&
                                         existingAction.DateOfLapse != null &&
                                         !existingAction.ActionTaken)
                                { }
                                /// has been taken and is not locked or lapsed
                                else if (existingAction.ActionTaken &&
                                         !existingAction.IsLocked &&
                                         existingAction.DateOfLapse == null)
                                { }
                                /// action is just chillin
                                /// do not process any more points letters beyond this point
                                /// this is to get around HR's dumb rule of needing an employee to miss another day 
                                /// before they are eligible for their new point level tier
                                else if(!actionHasExpired &&
                                        !existingAction.ActionTaken &&
                                        !existingAction.IsLocked &&
                                        existingAction.DateOfLapse == null)
                                {
                                    stopLoop = true;
                                    break;
                                }
                            }
                            /// re-process this employee with updated actions cache
                            if (!ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS) break;
                            /// do not process any more point letters but do not re-loop
                            else if (stopLoop) break;
                        }
                        /// re-process this employee with updated actions cache
                        if (!ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS) break;
                    }

                    /// fmla
                    if (ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS)
                    {
                        DateTime fmlaHitOn = DateTime.Now;

                        /// this comparison checks against non-vacation FMLA entries
                        /// (vacation FMLA is used after this 32 hour mark has been reached, per HR, but the data does not really reflect that(?))
                        /// technically this should also check for 5 separate occurrences having been used
                        /// but Paulette Williams in HR indicated that it would be unreliable to base
                        /// this information on separate FMLA entries because it is more prone to error
                        /// when entering this data into Ceridian

                        double fmlatotal_nonvacation;
                        double fmlatotal_vacation;
                        /// data coming in from HR can never be trusted...!
                        /// sometimes numbers aren't numbers, so test for that
                        if (double.TryParse(lblFMLANonVacationHours.Text, out fmlatotal_nonvacation) &&
                            double.TryParse(lblFMLAVacationHours.Text, out fmlatotal_vacation))
                        {
                            SELECTEDEMPLOYEE.FMLA_NonVacation = fmlatotal_nonvacation;
                            SELECTEDEMPLOYEE.FMLA_Vacation = fmlatotal_vacation;
                            if (fmlatotal_nonvacation >= 32.0)
                            {
                                /// add new entry
                                if (atrack.FMLA_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID) == false)
                                {
                                    FMLANotification fmla = new FMLANotification();
                                    fmla.ID = Guid.NewGuid();
                                    fmla.EmployeeID = SELECTEDEMPLOYEE.EmployeeID;
                                    fmla.EmployeeName = SELECTEDEMPLOYEE.EmployeeName;
                                    fmla.FMLANonVacationHoursCounter = 32;
                                    fmla.FMLANonVacationHoursTotal = fmlatotal_nonvacation;
                                    fmla.FMLAVacationHoursTotal = fmlatotal_vacation;
                                    fmla.FMLANonVacationHoursTotalHitOn = fmlaHitOn;
                                    fmla.Notified = false;
                                    //fmla.NotifiedOn = set later
                                    //atrack.dataset.FMLANotifications.Add(fmla);
                                    lFMLAToAdd.Add(fmla);
                                }
                                /// otherwise this user already generated a notification...
                                /// but, still update the FMLA totals
                                else
                                {
                                    /// obtain existing entry and mark it for removal
                                    IEnumerable<FMLANotification> notifications = atrack.FMLA_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID);
                                    lFMLAToRemove.AddRange(notifications);
                                    FMLANotification theNotification = notifications.First();
                                    /// modify entry and mark it to be (re-)added
                                    theNotification.FMLANonVacationHoursTotal = fmlatotal_nonvacation;
                                    theNotification.FMLAVacationHoursTotal = fmlatotal_vacation;
                                    lFMLAToAdd.Add(theNotification);
                                }
                            }
                            else
                            {
                                /// remove any entries since this employee has less than the required FMLA hour amount
                                if (atrack.FMLA_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID) == true)
                                {
                                    IQueryable<FMLANotification> fmla = atrack.dataset.FMLANotifications.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID);
                                    //atrack.dataset.FMLANotifications.RemoveRange(fmla);
                                    lFMLAToRemove.AddRange(fmla);
                                }
                            }
                        }
                        else
                        {
                            SELECTEDEMPLOYEE.FMLA_NonVacation =
                            SELECTEDEMPLOYEE.FMLA_Vacation = 0;

                            /// remove any entries since this employee has less than the required FMLA hour amount
                            if (atrack.FMLA_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID) == true)
                            {
                                IQueryable<FMLANotification> fmla = atrack.dataset.FMLANotifications.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID);
                                //atrack.dataset.FMLANotifications.RemoveRange(fmla);
                                lFMLAToRemove.AddRange(fmla);
                            }
                        }
                    }

                    /// high point/all employee view
                    /// this will only ever be entered once
                    if (ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS)
                    {
                        /// insert records into HighPoints
                        for (int k = 0; k < gridView2.RowCount; k++)
                        {
                            string paycode = gridView2.GetRowCellValue(k, "Pay Code").ToString();
                            DateTime paydate = Convert.ToDateTime(gridView2.GetRowCellValue(k, "Pay Date").ToString());
                            double points = Convert.ToDouble(gridView2.GetRowCellValue(k, "Points").ToString().Replace(" ", ""));
                            double nethours = Convert.ToDouble((gridView2.GetRowCellValue(k, "Net Hours").ToString() == string.Empty ? "0" : gridView2.GetRowCellValue(k, "Net Hours").ToString()));
                            string description = gridView2.GetRowCellValue(k, "Description").ToString();

                            HighPoint entry = new HighPoint();
                            entry.ID = Guid.NewGuid();
                            entry.index = highPointsEntryIndex++;
                            entry.EmployeeID = SELECTEDEMPLOYEE.EmployeeID;
                            entry.Name = SELECTEDEMPLOYEE.EmployeeName;
                            entry.Department = SELECTEDEMPLOYEE.Department;
                            entry.Pay_Date = paydate;
                            entry.Pay_Code = paycode;
                            entry.Net_Hours = nethours;
                            entry.Description = description;
                            entry.Points = points;
                            atrack.dataset.HighPoints.Add(entry); 
                            //lHighPointsToAdd.Add(entry);
                        }
                        /// save changes after for-loop
                        atrack.dataset.SaveChanges();
                    }

                    /// re-loop this employee if something happened which requires further processing
                    if (!ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS) --i;
                }

                /// cleanups and saves after all employees have been parsed 
                atrack.RefreshEntity();
                try
                {
                    /////////////// vvv HighPoints are now saved as they are calculated
                    /////////////// initial cleanups are now performed up top, where the data binds take place per newly-selected employee
                    /////////////// this ensures that any user changes made while an update is processing do not get overwritten during this process
                    /////////////// the cleanup of entries in this table which do not belong to a current employee was moved to be within UpdateCurrentEmployeeTable()

                    //if (pProcessThisIndex == -1) atrack.dataset.Database.ExecuteSqlCommand("TRUNCATE TABLE [dbo].[HighPoints]");
                    //else atrack.dataset.HighPoints.RemoveRange(atrack.dataset.HighPoints.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID));
                    //atrack.dataset.HighPoints.AddRange(lHighPointsToAdd);
                    //atrack.dataset.SaveChanges();
                }
                catch (Exception ex)
                {
                    atrack.LogAndDisplayError(ex.ToString(), true);
                }
                try
                {
                    atrack.dataset.FMLANotifications.RemoveRange(lFMLAToRemove);
                    atrack.dataset.SaveChanges();
                    atrack.dataset.FMLANotifications.AddRange(lFMLAToAdd);
                    atrack.dataset.SaveChanges();
                }
                catch (Exception ex)
                {
                    //atrack.LogAndDisplayError(ex.ToString(), true);
                }

                if (pProcessThisIndex == -1) atrack.BindActionsTakenData();
                else atrack.BindActionsTakenData(SELECTEDEMPLOYEE.EmployeeID);
                atrack.BindFMLAData();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        public void UpdateCurrentEmployeeTable()
        {
            try
            {
                /// this is the barrier-to-entry, so to speak, of getting data from ceridian into this app
                /// if an employee is not within this list then their data will not be reflected
                /// this eliminates supervisors, agency employees, salaried employees, etc.
                /// this also ensures only Berwick employees are considered

                /// bind necessary data
                atrack.BindHeadcountData();
                atrack.BindShiftRotationsData();
                atrack.BindOrgChartData();

                DateTime now = DateTime.Now;
                /// default is name - id, strip out ids
                List<string> paytypes = new List<string>();
                paytypes.Add("Hourly");
                paytypes.Add("Hourly 1776");
                paytypes.Add("Hourly Teamsters");
                List<string> dumblistOfIDsFromHeadcountReport = atrack.API_HEADCOUNT_LIST.Where(z => z.OrgUnit_ShortName.Contains("Berwick") &&
                                                                                                     z.PayGroup_ShortName != "Agency Employees" &&
                                                                                                     z.DeptJob_ShortName != "Distribution Driver" &&
                                                                                                     z.UnionNonUnionNonUnionOperations != "Non Union" &&
                                                                                                     z.UnionNonUnionNonUnionOperations != "Non Union Operations" &&  /// does not actually appear in the data but the column name indicates that it could
                                                                                                     paytypes.Contains(z.PayType_LongName))
                                                                                         /// too aggressive; removed
                                                                                         /// && z.EmploymentStatus_LongName == "Active")
                                                                                         .Select(z => z.Employee_NameNumber)
                                                                                         .Distinct()
                                                                                         .ToList();
                List<string> listOfIDsFromHeadcountReport = new List<string>();
                /// all ids are converted to an int to stay inline with what is currently done in SQL
                foreach (string dumb in dumblistOfIDsFromHeadcountReport)
                {
                    /// there is an entry for this employee in the headcount report and their employment status is not "Active"
                    if (atrack.API_HEADCOUNT_LIST.Any(z => z.Employee_EmployeeId == dumb && z.EmploymentStatus_LongName != "Active"))
                    {
                        /// also check to see if this employee has not had any entries within the primary data source (the Pay Summary report) for 14 days
                        DateTime dateCutOff = DateTime.Today.AddDays(-14);
                        if (!atrack.datasource_datelimited.Any(z => z.EmployeeEmploymentStatus_EmployeeNumber == dumb && z.PayDate.Date >= dateCutOff.Date))
                        {
                            /// if both of these conditions are true then do NOT add this employee
                            /// areas marked as "/// too aggressive; removed" were originally in place and were replaced with this logic instead
                            continue;
                        }
                        /// hitting here implies that the employee may not be "Active" but still has entries within the Pay Summary
                        /// report within the last 14 days and therefore can be added
                    }

                    listOfIDsFromHeadcountReport.Add(Convert.ToInt32(dumb.Substring(dumb.LastIndexOf(" - ") + " - ".Length)).ToString());
                }

                /// remove any stale entries from .CurrentEmployees (an entry where an ID was not found in the headcount report)
                if (atrack.dataset.CurrentEmployees.Any(z => !listOfIDsFromHeadcountReport.Contains(z.EmployeeID)))
                {
                    List<CurrentEmployee> listOfStaleCurrentEmployeesToRemove = atrack.dataset.CurrentEmployees.Where(z => !listOfIDsFromHeadcountReport.Contains(z.EmployeeID)).ToList();
                    atrack.dataset.CurrentEmployees.RemoveRange(listOfStaleCurrentEmployeesToRemove);
                    /// save after bulk change
                    atrack.dataset.SaveChanges();
                }

                foreach (string empid in listOfIDsFromHeadcountReport)
                {   
                    /// obtain info; sources go from most -> least accurate
                    string name = string.Empty;
                    string department = string.Empty;
                    int shiftNumber = 0;
                    string supervisor = string.Empty;
                    string seniority = string.Empty;
                    string type = string.Empty;

                    /// from paysummary report
                    /// best data source (1/4)
                    if (atrack.datasource_datelimited.Any(z => z.EmployeeEmploymentStatus_EmployeeNumber == empid))
                    {
                        DataSource paySummaryEntry = atrack.datasource_datelimited.Where(z => z.EmployeeEmploymentStatus_EmployeeNumber == empid)
                                                                                  .OrderByDescending(z => z.PayDate)
                                                                                  .First();
                        if (name == string.Empty) name = paySummaryEntry.Employee_DisplayName;
                        if (department == string.Empty) department = paySummaryEntry.OrgUnit_ShortName;
                        /// no shift
                        /// no supervisor
                        /// no seniority
                        /// no type
                    }
                    /// from headcount report
                    /// good data source (2/4)
                    if (atrack.API_HEADCOUNT_LIST.Any(z => z.Employee_NameNumber.EndsWith(empid)))
                    {
                        FromCeridianAPI_Headcount headcountEntry = atrack.API_HEADCOUNT_LIST.Where(z => z.Employee_NameNumber.EndsWith(empid))
                                                                                            .OrderByDescending(z => z.ID)
                                                                                            .First();
                        if (name == string.Empty) name = headcountEntry.Employee_DisplayName;
                        if (department == string.Empty) department = headcountEntry.OrgUnit_ShortName;
                        /// no shift
                        /// no supervisor
                        /// no seniority
                        if (type == string.Empty) type = headcountEntry.PayClass_ShortName;
                    }
                    /// from orgchart report
                    /// OK data source; sometimes consistent, sometimes no logical ordering (3/4)
                    if (atrack.API_ORGCHART_LIST.Any(z => z.EmployeeEmploymentStatus_EmployeeNumber == empid))
                    {
                        FromCeridianAPI_OrgChart orgChartEntry = atrack.API_ORGCHART_LIST.Where(z => z.EmployeeEmploymentStatus_EmployeeNumber == empid)
                                                                                         .OrderByDescending(z => z.ID)
                                                                                         .First();
                        if (name == string.Empty) name = orgChartEntry.Employee_DisplayName;
                        if (department == string.Empty) department = orgChartEntry.OrgUnit_ShortName;
                        /// no shift
                        if (supervisor == string.Empty) supervisor = orgChartEntry.EmployeeManager_ManagerDisplayName;
                        /// no seniority
                        if (type == string.Empty) type = orgChartEntry.PayClass_ShortName;
                    }
                    /// from shiftrotation report
                    /// worst data source; no logical ordering whatsoever (4/4)
                    if (atrack.API_SHIFTROTATIONS_LIST.Any(z => z.EmploymentStatus_ShortName == "Active" &&
                                                                (z.PayClass_ShortName == "Flex" || z.PayClass_ShortName == "FT") &&
                                                                z.EmployeeEmploymentStatus_EmployeeNumber == empid &&
                                                                z.ShiftRotation_LongName != string.Empty))
                    {
                        FromCeridianAPI_ShiftRotations shiftRotationEntry = atrack.API_SHIFTROTATIONS_LIST.Where(z => z.EmploymentStatus_ShortName == "Active" &&
                                                                                                                      (z.PayClass_ShortName == "Flex" || z.PayClass_ShortName == "FT") &&
                                                                                                                      z.EmployeeEmploymentStatus_EmployeeNumber == empid &&
                                                                                                                      z.ShiftRotation_LongName != string.Empty &&
                                                                                                                      (supervisor == string.Empty || z.EmployeeManager_ManagerDisplayName == supervisor))
                                                                                                          .OrderBy(z => z.ID)
                                                                                                          .First();
                        if (name == string.Empty) name = shiftRotationEntry.Employee_DisplayName;
                        /// no department
                        /// only source of this data vvv
                        if (shiftNumber == 0) shiftNumber = Convert.ToInt32(shiftRotationEntry.ShiftRotation_LongName.Trim().Substring(0, 1));
                        if (supervisor == string.Empty) supervisor = shiftRotationEntry.EmployeeManager_ManagerDisplayName;
                        /// only source of this data vvv
                        if (seniority == string.Empty) seniority = shiftRotationEntry.Employee_SeniorityDate;
                        if (type == string.Empty) type = shiftRotationEntry.PayClass_ShortName;
                    }

                    /// user does NOT exist in .CurrentEmployees...
                    if (!atrack.dataset.CurrentEmployees.Any(z => z.EmployeeID == empid))
                    {
                        /// create new employee record
                        CurrentEmployee newEmployee = new CurrentEmployee();
                        /// fill in details
                        newEmployee.EmployeeID = empid;
                        newEmployee.EmployeeName = name;
                        newEmployee.Department = department;
                        newEmployee.Shift = shiftNumber;
                        newEmployee.Supervisor = supervisor;
                        newEmployee.SeniorityDate = seniority;
                        newEmployee.EmployeeType = type;
                        newEmployee.LastPointLevelActedOn = 0;
                        newEmployee.FMLA_NonVacation = 0;
                        newEmployee.FMLA_Vacation = 0;
                        /// specific to new employees...
                        if (atrack.ACTIONSTAKEN_LIST.Any(z => z.EmployeeID == empid && z.ActionTaken))
                        {
                            /// if this employee used to be in .CurrentEmployees but isn't now, and is actually being re-added here, then
                            /// retrieve their last-acted-on point level
                            newEmployee.LastPointLevelActedOn = atrack.ACTIONSTAKEN_LIST.Where(z => z.EmployeeID == empid && z.ActionTaken).OrderByDescending(z => z.DateOfOccurrence).First().PointValue;
                        }
                        atrack.dataset.CurrentEmployees.Add(newEmployee);
                        /// save after each change
                        atrack.dataset.SaveChanges();
                    }
                    /// user DOES exist in .CurrentEmployees...
                    else
                    {
                        /// grab existing entry in .CurrentEmployees for updating
                        CurrentEmployee oldEmployee = atrack.dataset.CurrentEmployees
                                                                    .Where(z => z.EmployeeID == empid)
                                                                    .First();
                        /// update entry
                        oldEmployee.EmployeeName = name;
                        oldEmployee.Department = department;
                        oldEmployee.Shift = shiftNumber;
                        oldEmployee.Supervisor = supervisor;
                        oldEmployee.SeniorityDate = seniority;
                        oldEmployee.EmployeeType = type;
                        /// save after each change
                        atrack.dataset.SaveChanges();
                    }
                }

                /// get list of employees
                List<string> listOfCurrentEmployeeIDs = atrack.dataset.CurrentEmployees.Select(z => z.EmployeeID).Distinct().ToList();
                /// using this list -- remove any pending actions (NOT locked and NOT acted on) belonging to an employee which is no longer considered active
                /// do not use ACTIONSTAKEN_LIST as its a List<> and not IQueryable<>
                List<ActionsTaken> listOfActionsToRemove = atrack.dataset.ActionsTakens.Where(z => !listOfCurrentEmployeeIDs.Contains(z.EmployeeID) && !z.IsLocked && !z.ActionTaken).ToList();
                atrack.dataset.ActionsTakens.RemoveRange(listOfActionsToRemove);
                /// save after bulk change
                atrack.dataset.SaveChanges();
                /// using this list -- remove any high points entries which do not belong to a current employee
                List<HighPoint> listOfHighPointsToRemove = atrack.dataset.HighPoints.Where(z => !listOfCurrentEmployeeIDs.Contains(z.EmployeeID)).ToList();
                atrack.dataset.HighPoints.RemoveRange(listOfHighPointsToRemove);
                /// save after bulk change
                atrack.dataset.SaveChanges();

                /// update list of UserOptions, too
                /// mark as inactive any user who has not logged in within the past year
                /// this is done this way because there is no way to reliably link together the information in .UserOptions (which is gathered from local/AD variables)
                /// and information in .CurrentEmployees, or any other table for that matter...
                /// that's because there's no way of putting EmployeeID into .UserOptions
                /// if the names, or email addresses, or anything really matched up between that table and .CurrentEmployees it would be a different story
                /// so, for now, this is limited using the date only
                DateTime toTest = DateTime.Now.AddYears(-1);
                IQueryable<UserOption> listOptions = atrack.dataset.UserOptions.Where(z => z.FullName != "Admin" &&
                                                                                           z.FullName != "Automation" &&
                                                                                           z.LastLogin < toTest);
                foreach (UserOption option in listOptions)
                {
                    option.Active = false;
                }
                /// save after all changes have finished
                atrack.dataset.SaveChanges();

                atrack.BindUserOptionsData();       /// update the cache of UserOptions
                atrack.BindCurrentEmployeesData();  /// update the cache of CurrentEmployees
                atrack.BindMyEmployees_NiceList();  /// update the cache of CurrentEmployees stored in _NiceList
                atrack.BindActionsTakenData();      /// update the cache of ActionsTaken
                BindEmployeeList();                 /// re-update the selectable list of employees
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        public void SendActionsRequiredEmails(bool pHasLapsed, bool pAttachExport)
        {
            /// all email processes use non-cached data

            try
            {
                GridView thisGrid = atrack.gcForDataExport.MainView as GridView;
                /// each email is generated individually because they are personalized to each user's selections within Options
                /// this normally includes .Active, which is a good idea, but not everyone logs in regularly...ar at all...
                /// and this can be expanded on but for now .Active is only ever really used in this one spot!
                /// .Active is still SET, however, but it is not used
                foreach (UserOption user in atrack.dataset.UserOptions.Where(z => z.Departments != string.Empty &&
                                                                                  z.FullName != "Automation")
                                                                                  //z.FullName != "Admin" &&
                                                                                  //z.Active)
                                                                      .OrderBy(z => z.FullName)
                                                                      .ToList())
                {
                    try
                    {
                        /// get list of departments for this user
                        List<string> user_employee_departments = user.Departments.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        /// get list of shifts for this user
                        string[] shiftArray_temp = user.Shift.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        List<int> user_employee_shifts = new List<int>();
                        /// shift 0 indicates that a shift was not found; so add it for everyone
                        user_employee_shifts.Add(0);
                        for (int i = 0; i < shiftArray_temp.Length; i++) { user_employee_shifts.Add(Convert.ToInt32(shiftArray_temp[i])); }

                        /// only active employees, and their respective departments + shifts, are considered
                        /// sort by EmployeeName but obtain EmployeeID
                        List<CurrentEmployee> user_employees = atrack.dataset.CurrentEmployees.Where(z => user_employee_departments.Contains(z.Department) &&
                                                                                                          user_employee_shifts.Contains(z.Shift))
                                                                                              .Distinct()
                                                                                              .OrderBy(z => z.EmployeeName)
                                                                                              .ToList();
                        List<string> user_employees_ids = new List<string>();
                        foreach (CurrentEmployee cEmp in user_employees) { user_employees_ids.Add(cEmp.EmployeeID); }
                        /// create and populate the list of actions that will be included in this email
                        List<ActionsTaken> listOfActionsToEmail;
                        if (!pHasLapsed)
                        {
                            /// point letters which are pending but not locked/missed/expired
                            listOfActionsToEmail = atrack.dataset.ActionsTakens.Where(z => user_employees_ids.Contains(z.EmployeeID) &&
                                                                                           z.ActionTaken == false &&
                                                                                           z.IsLocked == false).ToList();
                        }
                        else
                        {
                            /// point letters which are missed
                            /// this will only output a single missed point letter per employee
                            /// this is to prevent any historic changes from sending out a mass amount of these emails
                            /// this will, essentially, keep reminding supervisors of missed letters UNLESS they act on it OR 7 days elapses
                            /// which is probably a little annoying, but is also probably better

                            listOfActionsToEmail = new List<ActionsTaken>();
                            /// parse list of employees
                            DateTime missedCutOffDate = DateTime.Today.AddDays(-7);
                            foreach (string id in user_employees_ids)
                            {
                                /// obtain list of point letters for this employee ordered by descending pay date
                                foreach (ActionsTaken suspect in atrack.dataset.ActionsTakens.Where(z => z.EmployeeID == id).OrderByDescending(z => z.DateOfOccurrence))
                                {
                                    /// if the top, or next, point letter has already been taken then this employee is OK
                                    if (suspect.ActionTaken && (!suspect.IsLocked && suspect.DateOfLapse == null))
                                    {
                                        break;
                                    }
                                    /// if, however, the top (or next) point letter has not been taken and is locked then...
                                    else if (!suspect.ActionTaken && (suspect.IsLocked && suspect.DateOfLapse != null))
                                    {
                                        /// suspect is added to listOfActionsToEmail so IT IS ADDED TO THE NEXT EMAIL
                                        /// ...on the condition that it expired within the past week
                                        /// this is to cut down on how often people are reminded of missed letters
                                        /// especially in cases where the missed letter brought the employee back down under
                                        /// the threshhold for that letter in the first place
                                        if (suspect.DateOfLapse != null &&
                                            Convert.ToDateTime(suspect.DateOfLapse).Date > missedCutOffDate.Date)
                                        {
                                            listOfActionsToEmail.Add(suspect);
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                        /// do not continue if this list is empty
                        if (listOfActionsToEmail.Count <= 0) continue;

                        /// build email body per-employee for this user
                        string emailbody = string.Empty;
                        foreach (var employee in listOfActionsToEmail.Select(z => new { z.EmployeeID, z.EmployeeName })
                                                                     .Distinct()
                                                                     .OrderBy(z => z.EmployeeName))
                        {
                            /// obtain next employee and gather basic info
                            CurrentEmployee empThis = atrack.dataset.CurrentEmployees.Where(z => z.EmployeeID == employee.EmployeeID).First();
                            string empDepartment = empThis.Department.ToString();
                            string empShift = empThis.Shift.ToString();
                            string empSupervisor = empThis.Supervisor;
                            /// build a string of the levels which were hit/missed
                            List<double> empActionLevels = listOfActionsToEmail.Where(z => z.EmployeeID == employee.EmployeeID)
                                                                               .Select(z => z.PointValue)
                                                                               .ToList();
                            empActionLevels.Sort();
                            string empActionLevels_str = string.Empty;
                            foreach (double aVal in empActionLevels)
                            {
                                if (empActionLevels_str != string.Empty) empActionLevels_str += ", ";
                                empActionLevels_str += aVal.ToString("N2");
                            }

                            /// construct email body
                            if (!pHasLapsed)
                            {
                                /// also build a string of dates on which these levels were hit
                                List<DateTime> empActionDates = listOfActionsToEmail.Where(z => z.EmployeeID == employee.EmployeeID)
                                                                                    .Select(z => z.DateOfOccurrence)
                                                                                    .ToList();
                                empActionDates.Sort();
                                string empActionDates_str = string.Empty;
                                foreach (DateTime aDate in empActionDates)
                                {
                                    if (empActionDates_str != string.Empty) empActionDates_str += ", ";
                                    empActionDates_str += aDate.ToShortDateString();
                                }

                                emailbody += "<b>Name:</b> " + empThis.EmployeeName + " (#" + empThis.EmployeeID + ")" +
                                             "<br><b>Point level reached:</b> " + empActionLevels_str +
                                             "<br><b>Date reached:</b> " + empActionDates_str +
                                             "<br><b>Department:</b> " + empDepartment + " (shift #" + empShift + ")" +
                                             "<br><b>Supervisor:</b> " + empSupervisor +
                                             "<br><br>";
                            }
                            else
                            {
                                /// also build a string of dates on which these letters were missed
                                List<DateTime?> empActionExpiredDates = listOfActionsToEmail.Where(z => z.EmployeeID == employee.EmployeeID)
                                                                                            .Select(z => z.DateOfLapse)
                                                                                            .ToList();
                                empActionExpiredDates.Sort();
                                string empActionExpiredDates_str = string.Empty;
                                foreach (DateTime aExpDate in empActionExpiredDates)
                                {
                                    if (empActionExpiredDates_str != string.Empty) empActionExpiredDates_str += ", ";
                                    empActionExpiredDates_str += aExpDate.ToShortDateString();
                                }

                                emailbody += "<b>Name:</b> " + empThis.EmployeeName + " (#" + empThis.EmployeeID + ")" +
                                             "<br><b>Point level missed:</b> " + empActionLevels_str +
                                             "<br><b>Expired on:</b> " + empActionExpiredDates_str +
                                             "<br><b>Department:</b> " + empDepartment + " (shift #" + empShift + ")" +
                                             "<br><b>Supervisor:</b> " + empSupervisor +
                                             "<br><br>";
                            }
                        }

                        /// build attachments
                        List<System.Net.Mail.Attachment> lAttachments = null;
                        if (pAttachExport)
                        {
                            /// prep and set properties + data source
                            atrack.gcForDataExport.DataSource = null;
                            thisGrid.Columns.Clear();

                            thisGrid.OptionsPrint.AutoWidth = false;
                            thisGrid.OptionsView.ColumnAutoWidth = false;

                            atrack.gcForDataExport.DataSource = listOfActionsToEmail.Select(z => new {
                                z.EmployeeName,
                                z.EmployeeID,
                                z.DateOfOccurrence,
                                z.PointValue,
                                z.ActionToTake
                            })
                                                                                    .OrderBy(z => z.PointValue)
                                                                                    .ThenBy(z => z.EmployeeName)
                                                                                    .ThenBy(z => z.DateOfOccurrence)
                                                                                    .ToList();
                            thisGrid.OptionsView.BestFitMode = GridBestFitMode.Full;
                            thisGrid.OptionsView.BestFitMaxRowCount = 100;
                            /// placed in a separate try/catch because sometimes .BestFitColumns() would crash (not sure why)
                            try { thisGrid.BestFitColumns(); }
                            catch { }
                            thisGrid.FocusedRowHandle = -1;

                            /// set some properties, export to stream, then to attachment
                            MemoryStream stream = new MemoryStream();
                            XlsxExportOptionsEx options = new XlsxExportOptionsEx();
                            //options.ExportType = DevExpress.Export.ExportType.WYSIWYG;
                            options.ExportMode = XlsxExportMode.SingleFile;
                            options.TextExportMode = TextExportMode.Text;
                            if (!pHasLapsed) { options.SheetName = "Point Letters Required"; }
                            else { options.SheetName = "Missed Point Letters"; }
                            atrack.gcForDataExport.ExportToXlsx(stream, options);
                            stream.Seek(0, SeekOrigin.Begin);
                            /// set attachment properties
                            System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                            if (!pHasLapsed) { attachment.ContentDisposition.FileName = "Point Letters Required for " + DateTime.Today.Month + "-" + DateTime.Today.Day + "-" + DateTime.Today.Year + ".xlsx"; }
                            else { attachment.ContentDisposition.FileName = "Missed Point Letters for " + DateTime.Today.Month + "-" + DateTime.Today.Day + "-" + DateTime.Today.Year + ".xlsx"; }
                            lAttachments = new List<System.Net.Mail.Attachment>();
                            lAttachments.Add(attachment);
                        }

                        /// send respective emails
                        if (!pHasLapsed)
                        {
                            atrack.SendEmail(new string[] { user.EmailAddress },
                                             null,
                                             new string[] { Properties.Settings.Default.AdminEmail },
                                             "Point Letters Required for " + DateTime.Today.ToShortDateString(),
                                             "<b>The following point letters must be distributed within their allotted time windows --</b><br><i>Access the Wise Attendance Tracker application in <a href='citrix.wisesnacks.com'>Citrix</a> for a full breakdown.</i><br><br>" + emailbody + atrack.EMAIL_FOOTER,
                                             lAttachments,
                                             false);
                        }
                        else
                        {
                            atrack.SendEmail(new string[] { user.EmailAddress },
                                             null,
                                             new string[] { Properties.Settings.Default.AdminEmail },
                                             "Missed Point Letters for " + DateTime.Today.ToShortDateString(),
                                             "<b>The following point letters were not distributed within their allotted time windows and are now locked.<br>Each entry will appear for 7 days, or until a point letter is distributed for that employee. --</b><br><i>Access the Wise Attendance Tracker application in <a href='citrix.wisesnacks.com'>Citrix</a> for a full breakdown.</i><br><br>" + emailbody + atrack.EMAIL_FOOTER,
                                             lAttachments,
                                             false);
                        }
                    }
                    catch (Exception ex)
                    {
                        atrack.LogAndDisplayError(ex.ToString(), true);
                    }
                }
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        public void SendFMLANotificationEmails()
        {
            /// all email processes use non-cached data

            try
            {
                /// only process this email if there is something to be notified of
                if (!atrack.dataset.FMLANotifications.Any(z => z.Notified == false)) return;

                /// construct email
                string emailbody = string.Empty;
                foreach (FMLANotification fmla in atrack.dataset.FMLANotifications.Where(z => z.Notified == false).OrderBy(z => z.EmployeeName))
                {
                    CurrentEmployee empThis = atrack.dataset.CurrentEmployees.Where(z => z.EmployeeID == fmla.EmployeeID).First();
                    string empDepartment = empThis.Department.ToString();
                    string empShift = empThis.Shift.ToString();
                    string empSupervisor = empThis.Supervisor;
                    emailbody += "<b>Name:</b> " + fmla.EmployeeName + " (#" + fmla.EmployeeID + ")<br><b>Department:</b> " + empDepartment + " (shift #" + empShift + ")<br><b>Supervisor:</b> " + empSupervisor + "<br><b>Date of occurrence:</b> " + fmla.FMLANonVacationHoursTotalHitOn.ToShortDateString() + "<br><b>Current FMLA non-vacation hours:</b> " + fmla.FMLANonVacationHoursTotal.ToString() + "<br><b>Current FMLA vacation hours:</b> " + fmla.FMLAVacationHoursTotal.ToString() + "<br><br>";
                }

                /// send composed email                
                atrack.SendEmail(atrack.dataset.EmailRecipients.Where(z => z.Type == "FMLA").Select(z => z.EmailRecipient_Email).ToArray(),
                                 new string[] { Properties.Settings.Default.AdminEmail },
                                 null,
                                 "FMLA Hours Exceeded for " + DateTime.Today.ToShortDateString(),
                                 "<b>The following employees have hit or exceeded 32 FMLA non-vacation hours --</b><br><i>Access the Wise Attendance Tracker application in <a href='citrix.wisesnacks.com'>Citrix</a> for a full breakdown.</i><br><br>" + emailbody + atrack.EMAIL_FOOTER,
                                 null,
                                 true);

                /// update db entries
                DateTime now = DateTime.Now;
                foreach (FMLANotification fmla in atrack.dataset.FMLANotifications.Where(z => z.Notified == false).ToList())
                {
                    fmla.Notified = true;
                    fmla.NotifiedOn = now;
                }
                atrack.dataset.SaveChanges();

                /// update data cache
                atrack.BindFMLAData();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        public void SendPADNotificationEmails()
        {
            /// all email processes use non-cached data

            try
            {
                /// only process this email if there is something to be notified of
                //DateTime featureImplementedOn = new DateTime(2021, 2, 1);
                /// only check the last 7 days rather than the start of when this feature was added
                /// too many 'omg sorry for flooding your inbox' instances when data changes due to the rolling year setup
                DateTime dontgobeforethisdate = DateTime.Now.AddDays(-3);
                if (!atrack.dataset.TrackedBonuses.Any(z => z.BonusPayCode == "PAD" &&
                                                            z.BonusDate >= dontgobeforethisdate)) return;
                List<TrackedBonus> lTrackedPADs = atrack.dataset.TrackedBonuses.Where(z => z.BonusPayCode == "PAD" &&
                                                                                           z.BonusDate >= dontgobeforethisdate).ToList();
                List<TrackedBonus> lTrackedPADsToSend = new List<TrackedBonus>();
                foreach (TrackedBonus pad in lTrackedPADs)
                {
                    if (!atrack.dataset.PADNotifications.Any(z => z.EmployeeID == pad.EmployeeID &&
                                                                  z.PADHitOn == pad.BonusDate))
                    {
                        lTrackedPADsToSend.Add(pad);
                    }
                }
                if (lTrackedPADsToSend.Count <= 0) return;

                foreach (TrackedBonus padToSend in lTrackedPADsToSend)
                {
                    /// construct email
                    string emailbody = string.Empty;
                    CurrentEmployee empThis = atrack.dataset.CurrentEmployees.Where(z => z.EmployeeID == padToSend.EmployeeID).First();
                    string empName = empThis.EmployeeName;
                    string empDepartment = empThis.Department.ToString();
                    string empShift = empThis.Shift.ToString();
                    string empSupervisor = empThis.Supervisor;
                    emailbody += "<b>Name:</b> " + empName + " (#" + padToSend.EmployeeID + ")<br><b>Department:</b> " + empDepartment + " (shift #" + empShift + ")<br><b>Supervisor:</b> " + empSupervisor + "<br><b>Date of award:</b> " + padToSend.BonusDate.ToShortDateString() + "<br><b>Awarded because:</b> " + padToSend.BonusDescription + "<br><br>";

                    /// find managers
                    List<string> lstrEmpManagers = atrack.dataset.EmailRecipients.Where(z => z.Type == "DepartmentalChangeRequests" &&
                                                                                             z.Department == empDepartment)
                                                                                 .Select(z => z.EmailRecipient_Email)
                                                                                 .ToList();
                    /// find supervisors
                    List<string> lstrEmpSupervisors = atrack.dataset.UserOptions.Where(z => z.Shift.Contains(empShift) &&
                                                                                            z.Departments.Contains(empDepartment))
                                                                                .Select(z => z.EmailAddress)
                                                                                .ToList();
                    /// find intended PAD award recipients
                    List<string> lstrEmailRecipients = atrack.dataset.EmailRecipients.Where(z => z.Type == "PAD")
                                                                                     .Select(z => z.EmailRecipient_Email)
                                                                                     .ToList();
                    /// combine
                    List<string> lstrCombined = new List<string>();
                    lstrCombined.AddRange(lstrEmpManagers);
                    lstrCombined.AddRange(lstrEmpSupervisors);
                    lstrCombined.AddRange(lstrEmailRecipients);

                    /// send composed email                
                    atrack.SendEmail(lstrCombined.ToArray(),
                                     new string[] { Properties.Settings.Default.AdminEmail },
                                     null,
                                     "Perfect Attendance Day awarded to " + empName + " (#" + padToSend.EmployeeID + ") on " + padToSend.BonusDate.ToShortDateString(),
                                     "<b>The following employee has been awarded a Perfect Attendance Day --</b><br><i>Access the Wise Attendance Tracker application in <a href='citrix.wisesnacks.com'>Citrix</a> for a full breakdown.</i><br><br>" + emailbody + atrack.EMAIL_FOOTER,
                                     null,
                                     true);

                    /// add db entry
                    DateTime now = DateTime.Now;
                    PADNotification newPad = new PADNotification();
                    newPad.ID = Guid.NewGuid();
                    newPad.EmployeeID = padToSend.EmployeeID;
                    newPad.EmployeeName = empName;
                    newPad.PADHitOn = padToSend.BonusDate;
                    newPad.Notified = true;
                    newPad.NotifiedOn = now;
                    atrack.dataset.PADNotifications.Add(newPad);
                    /// and save
                    atrack.dataset.SaveChanges();
                }

                ///// update data cache
                //atrack.BindPADData();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        public void SendReportEmails()
        {
            try
            {
                string wordoftheday = "Reporting";
                List<Attachment> lAttachments = null;
                atrack.AddReportPage();

                lAttachments = (atrack.GetTabFromMain(wordoftheday) as ReportingTemplate).ReturnAllReportsAsListOfAttachments("PDF");

                if (lAttachments != null)
                {
                    atrack.SendEmail(atrack.dataset.EmailRecipients.Where(z => z.Type == wordoftheday).Select(z => z.EmailRecipient_Email).ToArray(),
                                     new string[] { Properties.Settings.Default.AdminEmail },
                                     null,
                                     "Attendance Tracker Reports for " + DateTime.Today.ToShortDateString(),
                                     "Attendance Tracker reports are attached to this email." + atrack.EMAIL_FOOTER,
                                     lAttachments,
                                     false);
                }
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        private void BindEmployeeList()
        {
            try
            {
                cbEmployeeList.Properties.Items.Clear();
                cbEmployeeList.Properties.Items.AddRange(atrack.MYEMPLOYEES_NICELIST);
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        /// select the employee event table to prevent the scrollwheel from changing the employee after selecting one
        private void CbEmployeeList_SelectedIndexChanged(object sender, EventArgs e) { gcEmployeeEvents.Select(); BindEmployeeEntries(); }
        private void deListByPayDate_EditValueChanging(object sender, ChangingEventArgs e)
        {
            try
            {
                if (e.NewValue != null && e.NewValue.ToString() != string.Empty)
                {
                    DateTime from = (sender == deListByPayDate_From) ? Convert.ToDateTime(e.NewValue) : deListByPayDate_From.DateTime;
                    DateTime to = (sender == deListByPayDate_To) ? Convert.ToDateTime(e.NewValue) : deListByPayDate_To.DateTime;
                    if (from != atrack.Date_EarliestPossible && to != atrack.Date_EarliestPossible && from > to)
                    {
                        e.Cancel = true;
                        MessageBox.Show("The 'From' date must come before the 'To' date.",
                                        "Invalid Dates",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Warning);
                    }
                }
            }
            catch { }
        }
        private void deListByPayDate_EditValueChanged(object sender, EventArgs e) { BindEmployeeEntries(); }
        private void ListViewType_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e) { BindEmployeeEntries(); }
        private void ListViewType_ItemChecking(object sender, DevExpress.XtraEditors.Controls.ItemCheckingEventArgs e)
        {
            int otherindex = e.Index == 0 ? 1 : 0;
            if (e.NewValue == CheckState.Unchecked && listViewType.Items[otherindex].CheckState == CheckState.Unchecked)
            {
                e.Cancel = true;
            }
        }
        private void BindEmployeeEntries()
        {
            try
            {
                gcEmployeeEntries.DataSource = null;
                ClearPointDetailArea();
                ClearEmployeeInfo();

                if (!atrack.RUNNINGAUTOMATED && !atrack.RUNNINGADMINTASK)
                {
                    /// garbage is collected upon selecting a new employee
                    /// this has no impact on the automated process and just makes it take longer (by about 25%)
                    GC.Collect();
                    atrack.ShowSplash("Finding employee data");
                }

                if (cbEmployeeList.SelectedIndex < 0 || cbEmployeeList.SelectedItem == null)
                {
                    btnRefresh.Enabled = false;
                    return;
                }
                else 
                {
                    btnRefresh.Enabled = true;
                }

                string selection = cbEmployeeList.SelectedItem.ToString();
                string id = atrack.GetEmployeeIDFromSelection(selection);
                if (!atrack.dataset.CurrentEmployees.Any(z => z.EmployeeID == id)) return;
                SELECTEDEMPLOYEE = atrack.dataset.CurrentEmployees.Where(z => z.EmployeeID == id).First();
                /// data may only be processed if a single individual is selected
                if (SELECTEDEMPLOYEE.EmployeeID == null || SELECTEDEMPLOYEE.EmployeeID == string.Empty) return;

                SELECTEDDATE_FROM = deListByPayDate_From.DateTime;
                SELECTEDDATE_TO = deListByPayDate_To.DateTime;

                if (thisPageName.Text != "AdminTasks") thisPageName.Text = TITLE_ViewEmployeeData = TITLE_ViewEmployeeData_Default;

                Dictionary<string, int> groupIndexDictionary = new Dictionary<string, int>();
                Dictionary<string, int> visibleIndexList = new Dictionary<string, int>();

                if (!atrack.RUNNINGAUTOMATED)
                {
                    foreach (GridColumn col in gridView1.Columns)
                    {
                        groupIndexDictionary.Add(col.FieldName, col.GroupIndex);
                    }
                    foreach (GridColumn col in gridView1.Columns.OrderBy(z => z.VisibleIndex))
                    {
                        visibleIndexList.Add(col.FieldName, col.VisibleIndex);
                    }

                    gridView1.Columns.Clear();
                }

                /// VIEWACTIVEONLY
                if (listViewType.Items[0].CheckState == CheckState.Checked && listViewType.Items[1].CheckState == CheckState.Unchecked) VIEWACTIVEONLY = true;
                else if (listViewType.Items[0].CheckState == CheckState.Unchecked && listViewType.Items[1].CheckState == CheckState.Checked) VIEWACTIVEONLY = false;
                else VIEWACTIVEONLY = null;

                TABLEENTRIES = atrack.datasource_datelimited;
                TABLEENTRIES = TABLEENTRIES.Where(z => z.EmployeeEmploymentStatus_EmployeeNumber == SELECTEDEMPLOYEE.EmployeeID);
                /// check if employee is a rehire
                bool isRehire = false;
                DateTime rehireDate = DateTime.Today;
                if (atrack.dataset.Rehires.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID))
                {
                    isRehire = true;
                    rehireDate = atrack.dataset.Rehires.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID).First().RehireDate;
                    TABLEENTRIES = TABLEENTRIES.Where(z => z.PayDate >= rehireDate.Date);
                }
                if (VIEWACTIVEONLY != null) TABLEENTRIES = TABLEENTRIES.Where(z => z.IsCurrentRecord == VIEWACTIVEONLY);
                if (SELECTEDDATE_FROM != null && SELECTEDDATE_FROM != atrack.Date_EarliestPossible) TABLEENTRIES = TABLEENTRIES.Where(z => z.PayDate >= SELECTEDDATE_FROM);
                if (SELECTEDDATE_TO != null && SELECTEDDATE_TO != atrack.Date_EarliestPossible) TABLEENTRIES = TABLEENTRIES.Where(z => z.PayDate <= SELECTEDDATE_TO);
                TABLEENTRIES = TABLEENTRIES.Where(z => z.PayDate <= atrack.Date_LatestPossible);
                TABLEENTRIES = TABLEENTRIES.OrderBy(z => z.PayDate);
                TABLEENTRIES_LIST.Clear();
                foreach (DataSource ptds in TABLEENTRIES) { TABLEENTRIES_LIST.Add(DeepClone(ptds)); }
                TABLEENTRIES = null;
                if (VIEWACTIVEONLY != null && VIEWACTIVEONLY == true)
                {
                    gcEmployeeEntries.DataSource = TABLEENTRIES_LIST.Select(z => new
                    {
                        z.PayDate,
                        z.RoundedInOutPunches,
                        z.PayPeriod,
                        z.PayAdjCode_ShortName,
                        z.PayCategory_ShortName,
                        z.EmployeePaySummary_NetHours,
                        z.EmployeeComments,
                        z.ManagerComments,
                        z.EmployeePayAdjust_ReferenceDate,
                        z.EntryDate
                    }).OrderBy(z => z.PayDate).ThenBy(z => z.RoundedInOutPunches).ThenBy(z => z.PayAdjCode_ShortName).ThenBy(z => z.PayCategory_ShortName).ToList();
                }
                else if (VIEWACTIVEONLY == null || VIEWACTIVEONLY == false)
                {
                    gcEmployeeEntries.DataSource = TABLEENTRIES_LIST.Select(z => new
                    {
                        z.PayDate,
                        z.RoundedInOutPunches,
                        z.PayPeriod,
                        z.PayAdjCode_ShortName,
                        z.PayCategory_ShortName,
                        z.EmployeePaySummary_NetHours,
                        z.EmployeeComments,
                        z.ManagerComments,
                        z.EmployeePayAdjust_ReferenceDate,
                        z.EntryDate,
                        z.IsCurrentRecord,
                        z.RemovalDate
                    }).OrderBy(z => z.PayDate).ThenBy(z => z.RoundedInOutPunches).ThenBy(z => z.PayAdjCode_ShortName).ThenBy(z => z.PayCategory_ShortName).ToList();
                }
                else return;

                if (!atrack.RUNNINGAUTOMATED)
                {
                    SetGridViewColumnOptions();

                    int theend = 100;
                    foreach (GridColumn col in gridView1.Columns)
                    {
                        if (groupIndexDictionary.Count() > 0 && col.Visible && groupIndexDictionary.ContainsKey(col.FieldName)) { col.GroupIndex = groupIndexDictionary[col.FieldName]; }
                        if (visibleIndexList.Count() > 0 && col.Visible)
                        {
                            if (visibleIndexList.ContainsKey(col.FieldName)) { col.VisibleIndex = visibleIndexList[col.FieldName]; }
                            else { col.VisibleIndex = (theend += 10); }   /// push new columns to the end
                        }
                    }

                    GridView employeeEntries = (gcEmployeeEntries.MainView as GridView);
                    employeeEntries.OptionsView.BestFitMode = GridBestFitMode.Fast;
                    employeeEntries.OptionsView.BestFitMaxRowCount = 100;
                    employeeEntries.BestFitColumns();
                    employeeEntries.FocusedRowHandle = -1;
                    employeeEntries.Columns["PayDate"].GroupIndex = 0;
                }
                else
                {
                    if (!GRIDVIEW1PROPSSET) SetGridViewColumnOptions();
                }

                /// only perform full analysis and display point/FMLA hour counters if
                /// 1) only active records are selected, and
                /// 2) all dates are being viewed
                if ((VIEWACTIVEONLY != null && VIEWACTIVEONLY != false) &&
                    (SELECTEDDATE_FROM == null || SELECTEDDATE_FROM == atrack.Date_EarliestPossible) &&
                    (SELECTEDDATE_TO == null || SELECTEDDATE_TO == atrack.Date_EarliestPossible))
                {
                    if (atrack.RUNNINGAUTOMATED || atrack.RUNNINGADMINTASK) AnalyzePoints(true, false, null);
                    else AnalyzePoints_DisplayCache();

                    ShowPointPanels(true);
                }
                else
                {
                    AnalyzePoints(false, false, null);
                    ShowPointPanels(false);
                    if (!atrack.RUNNINGAUTOMATED) atrack.CloseSplash();
                    MessageBox.Show("Point and FMLA totals will not be shown due to your selected filters.\n\nTo view this info you cannot filter by date or have 'Record Type' set to 'Removed'.",
                                    "Analysis Disabled",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }

                /// set user info --
                lblDepartment.Text = SELECTEDEMPLOYEE.Department;
                lblShift.Text = SELECTEDEMPLOYEE.Shift.ToString();
                /// set other info
                /// employee type (if there is one)                
                if (SELECTEDEMPLOYEE.EmployeeType != string.Empty && SELECTEDEMPLOYEE.EmployeeType != null)
                {
                    lblEmployeeType.Text = SELECTEDEMPLOYEE.EmployeeType;
                    lblEmployeeType.Visible = true;
                }
                else lblEmployeeType.Visible = false;
                /// seniority date (if there is one)
                if (SELECTEDEMPLOYEE.SeniorityDate != string.Empty && SELECTEDEMPLOYEE.SeniorityDate != null)
                {
                    lblSeniorityDate.Text = Convert.ToDateTime(SELECTEDEMPLOYEE.SeniorityDate.Substring(0, SELECTEDEMPLOYEE.SeniorityDate.IndexOf("T"))).ToShortDateString();
                    lblSeniorityDate.Visible = true;
                }
                else lblSeniorityDate.Visible = false;
                /// supervisor (if there is one)
                if (SELECTEDEMPLOYEE.Supervisor != string.Empty && SELECTEDEMPLOYEE.Supervisor != null)
                {
                    lblSupervisor.Text = SELECTEDEMPLOYEE.Supervisor;
                    lblSupervisor.Visible = true;
                }
                else lblSupervisor.Visible = false;
                /// re-hire date (if there is one)
                if (isRehire)
                {
                    lblRehireDate.Text = rehireDate.ToShortDateString();
                    lblRehireDate.Visible = true;
                }
                else lblRehireDate.Visible = false;

                /// set page name
                string title = string.Empty;
                title += SELECTEDEMPLOYEE.EmployeeName;
                //title += SELECTEDEMPLOYEENAME + " (#" + SELECTEDEMPLOYEEID + ")";
                if (thisPageName.Text != "AdminTasks") thisPageName.Text = TITLE_ViewEmployeeData = title == string.Empty ? TITLE_ViewEmployeeData_Default : title;
            }
            catch (Exception ex)
            {
                if (atrack.UPDATEACTIVE) { MessageBox.Show("Please close the page you are on and try again.", "Data Update Failed", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                else { atrack.LogAndDisplayError(ex.ToString(), true); }
            }
            finally
            {
                if (!atrack.RUNNINGAUTOMATED && !atrack.RUNNINGADMINTASK) atrack.CloseSplash();
            }
        }

        private void SetGridViewColumnOptions()
        {
            if (gridView1.Columns["PayDate"] != null)
            {
                gridView1.Columns["PayDate"].Caption = "Pay Date";
                /// goes to gridView1_CustomColumnSort()
                gridView1.Columns["PayDate"].SortMode = ColumnSortMode.Custom;
                /// this must be sorted by date (and punches) in ascending order for the analysis to function properly
                gridView1.SortInfo.Add(gridView1.Columns["PayDate"], ColumnSortOrder.Ascending);
            }
            if (gridView1.Columns["Employee_DisplayName"] != null) { gridView1.Columns["Employee_DisplayName"].Caption = "Name"; }
            if (gridView1.Columns["EmployeeEmploymentStatus_EmployeeNumber"] != null) { gridView1.Columns["EmployeeEmploymentStatus_EmployeeNumber"].Caption = "#"; }
            if (gridView1.Columns["OrgUnit_ShortName"] != null) { gridView1.Columns["OrgUnit_ShortName"].Caption = "Org. Unit"; }
            if (gridView1.Columns["Department_ShortName"] != null) { gridView1.Columns["Department_ShortName"].Caption = "Department"; }
            if (gridView1.Columns["Job_ShortName"] != null) { gridView1.Columns["Job_ShortName"].Caption = "Job"; }
            if (gridView1.Columns["RoundedInOutPunches"] != null)
            {
                gridView1.Columns["RoundedInOutPunches"].Caption = "In/Out Punches";
                /// goes to gridView1_CustomColumnSort() (see notes in PayDate above)
                gridView1.Columns["RoundedInOutPunches"].SortMode = ColumnSortMode.Custom;
            }
            if (gridView1.Columns["PayAdjCode_ShortName"] != null) { gridView1.Columns["PayAdjCode_ShortName"].Caption = "Pay Code"; }
            if (gridView1.Columns["PayCategory_ShortName"] != null) { gridView1.Columns["PayCategory_ShortName"].Caption = "Pay Category"; }
            if (gridView1.Columns["NetHours"] == null) { 
                GridColumn col = new GridColumn();
                col.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
                col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                col.DisplayFormat.FormatString = "n2";
                col.UnboundExpression = "[EmployeePaySummary_NetHours]";
                col.Name = col.FieldName = "NetHours";
                col.Caption = "Net Hours";
                gridView1.Columns["EmployeePaySummary_NetHours"].Visible = false;
                gridView1.Columns.Add(col);
                col.VisibleIndex = 100;                                         /// push to end
            }
            if (gridView1.Columns["ReferenceDate"] == null) {
                GridColumn col = new GridColumn();
                col.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
                col.UnboundExpression = "IsNull([EmployeePayAdjust_ReferenceDate], '')";
                col.Name = col.FieldName = "ReferenceDate";
                col.Caption = "Reference Date";
                gridView1.Columns["EmployeePayAdjust_ReferenceDate"].Visible = false;
                gridView1.Columns.Add(col);
                col.VisibleIndex = 110;                                         /// push to end
            }
            if (gridView1.Columns["EntryDate"] != null)
            {
                gridView1.Columns["EntryDate"].Caption = "Date Entered into Ceridian";
                gridView1.Columns["EntryDate"].VisibleIndex = 120;              /// push to end
            }
            if (gridView1.Columns["IsCurrentRecord"] != null)
            {
                gridView1.Columns["IsCurrentRecord"].Caption = "Active";
                gridView1.Columns["IsCurrentRecord"].VisibleIndex = 130;        /// push to end

            }
            if (gridView1.Columns["RemovalDate"] != null)
            {
                gridView1.Columns["RemovalDate"].Caption = "Date Removed";
                gridView1.Columns["RemovalDate"].VisibleIndex = 140;            /// push to end
            }

            GRIDVIEW1PROPSSET = true;
        }
        private void SetGridView2ColumnOptions()
        {
            try
            {
                GridColumn col = new GridColumn();
                col.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
                col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                col.DisplayFormat.FormatString = "n2";
                col.UnboundExpression = "[Net Hours]";
                col.Name = col.FieldName = "NetHours";
                col.Caption = "Net Hours";
                col.VisibleIndex = gridView2.Columns["Net Hours"].VisibleIndex;
                gridView2.Columns["Net Hours"].Visible = false;
                gridView2.Columns.Add(col);
            }
            catch { }

            int index = 0;
            gridView2.Columns["Pay Date"].VisibleIndex = index++;
            gridView2.Columns["Pay Code"].VisibleIndex = index++;
            gridView2.Columns["NetHours"].VisibleIndex = index++;
            gridView2.Columns["Description"].VisibleIndex = index++;
            gridView2.Columns["Points"].VisibleIndex = index++;

            GRIDVIEW2PROPSSET = true;
        }

        private void AnalyzePoints(bool pIncludeRules, bool pRerunWithForgiveness, DataTable dt)
        {
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\    < THE POINT SYSTEM >    //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\    NOTES           //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\
            //\\//\\//\\    One of the most important pieces to this puzzle is the .LastPointLevelActedOn field within the CurrentEmployees table
            //\\//\\//\\    this is THE VALUE which determines if a newly added action is CURRENT or OBSOLETE
            //\\//\\//\\    because of the rolling calendar year, and how bonuses/penalties can influence historical data, it is IMPERATIVE that this
            //\\//\\//\\    value be maintained as it is the ONLY thing stopping actions from being scaffolded in the past if something changes!!!
            //\\//\\//\\    This would snowball the data as it would result in many of these actions being "missed" due to the 14-day rule in the union agreement.
            //\\//\\//\\    
            //\\//\\//\\    .LastPointLevelActedOn acts as the LOWEST POINT LEVEL WHICH HAS EITHER BEEN ACTED ON OR REACHED BY THE EMPLOYEE
            //\\//\\//\\    it reflects an employee's current standing in a linear fashion
            //\\//\\//\\
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\    PENALTIES       //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\
            //\\//\\//\\    P01. Absent = 1.00 point
            //\\//\\//\\    P02. Did not attend a scheduled safety meeting = 0.25 points
            //\\//\\//\\    P03. Tardiness / leaving work early (with time missed of one and one-half(1 1 / 2) hour or less) = 0.25 points
            //\\//\\//\\    P04. Tardiness / leaving work early (with time missed of between one and one-half(1 1 / 2) and up to five and one-half(5 1 / 2) hours) = 0.50 points
            //\\//\\//\\    P05. Tardiness / leaving work early (with time missed of more than five and one-half(5 1 / 2) hours) = 1.00 point
            //\\//\\//\\    P06. Chronic tardiness / leaving early (four(4) partial occurrences within a two(2) calendar months period) = 1.00 point
            //\\//\\//\\    P07. Overtime (employee volunteered and failed to work the assigned shift) = 1.00 point
            //\\//\\//\\    P08. Overtime (employee was drafted and failed to work the assigned shift) = 0.50 points
            //\\//\\//\\
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\    BONUSES     //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\
            //\\//\\//\\    B01.  Worked all scheduled hours including overtime for ninety (90) consecutive days: Remove oldest point (down to 0)
            //\\//\\//\\    B02.  Absent up to three (3) consecutive days (usable one (1) time per calendar year): Penalty reduced to 1 point
            //\\//\\//\\    B03.  Worked fourteen (14) consecutive scheduled calendar days including overtime (consecutive day streak must be
            //\\//\\//\\          broken and one full work week must pass before this can recur; streaks are broken by an absence for any reason): Remove oldest point (down to 0)
            //\\//\\//\\    B03+. Worked another seven (7) consecutive scheduled calendar days including overtime, with no absences following
            //\\//\\//\\          the original fourteen (14) consecutive calendar days (consecutive day streak must be broken and one full work week must pass before this can recur; 
            //\\//\\//\\          streaks are broken by an absence for any reason): Remove oldest point (down to 0)
            //\\//\\//\\
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\    CODES       //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\
            //\\//\\//\\    PayAdjCode_ShortName                PayAdjCode_IsAbsence    Counts as penalty?      Violates which rule?    Breaks attendance streak?
            //\\//\\//\\    C01. ADA ABSENT                     TRUE                    FALSE                                           TRUE
            //\\//\\//\\    C02. BRK                            FALSE                   FALSE                                           FALSE
            //\\//\\//\\    C03. BRV                            TRUE                    FALSE                                           TRUE  (from HR: "if paid")
            //\\//\\//\\    C04. E-Out                          TRUE                    FALSE                                           TRUE  (from HR: "someone who left early and subject to attendance")
            //\\//\\//\\    C05. Excused Absence                TRUE                    FALSE                                           TRUE
            //\\//\\//\\    C06. FMLA ABSENT                    TRUE                    FALSE                   F01                     TRUE
            //\\//\\//\\    C07. FMLA REFUSED OVERTIME          TRUE                    FALSE                   F01                     TRUE
            //\\//\\//\\    C08. FMlA VACATION                  TRUE                    FALSE                                           TRUE
            //\\//\\//\\    C09. HOL                            FALSE                   FALSE                                           FALSE
            //\\//\\//\\    C10. HW                             FALSE                   FALSE                                           FALSE
            //\\//\\//\\    C11. JURY                           TRUE                    FALSE                                           FALSE
            //\\//\\//\\    C12. L-In                           TRUE                    FALSE                                           TRUE  (from HR: "someone who came in late and subject to attendance policy")
            //\\//\\//\\    C13. L-Out                          FALSE                   FALSE                                           FALSE
            //\\//\\//\\    C14. MEAL                           FALSE                   FALSE                                           FALSE
            //\\//\\//\\    C15. MILITARY ABSENT                TRUE                    FALSE                                           FALSE (from HR, but it's contradictory: "Yes, but when EE returns, should continue where they left off")
            //\\//\\//\\    C16. Missed Safety Meeting          FALSE                   TRUE                    P02                     FALSE
            //\\//\\//\\    C17. NO CALL/ NO SHOW               TRUE                    TRUE                    P01                     TRUE
            //\\//\\//\\    C18. No Work Available              FALSE                   FALSE                                           FALSE
            //\\//\\//\\    C19. Perfect Attendance Bonus Day   TRUE                    FALSE                                           FALSE
            //\\//\\//\\    C20. PH                             TRUE                    FALSE                                           FALSE
            //\\//\\//\\    C21. Refused Overtime               FALSE                   TRUE                    P07                     TRUE  (is not an absence)
            //\\//\\//\\    C22. SICK                           TRUE                    FALSE                                           TRUE
            //\\//\\//\\    C23. SICK ABSENT                    TRUE                    TRUE                    P01                     TRUE
            //\\//\\//\\    C24. SU                             FALSE                   FALSE                                           FALSE
            //\\//\\//\\    C25. SUSPENSION WITHOUT PAY         TRUE                    FALSE                                           TRUE
            //\\//\\//\\    C26. Unexcused                      TRUE                    TRUE                                            TRUE
            //\\//\\//\\    C27. Union Business                 TRUE                    FALSE                                           FALSE
            //\\//\\//\\    C28. VAC                            TRUE                    FALSE                                           FALSE
            //\\//\\//\\    C29. VACATION WEEK ABSENT           TRUE                    FALSE                                           FALSE
            //\\//\\//\\    C30. Volunteer Off                  FALSE                   FALSE                                           FALSE
            //\\//\\//\\    C31. Volunteer Refused OT           FALSE                   TRUE                    P08                     TRUE  (is not an absence)
            //\\//\\//\\    C32. VSD                            TRUE                    FALSE                                           FALSE
            //\\//\\//\\    C33. VSH                            TRUE                    FALSE                                           FALSE
            //\\//\\//\\    C34. VWE                            TRUE                    FALSE                                           FALSE (from HR: "We don’t know what this Code is?  If it’s vacation weekend, then NO")
            //\\//\\//\\    C35. WRK                            FALSE                   FALSE                                           FALSE
            //\\//\\//\\
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\    REPRIMANDS      //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\
            //\\//\\//\\    R01. When an employee has three(3) occurrences within a calendar year: 
            //\\//\\//\\            * A meeting with the employee and Department Supervisor will be held to focus on improving the attendance of the employee and to identify potential problems.
            //\\//\\//\\    R02. When an employee has six(6) occurrences:
            //\\//\\//\\            * The Department Supervisor will meet with the employee and issue a letter of warning.
            //\\//\\//\\    R03. When an employee has nine(9) occurrences:
            //\\//\\//\\            * The employee will meet with the Department Supervisor and HR Representative and receive a second written warning.
            //\\//\\//\\    R04. When an employee has eleven(11) occurrences:
            //\\//\\//\\            * The employee will meet with the Department Supervisor, Shift Steward and HR representative for review of the file and the employee will be given a final warning letter.
            //\\//\\//\\    R05. When an employee has twelve(12) occurrences within a calendar year:
            //\\//\\//\\            * The Department Manager or Lead Department Supervisor, Shift Steward and HR representative will meet with the employee, review the record, and the employee will be terminated.
            //\\//\\//\\            * Provided, however, on one occasion during each calendar 12 month period the employee may avoid termination for the last occurrence by providing the required Doctor’s statement immediately upon return to work.
            //\\//\\//\\            * This statement must meet the following criteria: the employee actually visited the Doctor and obtained the statement the day before or day of the absence resulting in the 12th occurrence.
            //\\//\\//\\            * In the event of a challenged termination for occurrences in a rolling year, the Union may request expedited arbitration, provided, however, the arbitration will be held within three(3) weeks of the arbitrator’s selection.
            //\\//\\//\\            * The arbitrator may award injunctive relief or other relief consistent with the terms of this agreement.If necessary, any resulting Award may be enforced in a Court of Law.
            //\\//\\//\\    R06. All attendance letters will be distributed to employees within fourteen (14) days of actual work (complete shifts) by the employee from the occurrence. 
            //\\//\\//\\            * If the letter is not distributed within that period, then an employee’s points will be rolled back to the last point accrued prior to the untimely letter being issued.
            //\\//\\//\\
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\    MISCELLANEOUS   //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\
            //\\//\\//\\    M01. PERFECT ATTENDANCE BONUS DAY (just a signifier that an employee is eligible; still must be done in Ceridian);
            //\\//\\//\\            For each consecutive four (4) month period of perfect attendance, or 693 hours worked (whichever comes first)
            //\\//\\//\\            an employee will receive a paid day off to use at the employee's convenience with a limit of three (3) days per year.
            //\\//\\//\\            All paid days must be utilized within the following twelve (12) month period of time and twenty-four (24) hours' notice must be provided
            //\\//\\//\\            Scheduling of time must be approved by supervision.
            //\\//\\//\\            (Note: Any employee that does not meet the consecutive time or hours worked specifications as noted above,
            //\\//\\//\\            including absences covered by the Family Medical Leave Act or the Americans with Disabilities Act will be ineligible to receive the Attendance Bonus.)
            //\\//\\//\\            If an employee chooses to use his or her Bonus Day on a Friday or Monday, the employee will not be required to work the weekend.
            //\\//\\//\\
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\    FMLA        //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\
            //\\//\\//\\    F01. Upon exhausting five (5) single occurrences or a total of thirty-two (32) hours of absence for FMLA qualifying reasons,
            //\\//\\//\\            whichever comes first, either intermittent or continuous, an employee will be required to
            //\\//\\//\\            use one - half(½) of his or her paid vacation time, up to two(2) weeks, depending on the employee's vacation eligibility,
            //\\//\\//\\            for an intermittent or continuous leave taken for any reason other than one that would entitle the employee to disability pay.
            //\\//\\//\\            An employee can choose to use more than the required amount of vacation to run concurrently with an FMLA leave.
            //\\//\\//\\            (Note: the substitution of paid leave time for unpaid leave time does not extend the FMLA leave period).
            //\\//\\//\\    
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\   </ THE POINT SYSTEM >    //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
            //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\

            try
            {
                /// fmla tracker
                double fmlatotal_nonvacation = 0;
                double fmlatotal_vacation = 0;

                /// determine if logic should be run to apply new deductions
                bool bonusesExistWhichHaveNotBeenApplied = false;

                /// prime date trackers appropriately
                EmployeeDayBeforeEarliestWorkDate = GetFirstEntryForEmployee(SELECTEDEMPLOYEE.EmployeeID).AddDays(-1);
                DateTime lastB01happening = EmployeeDayBeforeEarliestWorkDate;
                DateTime lastB02happening = new DateTime();
                bool? B02a3dayExistsInThisDataset = null;
                DateTime lastB03happening = new DateTime();
                DateTime lastB03happening_bonusCondition = new DateTime();
                DateTime lastP06violation = EmployeeDayBeforeEarliestWorkDate;
                DateTime lastM01happening = EmployeeDayBeforeEarliestWorkDate;
                int M01counter = CountM01Occurences(SELECTEDEMPLOYEE.EmployeeID);
                List<DateTime> M01counter_DatesToReclaimPADs = new List<DateTime>();

                SetLastB01Occurrence(ref lastB01happening);
                SetLastB02Occurrence(ref lastB02happening);
                SetLastB03Occurrence(ref lastB03happening, ref lastB03happening_bonusCondition);
                SetLastP06Occurrence(ref lastP06violation);
                SetLastM01Occurrence(ref lastM01happening);

                List<DateTime> checkedForNonAbsenceEffects = new List<DateTime>();
                double pointtotal = 0;

                if (!ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS && dt == null) dt = ANALYZEPOINTS_DATATABLE_CACHE;
                else ANALYZEPOINTS_DATATABLE_CACHE = null;

                /// analysis table
                if (dt == null || dt.Rows.Count <= 0)
                {
                    dt = new DataTable();
                    dt.Columns.Add("Pay Date");
                    dt.Columns.Add("Pay Code");
                    dt.Columns.Add("Net Hours");
                    dt.Columns.Add("Description");
                    dt.Columns.Add("Points");
                    dt.Columns.Add("Cumulative Points");
                }
                /// if this is being re-run with point forgiveness then strip out everything EXCEPT informational/rule-inserted rows
                else if (ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS &&
                         pRerunWithForgiveness)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string toTest = dt.Rows[i][1].ToString();
                        if (!toTest.StartsWith("ADDED: ") &&
                            !toTest.StartsWith("ADJUSTED: ") &&
                            !toTest.StartsWith("REMOVED: ") &&
                            !toTest.StartsWith("BONUS") &&
                            !toTest.StartsWith("PAD") &&
                            !toTest.StartsWith("PENALTY") &&
                            !toTest.StartsWith("FORGIVENESS"))
                        { dt.Rows.RemoveAt(i--); }
                    }
                }
                /// if this is being re-run to sort out point letters then strip out ALL point letter-related entries (except missed point letters which are also stored as bonuses)
                else if (!ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS &&
                         !pRerunWithForgiveness)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string toTest = dt.Rows[i][1].ToString();
                        if (toTest.StartsWith("POINT LEVEL REACHED") && 
                            toTest != "POINT LEVEL REACHED (MISSED LETTER)") { dt.Rows.RemoveAt(i--); }
                    }
                }

                if (ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS &&
                    !pRerunWithForgiveness)
                {
                    /// also determine if COVID-19 forgiveness day were granted for this employee
                    /// this is done in CheckForAndEnterManualAdjustments() but empty the date list here
                    COVID_FORGIVENESS_DAYS.Clear();

                    /// there are 3 types of manual adjustments --
                    /// add, edit, and remove
                    ///     add simply adds a new record into the dataset which did not exist before
                    ///     edit changes an entry and updates it with a new one, and
                    ///     remove gets rid of a record from the dataset
                    /// these impact how the data is displayed and what kind of information is conveyed to the user...
                    /// it's very important that these changes are made to TABLEENTRIES_LIST because that is
                    /// the datasource passed to each function during this analysis to determine rules, points, etc...
                    /// so, check for and include these adjustments FIRST
                    /// must come before the point-forgiveness reruns so the data is, according to the adjustments table, "correct"
                    TABLEENTRIES_LIST = CheckForAndEnterManualAdjustments(dt, TABLEENTRIES_LIST);

                    /// also enter any tracked bonuses and penalties this employee has earned in the past
                    /// (and get a total of how many bonus points this employee has earned for logging usage)
                    dt = CheckForAndEnterTrackedBonusesAndPenalties(dt);
                }

                /// iterate the table
                foreach (DataSource entry in TABLEENTRIES_LIST)
                {
                    string ceridianisannoying = entry.PayAdjCode_ShortName.Trim().ToUpper();
                    double nethours = Convert.ToDouble(entry.EmployeePaySummary_NetHours);

                    /// parse FMLA data
                    if ((ceridianisannoying == "FMLA ABSENT" ||
                         ceridianisannoying == "FMLA REFUSED OVERTIME" ||
                         ceridianisannoying == "FMLA VACATION") &&
                        (atrack.ADMINOPTION_DONOTIGNOREOBSOLETEPOINTS || entry.PayDate >= atrack.Date_StartOfRollingYear))
                    {
                        if (ceridianisannoying == "FMLA ABSENT" || ceridianisannoying == "FMLA REFUSED OVERTIME") fmlatotal_nonvacation += nethours;
                        else if (ceridianisannoying == "FMLA VACATION") fmlatotal_vacation += nethours;
                    }

                    /// then start the primary analysis
                    if (ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS)
                    {
                        /// check when an entry carries this attendance streak
                        if (pIncludeRules &&
                            !pRerunWithForgiveness &&
                            ATTENDANCECARRYINGPAYCODES_LIST.Contains(entry.PayAdjCode_ShortName) &&
                            !checkedForNonAbsenceEffects.Contains(entry.PayDate) &&
                            (atrack.ADMINOPTION_DONOTIGNOREOBSOLETEPOINTS || entry.PayDate >= atrack.Date_StartOfRollingYear))
                        {
                            /// THESE GRANT FORGIVENESS and will cause this logic to be re-run with those deductions applied
                            /// B01
                            /// work 90 consecutively scheduled days
                            if (entry.PayDate.Date >= lastB01happening.AddDays(90).Date &&
                                ConfirmaaaB01Benefit(ref lastB01happening, dt, entry.PayDate)) bonusesExistWhichHaveNotBeenApplied = true;
                            /// B03
                            /// work 14 consecutive calendar days with overtime; +7 consecutive calendar days with overtime
                            if (ConfirmOneOfThoseB03Deals(ref lastB03happening, ref lastB03happening_bonusCondition, dt, entry.PayDate)) bonusesExistWhichHaveNotBeenApplied = true;
                            /// M01
                            /// Perfect Attendance Bonus Day
                            if (entry.PayDate.Date >= lastM01happening.AddDays(60).Date) CheckIfM01ShouldBeGranted(ref M01counter_DatesToReclaimPADs, ref lastM01happening, ref M01counter, dt, entry.PayDate);

                            /// non-absence effects are only tested for once per day
                            checkedForNonAbsenceEffects.Add(entry.PayDate);
                        }

                        ///// only process demerits if a valid paycode match was found
                        ///// also only process this if the pay category was determined to be UNPAID
                        ///// this is a work around because something changed in Dayforce where
                        ///// entries with OT1.5 are being added as SICK ABSENT, or other absence pay codes, to make
                        ///// up an employee's weekly overtime balance...
                        ///// not sure why this happened, nobody else is, either, so just exclude them
                        //Guid demeritpaycodeid = atrack.PAYCODES_LIST.Where(z => z.Considered && z.Code.ToString().ToUpper() == ceridianisannoying).Select(z => z.ID).FirstOrDefault();
                        //if (demeritpaycodeid != new Guid() &&
                        //    entry.PayCategory_ShortName.Trim().ToUpper() == "UNPAID")
                        //{
                        /// only process demerits if a valid paycode match was found
                        Guid demeritpaycodeid = atrack.PAYCODES_LIST.Where(z => z.Considered && z.Code.ToString().ToUpper() == ceridianisannoying).Select(z => z.ID).FirstOrDefault();
                        if (demeritpaycodeid != new Guid())
                        {
                            foreach (Demerit demerit in atrack.DEMERITS_LIST)
                            {
                                if (demerit.UsesPayCodeID == null) continue;
                                else
                                {
                                    string[] arrtemp = demerit.UsesPayCodeID.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                                    List<string> arr = new List<string>();
                                    foreach (string arritem in arrtemp) { arr.Add(arritem.ToUpper()); }
                                    double points = demerit.Points; 
                                    if (arr.Contains(demeritpaycodeid.ToString().ToUpper()))
                                    {
                                        string infraction = demerit.Condition; 
                                        if (infraction.ToUpper() == "Absent".ToUpper())
                                        {
                                            /// late arrivals and early departures are calculated using the same Pay Code that generated an "Absent"
                                            /// the difference is in the amount of EmployeePaySummary_NetHours

                                            /// P03
                                            if (nethours <= 1.5)
                                            {
                                                points = 0.25;
                                                infraction = atrack.DEMERITS_LIST.Where(z => z.ID == new Guid("73B92582-6682-489D-9854-47F128D34052")).FirstOrDefault().Condition;
                                            }
                                            /// P04
                                            else if (nethours > 1.5 && nethours <= 5.5)
                                            {
                                                points = 0.50;
                                                infraction = atrack.DEMERITS_LIST.Where(z => z.ID == new Guid("F253801B-30C1-4981-8371-D002AE535B04")).FirstOrDefault().Condition;
                                            }
                                            /// P05
                                            else if (nethours > 5.5 && nethours < 8.0)
                                            {
                                                points = 1.00;
                                                infraction = atrack.DEMERITS_LIST.Where(z => z.ID == new Guid("21AD6980-8AA6-4E50-97B9-31251662AEE4")).FirstOrDefault().Condition;
                                            }
                                            /// P01 (standard "Absent")
                                            else
                                            {
                                                points = demerit.Points;
                                            }
                                        }
                                        
                                        double pointsReduced = points;
                                        bool bonusExists = ReducePenaltyByForgiveness(ref pointsReduced, entry);
                                        if (bonusExists)
                                        {
                                            if (entry.PayDate >= atrack.Date_StartOfRollingYear)
                                            {
                                                pointtotal += pointsReduced;
                                                if (pointtotal < 0) pointtotal = 0;
                                            }
                                            double pointsForBonusRow = points - pointsReduced;

                                            if (atrack.ADMINOPTION_DONOTIGNOREOBSOLETEPOINTS || entry.PayDate >= atrack.Date_StartOfRollingYear)
                                            { }
                                            else points = 0;

                                            dt.Rows.Add(new string[] { entry.PayDate.ToShortDateString(), ceridianisannoying, nethours.ToString(), infraction, "+ " + points, "n/a" });                                            

                                            DataRow newRow = dt.NewRow();
                                            newRow.ItemArray = new object[] { entry.PayDate.ToShortDateString(),
                                                                              "FORGIVENESS (BONUS POINTS)",
                                                                              "0.00",
                                                                              "Points reduced due to bonuses earned",
                                                                              "- " + pointsForBonusRow,
                                                                              "n/a" };
                                            dt.Rows.Add(newRow);
                                        }
                                        else
                                        {
                                            dt.Rows.Add(new string[] { entry.PayDate.ToShortDateString(), ceridianisannoying, nethours.ToString(), infraction, "+ " + points, "n/a" });

                                            if (entry.PayDate >= atrack.Date_StartOfRollingYear)
                                            {
                                                pointtotal += points;
                                                if (pointtotal < 0) pointtotal = 0;
                                            }                                            
                                        }

                                        if (pIncludeRules &&
                                            !pRerunWithForgiveness)
                                        {
                                            /// vvv these are now permanently granted by way of a signed letter by the employee, given out by a supervisor/manager
                                            ///// B02
                                            ///// 2-for-1 / 3-for-1 
                                            //if (entry.PayDate >= lastB02happening.Date.AddYears(1).Date)
                                            //{
                                                //if (entry.PayDate >= atrack.Date_StartOfRollingYear)
                                                //{
                                                    //    pointtotal += CheckForaaaB02Situation(dt, pointtotal, ref lastB02happening, ref B02a3dayExistsInThisDataset, entry.PayDate);
                                                    //    if (pointtotal < 0) pointtotal = 0;
                                                //} 
                                            //}                                                                                          

                                            /// P06
                                            /// employee has "chronically" late/left early (net hours < 8.0)
                                            if (CheckForP06Violation(TABLEENTRIES_LIST.Where(z => z.PayAdjCode_IsAbsence == "True").ToList(), entry, lastP06violation))
                                            {
                                                string chronicinfraction = atrack.DEMERITS_LIST.Where(z => z.ID == new Guid("98e5e247-0927-41ca-8fa1-35d236d88e88")).FirstOrDefault().Condition;
                                                double chronicpoints = atrack.DEMERITS_LIST.Where(z => z.ID == new Guid("98e5e247-0927-41ca-8fa1-35d236d88e88")).FirstOrDefault().Points;

                                                /// track this penalty acquisition
                                                bool penaltyExists_p06 = AddTrackedPenalty(entry.PayDate, "PENALTY", "0", chronicinfraction, chronicpoints, entry.PayAdjCode_ShortName, entry.PayDate);                                                                                       
                                                lastP06violation = entry.PayDate;
                                                double pointsReduced_p06 = chronicpoints;
                                                bool bonusExists_p06 = ReducePenaltyByForgiveness(ref pointsReduced_p06, entry);
                                                if (bonusExists_p06)
                                                {
                                                    if (entry.PayDate >= atrack.Date_StartOfRollingYear)
                                                    {
                                                        pointtotal += pointsReduced_p06;
                                                        if (pointtotal < 0) pointtotal = 0;
                                                    }
                                                    
                                                    double pointsForBonusRow_p06 = chronicpoints - pointsReduced_p06;

                                                    if (atrack.ADMINOPTION_DONOTIGNOREOBSOLETEPOINTS || entry.PayDate >= atrack.Date_StartOfRollingYear)
                                                    { }
                                                    else chronicpoints = 0;

                                                    if (penaltyExists_p06) dt.Rows.Add(new string[] { entry.PayDate.ToShortDateString(), "PENALTY", "0", chronicinfraction, "+ " + chronicpoints.ToString(), "n/a" });                                                    

                                                    DataRow newRow = dt.NewRow();
                                                    newRow.ItemArray = new object[] { entry.PayDate.ToShortDateString(),
                                                                                      "FORGIVENESS (BONUS POINTS)",
                                                                                      "0.00",
                                                                                      "Points reduced due to bonuses earned",
                                                                                      "- " + pointsForBonusRow_p06,
                                                                                      "n/a" };
                                                    dt.Rows.Add(newRow);
                                                }
                                                else
                                                {
                                                    if (penaltyExists_p06) dt.Rows.Add(new string[] { entry.PayDate.ToShortDateString(), "PENALTY", "0", chronicinfraction, "+ " + chronicpoints.ToString(), "n/a" });

                                                    if (entry.PayDate >= atrack.Date_StartOfRollingYear)
                                                    {
                                                        pointtotal += chronicpoints;
                                                        if (pointtotal < 0) pointtotal = 0;
                                                    }                                                        
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                /// re-run this analysis if there are points which are meant to be forgiven historically
                if (ANALYZEPOINTS_RUN_WITH_PRIMARY_ANALYSIS &&
                    pIncludeRules &&
                    !pRerunWithForgiveness &&
                    bonusesExistWhichHaveNotBeenApplied)
                {
                    AnalyzePoints(true, true, dt);
                    return;
                }

                /// add in the history of when point levels were reached
                if (pIncludeRules && atrack.ACTIONSTAKEN_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID))
                {
                    List<Guid> obsoleteActions_OriginalsIDs = new List<Guid>();
                    DateTime dayToCheck_From = (SELECTEDDATE_FROM != null && SELECTEDDATE_FROM != atrack.Date_EarliestPossible) ? Convert.ToDateTime(SELECTEDDATE_FROM) : EmployeeDayBeforeEarliestWorkDate;
                    DateTime dayToCheck_To = (SELECTEDDATE_TO != null && SELECTEDDATE_TO != atrack.Date_EarliestPossible) ? Convert.ToDateTime(SELECTEDDATE_TO) : DateTime.Today.Date;
                    foreach (ActionsTaken action in atrack.ACTIONSTAKEN_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                                                                                        z.DateOfOccurrence >= dayToCheck_From &&
                                                                                                        z.DateOfOccurrence <= dayToCheck_To)
                                                                            .OrderBy(z => z.PointValue)
                                                                            .ThenBy(z => z.DateOfOccurrence)
                                                                            .ToList())
                    {
                        /// obtain the position where this point letter notification should land
                        int insertAt = 0;
                        double pointValueCurrent = 0;
                        double pointValueToHit = action.PointValue;
                        DateTime paydateToHit = action.DateOfOccurrence;

                        /// these 2 tests filter out which point letters are displayed, where, and what the implied current point value is                                                
                        if (/// sets insertAt and pointValueCurrent
                            /// also checks to see if the point letter should be listed at all!
                            (!ObtainPositionAndPointLevelForNewPointLetter(dt, ref insertAt, ref pointValueCurrent, pointValueToHit, paydateToHit) ||
                             /// if the current point value is less than the action's point requirement then ignore this entry
                             /// this happens when adjustments are made to the data
                             pointValueCurrent < action.PointValue) &&
                             /// (unless its a missed letter, which are always displayed)
                             (!action.ActionTaken && action.IsLocked) == false)
                        { continue; }

                        DataRow newdr = dt.NewRow();
                        /// point level reached and action was taken
                        if (action.ActionTaken && !action.IsLocked)
                        {
                            try
                            {
                                string fullname = string.Empty;
                                if (atrack.USEROPTIONS_LIST.Any(z => z.Username.ToLower() == action.ActionTakenBy.ToLower()))
                                {
                                    fullname = atrack.USEROPTIONS_LIST.Where(z => z.Username.ToLower() == action.ActionTakenBy.ToLower()).First().FullName;
                                }
                                else fullname = action.ActionTakenBy;
                                DateTime? datetime = ((DateTime?)action.PointLetterDistributedOn) ?? action.ActionTakenOn;
                                newdr.ItemArray = new string[] { action.DateOfOccurrence.ToShortDateString(), "POINT LEVEL REACHED (LETTER GIVEN)", "0", action.PointValue.ToString() + " point level has been reached; letter distributed by " + fullname + " on " + ((DateTime)datetime).ToShortDateString(), "+ 0" };
                            }
                            catch
                            {
                                newdr.ItemArray = new string[] { action.DateOfOccurrence.ToShortDateString(), "POINT LEVEL REACHED (LETTER GIVEN)", "0", action.PointValue.ToString() + " point level has been reached", "+ 0" };
                            }

                            dt.Rows.InsertAt(newdr, insertAt);
                        }
                        /// point level reached and action is pending
                        else if (!action.ActionTaken && !action.IsLocked)
                        {
                            newdr.ItemArray = new string[] { action.DateOfOccurrence.ToShortDateString(), "POINT LEVEL REACHED (PENDING LETTER)", "0", action.PointValue.ToString() + " point level was reached but a letter has not yet been distributed", "+ 0" };

                            dt.Rows.InsertAt(newdr, insertAt);
                        }
                        /// point level reached but its obsolete/redundant
                        else if (action.ActionTaken && action.IsLocked)
                        {
                            ActionsTaken originalAction = atrack.ACTIONSTAKEN_LIST.Where(z => !obsoleteActions_OriginalsIDs.Contains(z.ID) &&
                                                                                                z.EmployeeID == action.EmployeeID &&
                                                                                                z.PointValue == action.PointValue &&
                                                                                                z.ActionTaken &&
                                                                                                !z.IsLocked)
                                                                                    .OrderBy(z => z.DateOfOccurrence)
                                                                                    .First();
                            /// the same "original" action cannot be referenced more than once
                            /// (otherwise the dates would not align properly)
                            /// when these are originally being built this is limited by both
                            /// .DateOfOccurence + .LastPointLevelActedOn, and
                            /// point letter cutoff date + .ActionTakenOn
                            /// here, however, since they are already built and confirmed to be correct, this is not necessary
                            obsoleteActions_OriginalsIDs.Add(originalAction.ID);
                            string fullname = string.Empty;
                            if (atrack.USEROPTIONS_LIST.Any(z => z.Username.ToLower() == originalAction.ActionTakenBy.ToLower()))
                            {
                                fullname = atrack.USEROPTIONS_LIST.Where(z => z.Username.ToLower() == originalAction.ActionTakenBy.ToLower()).First().FullName;
                            }
                            else fullname = originalAction.ActionTakenBy;
                            DateTime? datetime = ((DateTime?)originalAction.PointLetterDistributedOn) ?? originalAction.ActionTakenOn;
                            newdr.ItemArray = new string[] { action.DateOfOccurrence.ToShortDateString(), "POINT LEVEL REACHED (LETTER IS REDUNDANT)", "0", action.PointValue.ToString() + " point level has been reached; original letter distributed by " + fullname + " on " + ((DateTime)datetime).ToShortDateString(), "+ 0" };

                            dt.Rows.InsertAt(newdr, insertAt);
                        }
                        /// point level reached and action was missed
                        else if (!action.ActionTaken && action.IsLocked)
                        {
                            /// R06 2 of 2
                            /// determine the last-added point value which brought the total to this .action's .PointValue
                            double pointsaddedlast = 0.0;
                            double pointscurrent = 0.0;
                            foreach (DataRow dr in dt.Rows)
                            {
                                DateTime tempDate = Convert.ToDateTime(dr["Pay Date"].ToString().Replace(" ", ""));
                                double tempPoints = Convert.ToDouble(dr["Points"].ToString().Replace(" ", ""));
                                pointscurrent += tempPoints;
                                if (pointscurrent < 0) pointscurrent = 0;
                                if (tempDate.Date <= action.DateOfOccurrence.Date &&
                                    pointsaddedlast == 0.0 &&
                                    tempPoints > 0 &&
                                    pointscurrent >= action.PointValue) pointsaddedlast = tempPoints;
                                else if (tempDate.Date <= action.DateOfOccurrence.Date &&
                                            pointscurrent < action.PointValue) pointsaddedlast = 0.0;
                            }
                            /// and subtract that amount
                            pointtotal += (pointsaddedlast * -1);
                            if (pointtotal < 0) pointtotal = 0;

                            /// track this bonus acquisition but store a negative points val as this is applied right away
                            if (AddTrackedBonus(action.DateOfOccurrence,
                                                "POINT LEVEL REACHED (MISSED LETTER)",
                                                "0",
                                                action.PointValue.ToString() + " point level was reached but no letter was distributed which led to forgiveness (cutoff was " + Convert.ToDateTime(action.DateOfLapse).ToShortDateString() + ")",
                                                -pointsaddedlast))
                            {
                                /// because these points are deducted right away, and their "occurrence" dates are not stored like other bonuses
                                /// only display this row if the AddTrackedBonus() function returned true, indicating that a new record was added
                                /// if this is not a new record then this information will be added via CheckForAndEnterTrackedBonusesAndPenalties()
                                newdr.ItemArray = new string[] { action.DateOfOccurrence.ToShortDateString(), "POINT LEVEL REACHED (MISSED LETTER)", "0", action.PointValue.ToString() + " point level was reached but no letter was distributed which led to forgiveness (cutoff was " + Convert.ToDateTime(action.DateOfLapse).ToShortDateString() + ")", "- " + pointsaddedlast.ToString() };

                                dt.Rows.InsertAt(newdr, insertAt);
                            }
                        }
                        else continue;
                    }
                }

                /// reposition entries which are not rebuilt upon each and every run of this function
                /// those are manual adjustments, bonuses, penalties, and forgivenesses
                /// these are the same rows which are excluded from the datatable wipe at the top 
                RepositionAllManualAdjustmentRows(ref dt);
                RepositionAllOtherInformationRows(ref dt);

                /// bind data
                GridView thisGrid = gcEmployeeEvents.MainView as GridView;
                gcEmployeeEvents.DataSource = null;
                if (!GRIDVIEW2PROPSSET) thisGrid.Columns.Clear();
                ANALYZEPOINTS_DATATABLE_CACHE = null;
                gcEmployeeEvents.DataSource = ANALYZEPOINTS_DATATABLE_CACHE = dt;
                thisGrid.OptionsView.BestFitMode = GridBestFitMode.Fast;
                thisGrid.OptionsView.BestFitMaxRowCount = 100;
                thisGrid.BestFitColumns();
                thisGrid.FocusedRowHandle = -1;
                if (!GRIDVIEW2PROPSSET) SetGridView2ColumnOptions();

                /// points, bonuses, and fmla
                hlinkEmployeePoints.Text = pointtotal.ToString("N2");
                hlinkEmployeeBonuses.Text = employeeBonusCounter.ToString("N2");
                lblFMLATotalHours.Text = (fmlatotal_nonvacation + fmlatotal_vacation).ToString("N2");
                lblFMLANonVacationHours.Text = fmlatotal_nonvacation.ToString("N2");
                lblFMLAVacationHours.Text = fmlatotal_vacation.ToString("N2");

                if (fmlatotal_nonvacation >= 32 && atrack.FMLA_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID && z.Notified == true)) labelFMLAHRNotified.Visible = true;
                else labelFMLAHRNotified.Visible = false;
            }
            catch (Exception ex)
            {
                if (atrack.UPDATEACTIVE) { MessageBox.Show("Please close the page you are on and try again.", "Data Update Failed", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                else { atrack.LogAndDisplayError(ex.ToString(), true); }
            }
        }
        private void AnalyzePoints_DisplayCache()
        {
            try
            {
                /// analysis table
                DataTable dt = new DataTable();
                dt.Columns.Add("Pay Date");
                dt.Columns.Add("Pay Code");
                dt.Columns.Add("Net Hours");
                dt.Columns.Add("Description");
                dt.Columns.Add("Points");
                dt.Columns.Add("Cumulative Points");

                /// point tracker
                double pointtotal = 0;
                foreach (HighPoint hp in atrack.dataset.HighPoints.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                                                              z.Pay_Date >= EmployeeDayBeforeEarliestWorkDate)
                                                                  .OrderBy(z => z.index))
                {
                    if (atrack.ADMINOPTION_DONOTIGNOREOBSOLETEPOINTS || hp.Pay_Date >= atrack.Date_StartOfRollingYear)
                    {
                        pointtotal += hp.Points;
                        if (pointtotal < 0) pointtotal = 0;
                        dt.Rows.Add(new string[] { hp.Pay_Date.ToShortDateString(), hp.Pay_Code, hp.Net_Hours.ToString("N2"), hp.Description, hp.Points.ToString("N2"), pointtotal.ToString("N2") });
                    }
                    else
                    {
                        /// allow negative point additions from the past to appear along with their point value
                        if (hp.Points < 0) dt.Rows.Add(new string[] { hp.Pay_Date.ToShortDateString(), hp.Pay_Code, hp.Net_Hours.ToString("N2"), hp.Description, hp.Points.ToString("N2"), "0.00" });
                        else dt.Rows.Add(new string[] { hp.Pay_Date.ToShortDateString(), hp.Pay_Code, hp.Net_Hours.ToString("N2"), hp.Description, "0.00", "0.00" });
                    }
                }

                GridView thisGrid = gcEmployeeEvents.MainView as GridView;
                gcEmployeeEvents.DataSource = null;
                if (!GRIDVIEW2PROPSSET) thisGrid.Columns.Clear();
                gcEmployeeEvents.DataSource = dt;
                thisGrid.OptionsView.BestFitMode = GridBestFitMode.Fast;
                thisGrid.OptionsView.BestFitMaxRowCount = 100;
                thisGrid.BestFitColumns();
                thisGrid.FocusedRowHandle = -1;
                if (!GRIDVIEW2PROPSSET) SetGridView2ColumnOptions();

                /// obtain this employee's bonuses
                atrack.BindBonusesData(SELECTEDEMPLOYEE.EmployeeID);
                /// count the number of bonus points this employee is eligible for
                /// reset employee's bonus counter first
                employeeBonusCounter = 0.00;
                foreach (double pointsToAdd in atrack.BONUSES_LIST.Where(z => z.IsParent &&
                                                                              (z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID || z.EmployeeID == "ALL") &&
                                                                              z.BonusDate >= EmployeeDayBeforeEarliestWorkDate &&
                                                                              z.BonusPayCode == "BONUS" &&
                                                                              !z.BonusDescription.StartsWith("Employee earned a Perfect Attendance Bonus Day"))
                                                                  .Select(z => z.BonusPointsRemaining)
                                                                  .ToList())
                {
                    /// all bonuses are worth 1 point...
                    if (pointsToAdd >= 0) employeeBonusCounter += 1;
                    /// ...except this guy which was granted due to the union negotiation and contract update which granted 5 bonus points to all employees
                    else employeeBonusCounter += Math.Abs(pointsToAdd);
                }

                hlinkEmployeePoints.Text = pointtotal.ToString("N2");
                hlinkEmployeeBonuses.Text = employeeBonusCounter.ToString("N2");
                lblFMLATotalHours.Text = (SELECTEDEMPLOYEE.FMLA_NonVacation + SELECTEDEMPLOYEE.FMLA_Vacation).ToString("N2");
                lblFMLANonVacationHours.Text = SELECTEDEMPLOYEE.FMLA_NonVacation.ToString("N2");
                lblFMLAVacationHours.Text = SELECTEDEMPLOYEE.FMLA_Vacation.ToString("N2");

                if (SELECTEDEMPLOYEE.FMLA_NonVacation >= 32 && atrack.FMLA_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID && z.Notified == true)) labelFMLAHRNotified.Visible = true;
                else labelFMLAHRNotified.Visible = false;
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }

        private bool ReducePenaltyByForgiveness(ref double pPointsForInfraction, DataSource pEntry)
        {
            if (atrack.BONUSES_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                             z.TargetPayCode == pEntry.PayAdjCode_ShortName &&
                                             z.TargetPayDate == pEntry.PayDate))
            {
                TrackedBonus bonusToUse = atrack.BONUSES_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                                                         z.TargetPayCode == pEntry.PayAdjCode_ShortName &&
                                                                         z.TargetPayDate == pEntry.PayDate)
                                                             .OrderBy(z => z.TargetPointValue)
                                                             .First();

                if (listConsumedBonusesForEmployee.Contains(bonusToUse)) return false;
                else
                {
                    listConsumedBonusesForEmployee.Add(bonusToUse);
                    pPointsForInfraction = bonusToUse.TargetPointValue;
                    return true;
                }
            }

            bool found = false;
            if (pPointsForInfraction > 0 &&
                (atrack.ADMINOPTION_DONOTIGNOREOBSOLETEPOINTS || pEntry.PayDate >= atrack.Date_StartOfRollingYear))
            {
                /// bonuses cannot be applied if the date in question has already been forgiven due to a 2- or 3-for-1
                if (!atrack.BONUSES_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                                  ((z.BonusPayCode == "FORGIVENESS (2-FOR-1)" && z.BonusDate == pEntry.PayDate) ||
                                                  (z.BonusPayCode == "FORGIVENESS (3-FOR-1)" && (z.BonusDate == pEntry.PayDate || z.BonusDate.AddDays(-1) == pEntry.PayDate)))))
                {
                    foreach (TrackedBonus bonus in atrack.BONUSES_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                                                                  z.BonusDate >= pEntry.PayDate &&
                                                                                  z.BonusPointsRemaining > 0 &&
                                                                                  z.TargetPayCode == string.Empty)
                                                                      .OrderBy(z => z.BonusDate)
                                                                      .ToList())
                    {
                        found = true;
                        TrackedBonus bonusInDB = atrack.dataset.TrackedBonuses.Where(z => z.ID == bonus.ID).First();

                        if (bonus.BonusPointsRemaining >= pPointsForInfraction)
                        {
                            double bonusRemainder = bonus.BonusPointsRemaining - pPointsForInfraction;
                            if (bonusRemainder > 0)
                            {
                                TrackedBonus newBonusForPointRemainder = new TrackedBonus();
                                newBonusForPointRemainder.ID = Guid.NewGuid();
                                newBonusForPointRemainder.EmployeeID = bonus.EmployeeID;
                                newBonusForPointRemainder.BonusDate = bonus.BonusDate;
                                newBonusForPointRemainder.BonusPayCode = bonus.BonusPayCode;
                                newBonusForPointRemainder.BonusNetHours = bonus.BonusNetHours;
                                newBonusForPointRemainder.BonusDescription = bonus.BonusDescription;
                                /// update .BonusPointsRemaining accordingly
                                newBonusForPointRemainder.BonusPointsRemaining = bonusRemainder;
                                newBonusForPointRemainder.TargetPayCode = string.Empty;
                                newBonusForPointRemainder.TargetPayDate = null;
                                newBonusForPointRemainder.TargetPointValue = 0.0;
                                newBonusForPointRemainder.IsParent = false;
                                atrack.dataset.TrackedBonuses.Add(newBonusForPointRemainder);
                                atrack.BONUSES_LIST.Add(newBonusForPointRemainder);
                                /// save changes to db
                                atrack.dataset.SaveChanges();
                            }

                            bonus.BonusPointsRemaining = bonusInDB.BonusPointsRemaining = bonusRemainder;
                            bonus.TargetPayCode = bonusInDB.TargetPayCode = pEntry.PayAdjCode_ShortName;
                            bonus.TargetPayDate = bonusInDB.TargetPayDate = pEntry.PayDate;
                            bonus.TargetPointValue = bonusInDB.TargetPointValue = pPointsForInfraction = 0;
                            /// save changes to db
                            atrack.dataset.SaveChanges();

                            listConsumedBonusesForEmployee.Add(bonus);

                            /// stop looping
                            return found;
                        }
                        else
                        {
                            pPointsForInfraction -= bonus.BonusPointsRemaining;

                            bonus.BonusPointsRemaining = bonusInDB.BonusPointsRemaining = 0;
                            bonus.TargetPayCode = bonusInDB.TargetPayCode = pEntry.PayAdjCode_ShortName;
                            bonus.TargetPayDate = bonusInDB.TargetPayDate = pEntry.PayDate;
                            bonus.TargetPointValue = bonusInDB.TargetPointValue = pPointsForInfraction;
                            /// save changes to db
                            atrack.dataset.SaveChanges();

                            listConsumedBonusesForEmployee.Add(bonus);

                            /// keep looping
                        }
                    }
                }
            }            

            return found;
        }

        private bool AddTrackedBonus(DateTime pBonusDate, string pBonusPayCode, string pBonusNetHours, string pBonusDescription, double pBonusPoints)
        {
            double touse_NetHours = Convert.ToDouble(pBonusNetHours.Replace(" ", ""));
            if (atrack.BONUSES_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                             z.BonusDate == pBonusDate &&
                                             z.BonusPayCode == pBonusPayCode &&
                                             z.BonusNetHours == touse_NetHours &&
                                             z.BonusDescription == pBonusDescription))
            {
                if (pBonusPayCode == "POINT LEVEL REACHED (MISSED LETTER)" && pBonusPoints > 0)
                {
                    int count = atrack.BONUSES_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                                               z.BonusDate == pBonusDate &&
                                                               z.BonusPayCode == pBonusPayCode &&
                                                               z.BonusNetHours == touse_NetHours &&
                                                               z.BonusDescription == pBonusDescription)
                               .Count();
                    /// this goes along with 'freepass' and will permit a duplicated missed letter once per level+date
                    if (count > 1) return false;
                }
                else return false;
            }

            TrackedBonus bonus = new TrackedBonus();
            bonus.ID = Guid.NewGuid();
            bonus.EmployeeID = SELECTEDEMPLOYEE.EmployeeID;
            bonus.BonusDate = pBonusDate;
            bonus.BonusPayCode = pBonusPayCode;
            bonus.BonusNetHours = touse_NetHours;
            bonus.BonusDescription = pBonusDescription;
            bonus.BonusPointsRemaining = pBonusPoints;
            bonus.TargetPayCode = string.Empty;
            bonus.TargetPayDate = null;
            bonus.TargetPointValue = 0.0;
            bonus.IsParent = true;
            atrack.dataset.TrackedBonuses.Add(bonus);
            atrack.dataset.SaveChanges();
            atrack.BONUSES_LIST.Add(bonus);

            return true;
        }
        private bool AddTrackedPenalty(DateTime pPenaltyPayDate, string pPenaltyPayCode, string pPenaltyNetHours, string pPenaltyDescription, double pPenaltyPoints, string pOriginatingPayCode, DateTime pOriginatingPayDate)
        {
            double touse_NetHours = Convert.ToDouble(pPenaltyNetHours.Replace(" ", ""));
            if (atrack.PENALTIES_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                               z.PenaltyDate == pPenaltyPayDate &&
                                               z.PenaltyPayCode == pPenaltyPayCode &&
                                               z.PenaltyNetHours == touse_NetHours &&
                                               z.PenaltyDescription == pPenaltyDescription))
            { return false; }

            TrackedPenalty penalty = new TrackedPenalty();
            penalty.ID = Guid.NewGuid();
            penalty.EmployeeID = SELECTEDEMPLOYEE.EmployeeID;
            penalty.PenaltyDate = pPenaltyPayDate;
            penalty.PenaltyPayCode = pPenaltyPayCode;
            penalty.PenaltyNetHours = touse_NetHours;
            penalty.PenaltyDescription = pPenaltyDescription;
            penalty.PenaltyPoints = pPenaltyPoints;
            penalty.OriginatingPayCode = pOriginatingPayCode;
            penalty.OriginatingPayDate = pOriginatingPayDate;
            atrack.dataset.TrackedPenalties.Add(penalty);
            atrack.dataset.SaveChanges();
            atrack.PENALTIES_LIST.Add(penalty);

            return true;
        }

        private void GridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            string ceridianisannoying = string.Empty;
            DateTime date = new DateTime();
            try
            { 
                ceridianisannoying = gridView1.GetRowCellValue(e.RowHandle, "PayAdjCode_ShortName").ToString();
                date = Convert.ToDateTime(gridView1.GetRowCellValue(e.RowHandle, "PayDate").ToString());
            }
            catch { return; }

            ceridianisannoying = ceridianisannoying.Trim().ToUpper();
            /// COVID-19 provision; test first to override other color schemes
            if (ATTENDANCEBREAKINGPAYCODES_LIST.Contains(ceridianisannoying) &&
                date.Date >= COVID_FORGIVENESS_START &&
                date.Date <= COVID_FORGIVENESS_END)
            {
                e.HighPriority = true;
                e.Appearance.BackColor = Color.Black;
                e.Appearance.ForeColor = Color.White;
            }
            else if (atrack.PAYCODES_LIST.Any(z => z.Considered && z.Code.ToString().ToUpper() == ceridianisannoying))
            {
                e.HighPriority = true;
                e.Appearance.BackColor = Color.IndianRed;
                e.Appearance.ForeColor = Color.White;
            }
            else if (ceridianisannoying.Contains("FMLA"))
            {
                e.HighPriority = true;
                e.Appearance.BackColor = Color.LightGoldenrodYellow;
                e.Appearance.ForeColor = Color.Black;
            }
        }
        private void gridView2_RowStyle(object sender, RowStyleEventArgs e)
        {
            try
            {
                if (e.RowHandle < 0) { return; }
                string entry = gridView2.GetRowCellValue(e.RowHandle, "Pay Code").ToString();
                DateTime date = Convert.ToDateTime(gridView2.GetRowCellValue(e.RowHandle, "Pay Date").ToString());
                if (date < atrack.Date_StartOfRollingYear)
                {
                    e.HighPriority = true;
                    e.Appearance.BackColor = Color.Gray;
                    e.Appearance.ForeColor = Color.Black;
                    e.Appearance.FontStyleDelta = FontStyle.Italic;
                }
                else if (entry == "POINT LEVEL REACHED (LETTER GIVEN)" ||
                         entry == "POINT LEVEL REACHED (LETTER IS REDUNDANT)")
                {
                    e.HighPriority = true;
                    e.Appearance.BackColor = Color.LimeGreen;
                    e.Appearance.ForeColor = Color.Black;
                }
                else if (entry == "POINT LEVEL REACHED (PENDING LETTER)")
                {
                    e.HighPriority = true;
                    e.Appearance.BackColor = Color.LightGoldenrodYellow;
                    e.Appearance.ForeColor = Color.Black;
                }
                else if (entry == "POINT LEVEL REACHED (MISSED LETTER)")
                {
                    e.HighPriority = true;
                    e.Appearance.BackColor = Color.IndianRed;
                    e.Appearance.ForeColor = Color.White;
                }
                else if (entry.StartsWith("ADDED: ") ||
                         entry.StartsWith("ADJUSTED: ") ||
                         entry.StartsWith("REMOVED: "))
                {
                    e.HighPriority = true;
                    e.Appearance.BackColor = Color.Black;
                    e.Appearance.ForeColor = Color.White;
                }
                else if (entry.StartsWith("BONUS") ||
                         entry.StartsWith("FORGIVENESS"))
                {
                    e.HighPriority = true;
                    e.Appearance.BackColor = Color.DarkBlue;
                    e.Appearance.ForeColor = Color.White;
                }
                else if (entry.StartsWith("PAD"))
                {
                    e.HighPriority = true;
                    e.Appearance.BackColor = Color.Purple;
                    e.Appearance.ForeColor = Color.White;
                }
                else if (entry.StartsWith("PENALTY"))
                {
                    e.HighPriority = true;
                    e.Appearance.BackColor = Color.Orange;
                    e.Appearance.ForeColor = Color.Black;
                }
            }
            catch { return; }
        }

        private void ClearPointDetailArea()
        {
            gcEmployeeEvents.DataSource = null;
            lblFMLATotalHours.Text =
            lblFMLANonVacationHours.Text =
            lblFMLAVacationHours.Text =
            hlinkEmployeeBonuses.Text = 
            hlinkEmployeePoints.Text = "#";
            ShowPointPanels(false);
            atrack.Refresh();
        }

        private void ClearEmployeeInfo()
        {
            lblEmployeeType.Text =
            lblDepartment.Text =
            lblShift.Text =
            lblSupervisor.Text =
            lblSeniorityDate.Text =
            lblRehireDate.Text = "n/a";
        }

        private void SetLastB01Occurrence(ref DateTime pLastDate)
        {
            string bonus = atrack.MERITS_LIST.Where(z => z.ID == new Guid("6EB36AF2-CEAF-4BC4-973C-00DDCD0715C2")).FirstOrDefault().Condition;
            if (atrack.BONUSES_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID && z.BonusDescription == bonus))
            {
                pLastDate = atrack.BONUSES_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID && z.BonusDescription == bonus)
                                               .OrderByDescending(z => z.BonusDate)
                                               .First()
                                               .BonusDate;
            }
        }
        private void SetLastB02Occurrence(ref DateTime pLastDate)
        {
            string bonus = atrack.MERITS_LIST.Where(z => z.ID == new Guid("c9d9cd01-8d2d-4dec-9802-2e592fbc2e44")).FirstOrDefault().Condition;
            if (atrack.BONUSES_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID && z.BonusDescription == bonus))
            {
                pLastDate = atrack.BONUSES_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID && z.BonusDescription == bonus)
                                               .OrderByDescending(z => z.BonusDate)
                                               .First()
                                               .BonusDate;
            }
        }
        private void SetLastB03Occurrence(ref DateTime pLastDate, ref DateTime pLastDate_extra)
        {
            string bonus = atrack.MERITS_LIST.Where(z => z.ID == new Guid("b11abece-e93e-4d5d-9ed7-29fdb2e89b62")).FirstOrDefault().Condition;
            if (atrack.BONUSES_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID && z.BonusDescription == bonus))
            {
                pLastDate = atrack.BONUSES_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID && z.BonusDescription == bonus)
                                               .OrderByDescending(z => z.BonusDate)
                                               .First()
                                               .BonusDate;
            }

            string bonusextra = atrack.MERITS_LIST.Where(z => z.ID == new Guid("9e1f54fc-c1db-4110-92fd-a851acd781db")).FirstOrDefault().Condition;
            if (atrack.BONUSES_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID && z.BonusDescription == bonusextra))
            {
                pLastDate_extra = atrack.BONUSES_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID && z.BonusDescription == bonusextra)
                                                     .OrderByDescending(z => z.BonusDate)
                                                     .First()
                                                     .BonusDate;
            }
        }
        private void SetLastP06Occurrence(ref DateTime pLastDate)
        {
            string penalty = atrack.DEMERITS_LIST.Where(z => z.ID == new Guid("98e5e247-0927-41ca-8fa1-35d236d88e88")).FirstOrDefault().Condition;
            if (atrack.PENALTIES_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID && z.PenaltyDescription == penalty))
            {
                pLastDate = atrack.PENALTIES_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID && z.PenaltyDescription == penalty)
                                                 .OrderByDescending(z => z.PenaltyDate)
                                                 .First()
                                                 .PenaltyDate;
            }
        }
        private void SetLastM01Occurrence(ref DateTime pLastDate)
        {
            if (atrack.BONUSES_LIST.Any(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID && z.BonusPayCode == "PAD"))
            {
                pLastDate = atrack.BONUSES_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID && z.BonusPayCode == "PAD")
                                               .OrderByDescending(z => z.BonusDate)
                                               .First()
                                               .BonusDate;
            }
        }

        private bool ConfirmaaaB01Benefit(ref DateTime pLastHappening, DataTable pDT, DateTime pDateToTest)
        {
            try
            {
                DateTime? cutoff = FindXCompleteWorkDaysAFTERDateForThisDataSource(90,
                                                                                   TABLEENTRIES_LIST,
                                                                                   pLastHappening,
                                                                                   true);
                if (cutoff != null && cutoff <= pDateToTest)
                {
                    pLastHappening = Convert.ToDateTime(cutoff);
                    string attendancebonus = atrack.MERITS_LIST.Where(z => z.ID == new Guid("6EB36AF2-CEAF-4BC4-973C-00DDCD0715C2")).FirstOrDefault().Condition;
                    double attendancepoints = 1.00;
                    /// track this bonus acquisition as it must be applied to the earliest absence
                    if (AddTrackedBonus(pLastHappening, "BONUS", "0.00", attendancebonus, attendancepoints))
                    {
                        pDT.Rows.Add(new string[] { pLastHappening.ToShortDateString(), "BONUS", "0.00", attendancebonus, "- 0", "n/a" });
                        employeeBonusCounter++;
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
                return false;
            }
        }
        private double CheckForaaaB02Situation(DataTable pDT, double pPointTotal, ref DateTime pLastB02happening, ref bool? pB02a3dayExistsInThisDataset, DateTime pPayDate)
        {
            try
            {
                /// if it has not yet been determined, check if there is a 3-for-1 occurrence in this dataset
                if (pB02a3dayExistsInThisDataset == null)
                {
                    foreach (DataSource row in TABLEENTRIES_LIST)
                    {
                        if (CheckIfXWorkdaysHaveBeenConsecutivelyMissedAfterDate(3, row.PayDate, false, ref pLastB02happening))
                        {
                            pB02a3dayExistsInThisDataset = true;
                            break;
                        }
                    }
                }
                if (pB02a3dayExistsInThisDataset == null) pB02a3dayExistsInThisDataset = false;

                /// only check for a 2-for-1 if there is no 3-for-1 in this dataset
                if (pB02a3dayExistsInThisDataset == false && CheckIfXWorkdaysHaveBeenConsecutivelyMissedAfterDate(2, pPayDate, true, ref pLastB02happening))
                {
                    string bonus = atrack.MERITS_LIST.Where(z => z.ID == new Guid("c9d9cd01-8d2d-4dec-9802-2e592fbc2e44")).FirstOrDefault().Condition;
                    /// 1 point is deducted
                    double bonuspoints = 1.0;
                    if (pPointTotal < bonuspoints) bonuspoints = pPointTotal;

                    /// track this bonus acquisition but store a negative points val as this is applied right away
                    if (AddTrackedBonus(pPayDate, "FORGIVENESS (2-FOR-1)", "0", bonus, -bonuspoints))
                    {
                        pDT.Rows.Add(new string[] { pPayDate.ToShortDateString(), "FORGIVENESS (2-FOR-1)", "0", bonus, "- " + bonuspoints, "n/a" });
                    }

                    if (atrack.ADMINOPTION_DONOTIGNOREOBSOLETEPOINTS || pPayDate >= atrack.Date_StartOfRollingYear) return (bonuspoints * -1);
                    else return 0;
                }
                /// only check for a 3-for-1 if it was confirmed that one exists in this dataset
                else if (pB02a3dayExistsInThisDataset == true && CheckIfXWorkdaysHaveBeenConsecutivelyMissedAfterDate(3, pPayDate, true, ref pLastB02happening))
                {
                    string bonus = atrack.MERITS_LIST.Where(z => z.ID == new Guid("c9d9cd01-8d2d-4dec-9802-2e592fbc2e44")).FirstOrDefault().Condition;
                    /// 2 points are deducted
                    double bonuspoints = 2.0;
                    if (pPointTotal < bonuspoints) bonuspoints = pPointTotal;

                    /// track this bonus acquisition but store a negative points val as this is applied right away
                    if (AddTrackedBonus(pPayDate, "FORGIVENESS (3-FOR-1)", "0", bonus, -bonuspoints))
                    {
                        pDT.Rows.Add(new string[] { pPayDate.ToShortDateString(), "FORGIVENESS (3-FOR-1)", "0", bonus, "- " + bonuspoints, "n/a" });
                    }

                    if (atrack.ADMINOPTION_DONOTIGNOREOBSOLETEPOINTS || pPayDate >= atrack.Date_StartOfRollingYear) return (bonuspoints * -1);
                    else return 0;
                }
                else return 0.0;
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
                return 0.0;
            }
        }

        private bool ConfirmOneOfThoseB03Deals(ref DateTime pLastHappening, ref DateTime pLastHappening_bonusCondition, DataTable pDT, DateTime pDateToTest)
        {
            try
            {
                DateTime? dateWhenThisRuleCanRecur = atrack.Date_EarliestPossible;

                /// PayDate is later than the last occurrence of B03
                if (pDateToTest > pLastHappening)
                {
                    /// the last occurrence is defined (so this employee has reached it before)                    
                    if (pLastHappening > atrack.Date_EarliestPossible)
                    {
                        /// determine if a break in absence + 1 full work week has elapsed between the last occurrence and this PayDate
                        dateWhenThisRuleCanRecur = DidAGapAndFullWorkWeekElapseBetweenTheseDatesForThisDataSource(pLastHappening, pDateToTest, TABLEENTRIES_LIST);
                        /// will not null if not
                    }

                    /// check for instance of B03
                    if (dateWhenThisRuleCanRecur != null &&
                        DetermineIfXConsecutiveCalendarDaysWereWorkedAndIncludeOverTimeOnOrBeforeDateForThisDataSource(14, TABLEENTRIES_LIST.Where(z => z.PayDate >= dateWhenThisRuleCanRecur).ToList(), pDateToTest))
                    {
                        /// get params
                        string attendancebonus = atrack.MERITS_LIST.Where(z => z.ID == new Guid("b11abece-e93e-4d5d-9ed7-29fdb2e89b62")).FirstOrDefault().Condition;
                        double attendancepoints = 1.00;

                        /// set last occurrence date
                        pLastHappening = pDateToTest;

                        /// track this bonus acquisition as it must be applied to the earliest absence
                        if (AddTrackedBonus(pDateToTest, "BONUS", "0.00", attendancebonus, attendancepoints))
                        {                            
                            /// add record and note the date for later
                            pDT.Rows.Add(new string[] { pDateToTest.ToShortDateString(), "BONUS", "0.00", attendancebonus, "- 0", "n/a" });
                            employeeBonusCounter++;
                            return true;
                        }
                    }

                    /// bonus condition; because this tests for calendar days, check and see if PayDate is 7 days (which is the bonus requirement) from the last B03 occurrence
                    if (pLastHappening_bonusCondition != pLastHappening &&
                        pLastHappening > atrack.Date_EarliestPossible &&
                        pDateToTest.Date == pLastHappening.AddDays(7).Date)
                    {
                        DateTime theNextDay = pLastHappening.AddDays(1);
                        if (DetermineIfXConsecutiveCalendarDaysWereWorkedAndIncludeOverTimeOnOrBeforeDateForThisDataSource(7, TABLEENTRIES_LIST.Where(z => z.PayDate >= theNextDay).ToList(), pDateToTest))
                        {
                            /// get params
                            string attendancebonusextra = atrack.MERITS_LIST.Where(z => z.ID == new Guid("9e1f54fc-c1db-4110-92fd-a851acd781db")).FirstOrDefault().Condition;
                            double attendancepointsextra = 1.00;

                            /// set last occurrence date
                            pLastHappening_bonusCondition = pLastHappening = pDateToTest;
                            /// track this bonus acquisition as it must be applied to the earliest absence
                            if (AddTrackedBonus(pDateToTest, "BONUS", "0.00", attendancebonusextra, attendancepointsextra))
                            {
                                /// add record and note the date for later
                                pDT.Rows.Add(new string[] { pDateToTest.ToShortDateString(), "BONUS", "0.00", attendancebonusextra, "- 0", "n/a" });
                                employeeBonusCounter++;
                                return true;
                            }
                        }
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
                return false;
            }
        }

        private bool CheckForP06Violation(List<DataSource> pEntries, DataSource pEntry, DateTime pDateOfLastViolation)
        {
            try
            {
                /// "Unexcused", "NO CALL/NO SHOW", "SICK ABSENT" should be changed to check for those values in the DB instead
                /// make note to .Trim().ToUpper() PayAdjCode_ShortName
                /// similar to CheckIfXWorkdaysHaveBeenConsecutivelyMissedAfterDate()

                DateTime thisViolationDate = pEntry.PayDate;
                DateTime EarliestDateToNabThisSucker = thisViolationDate.AddMonths(-2);
                if (pEntries.Where(z => z.PayDate > pDateOfLastViolation &&
                                        z.PayDate >= EarliestDateToNabThisSucker &&
                                        z.PayDate <= thisViolationDate &&
                                        z.EmployeePaySummary_NetHours < 8 &&
                                        z.PayAdjCode_IsAbsence == "True" &&
                                        new string[] { "Unexcused", "NO CALL/NO SHOW", "SICK ABSENT" }.Contains(z.PayAdjCode_ShortName) &&
                                        z.EmployeePaySummary_NetHours < 8).Count() >= 4) return true;
                else return false;
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
                return false;
            }
        }

        private void CheckIfM01ShouldBeGranted(ref List<DateTime> pM01counter_DatesToReclaimPADs, ref DateTime pLastHappening, ref int pHappeningCounter, DataTable pDT, DateTime pPayDate)
        {
            try
            {
                bool success = false;
                int type = 0;
                DateTime pLastHappening_copy = pLastHappening;
                DateTime avoidMarshalByReference = EmployeeDayBeforeEarliestWorkDate;
                if (pLastHappening == avoidMarshalByReference)
                {
                    if (!TABLEENTRIES_LIST.Any(z => z.PayDate >= pLastHappening_copy &&
                                                    z.PayDate <= pPayDate &&
                                                    ATTENDANCECARRYINGPAYCODES_LIST.Contains(z.PayAdjCode_ShortName))) return;
                    pLastHappening_copy =
                    pLastHappening = TABLEENTRIES_LIST.Where(z => z.PayDate >= pLastHappening_copy &&
                                                                  z.PayDate <= pPayDate &&
                                                                  ATTENDANCECARRYINGPAYCODES_LIST.Contains(z.PayAdjCode_ShortName))
                                                      .Select(z => z.PayDate)
                                                      .Distinct()
                                                      .OrderBy(z => z.Date)
                                                      .First()
                                                      .AddDays(-1);
                }

                /// there can't be any missed scheduled days
                /// this includes a special rule where any absences removed during the set COVID-19 forgiveness dates
                /// will also break the attendance streak; this is an HR directive
                if (TABLEENTRIES_LIST.Any(z => z.PayDate >= pLastHappening_copy &&
                                               z.PayDate <= pPayDate &&
                                               ATTENDANCEBREAKINGPAYCODES_LIST.Contains(z.PayAdjCode_ShortName.Trim().ToUpper())))
                {
                    /// if there are missed scheduled days then set the date on which this rule last occurred to the paydate of the next entry following a violation
                    /// this will make it so this logic does not re-consider dates which are already known to violate this rule
                    DateTime lastViolation = TABLEENTRIES_LIST.Where(z => z.PayDate > pLastHappening_copy &&
                                                                          z.PayDate > pPayDate &&
                                                                          ATTENDANCECARRYINGPAYCODES_LIST.Contains(z.PayAdjCode_ShortName.Trim().ToUpper()))
                                                              .OrderBy(z => z.PayDate)
                                                              .First()
                                                              .PayDate;

                    pLastHappening_copy = pLastHappening = lastViolation.AddDays(1);

                    return;
                }

                /// if at least 4 months have elapsed then check for the 4 month rule
                if (pLastHappening_copy <= pPayDate.AddMonths(-4))
                {
                    success = true;
                    type = 1;
                }
                /// otherwise 693 net hours must be hit
                else
                {
                    /// check all non-absence entries

                    /// *** be sure to use .Distinct() because there are duplicate entries in Ceridian based on shift, OT, etc.
                    /// .Distinct() is called on a combination of .PayPeriod, .RoundedInOutPunches, and .EmployeePaySummary_NetHours
                    /// this helps to ensure that any duplicate entries with the same PayPeriod, InOutPunches, and NetHours values are removed
                    /// otherwise, if .Distinct() is called on *only* NetHours then there is a very good/100% chance that meaningful data will be disregarded

                    decimal nonAbsenceHours = 0;
                    foreach (var entry in TABLEENTRIES_LIST.Where(z => z.PayDate >= pLastHappening_copy &&
                                                                       z.PayDate <= pPayDate &&
                                                                       ATTENDANCECARRYINGPAYCODES_LIST.Contains(z.PayAdjCode_ShortName))
                                                           .Select(z => new { z.PayPeriod, z.RoundedInOutPunches, z.EmployeePaySummary_NetHours })
                                                           .Distinct()
                                                           .Select(z => z.EmployeePaySummary_NetHours))
                    { nonAbsenceHours += entry; }

                    if (nonAbsenceHours >= 693)
                    {
                        success = true;
                        type = 2;
                    }
                }

                if (success && type > 0)
                {
                    if (pM01counter_DatesToReclaimPADs.Any(z => z.Date <= pPayDate.AddYears(-1).AddDays(-1)))
                    {
                        List<DateTime> datesToRemove = pM01counter_DatesToReclaimPADs.Where(z => z.Date <= pPayDate.AddYears(-1).AddDays(-1)).ToList();
                        foreach (DateTime dateToRemove in datesToRemove) { pM01counter_DatesToReclaimPADs.RemoveAt(pM01counter_DatesToReclaimPADs.IndexOf(dateToRemove)); }
                        pHappeningCounter -= datesToRemove.Count();
                    }

                    /// this can only occur 3 times within a year/rolling 12-month period
                    if (pHappeningCounter + 1 <= 3 && 
                        pM01counter_DatesToReclaimPADs.Count() + 1 <= 3)
                    {
                        pHappeningCounter++;
                        if (type == 1)
                        {
                            /// try to shift this bonus entry to be exactly 4 months after the last occurrence...
                            /// this can become skewed because qualifying for this bonus does not require ANY specific pay code entry
                            /// it simply requires a duration of time where certain pay codes were NOT found
                            /// even though this function will only be called the next time a pay code is found within ATTENDANCECARRYINGPAYCODES_LIST
                            /// this dateDelta fix will (try to) backdate it to the proper spot
                            int dateDelta = (pPayDate.AddMonths(-4) - pLastHappening_copy).Days;
                            string padMsg = "Employee earned a Perfect Attendance Bonus Day (worked 4 consecutive calendar months without an absence, " + pHappeningCounter.ToString() + " of 3 per year)";
                            /// track this bonus acquisition but do not store any bonus points as this is symbolic
                            if (AddTrackedBonus(pPayDate.AddDays(-dateDelta), "PAD", "0.00", padMsg, 0))
                            {
                                pDT.Rows.Add(new string[] { pPayDate.AddDays(-dateDelta).ToShortDateString(), "BONUS", "0.00", padMsg, "+ 0", "n/a" });
                            }
                            pLastHappening_copy = pLastHappening = pPayDate.AddDays(-dateDelta);
                            pM01counter_DatesToReclaimPADs.Add(pLastHappening);
                        }
                        else if (type == 2)
                        {
                            string padMsg = "Employee earned a Perfect Attendance Bonus Day (worked 693 hours without an absence, " + pHappeningCounter.ToString() + " of 3 per year)";
                            /// track this bonus acquisition but do not store any bonus points as this is symbolic
                            if (AddTrackedBonus(pPayDate, "PAD", "0.00", padMsg, 0))
                            {
                                pDT.Rows.Add(new string[] { pPayDate.ToShortDateString(), "BONUS", "0.00", padMsg, "+ 0", "n/a" });
                            }
                            pLastHappening_copy = pLastHappening = pPayDate;
                            pM01counter_DatesToReclaimPADs.Add(pLastHappening);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
                return;
            }
        }

        private DateTime? FindXCompleteWorkDaysAFTERDateForThisDataSource(int pX, List<DataSource> pSource, DateTime pDate, bool pMustBeConsecutive)
        {
            try
            {
                DateTime day = pDate;
                int workdays = 0;
                do
                {
                    day = day.AddDays(1);
                    if (day > DateTime.Today || !pSource.Any(z => z.PayDate >= day))
                    {
                        return null;
                    }
                    /// for B01
                    else if (pMustBeConsecutive)
                    {
                        /// there can't be any missed scheduled days or any time missed during a day which was worked
                        /// this includes a special rule where any absences removed during the set COVID-19 forgiveness dates
                        /// will also break the attendance streak; this is an HR directive
                        /// if anything is found which breaks attendance then reset the workdays counter
                        if (pSource.Any(z => z.PayDate == day &&
                                             ATTENDANCEBREAKINGPAYCODES_LIST.Contains(z.PayAdjCode_ShortName.Trim().ToUpper())))
                        {
                            /// this is reset to 0 because pMustBeConsecutive is used
                            workdays = 0;
                        }
                        /// otherwise, if nothing broke the attendance streak
                        /// and if there is a non-absence found for that day then increment and test the workdays counter
                        else if (pSource.Any(z => ATTENDANCECARRYINGPAYCODES_LIST.Contains(z.PayAdjCode_ShortName) &&
                                                  z.PayDate == day) &&
                                ++workdays == pX)
                        {
                            return day;
                        }
                    }
                    /// for 14 day point letter lockout
                    else if (!pMustBeConsecutive)
                    {
                        /// there can't be any missed scheduled days or any time missed during a day which was worked
                        /// this includes a special rule where any absences removed during the set COVID-19 forgiveness dates
                        /// will also break the attendance streak; this is an HR directive
                        /// otherwise, if nothing broke the attendance streak
                        /// and if there is work found for that day then increment and test the workdays counter
                        /// note that nothing resets the workdays counter in this case as pMustBeConsecutive is not used here
                        if (!pSource.Any(z => z.PayDate == day &&
                                              ATTENDANCEBREAKINGPAYCODES_LIST.Contains(z.PayAdjCode_ShortName.Trim().ToUpper())) &&
                            pSource.Any(z => ATTENDANCECARRYINGPAYCODES_LIST.Contains(z.PayAdjCode_ShortName) &&
                                             z.PayDate == day) &&
                            ++workdays == pX)
                        {
                            return day;
                        }
                    }
                } while (true);
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
                return null;
            }
        }
        private DateTime? DidAGapAndFullWorkWeekElapseBetweenTheseDatesForThisDataSource(DateTime pDateOne, DateTime pDateTwo, List<DataSource> pSource)
        {
            try
            {
                /// rule can recur on the Monday after a full work week elapses following a break in absence

                DateTime mondayWhenRuleCanRecur = new DateTime(9999, 12, 31);
                bool foundAttendanceBreak = false;
                DateTime dateWhenAttendanceStreakWasBroken = new DateTime();
                DateTime day = pDateOne.AddDays(1);
                while (day <= pDateTwo)
                {
                    /// test #1
                    /// attendance not yet broken and there isn't any work logged on this PayDate
                    if (!foundAttendanceBreak &&
                        pSource.Any(z => z.PayDate == day && ATTENDANCECARRYINGPAYCODES_LIST.Contains(z.PayAdjCode_ShortName)) == false)
                    {
                        /// attendance break was found; record date
                        foundAttendanceBreak = true;
                        dateWhenAttendanceStreakWasBroken = day;
                    }

                    /// test #2
                    /// when attendance break is found
                    if (foundAttendanceBreak)
                    {
                        /// loop until a Monday is hit
                        if (day.DayOfWeek == DayOfWeek.Monday)
                        {
                            /// if Monday is the same day that the attendance streak was broken then 2 weeks must be added
                            /// which is the completion of the current week plus one full work weel
                            if (dateWhenAttendanceStreakWasBroken == day) mondayWhenRuleCanRecur = day.AddDays(14);
                            /// otherwise only 1 week must be added to the date to determine when this rule can recur
                            else mondayWhenRuleCanRecur = day.AddDays(7);
                            break;
                        }
                    }

                    day = day.AddDays(1);
                }

                /// if tests #1 and 2 pass then return true
                if (foundAttendanceBreak && mondayWhenRuleCanRecur <= pDateTwo) return mondayWhenRuleCanRecur;
                else return null;
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
                return null;
            }
        }
        private bool DetermineIfXConsecutiveCalendarDaysWereWorkedAndIncludeOverTimeOnOrBeforeDateForThisDataSource(int pX, List<DataSource> pSource, DateTime pDate)
        {
            try
            {
                DateTime day = pDate;
                int workdays = 0;
                bool foundovertime = false;
                do
                {
                    if (pSource.Any(z => ATTENDANCECARRYINGPAYCODES_LIST.Contains(z.PayAdjCode_ShortName) &&
                                         z.PayDate == day))
                    {
                        if (!foundovertime)
                        {
                            List<string> todayspaycategories = pSource.Where(z => z.PayDate == day && ATTENDANCECARRYINGPAYCODES_LIST.Contains(z.PayAdjCode_ShortName)).Select(z => z.PayCategory_ShortName).ToList();
                            foreach (string category in todayspaycategories) { if (category.StartsWith("OT", StringComparison.Ordinal) || category.EndsWith("OT", StringComparison.Ordinal)) { foundovertime = true; break; } }
                        }
                        if (++workdays == pX) { return foundovertime; }
                    }
                    else return false;
                    day = day.AddDays(-1);
                } while (true);
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
                return false;
            }
        }
        private bool CheckIfXWorkdaysHaveBeenConsecutivelyMissedAfterDate(int pX, DateTime pPayDate, bool pLogEventDate, ref DateTime pLastB02happening)
        {
            try
            {
                string[] demeritIDs = atrack.DEMERITS_LIST.Where(z => z.Condition == "Absent").First().UsesPayCodeID.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                List<Guid> demeritGUIDs = new List<Guid>();
                foreach (string entry in demeritIDs) { demeritGUIDs.Add(new Guid(entry)); }
                List<string> listPayCodes = atrack.PAYCODES_LIST.Where(z => demeritGUIDs.Contains(z.ID)).Select(z => z.Code).ToList();
                int consecutive = 0;
                DateTime day = pPayDate;
                do
                {
                    if (!TABLEENTRIES_LIST.Any(z => z.PayDate == day))
                    { }
                    else if (TABLEENTRIES_LIST.Any(z => z.PayDate == day &&
                                                        z.EmployeePaySummary_NetHours >= 8 &&
                                                        listPayCodes.Contains(z.PayAdjCode_ShortName)))
                    {
                        if (++consecutive == pX)
                        {
                            if (pLogEventDate) pLastB02happening = pPayDate;
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }

                    day = day.AddDays(-1);
                    if (day < EmployeeDayBeforeEarliestWorkDate) return false;
                } while (true);
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
                return false;
            }
        }

        private List<DataSource> CheckForAndEnterManualAdjustments(DataTable pDT, List<DataSource> pPTDS)
        {
            try
            {
                /// NO DATABASE RECORDS ARE MODIFIED DURING THIS PROCESS BECAUSE pPTDS CONTAINS A DEEP COPY
                IEnumerable<DataSource> eDS;

                /// ADDS
                /// check if there are any matches
                /// can bulk add items using .EmployeeID as "ALL"
                if (atrack.ADJUSTMENTS_LIST.Any(z => (z.EmployeeID.ToUpper() == "ALL" || z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID) &&
                                                     z.DateToAdjust >= EmployeeDayBeforeEarliestWorkDate &&
                                                     z.ActualEntry_PayCode == string.Empty &&
                                                     z.AdjustedEntry_PayCode != string.Empty &&
                                                     z.ActualEntry_NetHours == 0 &&
                                                     z.AdjustedEntry_NetHours != 0))
                {
                    /// if so grab them
                    List<ManualAdjustment> mAdjusts = atrack.ADJUSTMENTS_LIST.Where(z => (z.EmployeeID.ToUpper() == "ALL" || z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID) &&
                                                                                         z.DateToAdjust >= EmployeeDayBeforeEarliestWorkDate &&
                                                                                         z.ActualEntry_PayCode == string.Empty &&
                                                                                         z.AdjustedEntry_PayCode != string.Empty &&
                                                                                         z.ActualEntry_NetHours == 0 &&
                                                                                         z.AdjustedEntry_NetHours != 0)
                                                                             .OrderBy(z => z.DateToAdjust)
                                                                             .ThenBy(z => z.ActualEntry_PayCode)
                                                                             .ToList();
                    foreach (ManualAdjustment mAdjust in mAdjusts)
                    {
                        /// reused
                        //////////////////////////////////////////////////////////////
                        string id = mAdjust.EmployeeID;
                        CurrentEmployee cEmployee = atrack.EMPLOYEES_LIST.Where(z => z.EmployeeID == mAdjust.EmployeeID).First();
                        string name = cEmployee.EmployeeName;
                        string department = cEmployee.Department;
                        DateTime paydate = mAdjust.DateToAdjust;
                        //string actual_paycode = mAdjust.ActualEntry_PayCode.Trim().ToUpper();
                        //decimal actual_nethours = mAdjust.ActualEntry_NetHours;
                        //string actual_nethours_string = mAdjust.ActualEntry_NetHours.ToString("N2");
                        string adjusted_paycode = mAdjust.AdjustedEntry_PayCode.Trim().ToUpper();
                        decimal adjusted_nethours = mAdjust.AdjustedEntry_NetHours;
                        string adjusted_nethours_string = mAdjust.AdjustedEntry_NetHours.ToString("N2");
                        string note = mAdjust.ShortNote;
                        //////////////////////////////////////////////////////////////                        

                        /// add in a visual indicator of this change (it will be repositioned later via RepositionAllManualAdjustmentRows())
                        DataRow newDR = pDT.NewRow();
                        newDR.ItemArray = new object[] { paydate.ToShortDateString(),
                                                         "ADDED: " + adjusted_paycode,
                                                         adjusted_nethours_string,
                                                         "Added because: " + note,
                                                         "+ 0" };
                        pDT.Rows.Add(newDR);

                        /// add new data
                        DataSource newEntry = new DataSource();
                        newEntry.ID = Guid.NewGuid();
                        newEntry.IsTotalItem = string.Empty;
                        newEntry.EmployeeEmploymentStatus_EmployeeNumber = id;
                        newEntry.Employee_DisplayName = name;
                        newEntry.OrgUnit_ShortName = department;
                        newEntry.Department_ShortName = string.Empty;
                        newEntry.Job_ShortName = string.Empty;
                        newEntry.PayPeriod = string.Empty;
                        newEntry.PayDate = paydate;
                        newEntry.PayAdjCode_ShortName = adjusted_paycode;
                        newEntry.PayCategory_ShortName = string.Empty;
                        newEntry.EmployeePaySummary_NetHours = adjusted_nethours;
                        newEntry.RoundedInOutPunches = string.Empty;
                        newEntry.EmployeeComments = string.Empty;
                        newEntry.ManagerComments = string.Empty;
                        newEntry.PayAdjCode_IsAbsence = string.Empty;
                        newEntry.EmployeePayAdjust_ReferenceDate = string.Empty;
                        newEntry.IsCurrentRecord = true;
                        newEntry.EntryDate = EmployeeDayBeforeEarliestWorkDate;
                        newEntry.RemovalDate = null;
                        pPTDS.Add(newEntry);
                    }
                }

                /// EDITS
                /// check if there are any matches
                /// can bulk edit items using .EmployeeID as "ALL" and .actual_paycode as "ALL" (and .ActualEntry_NetHours as 24)
                if (atrack.ADJUSTMENTS_LIST.Any(z => (z.EmployeeID.ToUpper() == "ALL" || z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID) &&
                                                     z.DateToAdjust >= EmployeeDayBeforeEarliestWorkDate &&
                                                     z.ActualEntry_PayCode != string.Empty &&
                                                     z.AdjustedEntry_PayCode != string.Empty &&
                                                     z.ActualEntry_NetHours != 0 &&
                                                     z.AdjustedEntry_NetHours != 0))
                {
                    /// if so grab them
                    List<ManualAdjustment> mAdjusts = atrack.ADJUSTMENTS_LIST.Where(z => (z.EmployeeID.ToUpper() == "ALL" || z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID) &&
                                                                                         z.DateToAdjust >= EmployeeDayBeforeEarliestWorkDate &&
                                                                                         z.ActualEntry_PayCode != string.Empty &&
                                                                                         z.AdjustedEntry_PayCode != string.Empty &&
                                                                                         z.ActualEntry_NetHours != 0 &&
                                                                                         z.AdjustedEntry_NetHours != 0)
                                                                             .OrderBy(z => z.DateToAdjust)
                                                                             .ThenBy(z => z.ActualEntry_PayCode)
                                                                             .ToList();
                    foreach (ManualAdjustment mAdjust in mAdjusts)
                    {
                        /// reused
                        //////////////////////////////////////////////////////////////
                        string id = mAdjust.EmployeeID;
                        //CurrentEmployee cEmployee = EMPLOYEES_LIST.Where(z => z.EmployeeID == mAdjust.EmployeeID).First();
                        //string name = cEmployee.EmployeeName;
                        //string department = cEmployee.Department;
                        DateTime paydate = mAdjust.DateToAdjust;
                        string actual_paycode = mAdjust.ActualEntry_PayCode.Trim().ToUpper();
                        decimal actual_nethours = mAdjust.ActualEntry_NetHours;
                        string actual_nethours_string = mAdjust.ActualEntry_NetHours.ToString("N2");
                        string adjusted_paycode = mAdjust.AdjustedEntry_PayCode.Trim().ToUpper();
                        decimal adjusted_nethours = mAdjust.AdjustedEntry_NetHours;
                        string adjusted_nethours_string = mAdjust.AdjustedEntry_NetHours.ToString("N2");
                        string note = mAdjust.ShortNote;
                        //////////////////////////////////////////////////////////////

                        /// find and modify data
                        /// make note to .Trim().ToUpper() PayAdjCode_ShortName
                        /// ALL_entails will be the "hooks" that can grab and correct data...
                        /// must exclude WRK, BRK, MEAL, etc. as there are simply too many of those to adjust
                        /// if something is not marked as .Considered then it will have to be referred to directly rather than via ALL
                        List<string> ALL_entails = atrack.PAYCODES_LIST.Where(z => z.Considered).Select(z => z.Code.Trim().ToUpper()).ToList();
                        /// must also include FMLA pay codes
                        ALL_entails.Add("FMLA ABSENT");
                        ALL_entails.Add("FMLA REFUSED OVERTIME");
                        ALL_entails.Add("FMLA VACATION");
                        eDS = pPTDS.Where(z => z.PayDate == paydate &&
                                               (id == "ALL" || z.EmployeeEmploymentStatus_EmployeeNumber == id) &&
                                               ((actual_paycode == "ALL" && ALL_entails.Contains(z.PayAdjCode_ShortName.Trim().ToUpper())) || z.PayAdjCode_ShortName.Trim().ToUpper() == actual_paycode) &&
                                               (actual_nethours == 24 || z.EmployeePaySummary_NetHours == actual_nethours));

                        for (int i = 0; i < eDS.Count(); i++)
                        {
                            /// unique to EDITS...
                            /// check if the original paycode and nethours values are the same as the values
                            /// they're being replaced by... and, if so, skip this entry
                            if (adjusted_paycode == eDS.ElementAt(i).PayAdjCode_ShortName.Trim().ToUpper() &&
                                adjusted_nethours == eDS.ElementAt(i).EmployeePaySummary_NetHours)
                            { continue; }

                            /// add in a visual indicator of this change (it will be repositioned later via RepositionAllManualAdjustmentRows())
                            DataRow newDR = pDT.NewRow();
                            newDR.ItemArray = new object[] { paydate.ToShortDateString(),
                                                            //"ADJUSTED: " + actual_paycode,
                                                            "ADJUSTED: " + eDS.ElementAt(i).PayAdjCode_ShortName.Trim().ToUpper(),
                                                            actual_nethours == 24 ? eDS.ElementAt(i).EmployeePaySummary_NetHours.ToString() : actual_nethours_string,
                                                            "Replaced with Pay Code '" + adjusted_paycode + "', Net Hours '" + adjusted_nethours_string + "' because: " + note,
                                                            "+ 0" };
                            pDT.Rows.Add(newDR);

                            /// modify entries
                            DataSource ptds = pPTDS.Where(z => z.ID == eDS.ElementAt(i).ID).First();
                            ptds.PayAdjCode_ShortName = adjusted_paycode;
                            ptds.EmployeePaySummary_NetHours = adjusted_nethours;
                        }
                    }
                }

                /// REMOVES
                /// check if there are any matches
                /// can bulk remove items using .EmployeeID as "ALL" and .actual_paycode as "ALL" (and .ActualEntry_NetHours as 24)
                if (atrack.ADJUSTMENTS_LIST.Any(z => (z.EmployeeID.ToUpper() == "ALL" || z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID) &&
                                                     z.DateToAdjust >= EmployeeDayBeforeEarliestWorkDate &&
                                                     z.ActualEntry_PayCode != string.Empty &&
                                                     z.AdjustedEntry_PayCode == string.Empty &&
                                                     z.ActualEntry_NetHours != 0 &&
                                                     z.AdjustedEntry_NetHours == 0))
                {
                    /// if so grab them
                    List<ManualAdjustment> mAdjusts = atrack.ADJUSTMENTS_LIST.Where(z => (z.EmployeeID.ToUpper() == "ALL" || z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID) &&
                                                                                         z.DateToAdjust >= EmployeeDayBeforeEarliestWorkDate &&
                                                                                         z.ActualEntry_PayCode != string.Empty &&
                                                                                         z.AdjustedEntry_PayCode == string.Empty &&
                                                                                         z.ActualEntry_NetHours != 0 &&
                                                                                         z.AdjustedEntry_NetHours == 0)
                                                                             .OrderBy(z => z.DateToAdjust)
                                                                             .ThenBy(z => z.ActualEntry_PayCode)
                                                                             .ToList();
                    foreach (ManualAdjustment mAdjust in mAdjusts)
                    {
                        /// reused
                        //////////////////////////////////////////////////////////////
                        string id = mAdjust.EmployeeID;
                        //CurrentEmployee cEmployee = EMPLOYEES_LIST.Where(z => z.EmployeeID == mAdjust.EmployeeID).First();
                        //string name = cEmployee.EmployeeName;
                        //string department = cEmployee.Department;
                        DateTime paydate = mAdjust.DateToAdjust;
                        string actual_paycode = mAdjust.ActualEntry_PayCode.Trim().ToUpper();
                        decimal actual_nethours = mAdjust.ActualEntry_NetHours;
                        string actual_nethours_string = mAdjust.ActualEntry_NetHours.ToString("N2");
                        //string adjusted_paycode = mAdjust.AdjustedEntry_PayCode.Trim().ToUpper();
                        //decimal adjusted_nethours = mAdjust.AdjustedEntry_NetHours;
                        //string adjusted_nethours_string = mAdjust.AdjustedEntry_NetHours.ToString("N2");
                        string note = mAdjust.ShortNote;
                        //////////////////////////////////////////////////////////////

                        /// find and remove data
                        /// make note to .Trim().ToUpper() PayAdjCode_ShortName
                        /// ALL_entails will be the "hooks" that can grab and correct data...
                        /// must exclude WRK, BRK, MEAL, etc. as there are simply too many of those to adjust
                        /// if something is not marked as .Considered then it will have to be referred to directly rather than via ALL
                        List<string> ALL_entails = atrack.PAYCODES_LIST.Where(z => z.Considered).Select(z => z.Code.Trim().ToUpper()).ToList();
                        /// must also include FMLA pay codes
                        ALL_entails.Add("FMLA ABSENT");
                        ALL_entails.Add("FMLA REFUSED OVERTIME");
                        ALL_entails.Add("FMLA VACATION");
                        eDS = pPTDS.Where(z => z.PayDate == mAdjust.DateToAdjust &&
                                               (id == "ALL" || z.EmployeeEmploymentStatus_EmployeeNumber == id) &&
                                               ((actual_paycode == "ALL" && ALL_entails.Contains(z.PayAdjCode_ShortName.Trim().ToUpper())) || z.PayAdjCode_ShortName.Trim().ToUpper() == actual_paycode) &&
                                               (actual_nethours == 24 || z.EmployeePaySummary_NetHours == actual_nethours));
                        if (eDS.Count() > 0)
                        {
                            /// SPECIAL COVID-19 PROVISION...
                            /// any pay code which is contained in ATTENDANCEBREAKINGPAYCODES_LIST
                            /// but was REMOVED due to the COVID-19 forgiveness period
                            /// ...ranging from COVID_FORGIVENESS_START to COVID_FORGIVENESS_END...
                            /// will still count as a break in an attendance streak
                            if (pPTDS.Any(z => z.PayDate >= COVID_FORGIVENESS_START &&
                                               z.PayDate <= COVID_FORGIVENESS_END &&
                                               ATTENDANCEBREAKINGPAYCODES_LIST.Contains(z.PayAdjCode_ShortName.Trim().ToUpper())))
                            {
                                COVID_FORGIVENESS_DAYS.AddRange(pPTDS.Where(z => z.PayDate >= COVID_FORGIVENESS_START &&
                                                                                 z.PayDate <= COVID_FORGIVENESS_END &&
                                                                                 ATTENDANCEBREAKINGPAYCODES_LIST.Contains(z.PayAdjCode_ShortName.Trim().ToUpper()))
                                                                     .Select(z => z.PayDate.Date)
                                                                     .Distinct()
                                                                     .ToList());
                            }

                            /// pPTDS impacts eDS, so removing entries from it also removes them from eDS
                            /// so, use a sneaky loop and always refer to element 0
                            for (int i = 0; true;)
                            {
                                /// add in a visual indicator of this change (it will be repositioned later via RepositionAllManualAdjustmentRows())
                                DataRow newDR = pDT.NewRow();
                                newDR.ItemArray = new object[] { paydate.ToShortDateString(),
                                                                //"REMOVED: " + actual_paycode,
                                                                "REMOVED: " + eDS.ElementAt(i).PayAdjCode_ShortName.Trim().ToUpper(),
                                                                actual_nethours == 24 ? eDS.ElementAt(i).EmployeePaySummary_NetHours.ToString() : actual_nethours_string,
                                                                "Removed because: " + note,
                                                                "+ 0" };
                                pDT.Rows.Add(newDR);

                                /// remove elements
                                DataSource ptds = pPTDS.Where(z => z.ID == eDS.ElementAt(i).ID).First();
                                /// SPECIAL COVID-19 PROVISION...
                                /// if this entry was removed due to COVID-19 then add in a placeholder for other aspects of this application
                                /// to see and respond to; THIS WILL NEVER ACTUALLY BE SEEN IN THE APPLICATION
                                /// BECAUSE THE TABLE BEING MODIFIED IS ONLY TO GENERATE THE POINTS TABLE
                                /// if this is not done then these COVID-19 removals will fly totally under the radar
                                /// and will never be seen or have a chance to impact data                                
                                if (COVID_FORGIVENESS_DAYS.Contains(ptds.PayDate.Date))
                                {
                                    DataSource newEntry = new DataSource();
                                    newEntry.ID = Guid.NewGuid();
                                    newEntry.IsTotalItem = ptds.IsTotalItem;
                                    newEntry.EmployeeEmploymentStatus_EmployeeNumber = ptds.EmployeeEmploymentStatus_EmployeeNumber;
                                    newEntry.Employee_DisplayName = ptds.Employee_DisplayName;
                                    newEntry.OrgUnit_ShortName = ptds.OrgUnit_ShortName;
                                    newEntry.Department_ShortName = ptds.Department_ShortName;
                                    newEntry.Job_ShortName = ptds.Job_ShortName;
                                    newEntry.PayPeriod = ptds.PayPeriod;
                                    newEntry.PayDate = ptds.PayDate;
                                    newEntry.PayAdjCode_ShortName = COVID_PLACEHOLDER_TEXT;
                                    newEntry.PayCategory_ShortName = ptds.PayCategory_ShortName;
                                    newEntry.EmployeePaySummary_NetHours = ptds.EmployeePaySummary_NetHours;
                                    newEntry.RoundedInOutPunches = ptds.RoundedInOutPunches;
                                    newEntry.EmployeeComments = ptds.EmployeeComments;
                                    newEntry.ManagerComments = ptds.ManagerComments;
                                    newEntry.PayAdjCode_IsAbsence = ptds.PayAdjCode_IsAbsence;
                                    newEntry.EmployeePayAdjust_ReferenceDate = ptds.EmployeePayAdjust_ReferenceDate;
                                    newEntry.IsCurrentRecord = ptds.IsCurrentRecord;
                                    newEntry.EntryDate = ptds.EntryDate;
                                    newEntry.RemovalDate = ptds.RemovalDate;
                                    int index = pPTDS.IndexOf(ptds);
                                    pPTDS.Insert(index, newEntry);
                                }
                                pPTDS.Remove(ptds);

                                if (eDS.Count() == 0) break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }

            /// re-order by paydate
            return pPTDS.OrderBy(z => z.PayDate).ToList();
        }

        private DataTable CheckForAndEnterTrackedBonusesAndPenalties(DataTable pDT)
        {
            /// count the number of bonus points this employee is eligible for
            /// reset employee's bonus counter first
            employeeBonusCounter = 0.00;
            foreach (double pointsToAdd in atrack.BONUSES_LIST.Where(z => z.IsParent &&
                                                                          (z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID || z.EmployeeID == "ALL") &&
                                                                          z.BonusDate >= EmployeeDayBeforeEarliestWorkDate &&
                                                                          z.BonusPayCode == "BONUS" &&
                                                                          !z.BonusDescription.StartsWith("Employee earned a Perfect Attendance Bonus Day"))
                                                              .Select(z => z.BonusPointsRemaining)
                                                              .ToList())
            {
                /// all bonuses are worth 1 point...
                if (pointsToAdd >= 0) employeeBonusCounter += 1;
                /// ...except this guy which was granted due to the union negotiation and contract update which granted 5 bonus points to all employees
                else employeeBonusCounter += Math.Abs(pointsToAdd);
            }

            /// reset list of consumed bonuses
            listConsumedBonusesForEmployee.Clear();

            foreach (TrackedBonus bonus in atrack.BONUSES_LIST.Where(z => z.IsParent &&
                                                                          (z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID || z.EmployeeID == "ALL") &&
                                                                          z.BonusDate >= EmployeeDayBeforeEarliestWorkDate)
                                                              .OrderBy(z => z.BonusDate)
                                                              .ThenBy(z => z.TargetPayDate))
            {
                /// add bonus row
                DataRow newDR = pDT.NewRow();
                double pointsToApply = 0.00;
                if (bonus.BonusPointsRemaining < 0) pointsToApply = bonus.BonusPointsRemaining;
                newDR.ItemArray = new object[] { bonus.BonusDate.ToShortDateString(),
                                                 bonus.BonusPayCode,
                                                 bonus.BonusNetHours.ToString(),
                                                 bonus.BonusDescription,
                                                 pointsToApply.ToString() };
                pDT.Rows.Add(newDR);
            }
            foreach (TrackedPenalty penalty in atrack.PENALTIES_LIST.Where(z => z.EmployeeID == SELECTEDEMPLOYEE.EmployeeID &&
                                                                                z.PenaltyDate >= EmployeeDayBeforeEarliestWorkDate))
            {
                DataRow newDR = pDT.NewRow();
                newDR.ItemArray = new object[] { penalty.PenaltyDate.ToShortDateString(),
                                                 penalty.PenaltyPayCode,
                                                 penalty.PenaltyNetHours.ToString(),
                                                 penalty.PenaltyDescription,
                                                 penalty.PenaltyPoints };
                pDT.Rows.Add(newDR);
            }

            return pDT;
        }

        private bool ObtainPositionAndPointLevelForNewPointLetter(DataTable pDT, ref int insertAt, ref double pointValueCurrent, double pPointValueToHit, DateTime pPaydateToHit)
        {
            try
            {
                foreach (DataRow dr in pDT.Rows)
                {
                    double drPoints = Convert.ToDouble(dr["Points"].ToString().Replace(" ", ""));
                    DateTime drPayDate = Convert.ToDateTime(dr["Pay Date"]);
                    string drPayCode = dr["Pay Code"].ToString();
                   
                    insertAt++;
                    if (atrack.ADMINOPTION_DONOTIGNOREOBSOLETEPOINTS || drPayDate >= atrack.Date_StartOfRollingYear)
                    {
                        pointValueCurrent += drPoints;
                        if (pointValueCurrent < 0) pointValueCurrent = 0;
                    }
                    else continue;

                    if (drPayCode.StartsWith("ADDED: ") ||
                        drPayCode.StartsWith("ADJUSTED: ") ||
                        drPayCode.StartsWith("REMOVED: ") ||
                        drPayCode.StartsWith("BONUS") ||
                        drPayCode.StartsWith("PAD") ||
                        drPayCode.StartsWith("PENALTY") ||
                        drPayCode.StartsWith("FORGIVENESS") ||
                        drPayCode.StartsWith("POINT LEVEL REACHED")) continue;                  

                    if (pointValueCurrent >= pPointValueToHit && drPayDate.Date == pPaydateToHit.Date) return true;
                    else if (drPayDate.Date > pPaydateToHit.Date) return false;
                }
                /// still display this row, at the bottom, if pPaydateToHit was never surpassed
                return true;
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
                return false;
            }
        }
        private void RepositionAllManualAdjustmentRows(ref DataTable pDT)
        {
            /// manual adjustments are specifically sorted to be near the entry which they are adjusting

            /// all entries start at the top of the screen, properly sorted chronologically

            /// track checked rows
            List<string> processed = new List<string>();
            /// loop all rows
            for (int i = 0; i < pDT.Rows.Count; i++)
            {
                /// get pay code and test it; continue if not ideal
                string dtPayCode = pDT.Rows[i]["Pay Code"].ToString();
                if (!dtPayCode.StartsWith("ADDED: ") &&
                    !dtPayCode.StartsWith("ADJUSTED: ") &&
                    !dtPayCode.StartsWith("REMOVED: ")) continue;

                /// get pay date; check if this row has already been analyzed and continue on if so
                /// record it if not
                DateTime dtPayDate = Convert.ToDateTime(pDT.Rows[i]["Pay Date"].ToString());
                if (processed.Contains(dtPayCode + dtPayDate.ToString())) continue;
                else processed.Add(dtPayCode + dtPayDate.ToString());

                /// for adjustments, compare only the pay code being modified (and not the prefix)
                string dtPayCode_toCompare = dtPayCode.Substring(dtPayCode.IndexOf(": ") + ": ".Length);
                /// look below this row only
                int insertAt = i + 1;
                /// used to record if found
                bool found = false;
                /// used to record if a row was passed which is not an informational row
                bool isValid = false;
                /// loop only remaining rows
                for (int k = insertAt; k < pDT.Rows.Count; k++)
                {
                    /// grab row's pay code and date
                    string rowPayCode = pDT.Rows[k]["Pay Code"].ToString();
                    DateTime rowPayDate = Convert.ToDateTime(pDT.Rows[k]["Pay Date"].ToString());

                    /// skip if the date isn't right
                    if (rowPayDate.Date < dtPayDate.Date)
                    {
                        insertAt++;
                        /// row is not just being moved between other informational rows
                        isValid = true;
                    }
                    /// skip passed any informational rows
                    else if (rowPayCode.StartsWith("ADDED: ") ||
                             rowPayCode.StartsWith("ADJUSTED: ") ||
                             rowPayCode.StartsWith("REMOVED: ") ||
                             rowPayCode.StartsWith("BONUS") ||
                             rowPayCode.StartsWith("PAD") ||
                             rowPayCode.StartsWith("PENALTY") ||
                             rowPayCode.StartsWith("FORGIVENESS") ||
                             rowPayCode == "POINT LEVEL REACHED (MISSED LETTER)")
                    {
                        insertAt++;
                    }
                    /// stop if date and pay codes match
                    /// this will make it appear before the paycode which this entry is adjusting (if applicable)
                    else if (rowPayDate.Date == dtPayDate.Date &&
                             rowPayCode == dtPayCode_toCompare)
                    {
                        found = true;
                        break;
                    }
                    /// stop if date has been exceeded (test last)
                    /// this will make it appear as the last entry on a given date
                    else if (rowPayDate.Date > dtPayDate.Date)
                    {
                        found = true;
                        break;
                    }
                }

                if (isValid)
                {
                    /// copy row contents, insert at desired position, remove original
                    DataRow newdr = pDT.NewRow();
                    newdr.ItemArray = pDT.Rows[i].ItemArray;
                    if (found) pDT.Rows.InsertAt(newdr, insertAt);
                    else pDT.Rows.Add(newdr);
                    pDT.Rows.RemoveAt(i);
                    /// decrement i to reprocess this index and reloop
                    i--;
                }
            }
        }
        private void RepositionAllOtherInformationRows(ref DataTable pDT)
        {
            /// sort all information rows to appear in their proper order

            /// for bonuses:
            /// the top row when bonuses are granted is the bonus signifier
            /// it has a pay code of BONUS (REMOVE OLDEST POINTS)
            /// ignore that here, so only compare == "BONUS"

            /// all entries start at the top of the screen, properly sorted chronologically

            /// track checked rows
            List<string> processed = new List<string>();
            /// loop all rows
            for (int i = 0; i < pDT.Rows.Count; i++)
            {
                /// get pay code and test it; continue if not ideal
                string dtPayCode = pDT.Rows[i]["Pay Code"].ToString();
                if (!dtPayCode.StartsWith("BONUS") &&
                    !dtPayCode.StartsWith("PAD") &&
                    !dtPayCode.StartsWith("PENALTY") &&
                    !dtPayCode.StartsWith("FORGIVENESS") &&
                    dtPayCode != "POINT LEVEL REACHED (MISSED LETTER)") continue;

                /// get pay date; check if this row has already been analyzed and continue on if so
                /// record it if not
                DateTime dtPayDate = Convert.ToDateTime(pDT.Rows[i]["Pay Date"].ToString());
                if (processed.Contains(dtPayCode + dtPayDate.ToString())) continue;
                else processed.Add(dtPayCode + dtPayDate.ToString());

                /// look below this row only
                int insertAt = i + 1;
                /// used to record if found
                bool found = false;
                /// used to record if a row was passed which is not an informational row
                bool isValid = false;
                /// loop only remaining rows
                for (int k = insertAt; k < pDT.Rows.Count; k++)
                {
                    /// grab row's pay code and date
                    string rowPayCode = pDT.Rows[k]["Pay Code"].ToString();
                    DateTime rowPayDate = Convert.ToDateTime(pDT.Rows[k]["Pay Date"].ToString());

                    /// skip if the date isn't right
                    if (((dtPayCode.StartsWith("BONUS") ||
                          dtPayCode.StartsWith("PAD")) && rowPayDate.Date < dtPayDate.Date) ||
                        (dtPayCode.StartsWith("FORGIVENESS") ||
                         dtPayCode.StartsWith("POINT LEVEL REACHED (MISSED LETTER)") ||
                         dtPayCode == "PENALTY") && rowPayDate.Date <= dtPayDate.Date)
                    {
                        insertAt++;
                        /// row is not just being moved between other informational rows
                        isValid = true;
                    }
                    /// skip passed any informational rows
                    else if (rowPayCode.StartsWith("ADDED: ") ||
                             rowPayCode.StartsWith("ADJUSTED: ") ||
                             rowPayCode.StartsWith("REMOVED: ") ||
                             rowPayCode.StartsWith("BONUS") ||
                             rowPayCode.StartsWith("PAD") ||
                             rowPayCode.StartsWith("PENALTY") ||
                             rowPayCode.StartsWith("FORGIVENESS") ||
                             rowPayCode == "POINT LEVEL REACHED (MISSED LETTER)")
                    {
                        insertAt++;
                    }
                    /// stop if date has been met or exceeded (test last)
                    /// this will make it appear as the first or last entry on a given date
                    else if (((dtPayCode.StartsWith("BONUS") ||
                               dtPayCode.StartsWith("PAD")) && rowPayDate.Date >= dtPayDate.Date) ||
                             (dtPayCode.StartsWith("FORGIVENESS") ||
                              dtPayCode.StartsWith("POINT LEVEL REACHED (MISSED LETTER)") ||
                              dtPayCode == "PENALTY") && rowPayDate.Date > dtPayDate.Date)
                    {
                        found = true;
                        break;
                    }
                }

                if (isValid)
                {
                    /// copy row contents, insert at desired position, remove original
                    DataRow newdr = pDT.NewRow();
                    newdr.ItemArray = pDT.Rows[i].ItemArray;
                    if (found) pDT.Rows.InsertAt(newdr, insertAt);
                    else pDT.Rows.Add(newdr);
                    pDT.Rows.RemoveAt(i);
                    /// decrement i to reprocess this index and reloop
                    i--;
                }
            }
        }

        /// rando events

        private void ButtonEdit1_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            cbEmployeeList.Text = string.Empty;
            if (thisPageName.Text != "AdminTasks") thisPageName.Text = TITLE_ViewEmployeeData = TITLE_ViewEmployeeData_Default;
            BindEmployeeEntries();
        }
        private void ButtonEdit2_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            deListByPayDate_From.Text = string.Empty;
            if (thisPageName.Text != "AdminTasks") thisPageName.Text = TITLE_ViewEmployeeData = TITLE_ViewEmployeeData_Default;
            BindEmployeeEntries();
        }
        private void buttonEdit3_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            deListByPayDate_To.Text = string.Empty;
            if (thisPageName.Text != "AdminTasks") thisPageName.Text = TITLE_ViewEmployeeData = TITLE_ViewEmployeeData_Default;
            BindEmployeeEntries();
        }

        private void HlinkEmployeePoints_Click(object sender, EventArgs e) { atrack.ShowReprimands(atrack.tabFormControl1.Items["btnShowReprimands"]); }
        private void hlinkEmployeeBonuses_Click(object sender, EventArgs e) { atrack.ShowPointSystem(atrack.tabFormControl1.Items["btnShowPointSystem"]); }

        private void TabPane1_SelectedPageChanging(object sender, DevExpress.XtraBars.Navigation.SelectedPageChangingEventArgs e)
        {
            try
            {
                if (thisPageName.Text == "AdminTasks") return;
                else if (e.Page == tabViewEmployeeData) thisPageName.Text = TITLE_ViewEmployeeData;
                else if (e.Page == tabViewAllData) thisPageName.Text = TITLE_ViewAllEmployees;
                else thisPageName.Text = string.Empty;
            }
            catch { thisPageName.Text = string.Empty; }
        }

        private void gridView1_CustomColumnSort(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnSortEventArgs e)
        {
            try
            {
                /// these columns will result in the same custom sort order
                if (e.Column.FieldName == "RoundedInOutPunches" || e.Column.FieldName == "PayDate")
                {
                    /// these are in the format of "12:34 AM/PM - 6:54 AM/PM"
                    /// get it
                    string val1 = gridView1.GetRowCellValue(e.ListSourceRowIndex1, "RoundedInOutPunches").ToString();
                    /// get first time portion
                    val1 = val1.Substring(0, val1.IndexOf(" - "));
                    /// determine if AM or PM
                    bool val1AM = val1.EndsWith(" AM") ? true : false;
                    /// get hour part
                    int val1hour = Convert.ToInt32(val1.Substring(0, val1.IndexOf(":")));
                    /// add 12 hours if its in the PM
                    if (!val1AM) val1hour += 12;
                    /// turn 12 AM to 0 AM; midnight
                    if (val1hour == 12) val1hour = 0;
                    /// turn 24 PM to 12 PM; noon
                    else if (val1hour == 24) val1hour = 12;
                    /// get minute part
                    int val1minute = Convert.ToInt32(val1.Substring(val1.IndexOf(":") + 1, val1.IndexOf(" ") - val1.IndexOf(":")));
                    /// get this entry's pay date
                    DateTime paydate1 = Convert.ToDateTime(gridView1.GetRowCellValue(e.ListSourceRowIndex1, "PayDate"));
                    /// build a new date to compare with the paydate's date but the derived time
                    DateTime time1 = new DateTime(paydate1.Year, paydate1.Month, paydate1.Day, val1hour, val1minute, 0);

                    /// the same as the above but for a subsequent record for comparison purposes
                    string val2 = gridView1.GetRowCellValue(e.ListSourceRowIndex2, "RoundedInOutPunches").ToString();
                    val2 = val2.Substring(0, val2.IndexOf(" - "));
                    bool val2AM = val2.EndsWith(" AM") ? true : false;
                    int val2hour = Convert.ToInt32(val2.Substring(0, val2.IndexOf(":")));
                    if (!val2AM) val2hour += 12;
                    if (val2hour == 12) val2hour = 0;
                    else if (val2hour == 24) val2hour = 12;
                    int val2minute = Convert.ToInt32(val2.Substring(val2.IndexOf(":") + 1, val2.IndexOf(" ") - val2.IndexOf(":")));
                    DateTime paydate2 = Convert.ToDateTime(gridView1.GetRowCellValue(e.ListSourceRowIndex2, "PayDate"));
                    DateTime time2 = new DateTime(paydate2.Year, paydate2.Month, paydate2.Day, val2hour, val2minute, 0);

                    /// calc result and mark as handled
                    e.Result = Convert.ToInt32((time1 - time2).TotalMinutes);
                    e.Handled = true;
                }
                else return;
            }
            catch { }
        }

        private void gridView1_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
        {
            GridGroupRowInfo info = e.Info as GridGroupRowInfo;
            if (info.Column.FieldName == "IsCurrentRecord") { info.GroupText = (info.GroupValueText == "Checked" ? "YES" : "NO"); }
        }

        private void hlinkLegend_Click(object sender, EventArgs e)
        {
            try { SHOWLEGEND(!LEGEND_SHOWN, true); }
            catch { }
        }
        private void SHOWLEGEND(bool pState, bool pAffectOtherPanels)
        {
            if (pState)
            {
                if (pAffectOtherPanels) SHOWEMPLOYEEINFO(false, false);

                LEGEND_SHOWN = true;
                hlinkLegend.Text = "Close Color Legend [—] ";
                flyoutLegend.BringToFront();
                flyoutLegend.ShowBeakForm();
            }
            else
            {
                LEGEND_SHOWN = false;
                hlinkLegend.Text = "Show Color Legend [+] ";
                flyoutLegend.HideBeakForm();
            }
        }
        private void hlinkEmployeeInfo_Click(object sender, EventArgs e)
        {
            try { SHOWEMPLOYEEINFO(!EMPLOYEEINFO_SHOWN, true); }
            catch { }
        }

        private void SHOWEMPLOYEEINFO(bool pState, bool pAffectOtherPanels)
        {
            if (pState)
            {
                if (pAffectOtherPanels) SHOWLEGEND(false, false);

                EMPLOYEEINFO_SHOWN = true;
                hlinkEmployeeInfo.Text = "Close Employee Info [—] ";
                flyoutEmployeeInfo.BringToFront();
                flyoutEmployeeInfo.ShowBeakForm();
            }
            else
            {
                EMPLOYEEINFO_SHOWN = false;
                hlinkEmployeeInfo.Text = "Show Employee Info [+] ";
                flyoutEmployeeInfo.HideBeakForm();
            }
        }

        public List<HighPoint> GetPointEntriesForEmployeeFromAdminTasksPage(string pEmployeeListing)
        {
            try
            {
                int index = 0;
                for (; index < cbEmployeeList.Properties.Items.Count; index++)
                {
                    if (cbEmployeeList.Properties.Items[index].ToString() == pEmployeeListing) break;
                }
                cbEmployeeList.SelectedIndex = -1;
                cbEmployeeList.SelectedIndex = index;
                List<HighPoint> returnList = new List<HighPoint>();

                /// this is COPIED from the normal daily process which funnels data into the HighPoints table
                for (int k = 0; k < gridView2.RowCount; k++)
                {
                    string paycode = gridView2.GetRowCellValue(k, "Pay Code").ToString();

                    DateTime paydate = Convert.ToDateTime(gridView2.GetRowCellValue(k, "Pay Date").ToString());
                    double points = Convert.ToDouble(gridView2.GetRowCellValue(k, "Points").ToString().Replace(" ", ""));
                    double nethours = Convert.ToDouble((gridView2.GetRowCellValue(k, "Net Hours").ToString() == string.Empty ? "0" : gridView2.GetRowCellValue(k, "Net Hours").ToString()));
                    string description = gridView2.GetRowCellValue(k, "Description").ToString();

                    HighPoint entry = new HighPoint();
                    entry.ID = Guid.NewGuid();
                    entry.index = k;
                    entry.EmployeeID = SELECTEDEMPLOYEE.EmployeeID;
                    entry.Name = SELECTEDEMPLOYEE.EmployeeName;
                    entry.Department = SELECTEDEMPLOYEE.Department;
                    entry.Pay_Date = paydate;
                    entry.Pay_Code = paycode;
                    entry.Net_Hours = nethours;
                    entry.Description = description;
                    entry.Points = points;

                    /// this is where it's different
                    returnList.Add(entry);
                }

                return returnList;
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
                return null;
            }
        }

        private DateTime GetFirstEntryForEmployee(string pID)
        {
            return atrack.datasource_datelimited.Where(z => z.EmployeeEmploymentStatus_EmployeeNumber == pID).OrderBy(z => z.PayDate).First().PayDate;
        }

        private int CountM01Occurences(string pID)
        {
            return atrack.dataset.TrackedBonuses.Where(z => z.EmployeeID == pID && z.BonusPayCode == "PAD").Count();
        }

        /// creates a deep copy, rather than a shallow copy, of a row of data
        public static T DeepClone<T>(T obj)
        {
            T objResult;
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, obj);
                ms.Position = 0;
                objResult = (T)bf.Deserialize(ms);
            }
            return objResult;
        }

        private void HideCeridianData()
        {
            CERIDIANDATASHOWN = false;
            this.splitContainer1.Panel1Collapsed = true;
            btnShowCeridianData.Appearance.LinkColor = Color.IndianRed;
            btnShowCeridianData.Text = "Ceridian data is currently hidden; click here to display it";
        }
        private void btnShowCeridianData_Click(object sender, EventArgs e) { ToggleCeridianData(); }
        private void ToggleCeridianData()
        {
            if (CERIDIANDATASHOWN)
            {
                CERIDIANDATASHOWN = false;
                this.splitContainer1.Panel1Collapsed = true;
                btnShowCeridianData.Appearance.LinkColor = Color.IndianRed;
                btnShowCeridianData.Text = "Ceridian data is currently hidden; click here to display it";
            }
            else
            {
                CERIDIANDATASHOWN = true;
                this.splitContainer1.Panel1Collapsed = false;
                btnShowCeridianData.Appearance.LinkColor = Color.LimeGreen;
                btnShowCeridianData.Text = "Ceridian data is being shown; click here to hide it again";
            }
        }

        private void gcEmployeeEvents_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F5)
            {
                int index = cbEmployeeList.SelectedIndex;
                cbEmployeeList.SelectedIndex = -1;
                cbEmployeeList.SelectedIndex = index;
            }
        }
        private void btnRefresh_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            int index = cbEmployeeList.SelectedIndex;
            cbEmployeeList.SelectedIndex = -1;
            cbEmployeeList.SelectedIndex = index;
        }

        private void btnPrint_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (gcEmployeeEvents.DataSource == null)
            {
                MessageBox.Show("There is no data available for printing. Please select an employee and try again.",
                                "Nothing To Print!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return;
            }
            else
            {
                gridView2.PrintDialog();
                return;
            }
        }
        private void gridView2_PrintInitialize(object sender, DevExpress.XtraGrid.Views.Base.PrintInitializeEventArgs e)
        {
            /// force the default setting in the print dialog to have a LANDSCAPE ORIENTATION
            PrintingSystemBase pb = e.PrintingSystem as PrintingSystemBase;
            pb.PageSettings.Landscape = true;

            /// the only way to make this duplex is to override the print settings in the _StartPrint event
            /// there is no way (that I've found) to set it in the print dialog
        }
    }
}