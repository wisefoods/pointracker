﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using DevExpress.XtraGrid.Views.Grid;
using System.IO;
using DevExpress.Pdf;

namespace AttendanceTracker
{
    public partial class AgreementTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;

        public AgreementTemplate(Form1 pForm)
        {
            try
            {
                InitializeComponent();
                atrack = pForm;
                BindPageData();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }
        public void BindPageData()
        {
            try
            {
                /// merge pdfs into one view
                PdfDocumentProcessor pdf = new PdfDocumentProcessor();
                pdf.AppendDocument(new MemoryStream(Properties.Resources.UFCW_CBA_09_2021));

                MemoryStream stream = new MemoryStream();
                pdf.SaveDocument(stream);
                stream.Seek(0, SeekOrigin.Begin);
                pdfViewer.LoadDocument(stream);
            }
            catch (Exception ex) { atrack.LogAndDisplayError(ex.ToString(), true); }
        }
    }
}
