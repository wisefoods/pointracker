﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using System.Net.Mail;
using System.IO;
using DevExpress.XtraBars;
using System.Net.Mime;

namespace AttendanceTracker
{
    public partial class RequestChangeTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;

        public RequestChangeTemplate(Form1 pForm)
        {
            try
            {
                InitializeComponent();

                atrack = pForm;
                BindEmployeeList();
                RefreshTabList();      
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }
        private void BindEmployeeList()
        {
            cbEmployeeList.SelectedIndex = -1;
            cbEmployeeList.Properties.Items.Clear();
            cbEmployeeList.Properties.Items.AddRange(atrack.MYEMPLOYEES_NICELIST);
        }
        private void cbTabNameForAttachment_MouseEnter(object sender, EventArgs e)
        {
            RefreshTabList();
        }
        private void RefreshTabList()
        {
            cbTabNameForAttachment.Properties.Items.Clear();
            cbTabNameForAttachment.Properties.Items.AddRange(atrack.PopulateTabNameList());
        }

        private void btnSubmitChangeRequest_Click(object sender, EventArgs e)
        {
            try
            {
                string selectedEmployee = string.Empty;
                string empID = string.Empty;
                CurrentEmployee thisEmployee;
                List<EmailRecipient> lEmailRecipients;
                string approvers = string.Empty;
                List<string> emailaddresses = new List<string>();
                string emailaddresses_string = string.Empty;

                if (rtbChangeRequest.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("Type in a change to request before clicking send.", "No Change Request", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else if (cbEmployeeList.Text == null || cbEmployeeList.Text == string.Empty)
                {
                    MessageBox.Show("Select an employee to request a change for.", "No Employee Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {
                    selectedEmployee = cbEmployeeList.Text;
                    empID = atrack.GetEmployeeIDFromSelection(selectedEmployee);
                    thisEmployee = atrack.dataset.CurrentEmployees.Where(z => z.EmployeeID == empID).First();
                    lEmailRecipients = atrack.dataset.EmailRecipients.Where(z => z.Type == "DepartmentalChangeRequests" &&
                                                                                 z.Department == thisEmployee.Department).ToList();
                    foreach (EmailRecipient er in lEmailRecipients) 
                    {
                        if (approvers == string.Empty)
                        {
                            approvers += er.EmailRecipient_Name;
                            emailaddresses_string += er.EmailRecipient_Email;
                        }
                        else
                        {
                            approvers += ", " + er.EmailRecipient_Name;
                            emailaddresses_string += ", " + er.EmailRecipient_Email;
                        }

                        emailaddresses.Add(er.EmailRecipient_Email);
                    }

                    string msg = "Name: " + selectedEmployee +
                                 "\n\nDepartment: " + thisEmployee.Department +
                                 "\n\nShift: " + thisEmployee.Shift +
                                 "\n\nApprover: " + approvers +
                                 "\n\nContinue?";

                    if (MessageBox.Show("Submitting change request:\n\n" + msg, "Submit Change Request", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
                }

                atrack.ShowSplash("Sending change request");

                ChangeRequest changereq = new ChangeRequest();
                changereq.ID = Guid.NewGuid();
                changereq.ChangeIsForEmployee = selectedEmployee;
                changereq.ChangeIsForDepartment = thisEmployee.Department;
                changereq.ChangeIsForShift = thisEmployee.Shift;
                changereq.ChangeRequest1 = rtbChangeRequest.Text;
                changereq.EmailRecipientName = approvers;
                changereq.EmailRecipientAddresses = emailaddresses_string;
                changereq.SubmittedBy = atrack.USERNAME;
                changereq.SubmittedOn = DateTime.Now;
                atrack.dataset.ChangeRequests.Add(changereq);
                atrack.dataset.SaveChanges();

                using (MemoryStream stream = new MemoryStream())
                {
                    /// write multiline text field as lines to preserve linebreaks
                    string lineChangeRequest = string.Empty;
                    foreach (string line in rtbChangeRequest.Lines) { lineChangeRequest += line + "<br>"; }

                    /// acquire attachment (if user opted to do so)
                    Attachment attachment = atrack.GetContentsOfTabAndAttach(cbTabNameForAttachment.Text, stream, false);
                    
                    /// attach and send email
                    atrack.SendEmail(emailaddresses.ToArray(),
                                     new string[] { atrack.EMAILADDRESS },
                                     new string[] { Properties.Settings.Default.AdminEmail },
                                     "New Change Request Submitted by " + atrack.FULLNAME,
                                     String.Format("<b>Submitted by</b>: {0}<br><b>Change is for employee</b>: {1}<br><b>Department</b>: {2}<br><b>Change Request</b>: {3}<br><br>*** If this request is <b>APPROVED</b> please forward to {4} to modify attendance data.<br>*** For reference this is Change Request ID: {5}",
                                                   atrack.FULLNAME, changereq.ChangeIsForEmployee, changereq.ChangeIsForDepartment + " (shift #" + changereq.ChangeIsForShift + ")", lineChangeRequest, Properties.Settings.Default.AdminEmail, changereq.ID),
                                     attachment != null ? new List<System.Net.Mail.Attachment> { attachment } : null,
                                     true);
                }
                atrack.tabFormControl1.Pages.Remove(atrack.tabFormControl1.SelectedPage);
                atrack.ReclaimButton("Request a Data Change");
                atrack.CloseSplash();
                MessageBox.Show("Change request has been sent!", "Thanks!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
            finally
            {
                atrack.CloseSplash();
            }
        }
    }
}