﻿namespace AttendanceTracker
{
    partial class PageTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageTemplate));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions buttonImageOptions1 = new DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions();
            DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions buttonImageOptions2 = new DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabViewEmployeeData = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel6 = new System.Windows.Forms.Panel();
            this.gcEmployeeEntries = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcEmployeeEvents = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tabNavigationPage3 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.tabNavigationPage4 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.comboBoxEdit2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panelBonuses = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.hlinkEmployeeBonuses = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.panel30 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.btnRefresh = new DevExpress.XtraEditors.ButtonEdit();
            this.labelCeridianUpdateDate = new System.Windows.Forms.Label();
            this.btnPrint = new DevExpress.XtraEditors.ButtonEdit();
            this.panelFMLA = new System.Windows.Forms.Panel();
            this.thewhiteline = new System.Windows.Forms.Panel();
            this.theactualwhiteline = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.lblFMLAVacationHours = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lblFMLANonVacationHours = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.lblFMLATotalHours = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.labelFMLAHRNotified = new System.Windows.Forms.Label();
            this.panelAnalysis = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.hlinkEmployeePoints = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panelTopSelectors = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.hlinkLegend = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.panel17 = new System.Windows.Forms.Panel();
            this.hlinkEmployeeInfo = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.listViewType = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.label4 = new System.Windows.Forms.Label();
            this.deListByPayDate_To = new DevExpress.XtraEditors.DateEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.deListByPayDate_From = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.cbEmployeeList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.panelLeftRunner = new System.Windows.Forms.Panel();
            this.buttonEdit3 = new DevExpress.XtraEditors.ButtonEdit();
            this.buttonEdit2 = new DevExpress.XtraEditors.ButtonEdit();
            this.buttonEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnShowCeridianData = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.tabViewAllData = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gcHighPoints = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel12 = new System.Windows.Forms.Panel();
            this.labelLastUpdate = new System.Windows.Forms.Label();
            this.flyoutEmployeeInfo = new DevExpress.Utils.FlyoutPanel();
            this.panelEmployeeInfo = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.lblRehireDate = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.lblSeniorityDate = new System.Windows.Forms.Label();
            this.lblInfo2 = new System.Windows.Forms.Label();
            this.panel29 = new System.Windows.Forms.Panel();
            this.lblSupervisor = new System.Windows.Forms.Label();
            this.lblinfo1 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.lblShift = new System.Windows.Forms.Label();
            this.lblInfo3 = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.lblDepartment = new System.Windows.Forms.Label();
            this.lblInfo4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblEmployeeType = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.flyoutLegend = new DevExpress.Utils.FlyoutPanel();
            this.flyoutPanelControl1 = new DevExpress.Utils.FlyoutPanelControl();
            this.panelLegendTop = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.panelLegendBottom = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblActionsInRed = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabViewEmployeeData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcEmployeeEntries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcEmployeeEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.tabNavigationPage3.SuspendLayout();
            this.tabNavigationPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panelBonuses.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrint.Properties)).BeginInit();
            this.panelFMLA.SuspendLayout();
            this.thewhiteline.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panelAnalysis.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panelTopSelectors.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listViewType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deListByPayDate_To.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deListByPayDate_To.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deListByPayDate_From.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deListByPayDate_From.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEmployeeList.Properties)).BeginInit();
            this.panelLeftRunner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).BeginInit();
            this.panel15.SuspendLayout();
            this.tabViewAllData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcHighPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flyoutEmployeeInfo)).BeginInit();
            this.flyoutEmployeeInfo.SuspendLayout();
            this.panelEmployeeInfo.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flyoutLegend)).BeginInit();
            this.flyoutLegend.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flyoutPanelControl1)).BeginInit();
            this.flyoutPanelControl1.SuspendLayout();
            this.panelLegendTop.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panelLegendBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1.Panel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1.Panel2)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPane1
            // 
            this.tabPane1.AllowHtmlDraw = true;
            this.tabPane1.Controls.Add(this.tabViewEmployeeData);
            this.tabPane1.Controls.Add(this.tabViewAllData);
            this.tabPane1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.tabPane1.Location = new System.Drawing.Point(0, 0);
            this.tabPane1.LookAndFeel.SkinName = "Sharp Plus";
            this.tabPane1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabViewEmployeeData,
            this.tabViewAllData});
            this.tabPane1.RegularSize = new System.Drawing.Size(1220, 752);
            this.tabPane1.SelectedPage = this.tabViewEmployeeData;
            this.tabPane1.Size = new System.Drawing.Size(1220, 752);
            this.tabPane1.TabIndex = 1;
            this.tabPane1.Text = "tabPane1";
            this.tabPane1.SelectedPageChanging += new DevExpress.XtraBars.Navigation.SelectedPageChangingEventHandler(this.TabPane1_SelectedPageChanging);
            // 
            // tabViewEmployeeData
            // 
            this.tabViewEmployeeData.Caption = "  View Employee Points";
            this.tabViewEmployeeData.Controls.Add(this.splitContainer1);
            this.tabViewEmployeeData.Controls.Add(this.tabNavigationPage3);
            this.tabViewEmployeeData.Controls.Add(this.panel4);
            this.tabViewEmployeeData.Controls.Add(this.panel15);
            this.tabViewEmployeeData.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("tabViewEmployeeData.ImageOptions.SvgImage")));
            this.tabViewEmployeeData.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabViewEmployeeData.LookAndFeel.SkinName = "Sharp";
            this.tabViewEmployeeData.LookAndFeel.UseDefaultLookAndFeel = false;
            this.tabViewEmployeeData.Name = "tabViewEmployeeData";
            this.tabViewEmployeeData.PageText = "  View Employee Points";
            this.tabViewEmployeeData.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabViewEmployeeData.Size = new System.Drawing.Size(1220, 712);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(260, 20);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel6);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gcEmployeeEvents);
            this.splitContainer1.Size = new System.Drawing.Size(960, 692);
            this.splitContainer1.SplitterDistance = 207;
            this.splitContainer1.SplitterWidth = 10;
            this.splitContainer1.TabIndex = 11;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.gcEmployeeEntries);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(956, 203);
            this.panel6.TabIndex = 0;
            // 
            // gcEmployeeEntries
            // 
            this.gcEmployeeEntries.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcEmployeeEntries.Location = new System.Drawing.Point(0, 0);
            this.gcEmployeeEntries.LookAndFeel.SkinName = "Sharp";
            this.gcEmployeeEntries.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gcEmployeeEntries.MainView = this.gridView1;
            this.gcEmployeeEntries.Name = "gcEmployeeEntries";
            this.gcEmployeeEntries.Size = new System.Drawing.Size(956, 203);
            this.gcEmployeeEntries.TabIndex = 5;
            this.gcEmployeeEntries.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(209)))), ((int)(((byte)(234)))));
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.GridControl = this.gcEmployeeEntries;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gridView1.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.gridView1_CustomDrawGroupRow);
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.GridView1_RowStyle);
            this.gridView1.CustomColumnSort += new DevExpress.XtraGrid.Views.Base.CustomColumnSortEventHandler(this.gridView1_CustomColumnSort);
            // 
            // gcEmployeeEvents
            // 
            this.gcEmployeeEvents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcEmployeeEvents.Location = new System.Drawing.Point(0, 0);
            this.gcEmployeeEvents.LookAndFeel.SkinName = "Sharp";
            this.gcEmployeeEvents.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gcEmployeeEvents.MainView = this.gridView2;
            this.gcEmployeeEvents.Name = "gcEmployeeEvents";
            this.gcEmployeeEvents.Size = new System.Drawing.Size(956, 471);
            this.gcEmployeeEvents.TabIndex = 4;
            this.gcEmployeeEvents.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            this.gcEmployeeEvents.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gcEmployeeEvents_KeyDown);
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(209)))), ((int)(((byte)(234)))));
            this.gridView2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView2.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridView2.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView2.GridControl = this.gcEmployeeEvents;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gridView2.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView2_RowStyle);
            this.gridView2.PrintInitialize += new DevExpress.XtraGrid.Views.Base.PrintInitializeEventHandler(this.gridView2_PrintInitialize);
            // 
            // tabNavigationPage3
            // 
            this.tabNavigationPage3.Caption = "View Employee Points";
            this.tabNavigationPage3.Controls.Add(this.tabNavigationPage4);
            this.tabNavigationPage3.Controls.Add(this.comboBoxEdit1);
            this.tabNavigationPage3.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("tabNavigationPage3.ImageOptions.SvgImage")));
            this.tabNavigationPage3.Name = "tabNavigationPage3";
            this.tabNavigationPage3.Size = new System.Drawing.Size(1120, 549);
            // 
            // tabNavigationPage4
            // 
            this.tabNavigationPage4.Caption = "View Employee Points";
            this.tabNavigationPage4.Controls.Add(this.comboBoxEdit2);
            this.tabNavigationPage4.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("tabNavigationPage4.ImageOptions.SvgImage")));
            this.tabNavigationPage4.Name = "tabNavigationPage4";
            this.tabNavigationPage4.Size = new System.Drawing.Size(1120, 549);
            // 
            // comboBoxEdit2
            // 
            this.comboBoxEdit2.EditValue = "Employee List";
            this.comboBoxEdit2.Location = new System.Drawing.Point(3, 3);
            this.comboBoxEdit2.Name = "comboBoxEdit2";
            this.comboBoxEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit2.Properties.DropDownRows = 10;
            this.comboBoxEdit2.Properties.Sorted = true;
            this.comboBoxEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit2.Size = new System.Drawing.Size(178, 20);
            this.comboBoxEdit2.TabIndex = 0;
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.EditValue = "Employee List";
            this.comboBoxEdit1.Location = new System.Drawing.Point(3, 3);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Properties.DropDownRows = 10;
            this.comboBoxEdit1.Properties.Sorted = true;
            this.comboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit1.Size = new System.Drawing.Size(178, 20);
            this.comboBoxEdit1.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 20);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.panel4.Size = new System.Drawing.Size(260, 692);
            this.panel4.TabIndex = 13;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panelBonuses);
            this.panel5.Controls.Add(this.panelBottom);
            this.panel5.Controls.Add(this.panelFMLA);
            this.panel5.Controls.Add(this.panelAnalysis);
            this.panel5.Controls.Add(this.panelTopSelectors);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(240, 692);
            this.panel5.TabIndex = 14;
            // 
            // panelBonuses
            // 
            this.panelBonuses.Controls.Add(this.panel24);
            this.panelBonuses.Controls.Add(this.panel30);
            this.panelBonuses.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBonuses.Location = new System.Drawing.Point(0, 434);
            this.panelBonuses.Name = "panelBonuses";
            this.panelBonuses.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.panelBonuses.Size = new System.Drawing.Size(240, 71);
            this.panelBonuses.TabIndex = 17;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.hlinkEmployeeBonuses);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel24.Location = new System.Drawing.Point(127, 3);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(113, 68);
            this.panel24.TabIndex = 13;
            // 
            // hlinkEmployeeBonuses
            // 
            this.hlinkEmployeeBonuses.Appearance.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold);
            this.hlinkEmployeeBonuses.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeeBonuses.Appearance.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeeBonuses.Appearance.Options.UseFont = true;
            this.hlinkEmployeeBonuses.Appearance.Options.UseForeColor = true;
            this.hlinkEmployeeBonuses.Appearance.Options.UseLinkColor = true;
            this.hlinkEmployeeBonuses.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeeBonuses.AppearanceDisabled.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeeBonuses.AppearanceDisabled.Options.UseForeColor = true;
            this.hlinkEmployeeBonuses.AppearanceDisabled.Options.UseLinkColor = true;
            this.hlinkEmployeeBonuses.AppearancePressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeeBonuses.AppearancePressed.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeeBonuses.AppearancePressed.Options.UseForeColor = true;
            this.hlinkEmployeeBonuses.AppearancePressed.Options.UseLinkColor = true;
            this.hlinkEmployeeBonuses.Dock = System.Windows.Forms.DockStyle.Right;
            this.hlinkEmployeeBonuses.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.hlinkEmployeeBonuses.Location = new System.Drawing.Point(77, 0);
            this.hlinkEmployeeBonuses.LookAndFeel.SkinName = "Sharp";
            this.hlinkEmployeeBonuses.LookAndFeel.UseDefaultLookAndFeel = false;
            this.hlinkEmployeeBonuses.Margin = new System.Windows.Forms.Padding(0);
            this.hlinkEmployeeBonuses.Name = "hlinkEmployeeBonuses";
            this.hlinkEmployeeBonuses.Padding = new System.Windows.Forms.Padding(7, 12, 3, 12);
            this.hlinkEmployeeBonuses.Size = new System.Drawing.Size(36, 63);
            this.hlinkEmployeeBonuses.TabIndex = 11;
            this.hlinkEmployeeBonuses.Text = "#";
            this.hlinkEmployeeBonuses.Click += new System.EventHandler(this.hlinkEmployeeBonuses_Click);
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.label24);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel30.Location = new System.Drawing.Point(0, 3);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(127, 68);
            this.panel30.TabIndex = 12;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Left;
            this.label24.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label24.ForeColor = System.Drawing.Color.DarkBlue;
            this.label24.Location = new System.Drawing.Point(0, 0);
            this.label24.Name = "label24";
            this.label24.Padding = new System.Windows.Forms.Padding(3, 23, 0, 23);
            this.label24.Size = new System.Drawing.Size(117, 65);
            this.label24.TabIndex = 4;
            this.label24.Text = "Bonus Points";
            // 
            // panelBottom
            // 
            this.panelBottom.Controls.Add(this.btnRefresh);
            this.panelBottom.Controls.Add(this.labelCeridianUpdateDate);
            this.panelBottom.Controls.Add(this.btnPrint);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 605);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(240, 87);
            this.panelBottom.TabIndex = 7;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(208, 15);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Properties.Appearance.Options.UseImage = true;
            this.btnRefresh.Properties.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            this.btnRefresh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnRefresh.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnRefresh.Properties.LookAndFeel.SkinName = "Sharp";
            this.btnRefresh.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnRefresh.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnRefresh.Size = new System.Drawing.Size(32, 32);
            this.btnRefresh.TabIndex = 17;
            this.btnRefresh.ToolTip = "Refreshes the current employee\'s data (you can also press F5)";
            this.btnRefresh.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnRefresh_ButtonClick);
            // 
            // labelCeridianUpdateDate
            // 
            this.labelCeridianUpdateDate.AutoSize = true;
            this.labelCeridianUpdateDate.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelCeridianUpdateDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelCeridianUpdateDate.Location = new System.Drawing.Point(0, 11);
            this.labelCeridianUpdateDate.Name = "labelCeridianUpdateDate";
            this.labelCeridianUpdateDate.Padding = new System.Windows.Forms.Padding(3, 20, 0, 3);
            this.labelCeridianUpdateDate.Size = new System.Drawing.Size(128, 36);
            this.labelCeridianUpdateDate.TabIndex = 10;
            this.labelCeridianUpdateDate.Text = "Data from: UNKNOWN";
            // 
            // btnPrint
            // 
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnPrint.EditValue = "Print current points";
            this.btnPrint.Location = new System.Drawing.Point(0, 47);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Properties.Appearance.BackColor = System.Drawing.Color.Green;
            this.btnPrint.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnPrint.Properties.Appearance.Options.UseBackColor = true;
            this.btnPrint.Properties.Appearance.Options.UseFont = true;
            this.btnPrint.Properties.Appearance.Options.UseImage = true;
            this.btnPrint.Properties.AutoHeight = false;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            editorButtonImageOptions2.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            editorButtonImageOptions2.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            serializableAppearanceObject5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            serializableAppearanceObject5.Options.UseFont = true;
            serializableAppearanceObject5.Options.UseTextOptions = true;
            serializableAppearanceObject5.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnPrint.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "     Print current status", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnPrint.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPrint.Properties.LookAndFeel.SkinName = "Sharp Plus";
            this.btnPrint.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnPrint.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnPrint.Size = new System.Drawing.Size(240, 40);
            this.btnPrint.TabIndex = 18;
            this.btnPrint.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnPrint_ButtonClick);
            // 
            // panelFMLA
            // 
            this.panelFMLA.Controls.Add(this.thewhiteline);
            this.panelFMLA.Controls.Add(this.panel23);
            this.panelFMLA.Controls.Add(this.panel8);
            this.panelFMLA.Controls.Add(this.panel22);
            this.panelFMLA.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFMLA.Location = new System.Drawing.Point(0, 306);
            this.panelFMLA.Name = "panelFMLA";
            this.panelFMLA.Size = new System.Drawing.Size(240, 128);
            this.panelFMLA.TabIndex = 16;
            // 
            // thewhiteline
            // 
            this.thewhiteline.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(73)))), ((int)(((byte)(88)))));
            this.thewhiteline.Controls.Add(this.theactualwhiteline);
            this.thewhiteline.Dock = System.Windows.Forms.DockStyle.Top;
            this.thewhiteline.Location = new System.Drawing.Point(0, 64);
            this.thewhiteline.Margin = new System.Windows.Forms.Padding(18, 3, 3, 3);
            this.thewhiteline.Name = "thewhiteline";
            this.thewhiteline.Padding = new System.Windows.Forms.Padding(18, 0, 0, 0);
            this.thewhiteline.Size = new System.Drawing.Size(240, 1);
            this.thewhiteline.TabIndex = 5;
            // 
            // theactualwhiteline
            // 
            this.theactualwhiteline.BackColor = System.Drawing.Color.White;
            this.theactualwhiteline.Dock = System.Windows.Forms.DockStyle.Right;
            this.theactualwhiteline.Location = new System.Drawing.Point(150, 0);
            this.theactualwhiteline.Name = "theactualwhiteline";
            this.theactualwhiteline.Size = new System.Drawing.Size(90, 1);
            this.theactualwhiteline.TabIndex = 5;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.panel18);
            this.panel23.Controls.Add(this.panel7);
            this.panel23.Controls.Add(this.panel2);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(240, 64);
            this.panel23.TabIndex = 14;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.panel11);
            this.panel18.Controls.Add(this.panel9);
            this.panel18.Controls.Add(this.label12);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel18.Location = new System.Drawing.Point(150, 21);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(90, 43);
            this.panel18.TabIndex = 14;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.lblFMLAVacationHours);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(12, 23);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(78, 23);
            this.panel11.TabIndex = 18;
            // 
            // lblFMLAVacationHours
            // 
            this.lblFMLAVacationHours.AutoSize = true;
            this.lblFMLAVacationHours.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblFMLAVacationHours.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblFMLAVacationHours.Location = new System.Drawing.Point(59, 0);
            this.lblFMLAVacationHours.Name = "lblFMLAVacationHours";
            this.lblFMLAVacationHours.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.lblFMLAVacationHours.Size = new System.Drawing.Size(19, 20);
            this.lblFMLAVacationHours.TabIndex = 4;
            this.lblFMLAVacationHours.Text = "#";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.lblFMLANonVacationHours);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(12, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(78, 23);
            this.panel9.TabIndex = 17;
            // 
            // lblFMLANonVacationHours
            // 
            this.lblFMLANonVacationHours.AutoSize = true;
            this.lblFMLANonVacationHours.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblFMLANonVacationHours.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblFMLANonVacationHours.Location = new System.Drawing.Point(59, 0);
            this.lblFMLANonVacationHours.Name = "lblFMLANonVacationHours";
            this.lblFMLANonVacationHours.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.lblFMLANonVacationHours.Size = new System.Drawing.Size(19, 23);
            this.lblFMLANonVacationHours.TabIndex = 4;
            this.lblFMLANonVacationHours.Text = "#";
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Left;
            this.label12.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Padding = new System.Windows.Forms.Padding(0, 26, 0, 0);
            this.label12.Size = new System.Drawing.Size(12, 43);
            this.label12.TabIndex = 7;
            this.label12.Text = "+";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel3);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(0, 21);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(150, 43);
            this.panel7.TabIndex = 18;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(24, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(126, 43);
            this.panel3.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(0, 23);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(10, 3, 0, 0);
            this.label5.Size = new System.Drawing.Size(124, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "Vacation Hours";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(21, 6, 0, 0);
            this.label7.Size = new System.Drawing.Size(124, 23);
            this.label7.TabIndex = 17;
            this.label7.Text = "Absent Hours";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label9);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(240, 21);
            this.panel2.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.LightGoldenrodYellow;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.label9.Size = new System.Drawing.Size(104, 22);
            this.label9.TabIndex = 4;
            this.label9.Text = "FMLA Total";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label10);
            this.panel8.Controls.Add(this.lblFMLATotalHours);
            this.panel8.Controls.Add(this.panel13);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(0, 64);
            this.panel8.Name = "panel8";
            this.panel8.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.panel8.Size = new System.Drawing.Size(240, 24);
            this.panel8.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Left;
            this.label10.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(150, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(12, 20);
            this.label10.TabIndex = 15;
            this.label10.Text = "=";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblFMLATotalHours
            // 
            this.lblFMLATotalHours.AutoSize = true;
            this.lblFMLATotalHours.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblFMLATotalHours.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblFMLATotalHours.Location = new System.Drawing.Point(221, 4);
            this.lblFMLATotalHours.Name = "lblFMLATotalHours";
            this.lblFMLATotalHours.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.lblFMLATotalHours.Size = new System.Drawing.Size(19, 19);
            this.lblFMLATotalHours.TabIndex = 14;
            this.lblFMLATotalHours.Text = "#";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label8);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(0, 4);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(150, 20);
            this.panel13.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(59, 0, 0, 0);
            this.label8.Size = new System.Drawing.Size(148, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Total Hours";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.labelFMLAHRNotified);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel22.Location = new System.Drawing.Point(0, 88);
            this.panel22.Margin = new System.Windows.Forms.Padding(0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(240, 40);
            this.panel22.TabIndex = 5;
            // 
            // labelFMLAHRNotified
            // 
            this.labelFMLAHRNotified.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFMLAHRNotified.Font = new System.Drawing.Font("Tahoma", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.labelFMLAHRNotified.ForeColor = System.Drawing.Color.LightGoldenrodYellow;
            this.labelFMLAHRNotified.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelFMLAHRNotified.Location = new System.Drawing.Point(0, 0);
            this.labelFMLAHRNotified.Name = "labelFMLAHRNotified";
            this.labelFMLAHRNotified.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.labelFMLAHRNotified.Size = new System.Drawing.Size(240, 40);
            this.labelFMLAHRNotified.TabIndex = 6;
            this.labelFMLAHRNotified.Text = "* HR notified for 32+ FMLA Absent hours";
            this.labelFMLAHRNotified.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelFMLAHRNotified.Visible = false;
            // 
            // panelAnalysis
            // 
            this.panelAnalysis.Controls.Add(this.panel14);
            this.panelAnalysis.Controls.Add(this.panel10);
            this.panelAnalysis.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAnalysis.Location = new System.Drawing.Point(0, 235);
            this.panelAnalysis.Name = "panelAnalysis";
            this.panelAnalysis.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.panelAnalysis.Size = new System.Drawing.Size(240, 71);
            this.panelAnalysis.TabIndex = 13;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.hlinkEmployeePoints);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(104, 3);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(136, 68);
            this.panel14.TabIndex = 13;
            // 
            // hlinkEmployeePoints
            // 
            this.hlinkEmployeePoints.Appearance.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold);
            this.hlinkEmployeePoints.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeePoints.Appearance.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeePoints.Appearance.Options.UseFont = true;
            this.hlinkEmployeePoints.Appearance.Options.UseForeColor = true;
            this.hlinkEmployeePoints.Appearance.Options.UseLinkColor = true;
            this.hlinkEmployeePoints.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeePoints.AppearanceDisabled.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeePoints.AppearanceDisabled.Options.UseForeColor = true;
            this.hlinkEmployeePoints.AppearanceDisabled.Options.UseLinkColor = true;
            this.hlinkEmployeePoints.AppearancePressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeePoints.AppearancePressed.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeePoints.AppearancePressed.Options.UseForeColor = true;
            this.hlinkEmployeePoints.AppearancePressed.Options.UseLinkColor = true;
            this.hlinkEmployeePoints.Dock = System.Windows.Forms.DockStyle.Right;
            this.hlinkEmployeePoints.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.hlinkEmployeePoints.Location = new System.Drawing.Point(100, 0);
            this.hlinkEmployeePoints.LookAndFeel.SkinName = "Sharp";
            this.hlinkEmployeePoints.LookAndFeel.UseDefaultLookAndFeel = false;
            this.hlinkEmployeePoints.Margin = new System.Windows.Forms.Padding(0);
            this.hlinkEmployeePoints.Name = "hlinkEmployeePoints";
            this.hlinkEmployeePoints.Padding = new System.Windows.Forms.Padding(7, 12, 3, 12);
            this.hlinkEmployeePoints.Size = new System.Drawing.Size(36, 63);
            this.hlinkEmployeePoints.TabIndex = 11;
            this.hlinkEmployeePoints.Text = "#";
            this.hlinkEmployeePoints.Click += new System.EventHandler(this.HlinkEmployeePoints_Click);
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label6);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(0, 3);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(104, 68);
            this.panel10.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.IndianRed;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(3, 23, 0, 23);
            this.label6.Size = new System.Drawing.Size(103, 65);
            this.label6.TabIndex = 4;
            this.label6.Text = "Point Total";
            // 
            // panelTopSelectors
            // 
            this.panelTopSelectors.Controls.Add(this.panel25);
            this.panelTopSelectors.Controls.Add(this.panel17);
            this.panelTopSelectors.Controls.Add(this.listViewType);
            this.panelTopSelectors.Controls.Add(this.label4);
            this.panelTopSelectors.Controls.Add(this.deListByPayDate_To);
            this.panelTopSelectors.Controls.Add(this.label1);
            this.panelTopSelectors.Controls.Add(this.deListByPayDate_From);
            this.panelTopSelectors.Controls.Add(this.label2);
            this.panelTopSelectors.Controls.Add(this.cbEmployeeList);
            this.panelTopSelectors.Controls.Add(this.label3);
            this.panelTopSelectors.Controls.Add(this.panelLeftRunner);
            this.panelTopSelectors.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTopSelectors.Location = new System.Drawing.Point(0, 0);
            this.panelTopSelectors.Name = "panelTopSelectors";
            this.panelTopSelectors.Size = new System.Drawing.Size(240, 235);
            this.panelTopSelectors.TabIndex = 5;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.hlinkLegend);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel25.Location = new System.Drawing.Point(26, 185);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(214, 27);
            this.panel25.TabIndex = 19;
            // 
            // hlinkLegend
            // 
            this.hlinkLegend.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.hlinkLegend.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkLegend.Appearance.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkLegend.Appearance.Options.UseFont = true;
            this.hlinkLegend.Appearance.Options.UseForeColor = true;
            this.hlinkLegend.Appearance.Options.UseLinkColor = true;
            this.hlinkLegend.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkLegend.AppearanceDisabled.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkLegend.AppearanceDisabled.Options.UseForeColor = true;
            this.hlinkLegend.AppearanceDisabled.Options.UseLinkColor = true;
            this.hlinkLegend.AppearancePressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkLegend.AppearancePressed.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkLegend.AppearancePressed.Options.UseForeColor = true;
            this.hlinkLegend.AppearancePressed.Options.UseLinkColor = true;
            this.hlinkLegend.Dock = System.Windows.Forms.DockStyle.Right;
            this.hlinkLegend.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.hlinkLegend.Location = new System.Drawing.Point(74, 0);
            this.hlinkLegend.LookAndFeel.SkinName = "Sharp";
            this.hlinkLegend.LookAndFeel.UseDefaultLookAndFeel = false;
            this.hlinkLegend.Margin = new System.Windows.Forms.Padding(0);
            this.hlinkLegend.Name = "hlinkLegend";
            this.hlinkLegend.Padding = new System.Windows.Forms.Padding(9, 9, 0, 0);
            this.hlinkLegend.Size = new System.Drawing.Size(140, 22);
            this.hlinkLegend.TabIndex = 21;
            this.hlinkLegend.Text = "Show Color Legend [+] ";
            this.hlinkLegend.Click += new System.EventHandler(this.hlinkLegend_Click);
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.hlinkEmployeeInfo);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(26, 158);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(214, 27);
            this.panel17.TabIndex = 6;
            // 
            // hlinkEmployeeInfo
            // 
            this.hlinkEmployeeInfo.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.hlinkEmployeeInfo.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeeInfo.Appearance.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeeInfo.Appearance.Options.UseFont = true;
            this.hlinkEmployeeInfo.Appearance.Options.UseForeColor = true;
            this.hlinkEmployeeInfo.Appearance.Options.UseLinkColor = true;
            this.hlinkEmployeeInfo.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeeInfo.AppearanceDisabled.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeeInfo.AppearanceDisabled.Options.UseForeColor = true;
            this.hlinkEmployeeInfo.AppearanceDisabled.Options.UseLinkColor = true;
            this.hlinkEmployeeInfo.AppearancePressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeeInfo.AppearancePressed.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.hlinkEmployeeInfo.AppearancePressed.Options.UseForeColor = true;
            this.hlinkEmployeeInfo.AppearancePressed.Options.UseLinkColor = true;
            this.hlinkEmployeeInfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.hlinkEmployeeInfo.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.hlinkEmployeeInfo.Location = new System.Drawing.Point(66, 0);
            this.hlinkEmployeeInfo.LookAndFeel.SkinName = "Sharp";
            this.hlinkEmployeeInfo.LookAndFeel.UseDefaultLookAndFeel = false;
            this.hlinkEmployeeInfo.Margin = new System.Windows.Forms.Padding(0);
            this.hlinkEmployeeInfo.Name = "hlinkEmployeeInfo";
            this.hlinkEmployeeInfo.Padding = new System.Windows.Forms.Padding(9, 9, 0, 0);
            this.hlinkEmployeeInfo.Size = new System.Drawing.Size(148, 22);
            this.hlinkEmployeeInfo.TabIndex = 21;
            this.hlinkEmployeeInfo.Text = "Show Employee Info [+] ";
            this.hlinkEmployeeInfo.Click += new System.EventHandler(this.hlinkEmployeeInfo_Click);
            // 
            // listViewType
            // 
            this.listViewType.CheckOnClick = true;
            this.listViewType.Dock = System.Windows.Forms.DockStyle.Top;
            this.listViewType.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "Active", System.Windows.Forms.CheckState.Checked),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "Removed")});
            this.listViewType.Location = new System.Drawing.Point(26, 112);
            this.listViewType.LookAndFeel.SkinName = "Sharp";
            this.listViewType.LookAndFeel.UseDefaultLookAndFeel = false;
            this.listViewType.Name = "listViewType";
            this.listViewType.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listViewType.Size = new System.Drawing.Size(214, 46);
            this.listViewType.SortOrder = System.Windows.Forms.SortOrder.Descending;
            this.listViewType.TabIndex = 7;
            this.listViewType.ItemChecking += new DevExpress.XtraEditors.Controls.ItemCheckingEventHandler(this.ListViewType_ItemChecking);
            this.listViewType.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.ListViewType_ItemCheck);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Location = new System.Drawing.Point(26, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Record Type";
            // 
            // deListByPayDate_To
            // 
            this.deListByPayDate_To.Dock = System.Windows.Forms.DockStyle.Top;
            this.deListByPayDate_To.EditValue = null;
            this.deListByPayDate_To.Location = new System.Drawing.Point(26, 79);
            this.deListByPayDate_To.Name = "deListByPayDate_To";
            this.deListByPayDate_To.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deListByPayDate_To.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deListByPayDate_To.Properties.LookAndFeel.SkinName = "Sharp";
            this.deListByPayDate_To.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.deListByPayDate_To.Size = new System.Drawing.Size(214, 20);
            this.deListByPayDate_To.TabIndex = 17;
            this.deListByPayDate_To.EditValueChanged += new System.EventHandler(this.deListByPayDate_EditValueChanged);
            this.deListByPayDate_To.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.deListByPayDate_EditValueChanging);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(26, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Optional: View By Pay Date (To)";
            // 
            // deListByPayDate_From
            // 
            this.deListByPayDate_From.Dock = System.Windows.Forms.DockStyle.Top;
            this.deListByPayDate_From.EditValue = null;
            this.deListByPayDate_From.Location = new System.Drawing.Point(26, 46);
            this.deListByPayDate_From.Name = "deListByPayDate_From";
            this.deListByPayDate_From.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deListByPayDate_From.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deListByPayDate_From.Properties.LookAndFeel.SkinName = "Sharp";
            this.deListByPayDate_From.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.deListByPayDate_From.Size = new System.Drawing.Size(214, 20);
            this.deListByPayDate_From.TabIndex = 2;
            this.deListByPayDate_From.EditValueChanged += new System.EventHandler(this.deListByPayDate_EditValueChanged);
            this.deListByPayDate_From.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.deListByPayDate_EditValueChanging);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Location = new System.Drawing.Point(26, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Optional: View By Pay Date (From)";
            // 
            // cbEmployeeList
            // 
            this.cbEmployeeList.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbEmployeeList.EditValue = "";
            this.cbEmployeeList.Location = new System.Drawing.Point(26, 13);
            this.cbEmployeeList.Name = "cbEmployeeList";
            this.cbEmployeeList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEmployeeList.Properties.DropDownRows = 15;
            this.cbEmployeeList.Properties.ImmediatePopup = true;
            this.cbEmployeeList.Properties.LookAndFeel.SkinName = "Sharp";
            this.cbEmployeeList.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbEmployeeList.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbEmployeeList.Size = new System.Drawing.Size(214, 20);
            this.cbEmployeeList.TabIndex = 0;
            this.cbEmployeeList.SelectedIndexChanged += new System.EventHandler(this.CbEmployeeList_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Location = new System.Drawing.Point(26, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Select An Employee";
            // 
            // panelLeftRunner
            // 
            this.panelLeftRunner.Controls.Add(this.buttonEdit3);
            this.panelLeftRunner.Controls.Add(this.buttonEdit2);
            this.panelLeftRunner.Controls.Add(this.buttonEdit1);
            this.panelLeftRunner.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeftRunner.Location = new System.Drawing.Point(0, 0);
            this.panelLeftRunner.Name = "panelLeftRunner";
            this.panelLeftRunner.Size = new System.Drawing.Size(26, 235);
            this.panelLeftRunner.TabIndex = 4;
            // 
            // buttonEdit3
            // 
            this.buttonEdit3.Location = new System.Drawing.Point(1, 79);
            this.buttonEdit3.Name = "buttonEdit3";
            this.buttonEdit3.Properties.Appearance.Options.UseImage = true;
            this.buttonEdit3.Properties.AutoHeight = false;
            editorButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions3.Image")));
            this.buttonEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEdit3.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.buttonEdit3.Properties.LookAndFeel.SkinName = "Sharp";
            this.buttonEdit3.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.buttonEdit3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.buttonEdit3.Size = new System.Drawing.Size(25, 20);
            this.buttonEdit3.TabIndex = 12;
            this.buttonEdit3.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEdit3_ButtonClick);
            // 
            // buttonEdit2
            // 
            this.buttonEdit2.Location = new System.Drawing.Point(1, 46);
            this.buttonEdit2.Name = "buttonEdit2";
            this.buttonEdit2.Properties.Appearance.Options.UseImage = true;
            this.buttonEdit2.Properties.AutoHeight = false;
            editorButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions4.Image")));
            this.buttonEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEdit2.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.buttonEdit2.Properties.LookAndFeel.SkinName = "Sharp";
            this.buttonEdit2.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.buttonEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.buttonEdit2.Size = new System.Drawing.Size(25, 20);
            this.buttonEdit2.TabIndex = 11;
            this.buttonEdit2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ButtonEdit2_ButtonClick);
            // 
            // buttonEdit1
            // 
            this.buttonEdit1.Location = new System.Drawing.Point(1, 13);
            this.buttonEdit1.Name = "buttonEdit1";
            this.buttonEdit1.Properties.Appearance.Options.UseImage = true;
            this.buttonEdit1.Properties.AutoHeight = false;
            editorButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions5.Image")));
            this.buttonEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEdit1.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.buttonEdit1.Properties.LookAndFeel.SkinName = "Sharp";
            this.buttonEdit1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.buttonEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.buttonEdit1.Size = new System.Drawing.Size(25, 20);
            this.buttonEdit1.TabIndex = 10;
            this.buttonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ButtonEdit1_ButtonClick);
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.btnShowCeridianData);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(1220, 20);
            this.panel15.TabIndex = 7;
            // 
            // btnShowCeridianData
            // 
            this.btnShowCeridianData.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnShowCeridianData.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.btnShowCeridianData.Appearance.LinkColor = System.Drawing.Color.IndianRed;
            this.btnShowCeridianData.Appearance.Options.UseFont = true;
            this.btnShowCeridianData.Appearance.Options.UseForeColor = true;
            this.btnShowCeridianData.Appearance.Options.UseLinkColor = true;
            this.btnShowCeridianData.Appearance.Options.UseTextOptions = true;
            this.btnShowCeridianData.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnShowCeridianData.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.btnShowCeridianData.AppearanceDisabled.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.btnShowCeridianData.AppearanceDisabled.Options.UseForeColor = true;
            this.btnShowCeridianData.AppearanceDisabled.Options.UseLinkColor = true;
            this.btnShowCeridianData.AppearancePressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.btnShowCeridianData.AppearancePressed.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(232)))), ((int)(((byte)(242)))));
            this.btnShowCeridianData.AppearancePressed.Options.UseForeColor = true;
            this.btnShowCeridianData.AppearancePressed.Options.UseLinkColor = true;
            this.btnShowCeridianData.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.btnShowCeridianData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnShowCeridianData.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.BottomCenter;
            this.btnShowCeridianData.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.btnShowCeridianData.Location = new System.Drawing.Point(0, 0);
            this.btnShowCeridianData.LookAndFeel.SkinName = "Sharp";
            this.btnShowCeridianData.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnShowCeridianData.Margin = new System.Windows.Forms.Padding(0);
            this.btnShowCeridianData.Name = "btnShowCeridianData";
            this.btnShowCeridianData.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.btnShowCeridianData.Size = new System.Drawing.Size(1220, 16);
            this.btnShowCeridianData.TabIndex = 12;
            this.btnShowCeridianData.Text = "MESSAGE TO SHOW OR HIDE CERIDIAN DATA";
            this.btnShowCeridianData.Click += new System.EventHandler(this.btnShowCeridianData_Click);
            // 
            // tabViewAllData
            // 
            this.tabViewAllData.Caption = " View All Data";
            this.tabViewAllData.Controls.Add(this.gcHighPoints);
            this.tabViewAllData.Controls.Add(this.panel12);
            this.tabViewAllData.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("tabViewAllData.ImageOptions.Image")));
            this.tabViewAllData.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabViewAllData.LookAndFeel.SkinName = "Sharp";
            this.tabViewAllData.LookAndFeel.UseDefaultLookAndFeel = false;
            this.tabViewAllData.Name = "tabViewAllData";
            this.tabViewAllData.PageText = " View All Employees";
            this.tabViewAllData.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabViewAllData.Size = new System.Drawing.Size(1220, 712);
            // 
            // gcHighPoints
            // 
            this.gcHighPoints.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcHighPoints.EmbeddedNavigator.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(44)))), ((int)(((byte)(54)))));
            this.gcHighPoints.EmbeddedNavigator.Appearance.Options.UseForeColor = true;
            this.gcHighPoints.Location = new System.Drawing.Point(0, 22);
            this.gcHighPoints.LookAndFeel.SkinName = "Sharp";
            this.gcHighPoints.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gcHighPoints.MainView = this.gridView3;
            this.gcHighPoints.Name = "gcHighPoints";
            this.gcHighPoints.Size = new System.Drawing.Size(1220, 690);
            this.gcHighPoints.TabIndex = 9;
            this.gcHighPoints.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.gridView3.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView3.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridView3.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView3.GridControl = this.gcHighPoints;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView3.OptionsBehavior.Editable = false;
            this.gridView3.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView3.OptionsCustomization.CustomizationFormSearchBoxVisible = true;
            this.gridView3.OptionsFind.AlwaysVisible = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gridView3.OptionsView.EnableAppearanceOddRow = true;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.labelLastUpdate);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1220, 22);
            this.panel12.TabIndex = 11;
            // 
            // labelLastUpdate
            // 
            this.labelLastUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelLastUpdate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelLastUpdate.Location = new System.Drawing.Point(0, 0);
            this.labelLastUpdate.Name = "labelLastUpdate";
            this.labelLastUpdate.Size = new System.Drawing.Size(1220, 22);
            this.labelLastUpdate.TabIndex = 3;
            this.labelLastUpdate.Text = "Data from: UNKNOWN";
            this.labelLastUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flyoutEmployeeInfo
            // 
            this.flyoutEmployeeInfo.Controls.Add(this.panelEmployeeInfo);
            this.flyoutEmployeeInfo.Location = new System.Drawing.Point(850, 80);
            this.flyoutEmployeeInfo.LookAndFeel.SkinName = "Sharp Plus";
            this.flyoutEmployeeInfo.LookAndFeel.UseDefaultLookAndFeel = false;
            this.flyoutEmployeeInfo.Name = "flyoutEmployeeInfo";
            this.flyoutEmployeeInfo.Options.AnimationType = DevExpress.Utils.Win.PopupToolWindowAnimation.Fade;
            this.flyoutEmployeeInfo.OptionsBeakPanel.BeakLocation = DevExpress.Utils.BeakPanelBeakLocation.Right;
            this.flyoutEmployeeInfo.OptionsBeakPanel.CloseOnOuterClick = false;
            this.flyoutEmployeeInfo.OptionsButtonPanel.ButtonPanelContentAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.flyoutEmployeeInfo.OptionsButtonPanel.ButtonPanelLocation = DevExpress.Utils.FlyoutPanelButtonPanelLocation.Top;
            this.flyoutEmployeeInfo.OptionsButtonPanel.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.Utils.PeekFormButton("[+] Open Legend", true, buttonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, false, false, true, null, -1, false)});
            this.flyoutEmployeeInfo.OptionsButtonPanel.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.flyoutEmployeeInfo.OwnerControl = this.hlinkEmployeeInfo;
            this.flyoutEmployeeInfo.Size = new System.Drawing.Size(340, 182);
            this.flyoutEmployeeInfo.TabIndex = 6;
            this.flyoutEmployeeInfo.Click += new System.EventHandler(this.hlinkEmployeeInfo_Click);
            // 
            // panelEmployeeInfo
            // 
            this.panelEmployeeInfo.Controls.Add(this.panel16);
            this.panelEmployeeInfo.Controls.Add(this.panel28);
            this.panelEmployeeInfo.Controls.Add(this.panel29);
            this.panelEmployeeInfo.Controls.Add(this.panel27);
            this.panelEmployeeInfo.Controls.Add(this.panel26);
            this.panelEmployeeInfo.Controls.Add(this.panel1);
            this.panelEmployeeInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEmployeeInfo.Location = new System.Drawing.Point(0, 0);
            this.panelEmployeeInfo.Name = "panelEmployeeInfo";
            this.panelEmployeeInfo.Size = new System.Drawing.Size(340, 182);
            this.panelEmployeeInfo.TabIndex = 22;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.lblRehireDate);
            this.panel16.Controls.Add(this.label21);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 150);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(340, 30);
            this.panel16.TabIndex = 10;
            // 
            // lblRehireDate
            // 
            this.lblRehireDate.AutoSize = true;
            this.lblRehireDate.Font = new System.Drawing.Font("Tahoma", 8F);
            this.lblRehireDate.Location = new System.Drawing.Point(114, 0);
            this.lblRehireDate.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.lblRehireDate.Name = "lblRehireDate";
            this.lblRehireDate.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblRehireDate.Size = new System.Drawing.Size(33, 33);
            this.lblRehireDate.TabIndex = 24;
            this.lblRehireDate.Text = "n/a";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Left;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(0, 0);
            this.label21.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label21.Name = "label21";
            this.label21.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.label21.Size = new System.Drawing.Size(105, 33);
            this.label21.TabIndex = 23;
            this.label21.Text = "• Re-hire Date: ";
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.lblSeniorityDate);
            this.panel28.Controls.Add(this.lblInfo2);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel28.Location = new System.Drawing.Point(0, 120);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(340, 30);
            this.panel28.TabIndex = 8;
            // 
            // lblSeniorityDate
            // 
            this.lblSeniorityDate.AutoSize = true;
            this.lblSeniorityDate.Font = new System.Drawing.Font("Tahoma", 8F);
            this.lblSeniorityDate.Location = new System.Drawing.Point(114, 0);
            this.lblSeniorityDate.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.lblSeniorityDate.Name = "lblSeniorityDate";
            this.lblSeniorityDate.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblSeniorityDate.Size = new System.Drawing.Size(33, 33);
            this.lblSeniorityDate.TabIndex = 24;
            this.lblSeniorityDate.Text = "n/a";
            // 
            // lblInfo2
            // 
            this.lblInfo2.AutoSize = true;
            this.lblInfo2.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblInfo2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblInfo2.Location = new System.Drawing.Point(0, 0);
            this.lblInfo2.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.lblInfo2.Name = "lblInfo2";
            this.lblInfo2.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblInfo2.Size = new System.Drawing.Size(114, 33);
            this.lblInfo2.TabIndex = 23;
            this.lblInfo2.Text = "• Seniority Date: ";
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.lblSupervisor);
            this.panel29.Controls.Add(this.lblinfo1);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel29.Location = new System.Drawing.Point(0, 90);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(340, 30);
            this.panel29.TabIndex = 8;
            // 
            // lblSupervisor
            // 
            this.lblSupervisor.AutoSize = true;
            this.lblSupervisor.Font = new System.Drawing.Font("Tahoma", 8F);
            this.lblSupervisor.Location = new System.Drawing.Point(114, 0);
            this.lblSupervisor.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.lblSupervisor.Name = "lblSupervisor";
            this.lblSupervisor.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblSupervisor.Size = new System.Drawing.Size(33, 33);
            this.lblSupervisor.TabIndex = 25;
            this.lblSupervisor.Text = "n/a";
            // 
            // lblinfo1
            // 
            this.lblinfo1.AutoSize = true;
            this.lblinfo1.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblinfo1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblinfo1.Location = new System.Drawing.Point(0, 0);
            this.lblinfo1.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.lblinfo1.Name = "lblinfo1";
            this.lblinfo1.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblinfo1.Size = new System.Drawing.Size(91, 33);
            this.lblinfo1.TabIndex = 24;
            this.lblinfo1.Text = "• Supervisor:";
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.lblShift);
            this.panel27.Controls.Add(this.lblInfo3);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel27.Location = new System.Drawing.Point(0, 60);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(340, 30);
            this.panel27.TabIndex = 8;
            // 
            // lblShift
            // 
            this.lblShift.AutoSize = true;
            this.lblShift.Font = new System.Drawing.Font("Tahoma", 8F);
            this.lblShift.Location = new System.Drawing.Point(114, 0);
            this.lblShift.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.lblShift.Name = "lblShift";
            this.lblShift.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblShift.Size = new System.Drawing.Size(33, 33);
            this.lblShift.TabIndex = 23;
            this.lblShift.Text = "n/a";
            // 
            // lblInfo3
            // 
            this.lblInfo3.AutoSize = true;
            this.lblInfo3.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblInfo3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblInfo3.Location = new System.Drawing.Point(0, 0);
            this.lblInfo3.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.lblInfo3.Name = "lblInfo3";
            this.lblInfo3.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblInfo3.Size = new System.Drawing.Size(59, 33);
            this.lblInfo3.TabIndex = 22;
            this.lblInfo3.Text = "• Shift: ";
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.lblDepartment);
            this.panel26.Controls.Add(this.lblInfo4);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel26.Location = new System.Drawing.Point(0, 30);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(340, 30);
            this.panel26.TabIndex = 7;
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            this.lblDepartment.Font = new System.Drawing.Font("Tahoma", 8F);
            this.lblDepartment.Location = new System.Drawing.Point(114, 0);
            this.lblDepartment.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDepartment.Size = new System.Drawing.Size(33, 33);
            this.lblDepartment.TabIndex = 22;
            this.lblDepartment.Text = "n/a";
            // 
            // lblInfo4
            // 
            this.lblInfo4.AutoSize = true;
            this.lblInfo4.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblInfo4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblInfo4.Location = new System.Drawing.Point(0, 0);
            this.lblInfo4.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.lblInfo4.Name = "lblInfo4";
            this.lblInfo4.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblInfo4.Size = new System.Drawing.Size(102, 33);
            this.lblInfo4.TabIndex = 21;
            this.lblInfo4.Text = "• Department: ";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblEmployeeType);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(340, 30);
            this.panel1.TabIndex = 9;
            // 
            // lblEmployeeType
            // 
            this.lblEmployeeType.AutoSize = true;
            this.lblEmployeeType.Font = new System.Drawing.Font("Tahoma", 8F);
            this.lblEmployeeType.Location = new System.Drawing.Point(114, 0);
            this.lblEmployeeType.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.lblEmployeeType.Name = "lblEmployeeType";
            this.lblEmployeeType.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblEmployeeType.Size = new System.Drawing.Size(33, 33);
            this.lblEmployeeType.TabIndex = 22;
            this.lblEmployeeType.Text = "n/a";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Left;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label14.Name = "label14";
            this.label14.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.label14.Size = new System.Drawing.Size(61, 33);
            this.label14.TabIndex = 21;
            this.label14.Text = "• Type: ";
            // 
            // flyoutLegend
            // 
            this.flyoutLegend.Controls.Add(this.flyoutPanelControl1);
            this.flyoutLegend.Location = new System.Drawing.Point(300, 80);
            this.flyoutLegend.LookAndFeel.SkinName = "Sharp Plus";
            this.flyoutLegend.LookAndFeel.UseDefaultLookAndFeel = false;
            this.flyoutLegend.Name = "flyoutLegend";
            this.flyoutLegend.Options.AnimationType = DevExpress.Utils.Win.PopupToolWindowAnimation.Fade;
            this.flyoutLegend.OptionsBeakPanel.BeakLocation = DevExpress.Utils.BeakPanelBeakLocation.Right;
            this.flyoutLegend.OptionsBeakPanel.CloseOnOuterClick = false;
            this.flyoutLegend.OptionsButtonPanel.ButtonPanelContentAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.flyoutLegend.OptionsButtonPanel.ButtonPanelLocation = DevExpress.Utils.FlyoutPanelButtonPanelLocation.Top;
            this.flyoutLegend.OptionsButtonPanel.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.Utils.PeekFormButton("[+] Open Legend", true, buttonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, false, false, true, null, -1, false)});
            this.flyoutLegend.OptionsButtonPanel.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.flyoutLegend.OwnerControl = this.hlinkLegend;
            this.flyoutLegend.Size = new System.Drawing.Size(512, 330);
            this.flyoutLegend.TabIndex = 5;
            this.flyoutLegend.Click += new System.EventHandler(this.hlinkLegend_Click);
            // 
            // flyoutPanelControl1
            // 
            this.flyoutPanelControl1.AutoSize = true;
            this.flyoutPanelControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flyoutPanelControl1.Controls.Add(this.panelLegendTop);
            this.flyoutPanelControl1.Controls.Add(this.panel21);
            this.flyoutPanelControl1.Controls.Add(this.panel20);
            this.flyoutPanelControl1.Controls.Add(this.panelLegendBottom);
            this.flyoutPanelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flyoutPanelControl1.FlyoutPanel = this.flyoutLegend;
            this.flyoutPanelControl1.Location = new System.Drawing.Point(0, 0);
            this.flyoutPanelControl1.Name = "flyoutPanelControl1";
            this.flyoutPanelControl1.Padding = new System.Windows.Forms.Padding(10);
            this.flyoutPanelControl1.Size = new System.Drawing.Size(512, 330);
            this.flyoutPanelControl1.TabIndex = 0;
            // 
            // panelLegendTop
            // 
            this.panelLegendTop.Controls.Add(this.label26);
            this.panelLegendTop.Controls.Add(this.label18);
            this.panelLegendTop.Controls.Add(this.label17);
            this.panelLegendTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLegendTop.Location = new System.Drawing.Point(12, 38);
            this.panelLegendTop.Name = "panelLegendTop";
            this.panelLegendTop.Size = new System.Drawing.Size(488, 70);
            this.panelLegendTop.TabIndex = 14;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.Black;
            this.label26.Dock = System.Windows.Forms.DockStyle.Top;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label26.Location = new System.Drawing.Point(0, 40);
            this.label26.Name = "label26";
            this.label26.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.label26.Size = new System.Drawing.Size(488, 20);
            this.label26.TabIndex = 11;
            this.label26.Text = "Entries which are removed/forgiven due to COVID-19";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.IndianRed;
            this.label18.Dock = System.Windows.Forms.DockStyle.Top;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label18.Location = new System.Drawing.Point(0, 20);
            this.label18.Name = "label18";
            this.label18.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.label18.Size = new System.Drawing.Size(488, 20);
            this.label18.TabIndex = 10;
            this.label18.Text = "Entries from Ceridian that led to a penalty";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.label17.Size = new System.Drawing.Size(488, 20);
            this.label17.TabIndex = 9;
            this.label17.Text = "FMLA entries from Ceridian";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.label20);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(12, 12);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(488, 26);
            this.panel21.TabIndex = 16;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Top;
            this.label20.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Name = "label20";
            this.label20.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.label20.Size = new System.Drawing.Size(239, 22);
            this.label20.TabIndex = 4;
            this.label20.Text = "Ceridian Data (Top Section)";
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.label19);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel20.Location = new System.Drawing.Point(12, 114);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(488, 27);
            this.panel20.TabIndex = 11;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Top;
            this.label19.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.label19.Location = new System.Drawing.Point(0, 0);
            this.label19.Name = "label19";
            this.label19.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.label19.Size = new System.Drawing.Size(271, 22);
            this.label19.TabIndex = 4;
            this.label19.Text = "Point Analysis (Bottom Section)";
            // 
            // panelLegendBottom
            // 
            this.panelLegendBottom.Controls.Add(this.label11);
            this.panelLegendBottom.Controls.Add(this.label22);
            this.panelLegendBottom.Controls.Add(this.label25);
            this.panelLegendBottom.Controls.Add(this.label13);
            this.panelLegendBottom.Controls.Add(this.label16);
            this.panelLegendBottom.Controls.Add(this.label15);
            this.panelLegendBottom.Controls.Add(this.lblActionsInRed);
            this.panelLegendBottom.Controls.Add(this.label23);
            this.panelLegendBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelLegendBottom.Location = new System.Drawing.Point(12, 141);
            this.panelLegendBottom.Name = "panelLegendBottom";
            this.panelLegendBottom.Size = new System.Drawing.Size(488, 177);
            this.panelLegendBottom.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Black;
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label11.Location = new System.Drawing.Point(0, 140);
            this.label11.Name = "label11";
            this.label11.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.label11.Size = new System.Drawing.Size(488, 33);
            this.label11.TabIndex = 8;
            this.label11.Text = "Entries that have been modified by an administrator\r\n(these changes DO NOT HAVE A" +
    "NY IMPACT on data in Ceridian or Payroll!)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.Orange;
            this.label22.Dock = System.Windows.Forms.DockStyle.Top;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label22.Location = new System.Drawing.Point(0, 120);
            this.label22.Name = "label22";
            this.label22.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.label22.Size = new System.Drawing.Size(488, 20);
            this.label22.TabIndex = 10;
            this.label22.Text = "Penalties which an employee has accrued";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.Purple;
            this.label25.Dock = System.Windows.Forms.DockStyle.Top;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label25.Location = new System.Drawing.Point(0, 100);
            this.label25.Name = "label25";
            this.label25.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.label25.Size = new System.Drawing.Size(488, 20);
            this.label25.TabIndex = 12;
            this.label25.Text = "Perfect attendance days which an employee has earned";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.DarkBlue;
            this.label13.Dock = System.Windows.Forms.DockStyle.Top;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label13.Location = new System.Drawing.Point(0, 80);
            this.label13.Name = "label13";
            this.label13.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.label13.Size = new System.Drawing.Size(488, 20);
            this.label13.TabIndex = 9;
            this.label13.Text = "Bonuses which an employee has earned";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.IndianRed;
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label16.Location = new System.Drawing.Point(0, 60);
            this.label16.Name = "label16";
            this.label16.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.label16.Size = new System.Drawing.Size(488, 20);
            this.label16.TabIndex = 7;
            this.label16.Text = "A point level was reached but no letter was distributed which led to forgiveness";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label15.Location = new System.Drawing.Point(0, 40);
            this.label15.Name = "label15";
            this.label15.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.label15.Size = new System.Drawing.Size(488, 20);
            this.label15.TabIndex = 6;
            this.label15.Text = "A point level was reached but a letter has not yet been distributed";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblActionsInRed
            // 
            this.lblActionsInRed.BackColor = System.Drawing.Color.LimeGreen;
            this.lblActionsInRed.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblActionsInRed.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblActionsInRed.ForeColor = System.Drawing.Color.Black;
            this.lblActionsInRed.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblActionsInRed.Location = new System.Drawing.Point(0, 20);
            this.lblActionsInRed.Name = "lblActionsInRed";
            this.lblActionsInRed.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.lblActionsInRed.Size = new System.Drawing.Size(488, 20);
            this.lblActionsInRed.TabIndex = 5;
            this.lblActionsInRed.Text = "A point level was reached and a letter was distributed";
            this.lblActionsInRed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.Gray;
            this.label23.Dock = System.Windows.Forms.DockStyle.Top;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label23.Location = new System.Drawing.Point(0, 0);
            this.label23.Name = "label23";
            this.label23.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.label23.Size = new System.Drawing.Size(488, 20);
            this.label23.TabIndex = 11;
            this.label23.Text = "Entries which are no longer considered because of their age";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = null;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Size = new System.Drawing.Size(957, 354);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            // 
            // PageTemplate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1220, 752);
            this.Controls.Add(this.flyoutLegend);
            this.Controls.Add(this.flyoutEmployeeInfo);
            this.Controls.Add(this.tabPane1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.LookAndFeel.SkinName = "Sharp";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "PageTemplate";
            this.Text = "PageTemplate";
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabViewEmployeeData.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcEmployeeEntries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcEmployeeEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.tabNavigationPage3.ResumeLayout(false);
            this.tabNavigationPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panelBonuses.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            this.panelBottom.ResumeLayout(false);
            this.panelBottom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrint.Properties)).EndInit();
            this.panelFMLA.ResumeLayout(false);
            this.thewhiteline.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panelAnalysis.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panelTopSelectors.ResumeLayout(false);
            this.panelTopSelectors.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listViewType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deListByPayDate_To.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deListByPayDate_To.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deListByPayDate_From.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deListByPayDate_From.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEmployeeList.Properties)).EndInit();
            this.panelLeftRunner.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).EndInit();
            this.panel15.ResumeLayout(false);
            this.tabViewAllData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcHighPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flyoutEmployeeInfo)).EndInit();
            this.flyoutEmployeeInfo.ResumeLayout(false);
            this.panelEmployeeInfo.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flyoutLegend)).EndInit();
            this.flyoutLegend.ResumeLayout(false);
            this.flyoutLegend.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flyoutPanelControl1)).EndInit();
            this.flyoutPanelControl1.ResumeLayout(false);
            this.panelLegendTop.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panelLegendBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1.Panel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1.Panel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabViewEmployeeData;
        private System.Windows.Forms.Panel panelTopSelectors;
        private DevExpress.XtraEditors.DateEdit deListByPayDate_From;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.CheckedListBoxControl listViewType;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage3;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage4;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panelLeftRunner;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit1;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit2;
        private DevExpress.XtraGrid.GridControl gcEmployeeEvents;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridControl gcEmployeeEntries;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel6;
        private DevExpress.XtraEditors.HyperlinkLabelControl hlinkEmployeePoints;
        private System.Windows.Forms.Label labelCeridianUpdateDate;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabViewAllData;
        private DevExpress.XtraGrid.GridControl gcHighPoints;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.Panel panel12;
        public System.Windows.Forms.Label labelLastUpdate;
        private System.Windows.Forms.Panel panelAnalysis;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panelFMLA;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.DateEdit deListByPayDate_To;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit3;
        private System.Windows.Forms.Label lblFMLAVacationHours;
        private System.Windows.Forms.Label lblFMLANonVacationHours;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panelLegendTop;
        private System.Windows.Forms.Label labelFMLAHRNotified;
        private System.Windows.Forms.Panel panelLegendBottom;
        private System.Windows.Forms.Label lblActionsInRed;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private DevExpress.Utils.FlyoutPanel flyoutLegend;
        private DevExpress.Utils.FlyoutPanelControl flyoutPanelControl1;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label lblInfo4;
        private System.Windows.Forms.Panel panelEmployeeInfo;
        private System.Windows.Forms.Label lblInfo2;
        private System.Windows.Forms.Label lblInfo3;
        private System.Windows.Forms.Panel panel17;
        private DevExpress.Utils.FlyoutPanel flyoutEmployeeInfo;
        private System.Windows.Forms.Label lblinfo1;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label lblDepartment;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label lblShift;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Label lblSeniorityDate;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Label lblSupervisor;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraEditors.HyperlinkLabelControl hlinkLegend;
        private DevExpress.XtraEditors.HyperlinkLabelControl hlinkEmployeeInfo;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblFMLATotalHours;
        private System.Windows.Forms.Panel panel15;
        private DevExpress.XtraEditors.HyperlinkLabelControl btnShowCeridianData;
        private System.Windows.Forms.Panel panelBottom;
        public DevExpress.XtraEditors.ComboBoxEdit cbEmployeeList;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel thewhiteline;
        private System.Windows.Forms.Panel theactualwhiteline;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblEmployeeType;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label lblRehireDate;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.ButtonEdit btnRefresh;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.ButtonEdit btnPrint;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panelBonuses;
        private System.Windows.Forms.Panel panel24;
        private DevExpress.XtraEditors.HyperlinkLabelControl hlinkEmployeeBonuses;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
    }
}