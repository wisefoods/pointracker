﻿namespace AttendanceTracker
{
    partial class AdminTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminTemplate));
            this.gcAdmin = new DevExpress.XtraGrid.GridControl();
            this.gvAdmin = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panelAddNewRehire = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.deDateOfRehire = new DevExpress.XtraEditors.DateEdit();
            this.cbRehireEmployeeName = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panel27 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.btnAddNewRehireDoIt = new DevExpress.XtraEditors.SimpleButton();
            this.panel25 = new System.Windows.Forms.Panel();
            this.btnAddNewRehireCancel = new DevExpress.XtraEditors.SimpleButton();
            this.panelAddEmailRecipient = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.cbAddEmailRecipient_Department = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbAddEmailRecipient_Type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tbAddEmailRecipient_Email = new System.Windows.Forms.TextBox();
            this.tbAddEmailRecipient_Name = new System.Windows.Forms.TextBox();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.btnSaveNewEmailRecipient = new DevExpress.XtraEditors.SimpleButton();
            this.panel20 = new System.Windows.Forms.Panel();
            this.btnCancelAddEmailRecipient = new DevExpress.XtraEditors.SimpleButton();
            this.panelUpdateSingleEmployee = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.SINGLE_cbEmployeeList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnGoUpdateAllSingleEmployee = new DevExpress.XtraEditors.SimpleButton();
            this.panel16 = new System.Windows.Forms.Panel();
            this.btnCancelUpdateAllSingleEmployee = new DevExpress.XtraEditors.SimpleButton();
            this.panelAddManualAdjustment = new System.Windows.Forms.Panel();
            this.panelIDK = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.ME_meLongNote = new DevExpress.XtraEditors.MemoEdit();
            this.ME_tbShortNote = new System.Windows.Forms.TextBox();
            this.ME_numNewNetHours = new System.Windows.Forms.NumericUpDown();
            this.ME_cbNewPayCode = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ME_numExistingNetHours = new System.Windows.Forms.NumericUpDown();
            this.ME_cbExistingPayCode = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ME_deDateToAdjust = new DevExpress.XtraEditors.DateEdit();
            this.ME_cbEmployeeList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnConfirmAddManualAdjustment = new DevExpress.XtraEditors.SimpleButton();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnCancelAddManualAdjustment = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblAllowEdit = new System.Windows.Forms.Label();
            this.lblRowCount = new System.Windows.Forms.Label();
            this.btnRefreshData = new DevExpress.XtraEditors.SimpleButton();
            this.cbArea = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panel8 = new System.Windows.Forms.Panel();
            this.cbOverrideAdminForReporting = new System.Windows.Forms.CheckBox();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddManualAdjustment = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddEmailRecipient = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddNewRehire = new DevExpress.XtraEditors.SimpleButton();
            this.btnUpdateAllForSingleEmployee = new DevExpress.XtraEditors.SimpleButton();
            this.btnUpdateCurrentEmployeesOnly = new DevExpress.XtraEditors.SimpleButton();
            this.btnSendPointLetterReminderEmail = new DevExpress.XtraEditors.SimpleButton();
            this.btnSendMissedPointLetterEmail = new DevExpress.XtraEditors.SimpleButton();
            this.btnSendFMLAEmail = new DevExpress.XtraEditors.SimpleButton();
            this.btnSendPADEmail = new DevExpress.XtraEditors.SimpleButton();
            this.btnSendReportEmail = new DevExpress.XtraEditors.SimpleButton();
            this.label5 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.btnPrintPointLetter = new DevExpress.XtraEditors.SimpleButton();
            this.panelPointLetter = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.cbPointLetterEmployeeName = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panel33 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panel35 = new System.Windows.Forms.Panel();
            this.btnPointLetterDoIt = new DevExpress.XtraEditors.SimpleButton();
            this.panel36 = new System.Windows.Forms.Panel();
            this.btnPointLetterCancel = new DevExpress.XtraEditors.SimpleButton();
            this.cbPointLetterPointValue = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gcAdmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAdmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelAddNewRehire.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deDateOfRehire.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateOfRehire.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbRehireEmployeeName.Properties)).BeginInit();
            this.panel27.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panelAddEmailRecipient.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbAddEmailRecipient_Department.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAddEmailRecipient_Type.Properties)).BeginInit();
            this.panel22.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panelUpdateSingleEmployee.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SINGLE_cbEmployeeList.Properties)).BeginInit();
            this.panel14.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panelAddManualAdjustment.SuspendLayout();
            this.panelIDK.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ME_meLongNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ME_numNewNetHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ME_cbNewPayCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ME_numExistingNetHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ME_cbExistingPayCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ME_deDateToAdjust.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ME_deDateToAdjust.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ME_cbEmployeeList.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbArea.Properties)).BeginInit();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panelPointLetter.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbPointLetterEmployeeName.Properties)).BeginInit();
            this.panel33.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbPointLetterPointValue.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcAdmin
            // 
            this.gcAdmin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcAdmin.Location = new System.Drawing.Point(263, 0);
            this.gcAdmin.LookAndFeel.SkinName = "Sharp";
            this.gcAdmin.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gcAdmin.MainView = this.gvAdmin;
            this.gcAdmin.Name = "gcAdmin";
            this.gcAdmin.Size = new System.Drawing.Size(812, 1358);
            this.gcAdmin.TabIndex = 6;
            this.gcAdmin.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvAdmin});
            // 
            // gvAdmin
            // 
            this.gvAdmin.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(209)))), ((int)(((byte)(234)))));
            this.gvAdmin.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvAdmin.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gvAdmin.Appearance.OddRow.Options.UseBackColor = true;
            this.gvAdmin.GridControl = this.gcAdmin;
            this.gvAdmin.Name = "gvAdmin";
            this.gvAdmin.OptionsBehavior.Editable = false;
            this.gvAdmin.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvAdmin.OptionsFind.AlwaysVisible = true;
            this.gvAdmin.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvAdmin.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gvAdmin.OptionsView.BestFitMaxRowCount = 1000;
            this.gvAdmin.OptionsView.BestFitMode = DevExpress.XtraGrid.Views.Grid.GridBestFitMode.Full;
            this.gvAdmin.OptionsView.EnableAppearanceOddRow = true;
            this.gvAdmin.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gvAdmin_RowUpdated);
            this.gvAdmin.ColumnFilterChanged += new System.EventHandler(this.gvAdmin_ColumnFilterChanged);
            this.gvAdmin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvAdmin_KeyDown);
            this.gvAdmin.DoubleClick += new System.EventHandler(this.gvAdmin_DoubleClick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            this.splitContainer1.Panel2Collapsed = true;
            this.splitContainer1.Size = new System.Drawing.Size(1079, 1362);
            this.splitContainer1.SplitterDistance = 428;
            this.splitContainer1.TabIndex = 7;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panelPointLetter);
            this.panel3.Controls.Add(this.panelAddNewRehire);
            this.panel3.Controls.Add(this.panelAddEmailRecipient);
            this.panel3.Controls.Add(this.panelUpdateSingleEmployee);
            this.panel3.Controls.Add(this.panelAddManualAdjustment);
            this.panel3.Controls.Add(this.gcAdmin);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1075, 1358);
            this.panel3.TabIndex = 7;
            // 
            // panelAddNewRehire
            // 
            this.panelAddNewRehire.BackColor = System.Drawing.Color.Black;
            this.panelAddNewRehire.Controls.Add(this.panel29);
            this.panelAddNewRehire.Controls.Add(this.panel23);
            this.panelAddNewRehire.Location = new System.Drawing.Point(316, 1013);
            this.panelAddNewRehire.Name = "panelAddNewRehire";
            this.panelAddNewRehire.Padding = new System.Windows.Forms.Padding(10, 10, 10, 0);
            this.panelAddNewRehire.Size = new System.Drawing.Size(587, 136);
            this.panelAddNewRehire.TabIndex = 31;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.Blue;
            this.panel29.Controls.Add(this.panel26);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel29.Location = new System.Drawing.Point(10, 10);
            this.panel29.Name = "panel29";
            this.panel29.Padding = new System.Windows.Forms.Padding(10);
            this.panel29.Size = new System.Drawing.Size(567, 65);
            this.panel29.TabIndex = 33;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.Blue;
            this.panel26.Controls.Add(this.deDateOfRehire);
            this.panel26.Controls.Add(this.cbRehireEmployeeName);
            this.panel26.Controls.Add(this.panel27);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel26.Location = new System.Drawing.Point(10, 10);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(547, 45);
            this.panel26.TabIndex = 0;
            // 
            // deDateOfRehire
            // 
            this.deDateOfRehire.Dock = System.Windows.Forms.DockStyle.Top;
            this.deDateOfRehire.EditValue = null;
            this.deDateOfRehire.Location = new System.Drawing.Point(200, 20);
            this.deDateOfRehire.Name = "deDateOfRehire";
            this.deDateOfRehire.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDateOfRehire.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDateOfRehire.Properties.LookAndFeel.SkinName = "Sharp";
            this.deDateOfRehire.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.deDateOfRehire.Size = new System.Drawing.Size(347, 20);
            this.deDateOfRehire.TabIndex = 109;
            // 
            // cbRehireEmployeeName
            // 
            this.cbRehireEmployeeName.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbRehireEmployeeName.EditValue = "";
            this.cbRehireEmployeeName.Location = new System.Drawing.Point(200, 0);
            this.cbRehireEmployeeName.Name = "cbRehireEmployeeName";
            this.cbRehireEmployeeName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbRehireEmployeeName.Properties.DropDownRows = 15;
            this.cbRehireEmployeeName.Properties.ImmediatePopup = true;
            this.cbRehireEmployeeName.Properties.LookAndFeel.SkinName = "Sharp";
            this.cbRehireEmployeeName.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbRehireEmployeeName.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbRehireEmployeeName.Size = new System.Drawing.Size(347, 20);
            this.cbRehireEmployeeName.TabIndex = 110;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.label29);
            this.panel27.Controls.Add(this.label30);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel27.Location = new System.Drawing.Point(0, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(200, 45);
            this.panel27.TabIndex = 29;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Dock = System.Windows.Forms.DockStyle.Top;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label29.Location = new System.Drawing.Point(0, 21);
            this.label29.Name = "label29";
            this.label29.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label29.Size = new System.Drawing.Size(90, 21);
            this.label29.TabIndex = 1;
            this.label29.Text = "Date of re-hire";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Dock = System.Windows.Forms.DockStyle.Top;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(0, 0);
            this.label30.Name = "label30";
            this.label30.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label30.Size = new System.Drawing.Size(62, 21);
            this.label30.TabIndex = 2;
            this.label30.Text = "Employee";
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.Black;
            this.panel23.Controls.Add(this.panel24);
            this.panel23.Controls.Add(this.panel25);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel23.Location = new System.Drawing.Point(10, 75);
            this.panel23.Name = "panel23";
            this.panel23.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.panel23.Size = new System.Drawing.Size(567, 61);
            this.panel23.TabIndex = 10;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.btnAddNewRehireDoIt);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel24.Location = new System.Drawing.Point(204, 10);
            this.panel24.Name = "panel24";
            this.panel24.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.panel24.Size = new System.Drawing.Size(363, 41);
            this.panel24.TabIndex = 26;
            // 
            // btnAddNewRehireDoIt
            // 
            this.btnAddNewRehireDoIt.Appearance.BackColor = System.Drawing.Color.Blue;
            this.btnAddNewRehireDoIt.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnAddNewRehireDoIt.Appearance.Options.UseBackColor = true;
            this.btnAddNewRehireDoIt.Appearance.Options.UseFont = true;
            this.btnAddNewRehireDoIt.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAddNewRehireDoIt.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnAddNewRehireDoIt.ImageOptions.SvgImage")));
            this.btnAddNewRehireDoIt.Location = new System.Drawing.Point(10, 1);
            this.btnAddNewRehireDoIt.LookAndFeel.SkinName = "Blue";
            this.btnAddNewRehireDoIt.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAddNewRehireDoIt.Name = "btnAddNewRehireDoIt";
            this.btnAddNewRehireDoIt.Size = new System.Drawing.Size(353, 40);
            this.btnAddNewRehireDoIt.TabIndex = 27;
            this.btnAddNewRehireDoIt.Text = "Save";
            this.btnAddNewRehireDoIt.Click += new System.EventHandler(this.btnAddNewRehireDoIt_Click);
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.btnAddNewRehireCancel);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel25.Location = new System.Drawing.Point(0, 10);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(204, 41);
            this.panel25.TabIndex = 25;
            // 
            // btnAddNewRehireCancel
            // 
            this.btnAddNewRehireCancel.Appearance.BackColor = System.Drawing.Color.Blue;
            this.btnAddNewRehireCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnAddNewRehireCancel.Appearance.Options.UseBackColor = true;
            this.btnAddNewRehireCancel.Appearance.Options.UseFont = true;
            this.btnAddNewRehireCancel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAddNewRehireCancel.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnAddNewRehireCancel.ImageOptions.SvgImage")));
            this.btnAddNewRehireCancel.Location = new System.Drawing.Point(0, 1);
            this.btnAddNewRehireCancel.LookAndFeel.SkinName = "Blue";
            this.btnAddNewRehireCancel.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAddNewRehireCancel.Name = "btnAddNewRehireCancel";
            this.btnAddNewRehireCancel.Size = new System.Drawing.Size(204, 40);
            this.btnAddNewRehireCancel.TabIndex = 27;
            this.btnAddNewRehireCancel.Text = "Cancel";
            this.btnAddNewRehireCancel.Click += new System.EventHandler(this.btnAddNewRehireCancel_Click);
            // 
            // panelAddEmailRecipient
            // 
            this.panelAddEmailRecipient.BackColor = System.Drawing.Color.Black;
            this.panelAddEmailRecipient.Controls.Add(this.panel17);
            this.panelAddEmailRecipient.Controls.Add(this.panel18);
            this.panelAddEmailRecipient.Location = new System.Drawing.Point(316, 823);
            this.panelAddEmailRecipient.Name = "panelAddEmailRecipient";
            this.panelAddEmailRecipient.Padding = new System.Windows.Forms.Padding(10, 10, 10, 0);
            this.panelAddEmailRecipient.Size = new System.Drawing.Size(587, 176);
            this.panelAddEmailRecipient.TabIndex = 30;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Blue;
            this.panel17.Controls.Add(this.panel21);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(10, 10);
            this.panel17.Name = "panel17";
            this.panel17.Padding = new System.Windows.Forms.Padding(10);
            this.panel17.Size = new System.Drawing.Size(567, 105);
            this.panel17.TabIndex = 32;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.Blue;
            this.panel21.Controls.Add(this.cbAddEmailRecipient_Department);
            this.panel21.Controls.Add(this.cbAddEmailRecipient_Type);
            this.panel21.Controls.Add(this.tbAddEmailRecipient_Email);
            this.panel21.Controls.Add(this.tbAddEmailRecipient_Name);
            this.panel21.Controls.Add(this.panel22);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.Location = new System.Drawing.Point(10, 10);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(547, 85);
            this.panel21.TabIndex = 0;
            // 
            // cbAddEmailRecipient_Department
            // 
            this.cbAddEmailRecipient_Department.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbAddEmailRecipient_Department.EditValue = "";
            this.cbAddEmailRecipient_Department.Location = new System.Drawing.Point(200, 62);
            this.cbAddEmailRecipient_Department.Name = "cbAddEmailRecipient_Department";
            this.cbAddEmailRecipient_Department.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbAddEmailRecipient_Department.Properties.DropDownRows = 15;
            this.cbAddEmailRecipient_Department.Properties.ImmediatePopup = true;
            this.cbAddEmailRecipient_Department.Properties.LookAndFeel.SkinName = "Sharp";
            this.cbAddEmailRecipient_Department.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbAddEmailRecipient_Department.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbAddEmailRecipient_Department.Size = new System.Drawing.Size(347, 20);
            this.cbAddEmailRecipient_Department.TabIndex = 111;
            // 
            // cbAddEmailRecipient_Type
            // 
            this.cbAddEmailRecipient_Type.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbAddEmailRecipient_Type.EditValue = "";
            this.cbAddEmailRecipient_Type.Location = new System.Drawing.Point(200, 42);
            this.cbAddEmailRecipient_Type.Name = "cbAddEmailRecipient_Type";
            this.cbAddEmailRecipient_Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbAddEmailRecipient_Type.Properties.DropDownRows = 15;
            this.cbAddEmailRecipient_Type.Properties.ImmediatePopup = true;
            this.cbAddEmailRecipient_Type.Properties.LookAndFeel.SkinName = "Sharp";
            this.cbAddEmailRecipient_Type.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbAddEmailRecipient_Type.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbAddEmailRecipient_Type.Size = new System.Drawing.Size(347, 20);
            this.cbAddEmailRecipient_Type.TabIndex = 110;
            // 
            // tbAddEmailRecipient_Email
            // 
            this.tbAddEmailRecipient_Email.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbAddEmailRecipient_Email.Location = new System.Drawing.Point(200, 21);
            this.tbAddEmailRecipient_Email.Name = "tbAddEmailRecipient_Email";
            this.tbAddEmailRecipient_Email.Size = new System.Drawing.Size(347, 21);
            this.tbAddEmailRecipient_Email.TabIndex = 109;
            // 
            // tbAddEmailRecipient_Name
            // 
            this.tbAddEmailRecipient_Name.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbAddEmailRecipient_Name.Location = new System.Drawing.Point(200, 0);
            this.tbAddEmailRecipient_Name.Name = "tbAddEmailRecipient_Name";
            this.tbAddEmailRecipient_Name.Size = new System.Drawing.Size(347, 21);
            this.tbAddEmailRecipient_Name.TabIndex = 108;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.label26);
            this.panel22.Controls.Add(this.label25);
            this.panel22.Controls.Add(this.label23);
            this.panel22.Controls.Add(this.label24);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(200, 85);
            this.panel22.TabIndex = 29;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Dock = System.Windows.Forms.DockStyle.Top;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(0, 63);
            this.label26.Name = "label26";
            this.label26.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label26.Size = new System.Drawing.Size(76, 21);
            this.label26.TabIndex = 4;
            this.label26.Text = "Department";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Dock = System.Windows.Forms.DockStyle.Top;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(0, 42);
            this.label25.Name = "label25";
            this.label25.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label25.Size = new System.Drawing.Size(83, 21);
            this.label25.TabIndex = 3;
            this.label25.Text = "Type of email";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Top;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(0, 21);
            this.label23.Name = "label23";
            this.label23.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label23.Size = new System.Drawing.Size(131, 21);
            this.label23.TabIndex = 1;
            this.label23.Text = "Email recipient - Email";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Top;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(0, 0);
            this.label24.Name = "label24";
            this.label24.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label24.Size = new System.Drawing.Size(133, 21);
            this.label24.TabIndex = 2;
            this.label24.Text = "Email recipient - Name";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Black;
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Controls.Add(this.panel20);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel18.Location = new System.Drawing.Point(10, 115);
            this.panel18.Name = "panel18";
            this.panel18.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.panel18.Size = new System.Drawing.Size(567, 61);
            this.panel18.TabIndex = 10;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.btnSaveNewEmailRecipient);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel19.Location = new System.Drawing.Point(204, 10);
            this.panel19.Name = "panel19";
            this.panel19.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.panel19.Size = new System.Drawing.Size(363, 41);
            this.panel19.TabIndex = 26;
            // 
            // btnSaveNewEmailRecipient
            // 
            this.btnSaveNewEmailRecipient.Appearance.BackColor = System.Drawing.Color.Blue;
            this.btnSaveNewEmailRecipient.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSaveNewEmailRecipient.Appearance.Options.UseBackColor = true;
            this.btnSaveNewEmailRecipient.Appearance.Options.UseFont = true;
            this.btnSaveNewEmailRecipient.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSaveNewEmailRecipient.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnSaveNewEmailRecipient.ImageOptions.SvgImage")));
            this.btnSaveNewEmailRecipient.Location = new System.Drawing.Point(10, 1);
            this.btnSaveNewEmailRecipient.LookAndFeel.SkinName = "Blue";
            this.btnSaveNewEmailRecipient.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSaveNewEmailRecipient.Name = "btnSaveNewEmailRecipient";
            this.btnSaveNewEmailRecipient.Size = new System.Drawing.Size(353, 40);
            this.btnSaveNewEmailRecipient.TabIndex = 27;
            this.btnSaveNewEmailRecipient.Text = "Save";
            this.btnSaveNewEmailRecipient.Click += new System.EventHandler(this.btnSaveNewEmailRecipient_Click);
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.btnCancelAddEmailRecipient);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel20.Location = new System.Drawing.Point(0, 10);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(204, 41);
            this.panel20.TabIndex = 25;
            // 
            // btnCancelAddEmailRecipient
            // 
            this.btnCancelAddEmailRecipient.Appearance.BackColor = System.Drawing.Color.Blue;
            this.btnCancelAddEmailRecipient.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnCancelAddEmailRecipient.Appearance.Options.UseBackColor = true;
            this.btnCancelAddEmailRecipient.Appearance.Options.UseFont = true;
            this.btnCancelAddEmailRecipient.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnCancelAddEmailRecipient.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnCancelAddEmailRecipient.ImageOptions.SvgImage")));
            this.btnCancelAddEmailRecipient.Location = new System.Drawing.Point(0, 1);
            this.btnCancelAddEmailRecipient.LookAndFeel.SkinName = "Blue";
            this.btnCancelAddEmailRecipient.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnCancelAddEmailRecipient.Name = "btnCancelAddEmailRecipient";
            this.btnCancelAddEmailRecipient.Size = new System.Drawing.Size(204, 40);
            this.btnCancelAddEmailRecipient.TabIndex = 27;
            this.btnCancelAddEmailRecipient.Text = "Cancel";
            this.btnCancelAddEmailRecipient.Click += new System.EventHandler(this.btnCancelAddEmailRecipient_Click);
            // 
            // panelUpdateSingleEmployee
            // 
            this.panelUpdateSingleEmployee.BackColor = System.Drawing.Color.Black;
            this.panelUpdateSingleEmployee.Controls.Add(this.panel28);
            this.panelUpdateSingleEmployee.Controls.Add(this.panel12);
            this.panelUpdateSingleEmployee.Location = new System.Drawing.Point(316, 699);
            this.panelUpdateSingleEmployee.Name = "panelUpdateSingleEmployee";
            this.panelUpdateSingleEmployee.Padding = new System.Windows.Forms.Padding(10, 10, 10, 0);
            this.panelUpdateSingleEmployee.Size = new System.Drawing.Size(587, 114);
            this.panelUpdateSingleEmployee.TabIndex = 29;
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.Blue;
            this.panel28.Controls.Add(this.panel13);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel28.Location = new System.Drawing.Point(10, 10);
            this.panel28.Name = "panel28";
            this.panel28.Padding = new System.Windows.Forms.Padding(10);
            this.panel28.Size = new System.Drawing.Size(567, 43);
            this.panel28.TabIndex = 33;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Blue;
            this.panel13.Controls.Add(this.SINGLE_cbEmployeeList);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(10, 10);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(547, 23);
            this.panel13.TabIndex = 0;
            // 
            // SINGLE_cbEmployeeList
            // 
            this.SINGLE_cbEmployeeList.Dock = System.Windows.Forms.DockStyle.Top;
            this.SINGLE_cbEmployeeList.EditValue = "";
            this.SINGLE_cbEmployeeList.Location = new System.Drawing.Point(200, 0);
            this.SINGLE_cbEmployeeList.Name = "SINGLE_cbEmployeeList";
            this.SINGLE_cbEmployeeList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SINGLE_cbEmployeeList.Properties.DropDownRows = 15;
            this.SINGLE_cbEmployeeList.Properties.ImmediatePopup = true;
            this.SINGLE_cbEmployeeList.Properties.LookAndFeel.SkinName = "Sharp";
            this.SINGLE_cbEmployeeList.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.SINGLE_cbEmployeeList.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SINGLE_cbEmployeeList.Size = new System.Drawing.Size(347, 20);
            this.SINGLE_cbEmployeeList.TabIndex = 28;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.label19);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(200, 23);
            this.panel14.TabIndex = 29;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Top;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(0, 0);
            this.label19.Name = "label19";
            this.label19.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label19.Size = new System.Drawing.Size(62, 21);
            this.label19.TabIndex = 1;
            this.label19.Text = "Employee";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Black;
            this.panel12.Controls.Add(this.panel15);
            this.panel12.Controls.Add(this.panel16);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel12.Location = new System.Drawing.Point(10, 53);
            this.panel12.Name = "panel12";
            this.panel12.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.panel12.Size = new System.Drawing.Size(567, 61);
            this.panel12.TabIndex = 10;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.btnGoUpdateAllSingleEmployee);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(204, 10);
            this.panel15.Name = "panel15";
            this.panel15.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.panel15.Size = new System.Drawing.Size(363, 41);
            this.panel15.TabIndex = 26;
            // 
            // btnGoUpdateAllSingleEmployee
            // 
            this.btnGoUpdateAllSingleEmployee.Appearance.BackColor = System.Drawing.Color.Blue;
            this.btnGoUpdateAllSingleEmployee.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnGoUpdateAllSingleEmployee.Appearance.Options.UseBackColor = true;
            this.btnGoUpdateAllSingleEmployee.Appearance.Options.UseFont = true;
            this.btnGoUpdateAllSingleEmployee.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnGoUpdateAllSingleEmployee.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnGoUpdateAllSingleEmployee.ImageOptions.SvgImage")));
            this.btnGoUpdateAllSingleEmployee.Location = new System.Drawing.Point(10, 1);
            this.btnGoUpdateAllSingleEmployee.LookAndFeel.SkinName = "Blue";
            this.btnGoUpdateAllSingleEmployee.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnGoUpdateAllSingleEmployee.Name = "btnGoUpdateAllSingleEmployee";
            this.btnGoUpdateAllSingleEmployee.Size = new System.Drawing.Size(353, 40);
            this.btnGoUpdateAllSingleEmployee.TabIndex = 27;
            this.btnGoUpdateAllSingleEmployee.Text = "Go";
            this.btnGoUpdateAllSingleEmployee.Click += new System.EventHandler(this.btnGoUpdateAllSingleEmployee_Click);
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.btnCancelUpdateAllSingleEmployee);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel16.Location = new System.Drawing.Point(0, 10);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(204, 41);
            this.panel16.TabIndex = 25;
            // 
            // btnCancelUpdateAllSingleEmployee
            // 
            this.btnCancelUpdateAllSingleEmployee.Appearance.BackColor = System.Drawing.Color.Blue;
            this.btnCancelUpdateAllSingleEmployee.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnCancelUpdateAllSingleEmployee.Appearance.Options.UseBackColor = true;
            this.btnCancelUpdateAllSingleEmployee.Appearance.Options.UseFont = true;
            this.btnCancelUpdateAllSingleEmployee.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnCancelUpdateAllSingleEmployee.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnCancelUpdateAllSingleEmployee.ImageOptions.SvgImage")));
            this.btnCancelUpdateAllSingleEmployee.Location = new System.Drawing.Point(0, 1);
            this.btnCancelUpdateAllSingleEmployee.LookAndFeel.SkinName = "Blue";
            this.btnCancelUpdateAllSingleEmployee.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnCancelUpdateAllSingleEmployee.Name = "btnCancelUpdateAllSingleEmployee";
            this.btnCancelUpdateAllSingleEmployee.Size = new System.Drawing.Size(204, 40);
            this.btnCancelUpdateAllSingleEmployee.TabIndex = 27;
            this.btnCancelUpdateAllSingleEmployee.Text = "Cancel";
            this.btnCancelUpdateAllSingleEmployee.Click += new System.EventHandler(this.btnCancelUpdateAllSingleEmployee_Click);
            // 
            // panelAddManualAdjustment
            // 
            this.panelAddManualAdjustment.BackColor = System.Drawing.Color.Black;
            this.panelAddManualAdjustment.Controls.Add(this.panelIDK);
            this.panelAddManualAdjustment.Controls.Add(this.panel7);
            this.panelAddManualAdjustment.Location = new System.Drawing.Point(316, 10);
            this.panelAddManualAdjustment.Name = "panelAddManualAdjustment";
            this.panelAddManualAdjustment.Padding = new System.Windows.Forms.Padding(10, 10, 10, 0);
            this.panelAddManualAdjustment.Size = new System.Drawing.Size(587, 683);
            this.panelAddManualAdjustment.TabIndex = 9;
            // 
            // panelIDK
            // 
            this.panelIDK.BackColor = System.Drawing.Color.Blue;
            this.panelIDK.Controls.Add(this.panel6);
            this.panelIDK.Controls.Add(this.panel4);
            this.panelIDK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelIDK.Location = new System.Drawing.Point(10, 10);
            this.panelIDK.Name = "panelIDK";
            this.panelIDK.Padding = new System.Windows.Forms.Padding(10);
            this.panelIDK.Size = new System.Drawing.Size(567, 612);
            this.panelIDK.TabIndex = 8;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.ME_meLongNote);
            this.panel6.Controls.Add(this.ME_tbShortNote);
            this.panel6.Controls.Add(this.ME_numNewNetHours);
            this.panel6.Controls.Add(this.ME_cbNewPayCode);
            this.panel6.Controls.Add(this.ME_numExistingNetHours);
            this.panel6.Controls.Add(this.ME_cbExistingPayCode);
            this.panel6.Controls.Add(this.ME_deDateToAdjust);
            this.panel6.Controls.Add(this.ME_cbEmployeeList);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(222, 10);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(335, 592);
            this.panel6.TabIndex = 26;
            // 
            // ME_meLongNote
            // 
            this.ME_meLongNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ME_meLongNote.Location = new System.Drawing.Point(0, 143);
            this.ME_meLongNote.Name = "ME_meLongNote";
            this.ME_meLongNote.Size = new System.Drawing.Size(335, 449);
            this.ME_meLongNote.TabIndex = 108;
            // 
            // ME_tbShortNote
            // 
            this.ME_tbShortNote.Dock = System.Windows.Forms.DockStyle.Top;
            this.ME_tbShortNote.Location = new System.Drawing.Point(0, 122);
            this.ME_tbShortNote.Name = "ME_tbShortNote";
            this.ME_tbShortNote.Size = new System.Drawing.Size(335, 21);
            this.ME_tbShortNote.TabIndex = 106;
            // 
            // ME_numNewNetHours
            // 
            this.ME_numNewNetHours.DecimalPlaces = 3;
            this.ME_numNewNetHours.Dock = System.Windows.Forms.DockStyle.Top;
            this.ME_numNewNetHours.Location = new System.Drawing.Point(0, 101);
            this.ME_numNewNetHours.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.ME_numNewNetHours.Name = "ME_numNewNetHours";
            this.ME_numNewNetHours.Size = new System.Drawing.Size(335, 21);
            this.ME_numNewNetHours.TabIndex = 105;
            this.ME_numNewNetHours.ThousandsSeparator = true;
            // 
            // ME_cbNewPayCode
            // 
            this.ME_cbNewPayCode.Dock = System.Windows.Forms.DockStyle.Top;
            this.ME_cbNewPayCode.EditValue = "";
            this.ME_cbNewPayCode.Location = new System.Drawing.Point(0, 81);
            this.ME_cbNewPayCode.Name = "ME_cbNewPayCode";
            this.ME_cbNewPayCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ME_cbNewPayCode.Properties.DropDownRows = 15;
            this.ME_cbNewPayCode.Properties.ImmediatePopup = true;
            this.ME_cbNewPayCode.Properties.LookAndFeel.SkinName = "Sharp";
            this.ME_cbNewPayCode.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ME_cbNewPayCode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ME_cbNewPayCode.Size = new System.Drawing.Size(335, 20);
            this.ME_cbNewPayCode.TabIndex = 104;
            // 
            // ME_numExistingNetHours
            // 
            this.ME_numExistingNetHours.DecimalPlaces = 3;
            this.ME_numExistingNetHours.Dock = System.Windows.Forms.DockStyle.Top;
            this.ME_numExistingNetHours.Location = new System.Drawing.Point(0, 60);
            this.ME_numExistingNetHours.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.ME_numExistingNetHours.Name = "ME_numExistingNetHours";
            this.ME_numExistingNetHours.Size = new System.Drawing.Size(335, 21);
            this.ME_numExistingNetHours.TabIndex = 103;
            this.ME_numExistingNetHours.ThousandsSeparator = true;
            // 
            // ME_cbExistingPayCode
            // 
            this.ME_cbExistingPayCode.Dock = System.Windows.Forms.DockStyle.Top;
            this.ME_cbExistingPayCode.EditValue = "";
            this.ME_cbExistingPayCode.Location = new System.Drawing.Point(0, 40);
            this.ME_cbExistingPayCode.Name = "ME_cbExistingPayCode";
            this.ME_cbExistingPayCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ME_cbExistingPayCode.Properties.DropDownRows = 15;
            this.ME_cbExistingPayCode.Properties.ImmediatePopup = true;
            this.ME_cbExistingPayCode.Properties.LookAndFeel.SkinName = "Sharp";
            this.ME_cbExistingPayCode.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ME_cbExistingPayCode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ME_cbExistingPayCode.Size = new System.Drawing.Size(335, 20);
            this.ME_cbExistingPayCode.TabIndex = 102;
            // 
            // ME_deDateToAdjust
            // 
            this.ME_deDateToAdjust.Dock = System.Windows.Forms.DockStyle.Top;
            this.ME_deDateToAdjust.EditValue = null;
            this.ME_deDateToAdjust.Location = new System.Drawing.Point(0, 20);
            this.ME_deDateToAdjust.Name = "ME_deDateToAdjust";
            this.ME_deDateToAdjust.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ME_deDateToAdjust.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ME_deDateToAdjust.Properties.LookAndFeel.SkinName = "Sharp";
            this.ME_deDateToAdjust.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ME_deDateToAdjust.Size = new System.Drawing.Size(335, 20);
            this.ME_deDateToAdjust.TabIndex = 101;
            // 
            // ME_cbEmployeeList
            // 
            this.ME_cbEmployeeList.Dock = System.Windows.Forms.DockStyle.Top;
            this.ME_cbEmployeeList.EditValue = "";
            this.ME_cbEmployeeList.Location = new System.Drawing.Point(0, 0);
            this.ME_cbEmployeeList.Name = "ME_cbEmployeeList";
            this.ME_cbEmployeeList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ME_cbEmployeeList.Properties.DropDownRows = 15;
            this.ME_cbEmployeeList.Properties.ImmediatePopup = true;
            this.ME_cbEmployeeList.Properties.LookAndFeel.SkinName = "Sharp";
            this.ME_cbEmployeeList.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ME_cbEmployeeList.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ME_cbEmployeeList.Size = new System.Drawing.Size(335, 20);
            this.ME_cbEmployeeList.TabIndex = 100;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(10, 10);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.panel4.Size = new System.Drawing.Size(212, 592);
            this.panel4.TabIndex = 25;
            // 
            // label18
            // 
            this.label18.Dock = System.Windows.Forms.DockStyle.Top;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(0, 484);
            this.label18.Name = "label18";
            this.label18.Padding = new System.Windows.Forms.Padding(0, 20, 0, 4);
            this.label18.Size = new System.Drawing.Size(202, 108);
            this.label18.TabIndex = 12;
            this.label18.Text = "ɞ Short note will be displayed within the main application pane whereas long note" +
    " is a reference field not visible anywhere except here (it should contain the em" +
    "ail trail which led to this request)";
            // 
            // label17
            // 
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(0, 367);
            this.label17.Name = "label17";
            this.label17.Padding = new System.Windows.Forms.Padding(0, 20, 0, 4);
            this.label17.Size = new System.Drawing.Size(202, 117);
            this.label17.TabIndex = 11;
            this.label17.Text = "ɞ When performing an EDIT or DELETE you can use an old net hours value of 24; doi" +
    "ng so will remove the requirement that the old net hours match (between what you" +
    " specify and what is actually there)";
            // 
            // label16
            // 
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(0, 303);
            this.label16.Name = "label16";
            this.label16.Padding = new System.Windows.Forms.Padding(0, 20, 0, 4);
            this.label16.Size = new System.Drawing.Size(202, 64);
            this.label16.TabIndex = 10;
            this.label16.Text = "ɞ To DELETE: Change old pay code and enter NONE (empty string) for new pay code";
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(0, 239);
            this.label15.Name = "label15";
            this.label15.Padding = new System.Windows.Forms.Padding(0, 20, 0, 4);
            this.label15.Size = new System.Drawing.Size(202, 64);
            this.label15.TabIndex = 9;
            this.label15.Text = "ɞ To EDIT: Change both old and new pay codes to anything but NONE (empty strings)" +
    "";
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Top;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(0, 168);
            this.label14.Name = "label14";
            this.label14.Padding = new System.Windows.Forms.Padding(0, 20, 0, 4);
            this.label14.Size = new System.Drawing.Size(202, 71);
            this.label14.TabIndex = 8;
            this.label14.Text = "ɞ To ADD: Change new pay code and enter NONE (empty string) for old pay code\r\n";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(0, 147);
            this.label12.Name = "label12";
            this.label12.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label12.Size = new System.Drawing.Size(63, 21);
            this.label12.TabIndex = 7;
            this.label12.Text = "Long note";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(0, 126);
            this.label11.Name = "label11";
            this.label11.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label11.Size = new System.Drawing.Size(67, 21);
            this.label11.TabIndex = 6;
            this.label11.Text = "Short note";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(0, 105);
            this.label10.Name = "label10";
            this.label10.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label10.Size = new System.Drawing.Size(177, 21);
            this.label10.TabIndex = 5;
            this.label10.Text = "New/adjusted net hours entry";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(0, 84);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label9.Size = new System.Drawing.Size(174, 21);
            this.label9.TabIndex = 4;
            this.label9.Text = "New/adjusted pay code entry";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(0, 63);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label8.Size = new System.Drawing.Size(185, 21);
            this.label8.TabIndex = 3;
            this.label8.Text = "Actual/existing net hours entry";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(0, 42);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label7.Size = new System.Drawing.Size(182, 21);
            this.label7.TabIndex = 2;
            this.label7.Text = "Actual/existing pay code entry";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(0, 21);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label4.Size = new System.Drawing.Size(88, 21);
            this.label4.TabIndex = 1;
            this.label4.Text = "Date to adjust";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label3.Size = new System.Drawing.Size(62, 21);
            this.label3.TabIndex = 0;
            this.label3.Text = "Employee";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Black;
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Controls.Add(this.panel11);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(10, 622);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.panel7.Size = new System.Drawing.Size(567, 61);
            this.panel7.TabIndex = 9;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.btnConfirmAddManualAdjustment);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(204, 10);
            this.panel10.Name = "panel10";
            this.panel10.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.panel10.Size = new System.Drawing.Size(363, 41);
            this.panel10.TabIndex = 26;
            // 
            // btnConfirmAddManualAdjustment
            // 
            this.btnConfirmAddManualAdjustment.Appearance.BackColor = System.Drawing.Color.Blue;
            this.btnConfirmAddManualAdjustment.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnConfirmAddManualAdjustment.Appearance.Options.UseBackColor = true;
            this.btnConfirmAddManualAdjustment.Appearance.Options.UseFont = true;
            this.btnConfirmAddManualAdjustment.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnConfirmAddManualAdjustment.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnConfirmAddManualAdjustment.ImageOptions.SvgImage")));
            this.btnConfirmAddManualAdjustment.Location = new System.Drawing.Point(10, 1);
            this.btnConfirmAddManualAdjustment.LookAndFeel.SkinName = "Blue";
            this.btnConfirmAddManualAdjustment.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnConfirmAddManualAdjustment.Name = "btnConfirmAddManualAdjustment";
            this.btnConfirmAddManualAdjustment.Size = new System.Drawing.Size(353, 40);
            this.btnConfirmAddManualAdjustment.TabIndex = 108;
            this.btnConfirmAddManualAdjustment.Text = "Save";
            this.btnConfirmAddManualAdjustment.Click += new System.EventHandler(this.btnConfirmAddManualAdjustment_Click);
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.btnCancelAddManualAdjustment);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(0, 10);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(204, 41);
            this.panel11.TabIndex = 25;
            // 
            // btnCancelAddManualAdjustment
            // 
            this.btnCancelAddManualAdjustment.Appearance.BackColor = System.Drawing.Color.Blue;
            this.btnCancelAddManualAdjustment.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnCancelAddManualAdjustment.Appearance.Options.UseBackColor = true;
            this.btnCancelAddManualAdjustment.Appearance.Options.UseFont = true;
            this.btnCancelAddManualAdjustment.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnCancelAddManualAdjustment.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnCancelAddManualAdjustment.ImageOptions.SvgImage")));
            this.btnCancelAddManualAdjustment.Location = new System.Drawing.Point(0, 1);
            this.btnCancelAddManualAdjustment.LookAndFeel.SkinName = "Blue";
            this.btnCancelAddManualAdjustment.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnCancelAddManualAdjustment.Name = "btnCancelAddManualAdjustment";
            this.btnCancelAddManualAdjustment.Size = new System.Drawing.Size(204, 40);
            this.btnCancelAddManualAdjustment.TabIndex = 109;
            this.btnCancelAddManualAdjustment.Text = "Cancel";
            this.btnCancelAddManualAdjustment.Click += new System.EventHandler(this.btnCancelAddManualAdjustment_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.lblAllowEdit);
            this.panel2.Controls.Add(this.lblRowCount);
            this.panel2.Controls.Add(this.btnRefreshData);
            this.panel2.Controls.Add(this.cbArea);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(263, 1358);
            this.panel2.TabIndex = 7;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.DarkOrange;
            this.label22.Location = new System.Drawing.Point(3, 186);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(179, 26);
            this.label22.TabIndex = 31;
            this.label22.Text = "Rows cannot be added\r\n(unless an option is provided below)";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.DarkOrange;
            this.label21.Location = new System.Drawing.Point(3, 146);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(127, 13);
            this.label21.TabIndex = 30;
            this.label21.Text = "Double-click a row to edit";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(3, 221);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(195, 26);
            this.label20.TabIndex = 29;
            this.label20.Text = "* Change any ID field at your own peril\r\n(but really don\'t do that, it\'ll fail)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.DarkOrange;
            this.label13.Location = new System.Drawing.Point(3, 166);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(158, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Use CTRL+ DEL to delete a row";
            // 
            // lblAllowEdit
            // 
            this.lblAllowEdit.AutoSize = true;
            this.lblAllowEdit.Location = new System.Drawing.Point(3, 125);
            this.lblAllowEdit.Name = "lblAllowEdit";
            this.lblAllowEdit.Size = new System.Drawing.Size(150, 13);
            this.lblAllowEdit.TabIndex = 27;
            this.lblAllowEdit.Text = "ALLOW TABLE EDIT MESSAGE";
            this.lblAllowEdit.Click += new System.EventHandler(this.lblAllowEdit_Click);
            // 
            // lblRowCount
            // 
            this.lblRowCount.AutoSize = true;
            this.lblRowCount.Location = new System.Drawing.Point(3, 104);
            this.lblRowCount.Name = "lblRowCount";
            this.lblRowCount.Size = new System.Drawing.Size(71, 13);
            this.lblRowCount.TabIndex = 17;
            this.lblRowCount.Text = "Row Count #";
            // 
            // btnRefreshData
            // 
            this.btnRefreshData.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnRefreshData.Appearance.Options.UseFont = true;
            this.btnRefreshData.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRefreshData.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRefreshData.ImageOptions.Image")));
            this.btnRefreshData.Location = new System.Drawing.Point(0, 61);
            this.btnRefreshData.Name = "btnRefreshData";
            this.btnRefreshData.Size = new System.Drawing.Size(243, 40);
            this.btnRefreshData.TabIndex = 16;
            this.btnRefreshData.Text = "Refresh Data";
            this.btnRefreshData.Click += new System.EventHandler(this.btnRefreshData_Click);
            // 
            // cbArea
            // 
            this.cbArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbArea.EditValue = "";
            this.cbArea.Location = new System.Drawing.Point(0, 41);
            this.cbArea.Name = "cbArea";
            this.cbArea.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbArea.Properties.DropDownRows = 100;
            this.cbArea.Properties.LookAndFeel.SkinName = "Sharp";
            this.cbArea.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbArea.Properties.Sorted = true;
            this.cbArea.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbArea.Size = new System.Drawing.Size(243, 20);
            this.cbArea.TabIndex = 15;
            this.cbArea.SelectedIndexChanged += new System.EventHandler(this.cbArea_SelectedIndexChanged);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.cbOverrideAdminForReporting);
            this.panel8.Controls.Add(this.simpleButton1);
            this.panel8.Controls.Add(this.btnAddManualAdjustment);
            this.panel8.Controls.Add(this.btnAddEmailRecipient);
            this.panel8.Controls.Add(this.btnAddNewRehire);
            this.panel8.Controls.Add(this.btnUpdateAllForSingleEmployee);
            this.panel8.Controls.Add(this.btnUpdateCurrentEmployeesOnly);
            this.panel8.Controls.Add(this.btnSendPointLetterReminderEmail);
            this.panel8.Controls.Add(this.btnSendMissedPointLetterEmail);
            this.panel8.Controls.Add(this.btnSendFMLAEmail);
            this.panel8.Controls.Add(this.btnSendPADEmail);
            this.panel8.Controls.Add(this.btnSendReportEmail);
            this.panel8.Controls.Add(this.btnPrintPointLetter);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(0, 792);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(243, 566);
            this.panel8.TabIndex = 14;
            // 
            // cbOverrideAdminForReporting
            // 
            this.cbOverrideAdminForReporting.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.cbOverrideAdminForReporting.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.cbOverrideAdminForReporting.Location = new System.Drawing.Point(0, 49);
            this.cbOverrideAdminForReporting.Name = "cbOverrideAdminForReporting";
            this.cbOverrideAdminForReporting.Size = new System.Drawing.Size(243, 37);
            this.cbOverrideAdminForReporting.TabIndex = 31;
            this.cbOverrideAdminForReporting.Text = "Override admin setting for Reporting tab?";
            this.cbOverrideAdminForReporting.UseVisualStyleBackColor = true;
            this.cbOverrideAdminForReporting.CheckedChanged += new System.EventHandler(this.cbOverrideAdminForReporting_CheckedChanged);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.simpleButton1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton1.ImageOptions.SvgImage")));
            this.simpleButton1.Location = new System.Drawing.Point(0, 86);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(243, 40);
            this.simpleButton1.TabIndex = 17;
            this.simpleButton1.Text = "Refresh All App Data";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btnAddManualAdjustment
            // 
            this.btnAddManualAdjustment.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnAddManualAdjustment.Appearance.Options.UseFont = true;
            this.btnAddManualAdjustment.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAddManualAdjustment.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnAddManualAdjustment.ImageOptions.SvgImage")));
            this.btnAddManualAdjustment.Location = new System.Drawing.Point(0, 126);
            this.btnAddManualAdjustment.Name = "btnAddManualAdjustment";
            this.btnAddManualAdjustment.Size = new System.Drawing.Size(243, 40);
            this.btnAddManualAdjustment.TabIndex = 26;
            this.btnAddManualAdjustment.Text = "Add Manual Adjustment";
            this.btnAddManualAdjustment.Click += new System.EventHandler(this.btnAddManualAdjustment_Click);
            // 
            // btnAddEmailRecipient
            // 
            this.btnAddEmailRecipient.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnAddEmailRecipient.Appearance.Options.UseFont = true;
            this.btnAddEmailRecipient.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAddEmailRecipient.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddEmailRecipient.ImageOptions.Image")));
            this.btnAddEmailRecipient.Location = new System.Drawing.Point(0, 166);
            this.btnAddEmailRecipient.Name = "btnAddEmailRecipient";
            this.btnAddEmailRecipient.Size = new System.Drawing.Size(243, 40);
            this.btnAddEmailRecipient.TabIndex = 28;
            this.btnAddEmailRecipient.Text = "Add Email Recipient";
            this.btnAddEmailRecipient.Click += new System.EventHandler(this.btnAddEmailRecipient_Click);
            // 
            // btnAddNewRehire
            // 
            this.btnAddNewRehire.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnAddNewRehire.Appearance.Options.UseFont = true;
            this.btnAddNewRehire.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAddNewRehire.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNewRehire.ImageOptions.Image")));
            this.btnAddNewRehire.Location = new System.Drawing.Point(0, 206);
            this.btnAddNewRehire.Name = "btnAddNewRehire";
            this.btnAddNewRehire.Size = new System.Drawing.Size(243, 40);
            this.btnAddNewRehire.TabIndex = 30;
            this.btnAddNewRehire.Text = "Add New Re-Hire";
            this.btnAddNewRehire.Click += new System.EventHandler(this.btnAddNewRehire_Click);
            // 
            // btnUpdateAllForSingleEmployee
            // 
            this.btnUpdateAllForSingleEmployee.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnUpdateAllForSingleEmployee.Appearance.Options.UseFont = true;
            this.btnUpdateAllForSingleEmployee.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnUpdateAllForSingleEmployee.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateAllForSingleEmployee.ImageOptions.Image")));
            this.btnUpdateAllForSingleEmployee.Location = new System.Drawing.Point(0, 246);
            this.btnUpdateAllForSingleEmployee.Name = "btnUpdateAllForSingleEmployee";
            this.btnUpdateAllForSingleEmployee.Size = new System.Drawing.Size(243, 40);
            this.btnUpdateAllForSingleEmployee.TabIndex = 27;
            this.btnUpdateAllForSingleEmployee.Text = "Update All for Single Employee";
            this.btnUpdateAllForSingleEmployee.Click += new System.EventHandler(this.btnUpdateAllForSingleEmployee_Click);
            // 
            // btnUpdateCurrentEmployeesOnly
            // 
            this.btnUpdateCurrentEmployeesOnly.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnUpdateCurrentEmployeesOnly.Appearance.Options.UseFont = true;
            this.btnUpdateCurrentEmployeesOnly.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnUpdateCurrentEmployeesOnly.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnUpdateCurrentEmployeesOnly.ImageOptions.SvgImage")));
            this.btnUpdateCurrentEmployeesOnly.Location = new System.Drawing.Point(0, 286);
            this.btnUpdateCurrentEmployeesOnly.Name = "btnUpdateCurrentEmployeesOnly";
            this.btnUpdateCurrentEmployeesOnly.Size = new System.Drawing.Size(243, 40);
            this.btnUpdateCurrentEmployeesOnly.TabIndex = 19;
            this.btnUpdateCurrentEmployeesOnly.Text = "Update Current Employees Only";
            this.btnUpdateCurrentEmployeesOnly.Click += new System.EventHandler(this.btnUpdateCurrentEmployeesOnly_Click);
            // 
            // btnSendPointLetterReminderEmail
            // 
            this.btnSendPointLetterReminderEmail.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSendPointLetterReminderEmail.Appearance.Options.UseFont = true;
            this.btnSendPointLetterReminderEmail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSendPointLetterReminderEmail.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnSendPointLetterReminderEmail.ImageOptions.SvgImage")));
            this.btnSendPointLetterReminderEmail.Location = new System.Drawing.Point(0, 326);
            this.btnSendPointLetterReminderEmail.Name = "btnSendPointLetterReminderEmail";
            this.btnSendPointLetterReminderEmail.Size = new System.Drawing.Size(243, 40);
            this.btnSendPointLetterReminderEmail.TabIndex = 24;
            this.btnSendPointLetterReminderEmail.Text = "Send Point Letter Reminder Emails";
            this.btnSendPointLetterReminderEmail.Click += new System.EventHandler(this.btnSendPointLetterReminderEmail_Click);
            // 
            // btnSendMissedPointLetterEmail
            // 
            this.btnSendMissedPointLetterEmail.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSendMissedPointLetterEmail.Appearance.Options.UseFont = true;
            this.btnSendMissedPointLetterEmail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSendMissedPointLetterEmail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSendMissedPointLetterEmail.ImageOptions.Image")));
            this.btnSendMissedPointLetterEmail.Location = new System.Drawing.Point(0, 366);
            this.btnSendMissedPointLetterEmail.Name = "btnSendMissedPointLetterEmail";
            this.btnSendMissedPointLetterEmail.Size = new System.Drawing.Size(243, 40);
            this.btnSendMissedPointLetterEmail.TabIndex = 23;
            this.btnSendMissedPointLetterEmail.Text = "Send Missed Point Letter Emails";
            this.btnSendMissedPointLetterEmail.Click += new System.EventHandler(this.btnSendMissedPointLetterEmail_Click);
            // 
            // btnSendFMLAEmail
            // 
            this.btnSendFMLAEmail.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSendFMLAEmail.Appearance.Options.UseFont = true;
            this.btnSendFMLAEmail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSendFMLAEmail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSendFMLAEmail.ImageOptions.Image")));
            this.btnSendFMLAEmail.Location = new System.Drawing.Point(0, 406);
            this.btnSendFMLAEmail.Name = "btnSendFMLAEmail";
            this.btnSendFMLAEmail.Size = new System.Drawing.Size(243, 40);
            this.btnSendFMLAEmail.TabIndex = 25;
            this.btnSendFMLAEmail.Text = "Send FMLA Emails";
            this.btnSendFMLAEmail.Click += new System.EventHandler(this.btnSendFMLAEmail_Click);
            // 
            // btnSendPADEmail
            // 
            this.btnSendPADEmail.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSendPADEmail.Appearance.Options.UseFont = true;
            this.btnSendPADEmail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSendPADEmail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSendPADEmail.ImageOptions.Image")));
            this.btnSendPADEmail.Location = new System.Drawing.Point(0, 446);
            this.btnSendPADEmail.Name = "btnSendPADEmail";
            this.btnSendPADEmail.Size = new System.Drawing.Size(243, 40);
            this.btnSendPADEmail.TabIndex = 32;
            this.btnSendPADEmail.Text = "Send PAD Emails";
            this.btnSendPADEmail.Click += new System.EventHandler(this.btnSendPADEmail_Click);
            // 
            // btnSendReportEmail
            // 
            this.btnSendReportEmail.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSendReportEmail.Appearance.Options.UseFont = true;
            this.btnSendReportEmail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSendReportEmail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSendReportEmail.ImageOptions.Image")));
            this.btnSendReportEmail.Location = new System.Drawing.Point(0, 486);
            this.btnSendReportEmail.Name = "btnSendReportEmail";
            this.btnSendReportEmail.Size = new System.Drawing.Size(243, 40);
            this.btnSendReportEmail.TabIndex = 29;
            this.btnSendReportEmail.Text = "Send Report Emails";
            this.btnSendReportEmail.Click += new System.EventHandler(this.btnSendReportEmail_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Location = new System.Drawing.Point(0, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Select an area";
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(243, 28);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(20, 1330);
            this.panel5.TabIndex = 9;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label6);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(263, 28);
            this.panel9.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.label6.Size = new System.Drawing.Size(64, 22);
            this.label6.TabIndex = 4;
            this.label6.Text = "Admin";
            // 
            // btnPrintPointLetter
            // 
            this.btnPrintPointLetter.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnPrintPointLetter.Appearance.Options.UseFont = true;
            this.btnPrintPointLetter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnPrintPointLetter.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.btnPrintPointLetter.Location = new System.Drawing.Point(0, 526);
            this.btnPrintPointLetter.Name = "btnPrintPointLetter";
            this.btnPrintPointLetter.Size = new System.Drawing.Size(243, 40);
            this.btnPrintPointLetter.TabIndex = 33;
            this.btnPrintPointLetter.Text = "Print Point Letter";
            this.btnPrintPointLetter.Click += new System.EventHandler(this.btnPrintPointLetter_Click);
            // 
            // panelPointLetter
            // 
            this.panelPointLetter.BackColor = System.Drawing.Color.Black;
            this.panelPointLetter.Controls.Add(this.panel31);
            this.panelPointLetter.Controls.Add(this.panel34);
            this.panelPointLetter.Location = new System.Drawing.Point(316, 1158);
            this.panelPointLetter.Name = "panelPointLetter";
            this.panelPointLetter.Padding = new System.Windows.Forms.Padding(10, 10, 10, 0);
            this.panelPointLetter.Size = new System.Drawing.Size(587, 144);
            this.panelPointLetter.TabIndex = 32;
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.Blue;
            this.panel31.Controls.Add(this.panel32);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel31.Location = new System.Drawing.Point(10, 10);
            this.panel31.Name = "panel31";
            this.panel31.Padding = new System.Windows.Forms.Padding(10);
            this.panel31.Size = new System.Drawing.Size(567, 73);
            this.panel31.TabIndex = 33;
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.Blue;
            this.panel32.Controls.Add(this.cbPointLetterPointValue);
            this.panel32.Controls.Add(this.cbPointLetterEmployeeName);
            this.panel32.Controls.Add(this.panel33);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel32.Location = new System.Drawing.Point(10, 10);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(547, 53);
            this.panel32.TabIndex = 0;
            // 
            // cbPointLetterEmployeeName
            // 
            this.cbPointLetterEmployeeName.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbPointLetterEmployeeName.EditValue = "";
            this.cbPointLetterEmployeeName.Location = new System.Drawing.Point(200, 0);
            this.cbPointLetterEmployeeName.Name = "cbPointLetterEmployeeName";
            this.cbPointLetterEmployeeName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPointLetterEmployeeName.Properties.DropDownRows = 15;
            this.cbPointLetterEmployeeName.Properties.ImmediatePopup = true;
            this.cbPointLetterEmployeeName.Properties.LookAndFeel.SkinName = "Sharp";
            this.cbPointLetterEmployeeName.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbPointLetterEmployeeName.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbPointLetterEmployeeName.Size = new System.Drawing.Size(347, 20);
            this.cbPointLetterEmployeeName.TabIndex = 110;
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.label27);
            this.panel33.Controls.Add(this.label28);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel33.Location = new System.Drawing.Point(0, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(200, 53);
            this.panel33.TabIndex = 29;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Dock = System.Windows.Forms.DockStyle.Top;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(0, 21);
            this.label27.Name = "label27";
            this.label27.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label27.Size = new System.Drawing.Size(70, 21);
            this.label27.TabIndex = 1;
            this.label27.Text = "Point Value";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Dock = System.Windows.Forms.DockStyle.Top;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(0, 0);
            this.label28.Name = "label28";
            this.label28.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.label28.Size = new System.Drawing.Size(62, 21);
            this.label28.TabIndex = 2;
            this.label28.Text = "Employee";
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.Color.Black;
            this.panel34.Controls.Add(this.panel35);
            this.panel34.Controls.Add(this.panel36);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel34.Location = new System.Drawing.Point(10, 83);
            this.panel34.Name = "panel34";
            this.panel34.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.panel34.Size = new System.Drawing.Size(567, 61);
            this.panel34.TabIndex = 10;
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.btnPointLetterDoIt);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel35.Location = new System.Drawing.Point(204, 10);
            this.panel35.Name = "panel35";
            this.panel35.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.panel35.Size = new System.Drawing.Size(363, 41);
            this.panel35.TabIndex = 26;
            // 
            // btnPointLetterDoIt
            // 
            this.btnPointLetterDoIt.Appearance.BackColor = System.Drawing.Color.Blue;
            this.btnPointLetterDoIt.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnPointLetterDoIt.Appearance.Options.UseBackColor = true;
            this.btnPointLetterDoIt.Appearance.Options.UseFont = true;
            this.btnPointLetterDoIt.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnPointLetterDoIt.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton2.ImageOptions.SvgImage")));
            this.btnPointLetterDoIt.Location = new System.Drawing.Point(10, 1);
            this.btnPointLetterDoIt.LookAndFeel.SkinName = "Blue";
            this.btnPointLetterDoIt.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnPointLetterDoIt.Name = "btnPointLetterDoIt";
            this.btnPointLetterDoIt.Size = new System.Drawing.Size(353, 40);
            this.btnPointLetterDoIt.TabIndex = 27;
            this.btnPointLetterDoIt.Text = "Save";
            this.btnPointLetterDoIt.Click += new System.EventHandler(this.btnPointLetterDoIt_Click);
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.btnPointLetterCancel);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel36.Location = new System.Drawing.Point(0, 10);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(204, 41);
            this.panel36.TabIndex = 25;
            // 
            // btnPointLetterCancel
            // 
            this.btnPointLetterCancel.Appearance.BackColor = System.Drawing.Color.Blue;
            this.btnPointLetterCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnPointLetterCancel.Appearance.Options.UseBackColor = true;
            this.btnPointLetterCancel.Appearance.Options.UseFont = true;
            this.btnPointLetterCancel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnPointLetterCancel.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton3.ImageOptions.SvgImage")));
            this.btnPointLetterCancel.Location = new System.Drawing.Point(0, 1);
            this.btnPointLetterCancel.LookAndFeel.SkinName = "Blue";
            this.btnPointLetterCancel.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnPointLetterCancel.Name = "btnPointLetterCancel";
            this.btnPointLetterCancel.Size = new System.Drawing.Size(204, 40);
            this.btnPointLetterCancel.TabIndex = 27;
            this.btnPointLetterCancel.Text = "Cancel";
            this.btnPointLetterCancel.Click += new System.EventHandler(this.btnPointLetterCancel_Click);
            // 
            // cbPointLetterPointValue
            // 
            this.cbPointLetterPointValue.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbPointLetterPointValue.EditValue = "";
            this.cbPointLetterPointValue.Location = new System.Drawing.Point(200, 20);
            this.cbPointLetterPointValue.Name = "cbPointLetterPointValue";
            this.cbPointLetterPointValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPointLetterPointValue.Properties.DropDownRows = 15;
            this.cbPointLetterPointValue.Properties.ImmediatePopup = true;
            this.cbPointLetterPointValue.Properties.LookAndFeel.SkinName = "Sharp";
            this.cbPointLetterPointValue.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbPointLetterPointValue.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbPointLetterPointValue.Size = new System.Drawing.Size(347, 20);
            this.cbPointLetterPointValue.TabIndex = 111;
            // 
            // AdminTemplate
            // 
            this.ClientSize = new System.Drawing.Size(1079, 1362);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AdminTemplate";
            ((System.ComponentModel.ISupportInitialize)(this.gcAdmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAdmin)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panelAddNewRehire.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deDateOfRehire.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateOfRehire.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbRehireEmployeeName.Properties)).EndInit();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panelAddEmailRecipient.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbAddEmailRecipient_Department.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAddEmailRecipient_Type.Properties)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panelUpdateSingleEmployee.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SINGLE_cbEmployeeList.Properties)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panelAddManualAdjustment.ResumeLayout(false);
            this.panelIDK.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ME_meLongNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ME_numNewNetHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ME_cbNewPayCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ME_numExistingNetHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ME_cbExistingPayCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ME_deDateToAdjust.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ME_deDateToAdjust.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ME_cbEmployeeList.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbArea.Properties)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panelPointLetter.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbPointLetterEmployeeName.Properties)).EndInit();
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            this.panel34.ResumeLayout(false);
            this.panel35.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbPointLetterPointValue.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraGrid.GridControl gcEmployeeEvents;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gcAdmin;
        private DevExpress.XtraGrid.Views.Grid.GridView gvAdmin;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel8;
        private DevExpress.XtraEditors.ComboBoxEdit cbArea;
        private DevExpress.XtraEditors.SimpleButton btnRefreshData;
        private System.Windows.Forms.Label lblRowCount;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btnUpdateCurrentEmployeesOnly;
        private DevExpress.XtraEditors.SimpleButton btnSendFMLAEmail;
        private DevExpress.XtraEditors.SimpleButton btnSendPointLetterReminderEmail;
        private DevExpress.XtraEditors.SimpleButton btnSendMissedPointLetterEmail;
        private DevExpress.XtraEditors.SimpleButton btnAddManualAdjustment;
        private System.Windows.Forms.Panel panelIDK;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox ME_tbShortNote;
        private DevExpress.XtraEditors.ComboBoxEdit ME_cbNewPayCode;
        private DevExpress.XtraEditors.ComboBoxEdit ME_cbExistingPayCode;
        private DevExpress.XtraEditors.ComboBoxEdit ME_cbEmployeeList;
        private DevExpress.XtraEditors.DateEdit ME_deDateToAdjust;
        private System.Windows.Forms.Panel panelAddManualAdjustment;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel10;
        private DevExpress.XtraEditors.SimpleButton btnConfirmAddManualAdjustment;
        private System.Windows.Forms.Panel panel11;
        private DevExpress.XtraEditors.SimpleButton btnCancelAddManualAdjustment;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown ME_numNewNetHours;
        private System.Windows.Forms.NumericUpDown ME_numExistingNetHours;
        private System.Windows.Forms.Label lblAllowEdit;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.SimpleButton btnUpdateAllForSingleEmployee;
        private System.Windows.Forms.Panel panelUpdateSingleEmployee;
        private System.Windows.Forms.Panel panel13;
        private DevExpress.XtraEditors.ComboBoxEdit SINGLE_cbEmployeeList;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel15;
        private DevExpress.XtraEditors.SimpleButton btnGoUpdateAllSingleEmployee;
        private System.Windows.Forms.Panel panel16;
        private DevExpress.XtraEditors.SimpleButton btnCancelUpdateAllSingleEmployee;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.SimpleButton btnAddEmailRecipient;
        private System.Windows.Forms.Panel panelAddEmailRecipient;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel19;
        private DevExpress.XtraEditors.SimpleButton btnSaveNewEmailRecipient;
        private System.Windows.Forms.Panel panel20;
        private DevExpress.XtraEditors.SimpleButton btnCancelAddEmailRecipient;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbAddEmailRecipient_Name;
        private System.Windows.Forms.TextBox tbAddEmailRecipient_Email;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private DevExpress.XtraEditors.ComboBoxEdit cbAddEmailRecipient_Department;
        private DevExpress.XtraEditors.ComboBoxEdit cbAddEmailRecipient_Type;
        private DevExpress.XtraEditors.MemoEdit ME_meLongNote;
        private DevExpress.XtraEditors.SimpleButton btnSendReportEmail;
        private System.Windows.Forms.Panel panelAddNewRehire;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel24;
        private DevExpress.XtraEditors.SimpleButton btnAddNewRehireDoIt;
        private System.Windows.Forms.Panel panel25;
        private DevExpress.XtraEditors.SimpleButton btnAddNewRehireCancel;
        private System.Windows.Forms.Panel panel26;
        private DevExpress.XtraEditors.DateEdit deDateOfRehire;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private DevExpress.XtraEditors.SimpleButton btnAddNewRehire;
        private DevExpress.XtraEditors.ComboBoxEdit cbRehireEmployeeName;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.CheckBox cbOverrideAdminForReporting;
        private DevExpress.XtraEditors.SimpleButton btnSendPADEmail;
        private DevExpress.XtraEditors.SimpleButton btnPrintPointLetter;
        private System.Windows.Forms.Panel panelPointLetter;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Panel panel32;
        private DevExpress.XtraEditors.ComboBoxEdit cbPointLetterPointValue;
        private DevExpress.XtraEditors.ComboBoxEdit cbPointLetterEmployeeName;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panel35;
        private DevExpress.XtraEditors.SimpleButton btnPointLetterDoIt;
        private System.Windows.Forms.Panel panel36;
        private DevExpress.XtraEditors.SimpleButton btnPointLetterCancel;
    }
}