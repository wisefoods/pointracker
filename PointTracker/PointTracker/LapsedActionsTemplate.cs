﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using DevExpress.XtraGrid.Views.Grid;

namespace AttendanceTracker
{
    public partial class LapsedActionsTemplate : DevExpress.XtraEditors.XtraForm
    {
        Form1 atrack;

        public LapsedActionsTemplate(Form1 pForm)
        {
            try
            {
                InitializeComponent();
                atrack = pForm;
                BindPageData();
            }
            catch (Exception ex)
            {
                atrack.LogAndDisplayError(ex.ToString(), true);
            }
        }
        public void BindPageData()
        {
            try
            {
                GridView thisGrid = gcLapsedActions.MainView as GridView;
                DateTime lasttasksupdate = atrack.LASTTASKSUPDATE;
                labelLastUpdate.Text = "Data from: " + lasttasksupdate.ToString();

                gcLapsedActions.DataSource = null;
                (gcLapsedActions.MainView as GridView).Columns.Clear();
                gcLapsedActions.DataSource = atrack.dataset.ActionsTakens
                                                           .Where(z => z.ActionTaken == false && z.IsLocked == true && atrack.MYEMPLOYEES_IDS.Contains(z.EmployeeID))
                                                           .Select(z => new { z.DateOfLapse, z.EmployeeName, z.EmployeeID, z.Department, z.DateOfOccurrence, z.PointValue, z.ActionToTake })
                                                           .OrderBy(z => z.DateOfLapse)
                                                           .ThenBy(z => z.EmployeeName)
                                                           .ThenBy(z => z.PointValue)
                                                           .ToList();
                (gcLapsedActions.MainView as GridView).Columns["DateOfLapse"].Caption = "Expired On";
                (gcLapsedActions.MainView as GridView).BestFitColumns();

                if ((gcLapsedActions.MainView as GridView).RowCount <= 0)
                {
                    atrack.tabFormControl1.Pages.Remove(atrack.tabFormControl1.Pages.Where(z => z.Name == "My Missed Point Letters").FirstOrDefault());
                    atrack.ReclaimButton("My Missed Point Letters");
                    atrack.CloseSplash();
                    MessageBox.Show("You have no missed point letters!", "Nothing To See", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                if (atrack.UPDATEACTIVE) { MessageBox.Show("Something went wrong; please close this page and try again.", "Data Update Failed", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                else { atrack.LogAndDisplayError(ex.ToString(), true); }
            }
        }
    }
}
